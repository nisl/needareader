//
//  SignUpResponseModel.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/15/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper

class SignUpResponseModel: Mappable {
    
    public var user_registration_temp_id: Int?
    public var new_token : String?

    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        new_token <- map["new_token"]
        user_registration_temp_id <- map["user_registration_temp_id"]
    }
}

class UpdateProfileResponseModel: Mappable {
    public var new_token : String?

    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        new_token <- map["new_token"]

    }

}
