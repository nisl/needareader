//
//  TappingService.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 5/30/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper

class TappingService: Mappable {
    
    public var is_taping : Bool?
    public var taping_rate : String?
    public var is_edit : Bool?
    public var editing_rate : String?
    public var service_detail : [ServiceDetail]?

    public required init?(map: Map) {
    }
    
 
    public func mapping(map: Map) {
        is_taping <- map["is_taping"]
        taping_rate <- map["taping_rate"]
        is_edit <- map["is_edit"]
        editing_rate <- map["editing_rate"]
        service_detail <- map["service_detail"]
    }
}


class ServiceDetail: Mappable {
    
    public var service_id : Int?
    public var service_name : String?
    public var create_time : String?
    public var update_time : String?
    public var is_active : Bool?
    
    public required init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        service_id <- map["service_id"]
        service_name <- map["service_name"]
        create_time <- map["create_time"]
        update_time <- map["update_time"]
        is_active <- map["is_active"]
    }
}

class TappingServiceRequestModel: Mappable {
    public var is_taping : Int?
    public var taping_rate : Int?
    public var is_edit : Int?
    public var editing_rate : Int?
    public var service_id : String?


    
    required init?(map: Map) {
        
    }
    
    init(is_taping : Int?, taping_rate : Int?, is_edit : Int?, editing_rate : Int?, service_id : String?) {
        self.is_taping = is_taping
        self.taping_rate = taping_rate
        self.is_edit = is_edit
        self.editing_rate = editing_rate
        self.service_id = service_id
    }
    
    func mapping(map: Map) {
        is_taping <- map["is_taping"]
        taping_rate <- map["taping_rate"]
        is_edit <- map["is_edit"]
        editing_rate <- map["editing_rate"]
        service_id <- map["service_id"]

    }
    
}
