//
//  CodeVerifyRequestModel.swift
//  MindFit
//
//  Created by ob_apple_2 on 8/28/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper

class CodeVerifyRequestModel: Mappable {
    public var  user_registration_temp_id: Int?
    public var licence_code : String?
    public var otp_token : Int?
    public var device_info : DeviceInfo?

    required init?(map: Map) {
        
    }
    
   
    
    init(user_registration_temp_id: Int?, licence_code: String?, device_info: DeviceInfo?) {
        
        self.user_registration_temp_id = user_registration_temp_id
        self.licence_code = licence_code
        self.device_info = device_info
    }
    
    init(user_registration_temp_id: Int?, otp_token : Int?, device_info: DeviceInfo?) {
        self.user_registration_temp_id = user_registration_temp_id
        self.otp_token = otp_token
        self.device_info = device_info

    }
    
    func mapping(map: Map) {
        
        user_registration_temp_id <- map["user_registration_temp_id"]
        licence_code <- map["licence_code"]
        otp_token <- map["otp_token"]
        device_info <- map["device_info"]

        
    }
    
}

class CodeVerificationResponseModel : Mappable {
    public var new_token : String?

    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        new_token <- map["new_token"]

    }
}
