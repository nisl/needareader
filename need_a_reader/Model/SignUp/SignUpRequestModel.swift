//
//  SignUpRequestModel.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/15/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper

class SignUpRequestModel: Mappable {
    public var  first_name: String?
    public var  last_name: String?

    public var email_id : String?
    public var password : String?
    public var gender : Int?
//    public var date_of_birth : String?
    public var signup_type : Int?
    public var contact_no : String?
    
    required init?(map: Map) {
        
    }
    
    init(first_name: String?, email_id: String?, password: String?, gender: Int?, signup_type : Int?, contact_no: String?, last_name: String?) {
        
        self.gender = gender
        self.password = password
        self.email_id = email_id
        self.first_name = first_name
        self.last_name = last_name

        self.signup_type = signup_type
        self.contact_no = contact_no
    }
   
    init(first_name: String?, gender: Int?, contact_no: String?, last_name: String?) {

        self.gender = gender
        self.first_name = first_name
        self.contact_no = contact_no
        self.last_name = last_name

    }
    
    func mapping(map: Map) {
        first_name <- map["first_name"]
        email_id <- map["email_id"]
        password <- map["password"]
        gender <- map["gender"]
        signup_type <- map["signup_type"]
        contact_no <- map["contact_no"]
        last_name <- map["last_name"]

    }
    
}

class UpdateProfileRequestModel: Mappable {
    public var  first_name: String?
    public var last_name : String?
    public var gender : Int?
    public var year_of_birth : String?
    public var team_id : String?
    
    required init?(map: Map) {
        
    }
    
    init(first_name: String?, last_name: String?, team_id: String?, gender: Int?, year_of_birth: String?) {
        
        self.year_of_birth = year_of_birth
        self.gender = gender
        self.team_id = team_id
        self.last_name = last_name
        self.first_name = first_name
    }
    
    func mapping(map: Map) {
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        team_id <- map["team_id"]
        gender <- map["gender"]
        year_of_birth <- map["year_of_birth"]
        
        
    }
    
}

class ProfileSetupRequestModel : Mappable {
    public var  hourly_rate: Int?
  
    required init?(map: Map) {
        
    }
    
     init(hourly_rate: Int?) {
        self.hourly_rate = hourly_rate
    }
    
     func mapping(map: Map) {
        hourly_rate <- map["hourly_rate"]

    }
}
