//
//  AddSkillsRequestModel.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/16/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper

class AddSkillsRequestModel: Mappable {

    public var sit_com : Int?
    public var hour_drama : Int?
    public var plays : Int?
    public var musicals : Int?
    public var feature_films : Int?
    public var dramedy : Int?
    public var single_camera : Int?
    public var co_star : Int?
    public var commercials : Int?
    public var shakespeare : Int?
    public var monologues : Int?
    public var accents : Int?
    public var create_time : String?
    public var update_time : String?
    
    required init?(map: Map) {
        
    }
    
    init(sit_com : Int?,hour_drama : Int?,plays : Int?,musicals : Int?,feature_films : Int?,single_camera : Int?,co_star : Int?,commercials : Int?,shakespeare : Int?,monologues : Int?, dramedy : Int?, accents : Int?) {
        
        self.sit_com = sit_com
        self.hour_drama = hour_drama
        self.plays = plays
        self.musicals = musicals
        self.feature_films = feature_films
        self.single_camera = single_camera
        self.co_star = co_star
        self.commercials = commercials
        self.shakespeare = shakespeare
        self.monologues = monologues
        self.dramedy = dramedy
        self.accents = accents
    }
    
    func mapping(map: Map) {
        sit_com <- map["sitcom"]
        hour_drama <- map["hour_drama"]
        plays <- map["plays"]
        musicals <- map["musicals"]
        feature_films <- map["feature_films"]
        single_camera <- map["single_camera"]
        co_star <- map["co_stars"]
        commercials <- map["commercials"]
        shakespeare <- map["shakespeare"]
        monologues <- map["monologues"]
        create_time <- map["create_time"]
        update_time <- map["update_time"]
        dramedy <- map["dramedy"]
        accents <- map["accents"]
    }
    
}


class HeadShotRequestModel: Mappable {
    public var  file: String?
   
    required init?(map: Map) {
        
    }
    
    init(file: String?) {
        self.file = file
    }
    
    func mapping(map: Map) {
        file <- map["file"]
        
    }
    
}


class HeadShotResponseModel: Mappable {
    public var headshot_original_image: String?
    public var headshot_compressed_image: String?
    public var headshot_thumbnail_image: String?
    public var resume_file: String?

    public required init?(map: Map) {
        
    }
    
    
    func mapping(map: Map) {
        headshot_original_image <- map["headshot_original_image"]
        headshot_thumbnail_image <- map["headshot_thumbnail_image"]
        headshot_compressed_image <- map["headshot_compressed_image"]

        resume_file <- map["resume_file"]

    }
    
}

