//
//  ForgotPasswordRequestModel.swift
//  MindFit
//
//  Created by ob_apple_2 on 8/29/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper


class ForgotPasswordRequestModel: Mappable {
    public var  email_id: String?
     public var  otp_token: Int?
    public var  user_registration_temp_id: Int?

    required init?(map: Map) {
        
    }
    
    init(email_id: String?) {
        
        self.email_id = email_id
        
    }
    
    init(user_registration_temp_id: Int?) {
        
        self.user_registration_temp_id = user_registration_temp_id

        
    }

    
    init(email_id: String?, otp_token: Int?) {
        
        
        self.otp_token = otp_token
        self.email_id = email_id
        
    }
    
    func mapping(map: Map) {
        otp_token <- map["otp_token"]
        email_id <- map["email_id"]
        user_registration_temp_id <- map["user_registration_temp_id"]
        
    }
    
}
