//
//  Ratingmodel.swift
//  need_a_reader
//
//  Created by ob_apple_3 on 17/09/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper

class RatingModel: Mappable {
 
    public var overall_rating : RatingOverallResponse?
    public var job_wise_rates : [RatingJobWiseResponse]?
    
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        overall_rating <- map["overall_rating"]
        job_wise_rates <- map["job_wise_rates"]

    }
    
}
class RatingOverallResponse: Mappable {
    
    public var average_rating : String?
    public var ratings : Int?
    public var is_on_time : Int?
    public var is_knowledgeable_on_content : Int?
    public var work_with_reader_again : Int?
    public var jobSatisFiedPer : Int?
    public var on_time_yes : Int?
    public var on_time_no : Int?
    public var is_knowledgeable_yes : Int?
    public var is_knowledgeable_no : Int?
    public var work_with_yes : Int?
    public var work_with_no : Int?
    public var job_satisfaction_yes : Int?
    public var job_satisfaction_no : Int?
    public var is_on_time_yes : Int?
    public var is_on_time_no : Int?
   
    required init?(map: Map) {
        
    }
    
    init(ratings : Int?, is_on_time : Int?, is_knowledgeable_on_content : Int?, work_with_reader_again : Int?, on_time_yes : Int?,  on_time_no : Int?, is_knowledgeable_yes : Int?, is_knowledgeable_no : Int?, work_with_yes : Int?, work_with_no : Int?, average_rating : String?,is_on_time_no:Int?,is_on_time_yes:Int?) {
        
        self.ratings = ratings
        self.is_on_time = is_on_time
        self.is_knowledgeable_on_content = is_knowledgeable_on_content
        self.work_with_reader_again = work_with_reader_again
        self.on_time_no = on_time_no
        self.is_knowledgeable_yes = is_knowledgeable_yes
        self.on_time_yes = on_time_yes
        
        self.is_knowledgeable_no = is_knowledgeable_no
        self.work_with_yes = work_with_yes
        self.work_with_no = work_with_no
        self.average_rating = average_rating
        self.is_on_time_no = is_on_time_no
        self.is_on_time_yes = is_on_time_yes
        
    }
    
    func mapping(map: Map) {
        
        ratings <- map["ratings"]
        is_on_time <- map["is_on_time"]
        is_knowledgeable_on_content <- map["is_knowledgeable_on_content"]
        work_with_reader_again <- map["work_with_reader_again"]
        on_time_no <- map["is_on_time_no"]
        is_knowledgeable_yes <- map["is_knowledgeable_yes"]
        is_knowledgeable_no <- map["is_knowledgeable_no"]
        work_with_yes <- map["work_with_yes"]
        on_time_yes <- map["is_on_time_yes"]
        work_with_no <- map["work_with_no"]
        average_rating <- map["average_rating"]
        job_satisfaction_yes <- map["job_satisfaction_yes"]
        job_satisfaction_no <- map["job_satisfaction_no"]
        jobSatisFiedPer <- map["job_satisfaction"]
        is_on_time_yes <- map["is_on_time_yes"]
        is_on_time_no <- map["is_on_time_no"]
    }
    
}
class RatingJobWiseResponse: Mappable {
    
    public var rate_id : Int?
    public var job_id : Int?
    public var sender_user_id : Int?
    public var receiver_user_id : Int?
    public var is_on_time : Int?
    public var is_knowledgeable_on_content : Int?
    public var work_with_reader_again : Int?
    public var job_satisfaction : Int?
    public var over_all_rating : Int?
    public var create_time : String?
    public var update_time : String?
    
    public var sender_first_name : String?
    public var sender_last_name : String?
    public var sender_headshot_original_image : String?
    public var sender_headshot_compressed_image : String?
    public var sender_headshot_thumbnail_image : String?
    public var job_description : String?
    
    
    required init?(map: Map) {
        
    }
    
//    init(ratings : Int?, is_on_time : Int?, is_knowledgeable_on_content : Int?, work_with_reader_again : Int?, on_time_yes : Int?,  on_time_no : Int?, is_knowledgeable_yes : Int?, is_knowledgeable_no : Int?, work_with_yes : Int?, work_with_no : Int?, average_rating : String?) {
//
//        self.ratings = ratings
//        self.is_on_time = is_on_time
//        self.is_knowledgeable_on_content = is_knowledgeable_on_content
//        self.work_with_reader_again = work_with_reader_again
//        self.on_time_no = on_time_no
//        self.is_knowledgeable_yes = is_knowledgeable_yes
//        self.on_time_yes = on_time_yes
//
//        self.is_knowledgeable_no = is_knowledgeable_no
//        self.work_with_yes = work_with_yes
//        self.work_with_no = work_with_no
//        self.average_rating = average_rating
//    }
    
    func mapping(map: Map) {
        
        rate_id <- map["rate_id"]
        job_id <- map["job_id"]
        sender_user_id <- map["sender_user_id"]
        receiver_user_id <- map["receiver_user_id"]
        is_on_time <- map["is_on_time"]
        is_knowledgeable_on_content <- map["is_knowledgeable_on_content"]
        work_with_reader_again <- map["work_with_reader_again"]
        job_satisfaction <- map["job_satisfaction"]
        over_all_rating <- map["over_all_rating"]
        create_time <- map["create_time"]
        update_time <- map["update_time"]
        
        sender_first_name <- map["sender_first_name"]
        sender_last_name <- map["sender_last_name"]
        sender_headshot_original_image <- map["sender_headshot_original_image"]
        sender_headshot_compressed_image <- map["sender_headshot_compressed_image"]
        sender_headshot_thumbnail_image <- map["sender_headshot_thumbnail_image"]
        job_description <- map["job_description"]
      
    }
    
}
