//
//  ResponseModel.swift
//  MindFit
//
//  Created by ob_apple_2 on 8/28/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseModel: Mappable {
    public var code : Int?
    public var message : String?
    public var cause : String?
    public var signUpResponse : SignUpResponseModel?
    public var loginResponse : LoginResponseModel?
    public var codeVerifyResponse : CodeVerificationResponseModel?
    public var findingActorResponse : FindingActorResponseModel?
    public var headShotResponse : HeadShotResponseModel?
    public var settingResponse : SettingResponseModel?
    public var skillResponse : AddSkillsRequestModel?
    public var inAppPurchasePaymentSync : InAppPurchasePaymentSync?
    public var userDetailResponse : User_detail!
    public var notificationResponse : NotificationResponseModel!
    public var jobResponse : JobResponseModel!
    public var jobResponseByJobId : Job_detail!
    public var hireReaderResponse : HireReaderResponseModel!
    public var viewRatingResponse : ViewRatingResponseModel!
    public var tappingServiceResponse : TappingService!
    public var contactReponse : contactResponseModel?
    public var viewForuserReponse : RatingModel?

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        message <- map["message"]
        cause <- map["cause"]
        signUpResponse <- map["data"]
        loginResponse <- map["data"]
        codeVerifyResponse <- map["data"]
        findingActorResponse <- map["data"]
        headShotResponse <- map["data"]
        settingResponse <- map["data"]
        skillResponse <- map["data"]
        inAppPurchasePaymentSync <- map["data"]
        userDetailResponse <- map["data"]
        notificationResponse <- map["data"]
        jobResponse <- map["data"]
        jobResponseByJobId <- map["data"]
        hireReaderResponse <- map["data"]
        viewRatingResponse <- map["data"]
        tappingServiceResponse <- map["data"]
        contactReponse <- map["data"]
        viewForuserReponse <- map["data"]

    }
    
}

