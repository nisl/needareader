//
//  FindingActorRequestModel.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/18/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper


class FindingActorRequestModel: Mappable {
    public var  page: Int?
    public var  latitude: Double?
    public var  longitude: Double?
    
    required init?(map: Map) {
        
    }
    init(latitude: Double?, longitude: Double?) {
        
        
//        self.page = page
        self.latitude = latitude
        self.longitude = longitude
        
    }
    
    func mapping(map: Map) {
//        page <- map["page"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        
        
    }
    
}

class FindingActorResponseModel: Mappable {
    
    public var reader_list : [User_detail]?
    public var readerData : User_detail!
//    public var skill_detail : [AddSkillsRequestModel]?
//    public var rating_detail : [RatingResponseModel]?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
//        skill_detail <- map["skill_details"]
        reader_list <- map["reader_list"]
        readerData <- map["data"]
//        rating_detail <- map["rating_details"]
        
    }
}

