//
//  HireReaderRequestModel.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 1/29/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper

class HireReaderRequestModel : Mappable {
    public var  receiver_user_id: Int?
    public var job_description: String?
    public var is_taping: Int?
    public var is_editing: Int?
    public var final_amount: Float?
    public var time_to_hire: Int?
    public var meeting_time : String?
    
    
    required init?(map: Map) {
        
    }
    
    init(receiver_user_id: Int?, job_description: String?, is_taping: Int?, is_editing: Int?, final_amount: Float?, time_to_hire: Int?, meeting_time : String?) {
        self.receiver_user_id = receiver_user_id
        self.job_description = job_description
        self.is_taping = is_taping
        self.is_editing = is_editing
        self.final_amount = final_amount
        self.time_to_hire = time_to_hire
        self.meeting_time = meeting_time
    }
    
    init(receiver_user_id: Int?, job_description: String?, is_taping: Int?, is_editing: Int?, final_amount: Float?, time_to_hire: Int?) {
        self.receiver_user_id = receiver_user_id
        self.job_description = job_description
        self.is_taping = is_taping
        self.is_editing = is_editing
        self.final_amount = final_amount
        self.time_to_hire = time_to_hire
    }
    
    func mapping(map: Map) {
        job_description <- map["job_description"]
        receiver_user_id <- map["receiver_user_id"]
        is_taping <- map["is_taping"]
        is_editing <- map["is_editing"]
        final_amount <- map["final_rate"]
        time_to_hire <- map["time_to_hire"]
        meeting_time <- map["meeting_time"]
    }
}

class HireReaderResponseModel : Mappable {

    public var  job_id: Int?
    public var waiting_time: Int?
    required init?(map: Map) {
        
    }
    
    
    func mapping(map: Map) {
        waiting_time <- map["waiting_time"]
        job_id <- map["job_id"]
        
    }
}

class MultipleHireReaderRequestModel : Mappable {
    public var  receiver_user_id: Int?
    public var job_description: String?
    public var is_taping: Int?
    public var is_editing: Int?
    public var final_amount: Float?
    public var time_to_hire: Int?
    public var meeting_time : String?
    public var meeting_type: Int?
    
    
    required init?(map: Map) {
        
    }
    
    init(receiver_user_id: Int?, job_description: String?, is_taping: Int?, is_editing: Int?, final_amount: Float?, time_to_hire: Int?, meeting_time : String?,meeting_type: Int?) {
        self.receiver_user_id = receiver_user_id
        self.job_description = job_description
        self.is_taping = is_taping
        self.is_editing = is_editing
        self.final_amount = final_amount
        self.time_to_hire = time_to_hire
        self.meeting_time = meeting_time
        self.meeting_type = meeting_type
    }
    
    func mapping(map: Map) {
        job_description <- map["job_description"]
        receiver_user_id <- map["receiver_user_id"]
        is_taping <- map["is_taping"]
        is_editing <- map["is_editing"]
        final_amount <- map["final_rate"]
        time_to_hire <- map["time_to_hire"]
        meeting_time <- map["meeting_time"]
        meeting_type <- map["meeting_type"]
    }
}
class MultipleHireReader : Mappable {
    public var  reader_list: [MultipleHireReaderRequestModel]?
  
    
    
    required init?(map: Map) {
        
    }
    
    init(reader_list: [MultipleHireReaderRequestModel]?) {
        self.reader_list = reader_list
       
    }
    func mapping(map: Map) {
        reader_list <- map["reader_list"]
        
    }
}
class ReaderRequestModel : Mappable {
   
    public var job_description: String?
    public var is_taping: Int?
    public var is_editing: Int?
    public var time_to_hire: Int?
    public var meeting_time : String?
    public var meeting_type: Int?
    
    
    required init?(map: Map) {
        
    }
    
    init(job_description: String?, is_taping: Int?, is_editing: Int?, time_to_hire: Int?, meeting_time : String?,meeting_type: Int?) {
        
        self.job_description = job_description
        self.is_taping = is_taping
        self.is_editing = is_editing
       
        self.time_to_hire = time_to_hire
        self.meeting_time = meeting_time
        self.meeting_type = meeting_type
    }
    
    func mapping(map: Map) {
        job_description <- map["job_description"]
        is_taping <- map["is_taping"]
        is_editing <- map["is_editing"]
        time_to_hire <- map["time_to_hire"]
        meeting_time <- map["meeting_time"]
        meeting_type <- map["meeting_type"]
    }
}
