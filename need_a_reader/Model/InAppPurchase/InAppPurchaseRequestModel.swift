//
//  InAppPurchaseRequestModel.swift
//  MindFit
//
//  Created by ob_apple_2 on 9/25/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class InAppPurchaseRequestModel: Mappable {
    public var order_info : Order_info?
    public var device_info : DeviceInfo?

    required init?(map: Map) {
        
    }
    init(order_info: Order_info?, device_info: DeviceInfo?) {
        
        self.device_info = device_info
        
        self.order_info = order_info
        
    }
    init(order_info: Order_info?) {
        
        self.order_info = order_info
        
    }
    func mapping(map: Map) {
        order_info <- map["order_info"]
        device_info <- map["device_info"]
        
        
    }
}

class Order_info: Mappable {
    public var user_id : Int?
    public var token : String?
    public var order_id : String?
    public var tot_order_amount : Double?
    public var package_name : String?
    public var product_id : String?
    public var purchase_time : String?
    public var purchase_state : String?
    public var purchase_token : String?
    public var auto_renewing : Bool?
    public var receipt_base64_data : String?
    public var mc_currency : String?
//    public var period : String?
    public var txn_type : String?
//    public var expiration_time : String?
    
    required init?(map: Map) {
        
    }
    
    init(user_id: Int?, token: String?, order_id: String?, tot_order_amount: Double?, package_name: String?, product_id: String?,  purchase_time: String?,purchase_state: String?, purchase_token: String?, auto_renewing: Bool?, receipt_base64_data: String?, mc_currency: String?, txn_type: String?) {
        
        
        self.user_id = user_id
        self.token = token
        self.order_id = order_id
        self.tot_order_amount = tot_order_amount
        self.package_name = package_name
        self.product_id = product_id
        self.purchase_time = purchase_time
        self.purchase_state = purchase_state
        self.purchase_token = purchase_token
        self.auto_renewing = auto_renewing
        self.receipt_base64_data = receipt_base64_data
        self.mc_currency = mc_currency
        self.txn_type = txn_type
        
        
    }
    
  
    func mapping(map: Map) {
        user_id <- map["user_id"]
        token <- map["token"]
        order_id <- map["order_id"]
        tot_order_amount <- map["tot_order_amount"]
        package_name <- map["package_name"]
        product_id <- map["product_id"]
        purchase_time <- map["purchase_time"]
        purchase_state <- map["purchase_state"]
        purchase_token <- map["purchase_token"]
        auto_renewing <- map["auto_renewing"]
        receipt_base64_data <- map["receipt_base64_data"]
        mc_currency <- map["mc_currency"]
//        period <- map["period"]
        txn_type <- map["txn_type"]
//        expiration_time <- map["expiration_time"]
      
    }
    
}


class InAppPurchasePaymentSync : Mappable {
    public var is_expired : Int?
    public var expiration_time : String?
    public var new_token : String?

    public required init?(map: Map) {
        
    }
    
    
    public func mapping(map: Map) {
        
        is_expired <- map["is_expired"]
        expiration_time <- map["expiration_time"]
        new_token <- map["new_token"]
    }
}

class PaymentRequestModel : Mappable {
    public var job_id : Int?
    public var source : String?
    public var amount : Double?

    public required init?(map: Map) {
        
    }
    
    init?(job_id : Int?,source : String?) {
        self.job_id = job_id
        self.source = source
    }
    
    init?(amount : Double?,source : String?) {
        self.amount = amount
        self.source = source
    }
    
    init?(job_id : Int?) {
        self.job_id = job_id
    }
    
    public func mapping(map: Map) {
        amount <- map["amount"]
        job_id <- map["job_id"]
        source <- map["source"]
    }
}
