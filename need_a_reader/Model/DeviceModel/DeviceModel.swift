//
//  DeviceModel.swift
//  MindFit
//
//  Created by ob_apple_2 on 8/28/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper

class DeviceInfo: Mappable {
     public var  user_id: Int?
    public var device_carrier : String?
    public var device_country_code : String?
    public var device_default_time_zone : String?
    public var device_language : String?
    public var device_latitude : String?
    public var device_library_version : String?
    public var device_application_version : String?
    public var device_local_code : String?
    public var device_longitude : String?
    public var device_model_name : String?
    public var device_os_version : String?
    public var device_platform : String?
    public var device_registration_date : String?
    public var device_resolution : String?
    public var device_type : String?
    public var device_udid : String?
    public var device_reg_id : String?
    public var device_vendor_name : String?
    public var project_package_name : String?
    
    required init?(map: Map) {
        
    }
    
    init(user_id: Int?, device_carrier: String?, device_country_code: String?, device_default_time_zone: String?, device_language: String?, device_latitude: String?, device_library_version: String?,  device_application_version: String?,device_local_code: String?, device_longitude: String?, device_model_name: String?, device_os_version: String?, device_platform: String?, device_registration_date: String?, device_resolution: String?, device_type: String?, device_udid: String?, device_reg_id: String?, device_vendor_name: String?, project_package_name: String?) {
        
        self.user_id = user_id
        self.device_carrier = device_carrier
        self.device_country_code = device_country_code
        self.device_default_time_zone = device_default_time_zone
        self.device_language = device_language
        self.device_latitude = device_latitude
        self.device_library_version = device_library_version
        self.device_application_version = device_application_version
        self.device_local_code = device_local_code
        self.device_longitude = device_longitude
        self.device_model_name = device_model_name
        self.device_os_version = device_os_version
        self.device_platform = device_platform
        self.device_registration_date = device_registration_date
        self.device_resolution = device_resolution
        self.device_type = device_type
        self.device_udid = device_udid
        self.device_reg_id = device_reg_id
        self.device_vendor_name = device_vendor_name
        self.project_package_name = project_package_name
        
    }
    
    init(device_carrier: String?, device_country_code: String?, device_default_time_zone: String?, device_language: String?, device_latitude: String?, device_library_version: String?,  device_application_version: String?,device_local_code: String?, device_longitude: String?, device_model_name: String?, device_os_version: String?, device_platform: String?, device_registration_date: String?, device_resolution: String?, device_type: String?, device_udid: String?, device_reg_id: String?, device_vendor_name: String?, project_package_name: String?) {
        
        
        self.device_carrier = device_carrier
        self.device_country_code = device_country_code
        self.device_default_time_zone = device_default_time_zone
        self.device_language = device_language
        self.device_latitude = device_latitude
        self.device_library_version = device_library_version
        self.device_application_version = device_application_version
        self.device_local_code = device_local_code
        self.device_longitude = device_longitude
        self.device_model_name = device_model_name
        self.device_os_version = device_os_version
        self.device_platform = device_platform
        self.device_registration_date = device_registration_date
        self.device_resolution = device_resolution
        self.device_type = device_type
        self.device_udid = device_udid
        self.device_reg_id = device_reg_id
        self.device_vendor_name = device_vendor_name
        self.project_package_name = project_package_name
        
    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
        device_carrier <- map["device_carrier"]
        device_country_code <- map["device_country_code"]
        device_default_time_zone <- map["device_default_time_zone"]
        device_language <- map["device_language"]
        device_latitude <- map["device_latitude"]
        device_library_version <- map["device_library_version"]
        device_application_version <- map["device_application_version"]
        device_local_code <- map["device_local_code"]
        device_longitude <- map["device_longitude"]
        device_model_name <- map["device_model_name"]
        device_os_version <- map["device_os_version"]
        device_platform <- map["device_platform"]
        device_registration_date <- map["device_registration_date"]
        device_resolution <- map["device_resolution"]
        device_type <- map["device_type"]
        device_udid <- map["device_udid"]
        device_reg_id <- map["device_reg_id"]
        device_vendor_name <- map["device_vendor_name"]
        project_package_name <- map["project_package_name"]
        
    }
    
}
