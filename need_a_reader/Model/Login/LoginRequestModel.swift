//
//  LoginRequestModel.swift
//  MindFit
//
//  Created by ob_apple_2 on 8/28/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper

class LoginRequestModel: Mappable {
    public var  email_id: String?
    public var password : String?
    public var device_info : DeviceInfo?

    
    required init?(map: Map) {
        
    }
    
    init(email_id: String?, password: String?, device_info: DeviceInfo?) {
        
        self.device_info = device_info

        self.password = password
        self.email_id = email_id
        
    }
    
    func mapping(map: Map) {
        password <- map["password"]
        email_id <- map["email_id"]
        device_info <- map["device_info"]
   
        
    }
    
}

class ClearSession : Mappable {
    public var  user_id: Int?
    
    required init?(map: Map) {
        
    }
    
    init(user_id: Int?) {
        
        
        self.user_id = user_id
        
    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
    }

}

class UpdateLocationRequestModel: Mappable {
    public var latitude : Double?
    public var longitude : Double?
    
    
    required init?(map: Map) {
        
    }
    
    init(latitude: Double?, longitude: Double?) {
        
        self.latitude = latitude
        self.longitude = longitude
    }
    
    func mapping(map: Map) {
        longitude <- map["longitude"]
        latitude <- map["latitude"]
        
    }
    
}
