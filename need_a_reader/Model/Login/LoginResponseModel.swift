//
//  LoginResponseModel.swift
//  MindFit
//
//  Created by ob_apple_2 on 8/28/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper

class LoginResponseModel: Mappable {
    
    public var token : String?
    public var user_detail : User_detail?
    public var user_id : Int?
    public var new_token : String?

    //Get userProfile
//    public var details : User_detail?
//    public var team_list : [List_user]?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        token <- map["token"]
        user_detail <- map["user_detail"]
//        details <- map["details"]
//        team_list <- map["team_list"]
        user_id <- map["user_id"]
        new_token <- map["new_token"]
    }
}

class User_detail: Mappable {
    public var user_id : Int?
    public var user_name : String?
    public var last_name : String?

    public var email_id : String?
    public var social_uid : String?
    public var signup_type : Int?
    public var is_active : Bool?
    public var create_time : String?
    public var update_time : String?
    public var profile_setup : Bool?
    public var headshot_setup : Bool?
    public var resume_setup : Bool?
    public var skill_setup : Bool?
    public var show_me_on : Bool?
    public var chat_id : Int?
    //Get userProfile
    public var maximum_distance : Int?
    public var search_by_gender: Int?
    
    public var gender : Int?
    public var date_of_birth : String?
    public var headshot_original_image: String?
    public var headshot_compressed_image: String?
    public var headshot_thumbnail_image: String?
    public var resume_file: String?
    public var contact_no: String?
    public var state: String?
    public var city: String?
    public var country: String?
    public var latitude : Int?
      public var longitude : Int?
  
    public var skill_detail : AddSkillsRequestModel?
    public var rating_detail : RatingResponseModel?
    public var is_first_time : Int?
    public var  expiration_time: String?
    public var  hourly_rate: Int?

    public var is_taping : Bool?
    public var taping_rate : String?
    public var is_edit : Bool?
    public var editing_rate : String?
    public var service_detail : [ServiceDetail]?
    public var mutual_friend : Int?
    public var video_file : String?
    public var is_uploadedVideo : Bool?
    public var is_slate : Bool?
    public var video_detail : VideoDetailModel?
    public var video_payment_status : Bool?
    public var isStripeSetup : Bool?
    public var final_rate : Float?


    public required init?(map: Map) {
        
    }
    init(final_rate : Float?) {
        
        self.final_rate = final_rate
        
    }
    
    public func mapping(map: Map) {
        search_by_gender <- map["search_by_gender"]
        maximum_distance <- map["maximum_distance"]
        chat_id <- map["chat_id"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        user_id <- map["user_id"]
        user_name <- map["first_name"]
        last_name <- map["last_name"]
        email_id <- map["email_id"]
        social_uid <- map["social_uid"]
        signup_type <- map["signup_type"]
        headshot_original_image <- map["headshot_original_image"]
        headshot_thumbnail_image <- map["headshot_thumbnail_image"]
        headshot_compressed_image <- map["headshot_compressed_image"]
        resume_file <- map["resume_file"]
        is_active <- map["is_active"]
        create_time <- map["create_time"]
        update_time <- map["update_time"]
        profile_setup <- map["profile_setup"]
        headshot_setup <- map["headshot_setup"]
        resume_setup <- map["resume_setup"]
        skill_setup <- map["skill_setup"]
        show_me_on <- map["show_me_on"]
        headshot_original_image <- map["headshot_original_image"]
        headshot_compressed_image <- map["headshot_compressed_image"]
        headshot_thumbnail_image <- map["headshot_thumbnail_image"]
        gender <- map["gender"]
        date_of_birth <- map["date_of_birth"]
        contact_no <- map["contact_no"]
        state <- map["state"]
        city <- map["city"]
        country <- map["country"]
        is_first_time <- map["is_first_time"]

        skill_detail <- map["skill_details"]
        rating_detail <- map["rating_details"]
        expiration_time <- map["expiration_time"]
        hourly_rate <- map["hourly_rate"]
        
        is_taping <- map["is_taping"]
        taping_rate <- map["taping_rate"]
        is_edit <- map["is_edit"]
        editing_rate <- map["editing_rate"]
        service_detail <- map["service_details"]
        mutual_friend <- map["mutual_friend"]
        video_file <- map["video_file"]
        is_uploadedVideo <- map["is_uploadedVideo"]
        
        is_slate <- map["is_slate"]
        video_detail <- map["video_detail"]
        video_payment_status <- map["video_payment_status"]
        isStripeSetup <- map["is_stripe_setup"]
        final_rate <- map["final_rate"]
}
}

class VideoDetailModel : Mappable {
    
    public var video_id : Int?
    public var payment_status : Bool?
    public var video_file : String?
    public var video_thumbnail_img : String?

    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        video_id <- map["video_id"]
        payment_status <- map["payment_status"]
        video_file <- map["video_file"]
        video_thumbnail_img <- map["video_thumbnail_img"]
    }
}
