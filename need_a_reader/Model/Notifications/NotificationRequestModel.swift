//
//  NotificationRequestModel.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 1/30/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper

class NotificationRequestModel: Mappable {
    
    public var job_id : Int?
    public var notificationId : Int?
    public var friend_id : Int?
    public var receiver_user_id : Int?

    
    //Get userProfile
    //    public var details : User_detail?
    //    public var team_list : [List_user]?
    
    public required init?(map: Map) {
        
    }
    
    init(job_id : Int?) {
        
        self.job_id = job_id

    }
    
    
    init(receiver_user_id : Int?) {
        
        self.receiver_user_id = receiver_user_id
        
    }
    
    init(notificationId : Int?) {
        
        self.notificationId = notificationId
        
    }
    
    init(friend_id : Int?) {
        
        self.friend_id = friend_id
        
    }
    public func mapping(map: Map) {
        notificationId <- map["notification_id"]
        job_id <- map["job_id"]
        friend_id <- map["friend_id"]
        receiver_user_id <- map["receiver_user_id"]
    }
}
class NotificationReadtModel: Mappable {
    
    public var notificationId : Int?
    
    public required init?(map: Map) {
        
    }

    init(notificationId : Int?) {
        
        self.notificationId = notificationId
        
    }
    
    public func mapping(map: Map) {
        notificationId <- map["notification_id"]
    
    }
}
