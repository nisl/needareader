//
//  NotificationResponseModel.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 1/30/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper

class NotificationResponseModel : Mappable {
    public var notification_list : [Notification_list]?
    public var new_token : String?

    required init?(map: Map) {
        
    }
//    init(notification_list : [Notification_list]?) {
//
//        self.notification_list = notification_list
//
//
//    }
    
    func mapping(map: Map) {
        notification_list <- map["notification_list"]
        new_token <- map["new_token"]
        
    }
}

class Notification_list : Mappable {
    public var id : Int?
    public var action_id : Int?
    public var receiver_id : Int?
    public var job_id : Int?
    public var notification_type : Int?
    public var ntf_message : String?
    public var is_read : Int?
    public var create_time : String?
    public var update_time : String?
    public var job_detail : Job_detail?
    public var friend_detail : Job_detail?
    public var is_fulfill : Int?
    public var meeting_type : Int?


    required init?(map: Map) {
        
    }
//    init(id : Int?,action_id : Int?,receiver_id : Int?, job_id : Int?,notification_type : Int?,ntf_message : String?, is_read : Int?,  create_time : String?, update_time : String?, job_detail : Job_detail?) {
//
//        self.id = id
//        self.action_id = action_id
//        self.receiver_id = receiver_id
//        self.job_id = job_id
//        self.notification_type = notification_type
//        self.ntf_message = ntf_message
//        self.is_read = is_read
//        self.create_time = create_time
//        self.update_time = update_time
//        self.job_detail = job_detail
//
//
//    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.action_id <- map["action_id"]
        self.receiver_id <- map["receiver_id"]
        self.job_id <- map["job_id"]
        self.notification_type <- map["notification_type"]
        self.ntf_message <- map["ntf_message"]
        self.is_read <- map["is_read"]
        self.create_time <- map["create_time"]
        self.update_time <- map["update_time"]
        self.job_detail <- map["job_detail"]
        self.friend_detail <- map["friend_detail"]
        self.is_fulfill <- map["is_fulfill"]
        self.meeting_type <- map["meeting_type"]
    }
}

class JobResponseModel : Mappable {
    public var job_list : [Job_detail]?
    
    required init?(map: Map) {
        
    }
    
    
    func mapping(map: Map) {
        self.job_list <- map["job_list"]
    }
    
}

 class Job_detail : Mappable {
    public var job_id : Int?
    public var sender_user_id : Int?
    public var receiver_user_id : Int?
    public var job_description : String?
    public var start_at : String?
    public var end_at : String?
    public var create_time : String?
    public var update_time : String?
    public var profile_img : String?
    public var is_active : Int?
    public var first_name : String?
    public var last_name : String?
    public var email_id : String?
    public var chat_id : Int?
    public var approval_status : Int?
    public var is_rated : Bool?
    public var hourly_rate : Int?
    public var friend_id : Int?
    public var meeting_time : String?
    public var finalRate : String?
    public var reader_list : [Readerlist_detail]?
    public var meeting_type : Int?

//    init(job_id : Int?,sender_user_id : Int?,receiver_user_id : Int?, job_description : String?,start_at : String?,end_at : String?, create_time : String?, update_time : String?, profile_img : String?) {
//
//        self.job_id = job_id
//        self.sender_user_id = sender_user_id
//        self.receiver_user_id = receiver_user_id
//        self.job_description = job_description
//        self.start_at = start_at
//        self.end_at = end_at
//        self.create_time = create_time
//        self.update_time = update_time
//        self.profile_img = profile_img
//
//
//    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.job_id <- map["job_id"]
        self.sender_user_id <- map["sender_user_id"]
        self.receiver_user_id <- map["receiver_user_id"]
        self.job_description <- map["job_description"]
        self.start_at <- map["start_at"]
        self.end_at <- map["end_at"]
        self.profile_img <- map["profile_img"]
        self.create_time <- map["create_time"]
        self.update_time <- map["update_time"]
        self.is_active <- map["is_active"]
        self.first_name <- map["first_name"]
        self.email_id <- map["email_id"]
        self.chat_id <- map["chat_id"]
        self.last_name <- map["last_name"]
        self.approval_status <- map["approval_status"]
        self.is_rated <- map["is_rated"]
        self.hourly_rate <- map["hourly_rate"]
        self.friend_id <- map["friend_id"]
        self.meeting_time <- map["meeting_time"]
        self.finalRate <- map["final_rate"]
        self.reader_list <- map["reader_list"]
        self.meeting_type <- map["meeting_type"]

    }
}

class Readerlist_detail : Mappable {
    public var id : Int?
    public var action_id : Int?
    public var receiver_id : Int?
    public var job_id : String?
    public var notification_type : Int?
    public var notification_description : String?
    public var ntf_message : String?
    public var is_read : Int?
    public var create_time : String?
    public var update_time : String?
    public var first_name : String?
    public var last_name : String?
    public var email_id : String?
    public var chat_id : Int?
    public var profile_img : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.action_id <- map["action_id"]
        self.receiver_id <- map["receiver_id"]
        self.job_id <- map["job_id"]
        self.notification_type <- map["notification_type"]
        self.notification_description <- map["notification_description"]
        self.ntf_message <- map["ntf_message"]
        self.is_read <- map["is_read"]
        self.create_time <- map["create_time"]
        self.update_time <- map["update_time"]
        self.first_name <- map["first_name"]
        self.email_id <- map["email_id"]
        self.chat_id <- map["chat_id"]
        self.last_name <- map["last_name"]
        self.profile_img <- map["profile_img"]
       
        
    }
}


