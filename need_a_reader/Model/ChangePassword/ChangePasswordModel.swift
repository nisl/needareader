//
//  ChangePasswordModel.swift
//  MindFit
//
//  Created by ob_apple_2 on 8/30/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper


class ChangePasswordModel: Mappable {
    public var  email_id: String?
    public var  token: String?
     public var  new_password: String?
    public var current_password: String?
    
    required init?(map: Map) {
        
    }
    
    
    init(email_id: String?, new_password: String?, token: String?) {
        
        
        self.token = token
        self.email_id = email_id
        self.new_password = new_password
        
    }
   
    init(new_password: String?, current_password: String?) {
        
        self.new_password = new_password
        self.current_password = current_password
    }
    
    func mapping(map: Map) {
        token <- map["token"]
        email_id <- map["email_id"]
        new_password <- map["new_password"]
        
        current_password <- map["current_password"]
    }
    
}


class changePwdRequestModel : Mappable {
    
    public var  current_password: String?
    public var  new_password: String?
        
    required init?(map: Map) {
        
    }
    
    
    init(current_password: String?, new_password: String?) {
        
        
        self.current_password = current_password
        self.new_password = new_password
        
    }
    
    func mapping(map: Map) {
        current_password <- map["current_password"]
        new_password <- map["new_password"]
        
        
    }
  
}
