//
//  SettingResponseModel.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/19/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper

class SettingResponseModel: Mappable {
    
    public var user_detail : User_detail?
    public var new_token : String?
    public var skill_detail : AddSkillsRequestModel?
    public var rating_detail : RatingResponseModel?
    public var video_detail : VideoDetailModel?
    //Get userProfile
    //    public var details : User_detail?
    //    public var team_list : [List_user]?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        
        skill_detail <- map["skill_detail"]
        user_detail <- map["user_detail"]
        //        details <- map["details"]
        //        team_list <- map["team_list"]
        rating_detail <- map["rating_detail"]
        new_token <- map["new_token"]
        video_detail <- map["video_detail"]

    }
}

class ViewRatingResponseModel : Mappable {
    //display rating variables
    public var job_id : Int?
    public var sender_user_id : Int?
    public var receiver_user_id : Int?
    public var over_all_rating : Int?
    public var is_on_time : Bool?
    public var is_knowledgeable_on_content : Bool?
    public var work_with_reader_again : Bool?
     public var job_satisfaction : Bool?
    public var  rate_id : Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        job_id <- map["job_id"]
        sender_user_id <- map["sender_user_id"]
        receiver_user_id <- map["receiver_user_id"]
        over_all_rating <- map["over_all_rating"]
        is_knowledgeable_on_content <- map["is_knowledgeable_on_content"]
        work_with_reader_again <- map["work_with_reader_again"]
        job_satisfaction <- map["job_satisfaction"]
        is_on_time <- map["is_on_time"]
        rate_id <- map["rate_id"]
    }
}

class RatingResponseModel: Mappable {
    
//    public var sender_user_id : Int?
//    public var receiver_user_id : Int?
//    public var is_on_time : Bool?
//    public var is_knowledgeable_on_content : Bool?
//    public var work_with_reader_again : Bool?
//    public var over_all_rating : Int?
//
//    public var create_time : String?
//    public var update_time : String?
    
    //Updated
    public var ratings : Int?
    public var is_on_time : Int?
    public var is_knowledgeable_on_content : Int?
    public var work_with_reader_again : Int?
    public var on_time_yes : Int?
    public var on_time_no : Int?
    public var is_knowledgeable_yes : Int?
    public var is_knowledgeable_no : Int?
    public var work_with_yes : Int?
    public var work_with_no : Int?
    public var average_rating : String?
   
    public var job_satisfaction_yes : Int?
    public var job_satisfaction_no : Int?
    public var jobSatisFiedPer : Int?

    
    
    required init?(map: Map) {
        
    }
    
   
    
    init(ratings : Int?, is_on_time : Int?, is_knowledgeable_on_content : Int?, work_with_reader_again : Int?, on_time_yes : Int?,  on_time_no : Int?, is_knowledgeable_yes : Int?, is_knowledgeable_no : Int?, work_with_yes : Int?, work_with_no : Int?, average_rating : String?) {
        
        self.ratings = ratings
        self.is_on_time = is_on_time
        self.is_knowledgeable_on_content = is_knowledgeable_on_content
        self.work_with_reader_again = work_with_reader_again
        self.on_time_no = on_time_no
        self.is_knowledgeable_yes = is_knowledgeable_yes
        self.on_time_yes = on_time_yes

        self.is_knowledgeable_no = is_knowledgeable_no
        self.work_with_yes = work_with_yes
        self.work_with_no = work_with_no
        self.average_rating = average_rating
    }
    
    func mapping(map: Map) {
        
        ratings <- map["ratings"]
        is_on_time <- map["is_on_time"]
        is_knowledgeable_on_content <- map["is_knowledgeable_on_content"]
        work_with_reader_again <- map["work_with_reader_again"]
        on_time_no <- map["is_on_time_no"]
        is_knowledgeable_yes <- map["is_knowledgeable_yes"]
        is_knowledgeable_no <- map["is_knowledgeable_no"]
        work_with_yes <- map["work_with_yes"]
        on_time_yes <- map["is_on_time_yes"]
        work_with_no <- map["work_with_no"]
        average_rating <- map["average_rating"]
        job_satisfaction_yes <- map["job_satisfaction_yes"]
        job_satisfaction_no <- map["job_satisfaction_no"]
       jobSatisFiedPer <- map["job_satisfaction"]
    }
    
}
