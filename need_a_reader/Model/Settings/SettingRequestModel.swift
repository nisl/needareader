//
//  SettingRequestModel.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/22/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import ObjectMapper

class SettingRequestModel: Mappable {
    public var  searchBygender: Int?
    public var  maximum_distance: Int?
    public var  show_me_on: Int?
    
    required init?(map: Map) {
        
    }
   
    init(search_by_gender: Int?) {
        //        self.page = page
        self.searchBygender = search_by_gender
        
    }
    
    init(show_me_on: Int?) {
        //        self.page = page
        self.show_me_on = show_me_on
        
    }
    
    init(maximum_distance: Int?) {
        //        self.page = page
        self.maximum_distance = maximum_distance
        
    }
    
    func mapping(map: Map) {
        //        page <- map["page"]
        searchBygender <- map["search_by_gender"]
        show_me_on <- map["show_me_on"]
        maximum_distance <- map["maximum_distance"]
    }
    
}

class RatingRequestModel: Mappable {
    public var is_on_time : Int?
    public var is_knowledgeable_on_content : Int?
    public var work_with_reader_again : Int?
    public var receiver_user_id : Int?
    public var over_all_rating : Float?
    public var chat_id : Int?
    public var  job_satisfaction : Int?
    required init?(map: Map) {
        
    }
    
    init(receiver_user_id : Int?,is_on_time : Int?,is_knowledgeable_on_content : Int?,work_with_reader_again : Int?, over_all_rating : Float?, job_satisfaction : Int?) {
        self.is_on_time = is_on_time
        self.is_knowledgeable_on_content = is_knowledgeable_on_content
        self.work_with_reader_again = work_with_reader_again
        self.receiver_user_id = receiver_user_id
        self.over_all_rating = over_all_rating
        self.job_satisfaction = job_satisfaction

    }
    
    init(chat_id : Int?) {
        
        self.chat_id = chat_id
        
    }
    
    func mapping(map: Map) {
        is_on_time <- map["is_on_time"]
        is_knowledgeable_on_content <- map["is_knowledgeable_on_content"]
        work_with_reader_again <- map["work_with_reader_again"]
        receiver_user_id <- map["job_id"]
        over_all_rating <- map["over_all_rating"]
        chat_id <- map["chat_id"]
        job_satisfaction <- map["job_satisfaction"]
    }
    
}

class ContactRequestModel : Mappable {
    public var search_query : String?
    public var receiver_user_id : Int?
    
    required init?(map: Map) {
        
    }
    
    init(search_query : String?) {
        
        self.search_query = search_query
    }
    
    init(receiver_user_id : Int?) {
        
        self.receiver_user_id = receiver_user_id
    }
    
    
    func mapping(map: Map) {
        search_query <- map["first_name"]
        receiver_user_id <- map["receiver_user_id"]
    }
}

class contactResponseModel: Mappable {
    public var searchUserDetails : [User_detail]?
    public var new_token : String?
    public var myContactLists : [User_detail]?
    public var totalFriend : [User_detail]?
    public var mutualFrndList : [User_detail]?

    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        searchUserDetails <- map["user_detail"]
        new_token <- map["new_token"]
        myContactLists <- map["my_friend_list"]
        totalFriend <- map["mutual_friend"]
        mutualFrndList <- map["mutual_friend_list"]
    }
}
