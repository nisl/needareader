//
//  GlobalFunction.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/14/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SCLAlertView
import AlamofireObjectMapper
import ReachabilitySwift
import UserNotifications

//Color
func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.characters.count) != 6) {
        return UIColor.gray
    }
    
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

//Set gradient color view

func setGradient(customView : UIView)
{
    let gradient = CAGradientLayer()
    
    gradient.frame = customView.bounds
    //    gradient.frame.size.width = customView.frame.size.width
    print("gradient:- \(gradient.frame.size.height)")
    //       gradient.frame.size.width = marqueeView.frame.size.width
    let startColor = hexStringToUIColor(hex: redColor).cgColor
    let endColor = hexStringToUIColor(hex: yellowcolor).cgColor
    //gradient.opacity = 0.8
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 0.5)
    gradient.colors = [endColor, startColor]
    
    //       gradient.locations = [0.0,0.1,0.5]
    
    customView.layer.addSublayer(gradient)
    
}

func getDeviceInfo() -> [String : Any] {
    
    //device_info
    let devicePlatform = "ios"
    let vendor_name = "Apple"
    let login_device_type = "phone"
    let resolution = "\(UIScreen.main.bounds.size.width)\(UIScreen.main.bounds.size.height)"
    
    var param : [String: Any] = [
        "device_carrier" : "",
        "device_country_code" : (NSLocale.current as NSLocale).object(forKey: .countryCode)!,
        "device_default_time_zone" : (NSTimeZone.local as NSTimeZone).name,
        "device_language" : NSLocale.preferredLanguages[0],
        "device_latitude" : "0.0",
        "device_library_version" : "",
        "device_local_code" : NSLocale.preferredLanguages[0],
        "device_longitude" : "0.0",
        "device_model_name" : UIDevice.current.model,
        "device_os_version" : UIDevice.current.systemVersion,
        "device_platform" : devicePlatform,
        "device_registration_date" : "\(NSDate())",
        "device_application_version": app_version,
        "device_resolution" : resolution,
        "device_type" : login_device_type,
        "device_udid" : UIDevice.current.identifierForVendor!.uuidString,
        "device_vendor_name" : vendor_name,
        "project_package_name" : Bundle.main.bundleIdentifier!
    ]
    
    if let deviceToken = UserDefaults.standard.value(forKey: "devicetoken") {
        param["device_reg_id"] = "\(deviceToken)"
    }
    
    return param
}

func registerDevice() {
    
    let device_info = DeviceInfo(device_carrier: getDeviceInfo()["device_carrier"] as! String?, device_country_code: getDeviceInfo()["device_country_code"] as! String?, device_default_time_zone: getDeviceInfo()["device_default_time_zone"] as! String?, device_language: getDeviceInfo()["device_language"] as! String?, device_latitude: getDeviceInfo()["device_latitude"] as! String?, device_library_version: getDeviceInfo()["device_library_version"] as! String?, device_application_version: getDeviceInfo()["device_application_version"] as! String?, device_local_code: getDeviceInfo()["device_local_code"] as! String?, device_longitude: getDeviceInfo()["device_longitude"] as! String?, device_model_name: getDeviceInfo()["device_model_name"] as! String?, device_os_version: getDeviceInfo()["device_os_version"] as! String?, device_platform: getDeviceInfo()["device_platform"] as! String?, device_registration_date: getDeviceInfo()["device_registration_date"] as! String?, device_resolution: getDeviceInfo()["device_resolution"] as! String?, device_type: getDeviceInfo()["device_type"] as! String?, device_udid: getDeviceInfo()["device_udid"] as! String?, device_reg_id: getDeviceInfo()["device_reg_id"] as! String?, device_vendor_name: getDeviceInfo()["device_vendor_name"] as! String?, project_package_name: getDeviceInfo()["project_package_name"] as! String?)
    
    let param = Mapper<DeviceInfo>().toJSON(device_info)
    print("device-Info :- \(param)")
    
    request(registerUserDeviceByDeviceUdid, method: .post, parameters: param, encoding: JSONEncoding(options: []), headers: nil).responseObject { (response: DataResponse<ResponseModel>) in
        switch response.result {
            
        case .success(let value):
            if let code = value.code {
                switch code {
                case 200:
                    alert.hideView()
                    UserDefaults.standard.set(true, forKey: "server_sync")
                    break
                case 400:
                    clearSession()
//                    let story = UIStoryboard(name: "Main", bundle: nil)
//                    
//                    let nextVC = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//                    //                    self.navigationController?.pushViewController(nextVC, animated: true)
//                    let nav = UINavigationController(rootViewController: nextVC)
                    
                    break
                case 401:
                    alert.hideView()
                    if let new_token = value.signUpResponse?.new_token {
                        UserDefaults.standard.set(new_token, forKey: "token")
                        registerDevice()
                    }
                    break
                default:
                    break
                }
            }
            
            break
        case .failure:
            break
        }
    }
}

func setIntValueForGender(gendertype: String) -> Int {
    switch gendertype {
    case "Male":
        return 1
    case "Female":
        return 2
    default:
        return 0
    }
}



func setStringValueForGender(gendertype: Int) -> String {
    switch gendertype {
    case 1:
        return "Actor"
    case 2:
        return "Actress"
    default:
        return "All"
    }
}

func setStringForGenderForUpdateProfile(gendertype: Int) -> String {
    switch gendertype {
    case 1:
        return "Male"
    default:
        return "Female"
    }
}

//SetSliderValue
func setSliderValue(slider : UISlider, val : Int, skillLbl : UILabel) {
    slider.value = Float(val)
    skillLbl.text = "\(val)"
}
//Clear session data
func clearSession() {
    
    UserDefaults.standard.removeObject(forKey: "token")
    defaults.removeObject(forKey: "readerSelected")
    defaults.removeObject(forKey: "setHireReaderInfo")

    //    defaults.removeObject(forKey: getFeelingValueForUser)
    
    defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
    defaults.synchronize()
}

func emptyFieldError(str: String) {
    let alertView = SCLAlertView()
    alertView.showError("Error", subTitle: "Please enter " + str, closeButtonTitle: "Ok")
}



//Subscription Payment

//func UTCToLocal(date:String) -> Date {
//    let dateFormatter = DateFormatter()
//    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//
//    let dt = dateFormatter.date(from: date)
////    dateFormatter.timeZone = TimeZone.current
////    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss a"
////
//    return dt!//ateFormatter.string(from: dt!)
//}

func convertstringToDate(str: String) -> Date{
    let dateString = "\(str) +0000"
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz"
    let date = dateFormatter.date(from:dateString)
    return date!
}

func convertMeetingTimeStringToDate(str: String) -> Date{
    let dateString = "\(str)"//"\(str) +0000"
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"//"yyyy-MM-dd HH:mm:ss +zzzz"
    let date = dateFormatter.date(from:dateString)
    return date!
}

func changeViewColor(view: [UIView], backgroundcolor : String) {
    for i in 0..<view.count {
        view[i].backgroundColor = hexStringToUIColor(hex: backgroundcolor)
    }
}

func changeButtonColor(view: [UIButton], backgroundcolor : String, fontColor: String) {
    for i in 0..<view.count {
        view[i].backgroundColor = hexStringToUIColor(hex: backgroundcolor)
        view[i].setTitleColor(hexStringToUIColor(hex: fontColor), for: .normal)
    }
}

func changeSliderColor(view: [UISlider], minimumTrackcolor : String, thumbColor: String) {
    for i in 0..<view.count {
        view[i].thumbTintColor = hexStringToUIColor(hex: thumbColor)
        view[i].minimumTrackTintColor = hexStringToUIColor(hex: minimumTrackcolor)
//        view[i].backgroundColor = hexStringToUIColor(hex: backgroundcolor)
//        view[i].setTitleColor(hexStringToUIColor(hex: fontColor), for: .normal)
    }
}

func changeSwitchColor(view: [UISlider], tintcolor : String, thumbColor: String) {
    for i in 0..<view.count {
        view[i].thumbTintColor = hexStringToUIColor(hex: thumbColor)
        view[i].tintColor = hexStringToUIColor(hex: tintcolor)
    }
}

func changeLabelColor(view: [UILabel], backcolor : String, fontColor: String) {
    for i in 0..<view.count {
        view[i].backgroundColor = hexStringToUIColor(hex: backcolor)
        view[i].textColor = hexStringToUIColor(hex: fontColor)
    }
}


var current_user_detail: User_detail? {
    if let value = defaults.value(forKey: "user_detail") as? [String: Any] {
        if let user = Mapper<User_detail>().map(JSON: value) {
            return user
        }
    }
    return nil
}

var default_password: String {
    if let value = current_user_detail {
        if let pwd = value.email_id {
            return pwd
        }
    }
    return ""
}

var dialogUsers: [QBUUser] {
    if let users = ServicesManager.instance().sortedUsers() {
        return users
    }
    
    return []
}

func getQBUUserFor(chatId: UInt) -> QBUUser? {
   // var temparr = [Int]()
    print(dialogUsers.count)
    for user in dialogUsers {
      
      //  temparr.append(Int(user.id))
       
        print("userid.ID:- \(user.id)")
        if user.id == chatId {
            return user
        }
    }
     //print(temparr.count)
    return nil
}

func getAllQBUUserFor(chatId: UInt, users: [QBUUser]) -> QBUUser? {
    print(users.count)
    for user in users {
        print("userid.ID:- \(user.id)")
        if user.id == chatId {
            return user
        }
    }
    return nil
}

func updateUserNameForCurrentUser(name: String) {
    let updateParameters = QBUpdateUserParameters()
    updateParameters.fullName = name
    updateParameters.tags = [room_name]
    QBRequest.updateCurrentUser(updateParameters, successBlock: { (response, currentUser) in
        print("success update user fullName")
    }) { (error) in
        print("error update user fullName; \(error.error?.error?.localizedDescription)")
    }
}

func updateUserPhotoForCurrentUser(image: UIImage) {
    let alphaInfo = image.cgImage?.alphaInfo
    let hasAlpha = !(alphaInfo == .none || alphaInfo == .noneSkipFirst || alphaInfo == .noneSkipLast)
    if let imageData = hasAlpha ? UIImagePNGRepresentation(image) : UIImageJPEGRepresentation(image, 1.0) {
        QBRequest.tUploadFile(imageData, fileName: "\(Date().timeIntervalSinceNow).\(hasAlpha ? "png" : "jpg")", contentType: "\(hasAlpha ? "image/png" : "image/jpeg")", isPublic: true, successBlock: { (response : QBResponse!, uploadedBlob :QBCBlob!) -> Void in
            if let uploadedFileID = uploadedBlob.publicUrl() {
                let updateParameters = QBUpdateUserParameters()
                updateParameters.customData = uploadedFileID
                updateParameters.tags = [room_name]
                QBRequest.updateCurrentUser(updateParameters, successBlock: { (response, currentUser) in
                    print("success update user fullName")
                }) { (error) in
                    print("error update user fullName; \(error.error?.error?.localizedDescription)")
                }
            }
        }, statusBlock: { (request : QBRequest?, status : QBRequestStatus?) -> Void in
            
        }) { (response : QBResponse!) -> Void in
        }
    }
}

func delete24HoursDialogs() {
//    DispatchQueue.global(qos: .background).async {
        let array = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogs(with: [])
//        let array = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
        DispatchQueue.main.async {
            for item in array {
                if let date = item.updatedAt {
                    let days = date.daysBetweenDate(toDate: Date())
                    if days > 7 {
                        ServicesManager.instance().chatService.deleteDialog(withID: item.id!, completion: { (response) -> Void in
                            guard response.isSuccess else {
                                print("delete error: \(response.error?.error)")
                                return
                            }
                            if response.isSuccess {
                                print("delete success")
                            }else {
                                print("delete not success")
                            }
                        })
                        
                    }
                }
            }
        }
//    }
}

func setSliderEnabled(slider: [UISlider]) {
    for i in 0..<slider.count {
        slider[i].isUserInteractionEnabled = false
    }
}

func setUI(arr: [UIView]) {
    for i in 0..<arr.count {
        viewCorners.addCornerToView(view: arr[i], value: 5)
        viewCorners.addShadowToView(view: arr[i])
    }
}

//func redirectToViewController(vcName: String, view: UIViewController) {
//    let vc = storyBoard.instantiateViewController(withIdentifier: vcName) as! view
//    let nav = UINavigationController(rootViewController: vc)
//    nav.navigationBar.isTranslucent = false
//    nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
//    nav.navigationBar.shadowImage = UIImage()
//    nav.interactivePopGestureRecognizer?.isEnabled = false
//    view.navigationController?.present(nav, animated: true, completion: nil)
//}

func redirectWebViewDialog(title: String, str: String, view: UIViewController) {
    let alertview = UIAlertController(title: title, message: str, preferredStyle: .alert)
    let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
        let story = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = story.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        //                        nextVC.userId = rece_id
        view.navigationController?.pushViewController(nextVC, animated: true)
    }
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
    alertview.addAction(okAction)
    alertview.addAction(cancelAction)
    view.present(alertview, animated: true, completion: nil)
}

func redirectToRootViewController(vcName: UIViewController) -> UINavigationController{
    let nav = UINavigationController(rootViewController: vcName)
    nav.interactivePopGestureRecognizer?.isEnabled = false
    nav.navigationBar.isTranslucent = false
    nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
    nav.navigationBar.shadowImage = UIImage()
    return nav
}

func noInternetMsgDialog() {
    let alertview = SCLAlertView()
    alertview.showError("Error", subTitle: noInternetMessage, closeButtonTitle: "Ok")
}

func setNavigaion(vc: UIViewController) {
    vc.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "OpenSans-Semibold", size: 18.0)]
    //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    vc.navigationController?.navigationBar.isTranslucent = false
    vc.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    vc.navigationController?.navigationBar.shadowImage = UIImage()
    vc.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
}

func setBtnTextColor(currentLbl:UILabel, alignment: NSTextAlignment, color: UIColor, text: String) {
    currentLbl.text = text
    currentLbl.textAlignment = alignment
    currentLbl.textColor = color
}

func setSelectedBtnColors(selectedBtn: UIButton, otherBtn: [UIButton]) {
    changeButtonColor(view: [selectedBtn], backgroundcolor: redColor, fontColor: "ffffff")
    clearBtnColor(btns: otherBtn, titleColor: redColor)
}

func clearBtnColor(btns: [UIButton], titleColor: String) {
    for i in 0..<btns.count {
        btns[i].setTitleColor(hexStringToUIColor(hex: titleColor), for: .normal)
        btns[i].backgroundColor = UIColor.clear
    }
}

func deleteNotificationByJobId(jobId: Int, isFromAppdelegate: Bool? = false)  {
    
//    }
        if #available(iOS 10.0, *) {
            
            UNUserNotificationCenter.current().getPendingNotificationRequests { (notificationRequests) in
                var identifiers: [String] = []
                for notification in notificationRequests {
                    print("not :-\(notification.trigger) ----- \(notification.identifier)")
                    if notification.identifier == "\(jobId)" {
                            let identifiers = notification.identifier
                            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [identifiers])
                        break
                    }
                }
            }
        } else {
            // Fallback on earlier versions
            let allNotification = UIApplication.shared.scheduledLocalNotifications!
            print("allnotification list :- \(allNotification.count)")
            //    if !(allNotification?.isEmpty)! {
            let totalNotification = allNotification.count
            for i in 0..<totalNotification {
                let event = allNotification[i].userInfo
                print("notificationid:- \(event?.keys)")
                if (event!["uid"] as? String) == "\(jobId)"  {
                    UIApplication.shared.cancelLocalNotification(allNotification[i])
                    break
                }
            }
        }
    
  
}

func setPaymentNotification(jobId: Int, notificationDate: Date) {
    
    let notificationDate1 = Calendar.current.date(byAdding: .day, value: 3, to: notificationDate)
    print("notificationDate :-\(notificationDate1)")
    
    let calendar = Calendar.current
    var components = calendar.dateComponents(in: .current, from: notificationDate1!)
    let newComponents = DateComponents(calendar: calendar, timeZone: .current, year: components.year, month: components.month, day: components.day, hour: components.hour, minute: components.minute, second: components.second)
    
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            content.title = ""//app_name
            content.subtitle = ""
            content.body = "If you have not paid your reader and released payment, please do so."
            content.sound = UNNotificationSound.default()

            //        (3 * 24 * 60 * 60)
            
            let notificationTrigger = UNCalendarNotificationTrigger(dateMatching: newComponents, repeats: false)//UNTimeIntervalNotificationTrigger(timeInterval: 60, repeats: false)
            
            let notificationRequest = UNNotificationRequest(identifier: "\(jobId)", content: content, trigger: notificationTrigger)
            UNUserNotificationCenter.current().add(notificationRequest) { (error) in
                if let error = error {
                    print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
                }
            }
            
        } else {
            let notification = UILocalNotification()
            notification.alertTitle = ""//app_name
            notification.alertBody = "If you have not paid your reader and released payment, please do so."
            notification.fireDate = notificationDate1
            UIApplication.shared.scheduleLocalNotification(notification)
            
        }
    
}

