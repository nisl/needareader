//
//  Constant.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/14/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView

let page_count = 10
var isFromSetting : Bool = false
var isCameraImg : Bool = false
var isGalleryImg : Bool = false
var card_number = ""

//In-app purchase
//let is_paid = defaults.set(false, forKey: "is_paid")

var isSettingChanged = false

//colors
let redColor = "f04100"//"f65a54"C40200
let lightGrayColor = "EEEEEE"
let darkGrayColor = "e3e3e3"
let extraDarkGrayColor = "717171"
let blackColor = "000000"
let greenColor = "6edd1c"
let purpleColor = "6e6c8a"
let yellowcolor = "ffb000"
let whiteColor = "FFFFFF"

let int_red : UInt = 0xf04100

//var waitingDuration = 10

//objects
let storyBoard = UIStoryboard(name: "Main", bundle:nil)
let appDelegate = UIApplication.shared.delegate as! AppDelegate
let defaults = UserDefaults.standard

let viewCorners = ViewCorners()
let googleApiKey = "AIzaSyD40rFBEFtuwgbpT4A6318lfKaj7uhZgAY"//"AIzaSyB-GVnKs0ddBRvJYIoWwn7_lsbxXkFXtWs"

//application
let app_id = "1329677465"
let app_name = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as! String
let app_version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
let app_package_name = Bundle.main.bundleIdentifier
let itunesAppLink = "https://itunes.apple.com/us/app/id"+app_id
let appStoreLink = "itms-apps://itunes.apple.com/app/id"+app_id
let developerAppStoreLink = "itms-apps://itunes.apple.com/us/developer/bhavesh-gabani/id1229134874"
let share_text = "I'm using \(app_name)! an awesome app! Get the app at "
let app_logo_name = "sidemenu_logo"
let appStoreLink1 = "itms-apps://itunes.apple.com/app/"

let videoPaymentAmount = 1.99

//Alamofire
let alamofireManager = appDelegate.sessionManager
let requestTimeout : TimeInterval = 35
var tokenHeader : [String: String] {
    if let token = UserDefaults.standard.value(forKey: "token") {
        return ["Authorization" : "Bearer \(token)"]
    }else {
        return [:]
    }
}

//size
let screenWidth = UIScreen.main.bounds.size.width
let screenHeight = UIScreen.main.bounds.size.height

//messages
let serverError = "\(app_name) is unable to connect with server."
//let noConnectionMessage = "\(app_name) is unable to find data. try again."
let noConnectionMessage = "No items found."
let noDataMessage = "No data found."
let noInternetMessage = "There is no internet connection."
let existEmailMsg = "E-mail already exist."
let passwordChangeMessage = "Password changed successfully."
let failureViewMsg = app_name +  " is unable to find data. Please Tap here to try again."
let notificationMsg = app_name + " wants to send you notification"
let reminderArrMsg = "Please select atleast one day."
let invalidEmailMessage = "Please Eeter valid email."
let passwordlengthMessage = "Password must contain at least 6 characters."
let noProfileMessage = "Please upload headshot."
let noResumeMessage = "Please upload resume."
let restoreFailedMessage = "Nothing to restore."
let purchaseFailedMessage = "Your purchased failed. Please try again."
let subcriptionfailErrorMessage = "Please subscribe to \(app_name) to continue."
let restoreProductMsg = "Product restored successfully."
let updateProfileMessage = "Profile updated successfully."


//URL
let privacyPolicyURL = "https://needareader.com/privacypolicy.html"
let termsOfUseURL = "https://needareader.com/termsandconditions.html"
let howToUseURL = "https://vimeo.com/298309415"

//Test server of need a reader:
//let serverURL = "https://needareader.com/test/need_a_reader_backend/api/public/api/"
//let imageBucketPath = "https://needareader.com/test/need_a_reader_backend/image_bucket/compressed/"


// local
//let serverURL = "http://192.168.0.114/need_a_reader_backend/api/public/api/"
//let imageBucketPath = "http://192.168.0.114/need_a_reader_backend/image_bucket/compressed/"


//live
let serverURL = "https://needareader.com/api/public/api/"
let imageBucketPath = "https://needareader.com/image_bucket/compressed/"

//api


let doSignUpUser = serverURL + "signupUser"
let doLoginForUser = serverURL + "doLoginForUser"
let verifyOTPForRegisterUser = serverURL + "verifyOTPForRegisterUser"
let verifyOTPForUser = serverURL + "verifyOTPForUser"
let resendOTPForRegisterUser = serverURL + "resendOTPForRegisterUser"
let resendOTPForUser = serverURL + "resendOTPForUser"
let newPasswordForUser = serverURL + "newPasswordForUser"
let forgotPasswordForSendOTP = serverURL + "forgotPasswordForSendOTP"
let registerUserDeviceByDeviceUdid = serverURL + "registerUserDeviceByDeviceUdid"
let forgotPasswordVerifyOtp = serverURL + "verifyOTPForUser"
let addNewPasswordForUser = serverURL + "newPasswordForUser"
let setHeadShotForUser = serverURL + "setHeadShotForUser"
let setResumeForUser = serverURL + "setResumeForUser"
let addSkillForUser = serverURL + "addSkillForUser"
let searchNearByReader = serverURL + "searchNearByReader"
let getUserProfileByUser = serverURL + "getUserProfileByUser"
let getSkillForUser = serverURL + "getSkillForUser"
let doLogout = serverURL + "doLogout"
let setGenderType = serverURL + "setGender"
let setMaximumDistance = serverURL + "setMaximumDistance"
let setShowMeOnForUser = serverURL + "setShowMeOnForUser"
let updateUserProfileByUser = serverURL + "updateUserProfileByUser"
let changePassword = serverURL + "changePassword"
let addRatingForUser = serverURL + "addRatingForUser"
let addSubscriptionPaymentForIOS = serverURL + "addSubscriptionPaymentForIOS"
let addConsumablePaymentForIOS = serverURL + "addConsumablePaymentForIOS"
let updateLocationByUser = serverURL + "updateLocationByUser"
let setIsFirstSwipe = serverURL + "setIsFirstSwipe"
let getUserPaymentSync = serverURL + "getUserPaymentSync"
let getUserProfileByChatId = serverURL + "getUserProfileByChatId"
let setHourlyRate = serverURL + "setHourlyRate"
let sendRequestToHireReader = serverURL + "sendRequestToHireReader"
let getAllNotificationForSync = serverURL + "getAllNotificationForSync"
let approveJob = serverURL + "approveJob"
let rejectJob = serverURL + "rejectJob"
let getAllJobs = serverURL + "getAllJobsPhase3" //"getAllJobs"
let releasePayment = serverURL + "releasePaymentV2"//"releasePayment"
let requestPayment = serverURL + "requestPayment"
let getJobDetailByJobId = serverURL + "getJobDetailByJobId"
let cancelJob = serverURL + "cancelJob"
let getRatingByJobId = serverURL + "getRatingByJobId"
let deleteNotificationById = serverURL + "deleteNotificationById"
let getServiceNameForUser = serverURL + "getServiceNameForUser"
let addTapingForUser = serverURL + "addTapingForUser"
let uploadVideo = serverURL + "UploadVideoByUser"
let searchUser = serverURL + "searchUser"
let sendFriendRequest = serverURL + "sendFriendRequest"
let getMyFriendList = serverURL + "getMyFriendList"
let rejectFriendRequest = serverURL + "rejectFriendRequest"
let acceptFriendRequest = serverURL + "acceptFriendRequest"
let getMutualFriendList = serverURL + "getMutualFriendList"
let releasePaymentForVideo = serverURL + "releasePaymentForVideo"
let disconnectFriendByUser = serverURL + "disconnectFriendByUser"

// phase 3
let deleteJob = serverURL + "deleteJobPhase3"
let ratingForUser = serverURL + "getAllRatingForUserPhase3"
let sendMultipleRequestToHireReaderPhase3 = serverURL + "sendMultipleRequestToHireReaderPhase3"
let setReadMarkPhase3 = serverURL + "setReadMarkPhase3"


//Alert
let closeButtonAppearance = SCLAlertView.SCLAppearance(
    showCloseButton: false
)
let alert = SCLAlertView(appearance: closeButtonAppearance)
let loadingTitle = "Loading..."
let errorTitle = ""

// Strip publishable Key
let publicKey = "pk_live_xPwMFuYu6cLzfdmRDNyYL3AQ" // live
//let publicKey = "pk_test_hjwaeycNGy2WIObsno6IdBMs" // local

//QuickBlox Live

let qb_app_id: UInt = 68121
let qb_auth_key = "k-Hjtemeu2BQ9UQ"
let qb_auth_secret = "gGf67w8dxxf2ZKh"
let qb_account_key = "zzQ3h8s3fy3WoQConxmY"
var room_name = "needareader"

//QuickBlox Local
//let qb_app_id: UInt = 66374
//let qb_auth_key = "OOmOBEcKcCG2vZY"
//let qb_auth_secret = "etQYpf2NEWRAYrJ"
//let qb_account_key = "Ysxfpx4CdjVCMtZfvmft"
//var room_name = "needareader"

let kDialogsPageLimit:UInt = 100
let kMessageContainerWidthPadding:CGFloat = 40.0
let kChatPresenceTimeInterval:TimeInterval = 45

let kQBAnswerTimeInterval = 60.0
let kQBDialingTimeInterval = 5.0
let kVoipEvent = "VOIPCall"

var AllQuickbloxUser : [QBUUser] = []

class Constants {
    class var QB_USERS_ENVIROMENT: String {
        return room_name
    }
}

var modelName: String {
    var systemInfo = utsname()
    uname(&systemInfo)
    let machineMirror = Mirror(reflecting: systemInfo.machine)
    let identifier = machineMirror.children.reduce("") { identifier, element in
        guard let value = element.value as? Int8 , value != 0 else { return identifier }
        return identifier + String(UnicodeScalar(UInt8(value)))
    }
    
    switch identifier {
    case "iPod5,1":                                 return "iPod Touch 5"
    case "iPod7,1":                                 return "iPod Touch 6"
    case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
    case "iPhone4,1":                               return "iPhone 4s"
    case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
    case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
    case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
    case "iPhone7,2":                               return "iPhone 6"
    case "iPhone7,1":                               return "iPhone 6 Plus"
    case "iPhone8,1":                               return "iPhone 6s"
    case "iPhone8,2":                               return "iPhone 6s Plus"
    case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
    case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
    case "iPhone8,4":                               return "iPhone SE"
    case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
    case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
    case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
    case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
    case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
    case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
    case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
    case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
    case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
    case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
    case "AppleTV5,3":                              return "Apple TV"
    case "i386", "x86_64":                          return "Simulator"
    default:                                        return identifier
    }
}


//Device info
let deviceInfo = DeviceInfo(device_carrier: getDeviceInfo()["device_carrier"] as! String?, device_country_code: getDeviceInfo()["device_country_code"] as! String?, device_default_time_zone: getDeviceInfo()["device_default_time_zone"] as! String?, device_language: getDeviceInfo()["device_language"] as! String?, device_latitude: getDeviceInfo()["device_latitude"] as! String?, device_library_version: getDeviceInfo()["device_library_version"] as! String?, device_application_version: getDeviceInfo()["device_application_version"] as! String?, device_local_code: getDeviceInfo()["device_local_code"] as! String?, device_longitude: getDeviceInfo()["device_longitude"] as! String?, device_model_name: getDeviceInfo()["device_model_name"] as! String?, device_os_version: getDeviceInfo()["device_os_version"] as! String?, device_platform: getDeviceInfo()["device_platform"] as! String?, device_registration_date: getDeviceInfo()["device_registration_date"] as! String?, device_resolution: getDeviceInfo()["device_resolution"] as! String?, device_type: getDeviceInfo()["device_type"] as! String?, device_udid: getDeviceInfo()["device_udid"] as! String?, device_reg_id: getDeviceInfo()["device_reg_id"] as! String?, device_vendor_name: getDeviceInfo()["device_vendor_name"] as! String?, project_package_name: getDeviceInfo()["project_package_name"] as! String?)
