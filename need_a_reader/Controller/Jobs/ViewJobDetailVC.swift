//
//  ViewJobDetailVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 1/30/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SCLAlertView
import AlamofireObjectMapper
import ObjectMapper
import ReachabilitySwift
import Stripe
//import UserNotifications

class ViewJobDetailVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var popupView: UIView!
    @IBOutlet var acceptRejectStackview: UIStackView!
    @IBOutlet var deleteJobBtn: UIButton!
    @IBOutlet var jobDescriptionLbl: UILabel!
    @IBOutlet var declineBtn: UIButton!
    @IBOutlet var acceptBtn: UIButton!
    @IBOutlet var txtView: UITextView!
    var job_detail : Job_detail!
    var readerlistArr : [Readerlist_detail]!
    var isFromNotification : Bool = true
    var notification_id : Int!
    var btnTitle : String = ""
    let chatBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
    var isUserRequestPayment : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        acceptBtn.setTitle("", for: .normal)
        tabBarController?.tabBar.isHidden = true
        if UIDevice.current.userInterfaceIdiom == .pad {
            declineBtn.frame.size.height = 70
            acceptBtn.frame.size.height = 70
            deleteJobBtn.frame.size.height = 70
        }
        
        if let readerlist = job_detail.reader_list {
            readerlistArr = readerlist
            print(readerlistArr?.count)
        }
        
        print(job_detail.hourly_rate)
        
        if readerlistArr?.count != nil {
            self.tableView.isHidden = false
            self.txtView.isHidden = true
        }else {
            self.tableView.isHidden = true
            self.txtView.isHidden = false
        }
      
        self.chatBtn.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureNavigationBar()
        
        tabBarController?.tabBar.isHidden = true
        setUI()
        if isUserRequestPayment {
            if let jobId = job_detail.job_id {
                userRequestPayment(job_id: jobId)
            }
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        tabBarController?.tabBar.isHidden = false
    }
    
    
    func setUI() {
        viewCorners.addCornerToView(view: acceptBtn)
        viewCorners.addCornerToView(view: declineBtn)
        viewCorners.addCornerToView(view: deleteJobBtn)
        
        declineBtn.isHidden = true
        acceptBtn.isHidden = true
        deleteJobBtn.isHidden = true
        viewCorners.addShadowToView(view: txtView)
        viewCorners.addCornerToView(view: txtView, value: 10)
        //        viewCorners.addShadowToView(view: jobDescriptionLbl)
        //        viewCorners.addCornerToView(view: jobDescriptionLbl, value: 10)
        jobDescriptionLbl.numberOfLines = 0
        if job_detail != nil {
            if let id = job_detail.job_id {
                txtView.text = job_detail.job_description
                getJobDetail(job_id: id)
            }
        }
        
        //        if isFromNotification {
        //
        //            declineBtn.isHidden = false
        //            chatBtn.isHidden = true
        //        }else {
        //            declineBtn.isHidden = true
        //            acceptBtn.setTitle("REQUEST PAYMENT", for: .normal)
        //        }
        if job_detail != nil {
            //            if isFromNotification {
            //                if notification_id != nil {
            //                    if notification_id == 1 && job_detail.approval_status != 1 {
            //                        declineBtn.isHidden = true
            //                        acceptBtn.setTitle("PAYMENT REQUESTED!!", for: .normal)
            //                    } else {
            //                        if  job_detail.approval_status == 2 {
            //                            declineBtn.isHidden = true
            //                            acceptBtn.setTitle("REQUEST PAYMENT", for: .normal)
            //                        } else if job_detail.approval_status == 5 {
            //                            declineBtn.isHidden = true
            //                            acceptBtn.setTitle("RATE A READER", for: .normal)
            //                        } else if job_detail.approval_status == 4 {
            //                            declineBtn.isHidden = true
            //                            acceptBtn.setTitle("PAY READER", for: .normal)
            //                        }
            //                    }
            //                }
            //
            //            }
            //            jobDescriptionLbl.text = job_detail.job_description
        }
    }
    
    
    
    //Set NavigationBar
    func configureNavigationBar() {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "OpenSans-Semibold", size: 18.0)]
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        let navigationItem = self.navigationItem
        navigationItem.title = "JOB DETAIL"
        
        self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        self.navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        if #available(iOS 11.0, *) {
            let chatwidthConstraint = chatBtn.widthAnchor.constraint(equalToConstant: 44)
            let chatheightConstraint = chatBtn.heightAnchor.constraint(equalToConstant: 44)
            chatheightConstraint.isActive = true
            chatwidthConstraint.isActive = true
            chatBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 0)//UIEdgeInsetsMake(8, 0, 8, 16)
        }else {
            chatBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 0)//UIEdgeInsetsMake(8, 0, 6, 12)
        }
        if readerlistArr?.count != nil {
            chatBtn.setImage(UIImage(named: "ic_questionMark"), for: .normal)  //ic_chat
            chatBtn.isHidden = false
        }else {
            chatBtn.setImage(UIImage(named: "ic_chat"), for: .normal)  //ic_chat
        }
        
        chatBtn.addTarget(self, action: #selector(chatBtnPressed(_:)), for: .touchUpInside)
        let chatButton = UIBarButtonItem(customView: chatBtn)
        navigationItem.rightBarButtonItems = [chatButton]
    }
    
    @objc func chatBtnPressed(_ sender: UIButton) {
        if readerlistArr?.count != nil {
            let popOverVC = storyBoard.instantiateViewController(withIdentifier: "PopupjobDetailsVC") as! PopupjobDetailsVC
            //            self.addChildViewController(popOverVC)
            //            popOverVC.view.frame = self.memberDetailView.frame
            popOverVC.modalPresentationStyle = .overFullScreen
            popOverVC.modalTransitionStyle = .crossDissolve
            //            self.view.addSubview(displayView)
            popOverVC.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
            self.navigationController?.present(popOverVC, animated: true, completion: nil)
            
        }else {
           
            
            //        if let chatId = job_detail.chat_id {
            //            if let email = job_detail.email_id {
            //                goToChat(chat_id: chatId, email: email, userName: job_detail.first_name!)
            //
            //            }
            //
            //        }
            alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
            let navigationArray = self.navigationController?.viewControllers
            let newStack = [] as NSMutableArray
            
            //change stack by replacing view controllers after ChatVC with ChatVC
            //        for vc in navigationArray! {
            //            // ChatViewController
            //            newStack.add(vc)
            //            if vc is DialogsViewController {
            //                let chatVC = self.storyboard!.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            //                chatVC.chat_id =  self.job_detail.chat_id
            //                chatVC.dataSource = appDelegate.dataSource
            //                chatVC.session = appDelegate.session
            //                //                chatVC.callUUID = self.callUUID
            //                //                chatVC.opponentIDs = self.opponentIDs
            //                newStack.add(chatVC)
            //                self.navigationController!.setViewControllers(newStack.copy() as! [UIViewController], animated: true)
            //                return;
            //            }
            //        }
            if (Reachability()?.isReachable)! {
                alert.hideView()
                
                let vc = storyBoard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                print(self.job_detail.chat_id)
                print(appDelegate.dataSource)
                print(appDelegate.session)
                vc.chat_id =  self.job_detail.chat_id
                vc.dataSource = appDelegate.dataSource
                vc.session = appDelegate.session
                //        vc.callUUID = self.callUUID
                //        vc.opponentIDs = self.opponentIDs
                let navigationController = UINavigationController(rootViewController: vc)
                present(navigationController, animated: true)
            } else {
                alert.hideView()
                //            let alertview = SCLAlertView()
                //            alertview.showError("Error", subTitle: noInternetMessage, closeButtonTitle: "Ok")
                noInternetMsgDialog()
            }
            
            //        self.navigationController?.pushViewController(vc, animated: true)
            //        self.performSegue(withIdentifier: "SA_STR_SEGUE_GO_TO_CHAT", sender: nil)
            
            
            
        }
        
        
    }
    
    @IBAction func deleteJobBtnClicked(_ sender: Any) {
        let alertview = UIAlertController(title: "Alert", message: "Are you sure you want to delete this job?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            if let jobId = self.job_detail.job_id {
                self.deleteJob(job_id: jobId)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alertview.addAction(okAction)
        alertview.addAction(cancelAction)
        
        self.present(alertview, animated: true, completion: nil)

    }
    
    @IBAction func declineBtnClicked(_ sender: Any) {
        let alertview = UIAlertController(title: "Alert", message: "Are you sure you want to decline this job?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            if let jobId = self.job_detail.job_id {
                self.jobAcceptReject(job_id: jobId, isAccept: false)
                
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alertview.addAction(okAction)
        alertview.addAction(cancelAction)
        
        self.present(alertview, animated: true, completion: nil)
        
    }
    
    @IBAction func acceptBtnClicked(_ sender: Any) {
        let userId = defaults.integer(forKey: "userId")
        if acceptBtn.title(for: .normal) == "REQUEST PAYMENT" {
            
            if let jobId = job_detail.job_id {
                userRequestPayment(job_id: jobId)
            }
            
            //            // Setup add card view controller
            //            let addCardViewController = STPAddCardViewController()
            //            addCardViewController.delegate = self
            //
            //            // Present add card view controller
            //            let navigationController = UINavigationController(rootViewController: addCardViewController)
            //            present(navigationController, animated: true)
        } else if acceptBtn.title(for: .normal) == "RATE A READER" {
            if let jobId = job_detail.job_id {
                if let isRated = job_detail.is_rated {
                    if !isRated {
                        let vc = storyBoard.instantiateViewController(withIdentifier: "RatingUserVC") as! RatingUserVC
                        vc.jobId = jobId
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        let alertview = SCLAlertView()
                        alertview.showError("Error", subTitle: "You have been already rated this user.", closeButtonTitle: "Ok")
                    }
                }
                
            }
        } else if acceptBtn.title(for: .normal) == "VIEW RATING"{
            if let jobId = job_detail.job_id {
                let vc = storyBoard.instantiateViewController(withIdentifier: "RatingUserVC") as! RatingUserVC
                vc.isViewRating = true
                vc.jobId = jobId
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if acceptBtn.title(for: .normal) == btnTitle {
            //            let addCardViewController = STPAddCardViewController()
            //            addCardViewController.delegate = self
            //            // Present add card view controller
            //            let navigationController = UINavigationController(rootViewController: addCardViewController)
            //            present(navigationController, animated: true)
            let vc = storyBoard.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
            vc.job_detail = job_detail
            self.navigationController?.pushViewController(vc, animated: true)
        } else if  acceptBtn.title(for: .normal) == "ACCEPT"{
            if let jobId = job_detail.job_id {
                jobAcceptReject(job_id: jobId, isAccept: true)
            }
        }else {
            print("Payment Requested")
            let alertview = SCLAlertView()
            alertview.showError("Error", subTitle: "You have already requested payment.", closeButtonTitle: "Ok")
        }
    }
    
    func jobAcceptReject(job_id: Int, isAccept : Bool) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        var apiString = ""
        if isAccept {
            apiString = approveJob
        } else {
            apiString = rejectJob
        }
        let user = NotificationRequestModel(job_id: job_id)
        let data_temp = Mapper<NotificationRequestModel>().toJSON(user)
        print("Accept and Reject request:- \(data_temp)")
        print(tokenHeader)
        //Call api
        alamofireManager.request(apiString, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        if let msg = value.message {
                            let alertView = SCLAlertView()
                            alertView.showSuccess("Success", subTitle: msg, closeButtonTitle: "Ok",  colorStyle: int_red)
                        }
//                        alertView.showSuccess("Success", subTitle: (response.result.value?.message)!, closeButtonTitle: "Ok",  colorStyle: int_red)
                        if isAccept {
                            self.declineBtn.isHidden = true
                            self.acceptBtn.setTitle("REQUEST PAYMENT", for: .normal)
                            self.show(animated:true)
                            self.chatBtn.isHidden = false
                        } else {
                            self.declineBtn.isHidden = true
                            self.acceptBtn.isHidden = true
                        }
                        break
                        
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.jobAcceptReject(job_id: job_id, isAccept : isAccept)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
    
    func userRequestPayment(job_id: Int) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        let user = PaymentRequestModel(job_id: job_id)
        let data_temp = Mapper<PaymentRequestModel>().toJSON(user!)
        print("Accept request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(requestPayment, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        let alertView = SCLAlertView()
                        
                        alertView.showSuccess("Success", subTitle: (response.result.value?.message)!, closeButtonTitle: "Ok",  colorStyle: int_red)
                        self.acceptBtn.setTitle("PAYMENT REQUESTED", for: .normal)
                        
                        //                            dismiss(animated: true)
                        break
                    case 202:
                        alert.hideView()
                        if let msg = value.message {
                            //                                redirectWebViewDialog(title: "Info", str: msg, view: self)
                            let alertview = UIAlertController(title: "Info", message: msg, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                let story = UIStoryboard(name: "Main", bundle: nil)
                                
                                let nextVC = story.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                                nextVC.vcName = "ViewJobDetailVC"
                                //                        nextVC.userId = rece_id
                                self.navigationController?.pushViewController(nextVC, animated: true)
                            }
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alertview.addAction(okAction)
                            alertview.addAction(cancelAction)
                            self.present(alertview, animated: true, completion: nil)
                        }
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.userRequestPayment(job_id: job_id)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
        
    }
    
    
    //    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
    //    }
    
    func getJobDetail(job_id: Int) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        let user = NotificationRequestModel(job_id: job_id)
        let data_temp = Mapper<NotificationRequestModel>().toJSON(user)
        print("Job Details request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(getJobDetailByJobId, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        if let value = value.jobResponseByJobId {
                            self.job_detail = value
                            self.setBtnUi()
                        }
                        break
                        
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.getJobDetail(job_id: job_id)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
            } .responseJSON { (response) in
                print("jobDetail :- \(response.result.value)")
        }
    }
    
    func deleteJob(job_id: Int) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        let user = NotificationRequestModel(job_id: job_id)
        let data_temp = Mapper<NotificationRequestModel>().toJSON(user)
        print("Delete JOb request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(cancelJob, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        if let msg = value.message {
                            let alertview = SCLAlertView()
                            alertview.showSuccess("Success", subTitle: msg, colorStyle: int_red)
                        }
                        self.navigationController?.popViewController(animated: true)
                        break
                        
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.getJobDetail(job_id: job_id)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
    
    func setBtnUi() {
        let userId = defaults.integer(forKey: "userId")
        
        if userId != nil {
            print(job_detail.approval_status)
            switch job_detail.approval_status {
            case 0?:
                if job_detail.receiver_user_id != userId {
                    deleteJobBtn.isHidden = false
                    declineBtn.isHidden = true
                    acceptBtn.isHidden = true
                    acceptRejectStackview.isHidden = true
                    chatBtn.isHidden = true
                }
                
                break
            case 1?:
                print(job_detail.receiver_user_id)
                print(userId)
                
                if job_detail.sender_user_id == userId {
                    
                    deleteJobBtn.isHidden = false
                    declineBtn.isHidden = true
                    acceptBtn.isHidden = true
                    acceptRejectStackview.isHidden = true
                    
                } else {
                    acceptBtn.setTitle("ACCEPT", for: .normal)
                    
                    declineBtn.isHidden = false
                    acceptBtn.isHidden = false
                    chatBtn.isHidden = true
                }
                
//                if job_detail.receiver_user_id == userId {
//                    acceptBtn.setTitle("ACCEPT", for: .normal)
//
//                    declineBtn.isHidden = false
//                    acceptBtn.isHidden = false
//                } else {
//                    deleteJobBtn.isHidden = false
//                    declineBtn.isHidden = true
//                    acceptBtn.isHidden = true
//                    acceptRejectStackview.isHidden = true
//                }
                break
            case 2?:
                print(job_detail.receiver_user_id)
                print(userId)
                if job_detail.receiver_user_id == userId {
                    declineBtn.isHidden = true
                    acceptBtn.setTitle("REQUEST PAYMENT", for: .normal)
                    acceptBtn.isHidden = false
                    chatBtn.isHidden = false
                }else {
                    chatBtn.isHidden = false
                }
                
                break
            case 3?:
                declineBtn.isHidden = true
                acceptBtn.isHidden = true
                chatBtn.isHidden = true
                break
            case 4?:
                if job_detail.receiver_user_id == userId {
                    declineBtn.isHidden = true
                    acceptBtn.setTitle("PAYMENT REQUESTED", for: .normal)
                    acceptBtn.isHidden = false
                    
                } else {
                    declineBtn.isHidden = true
                    if let totalPayment = job_detail.finalRate {
                        btnTitle = "PAY $" + totalPayment + " TO READER"
                        acceptBtn.setTitle(btnTitle, for: .normal)

                    } else if let amt = job_detail.hourly_rate {
                        let amnt = "$" + String(format: "%.2f", Float(amt))
                        btnTitle = "PAY " + amnt + " TO READER"
                        acceptBtn.setTitle(btnTitle, for: .normal)
                        
                    } else {
                        acceptBtn.setTitle("PAY READER", for: .normal)
                        
                    }
                    acceptBtn.isHidden = false
                    
                }
                chatBtn.isHidden = false
                break
                
            case 5?:
                if job_detail.receiver_user_id != userId {
                    
                    if job_detail.is_rated! {
                        declineBtn.isHidden = true
                        acceptBtn.setTitle("VIEW RATING", for: .normal)
                        acceptBtn.isHidden = false
                        
                    } else {
                        declineBtn.isHidden = true
                        acceptBtn.setTitle("RATE A READER", for: .normal)
                        acceptBtn.isHidden = false
                    }
                    
                    
                } else {
                    declineBtn.isHidden = true
                    acceptBtn.isHidden = true
                }
                chatBtn.isHidden = false
                //                else {
                //                    declineBtn.isHidden = true
                //                    acceptBtn.setTitle("VIEW RATING", for: .normal)
                //                    acceptBtn.isHidden = false
                //                }
                break
            case 6?:
                declineBtn.isHidden = true
                acceptBtn.setTitle("VIEW RATING", for: .normal)
                acceptBtn.isHidden = false
                chatBtn.isHidden = false
                break
            default:
                declineBtn.isHidden = true
//                acceptBtn.setTitle("VIEW RATING", for: .normal)
                acceptBtn.isHidden = true
                chatBtn.isHidden = false
                break
            }
        }
    }
    
    func show(animated:Bool){
        if animated {
            
            //            readerListView.alpha = 0.5
            let popOverVC = storyBoard.instantiateViewController(withIdentifier: "PopupVC") as! PopupVC
            //            self.addChildViewController(popOverVC)
            //            popOverVC.view.frame = self.memberDetailView.frame
            popOverVC.modalPresentationStyle = .overFullScreen
            popOverVC.modalTransitionStyle = .crossDissolve
            //            self.view.addSubview(displayView)
            popOverVC.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
            self.navigationController?.present(popOverVC, animated: true, completion: nil)
            
        }else{
        }
    }
    

    
    
}
// MARK:- tablview delegate methods

extension ViewJobDetailVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if readerlistArr?.count != nil {
           return (readerlistArr?.count)!+1
        }else {
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == readerlistArr?.count {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "JobTextdetailCell", for: indexPath) as! JobTextdetailCell
            
//            viewCorners.addBorderCornerTobutton(view: cell.userProfile, color: hexStringToUIColor(hex: purpleColor))
//
            viewCorners.addCornerToView(view: cell.jobDetailTV, value: 5)
            viewCorners.addShadowToView(view: cell.jobDetailTV)
            cell.jobDetailTV.text = job_detail.job_description
//
//            cell.userNameLbl.text = "Test name"
            return cell
        }else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! JobdetailCell
            
            viewCorners.addBorderCornerTobutton(view: cell.userProfile, color: hexStringToUIColor(hex: purpleColor))
            
            viewCorners.addCornerToView(view: cell.outerView, value: 5)
            viewCorners.addShadowToView(view: cell.outerView)
            
            //viewCorners.addBorderCornerTobutton(view: cell.userProfile), color: hexStringToUIColor(hex: blackColor))
            cell.userProfile.sd_setShowActivityIndicatorView(true)
            cell.userProfile.sd_setIndicatorStyle(.gray)
            if let str = readerlistArr![indexPath.row].profile_img {
                cell.userProfile.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "ic_defaultProfile"))
            }
        
            cell.userNameLbl.text = readerlistArr[indexPath.row].first_name
            
            if let is_read = readerlistArr[indexPath.row].is_read {
                if is_read == 1 {
                    cell.readImageview.image = UIImage(named:"ic_eyeSeen")
                }else {
                    cell.readImageview.image = UIImage(named:"ic_eye")
                }
            }
            cell.chatBtn.tag = indexPath.row
            cell.chatBtn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "ViewProfileVC") as! ViewProfileVC
        vc.chat_id = readerlistArr![indexPath.row].chat_id 
        let navigationController = UINavigationController(rootViewController: vc)
        present(navigationController, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == readerlistArr?.count {
            let hight = screenHeight * 0.5
            return hight
        }else {
            return 100
        }
        
    }
    @objc func buttonAction(sender: UIButton!) {
        print("click")
        
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        let navigationArray = self.navigationController?.viewControllers
        let newStack = [] as NSMutableArray
        
        //change stack by replacing view controllers after ChatVC with ChatVC
        //        for vc in navigationArray! {
        //            // ChatViewController
        //            newStack.add(vc)
        //            if vc is DialogsViewController {
        //                let chatVC = self.storyboard!.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        //                chatVC.chat_id =  self.job_detail.chat_id
        //                chatVC.dataSource = appDelegate.dataSource
        //                chatVC.session = appDelegate.session
        //                //                chatVC.callUUID = self.callUUID
        //                //                chatVC.opponentIDs = self.opponentIDs
        //                newStack.add(chatVC)
        //                self.navigationController!.setViewControllers(newStack.copy() as! [UIViewController], animated: true)
        //                return;
        //            }
        //        }
        if (Reachability()?.isReachable)! {
            alert.hideView()
            
            let vc = storyBoard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            print(self.job_detail.chat_id)
            print(appDelegate.dataSource)
            print(appDelegate.session)
            vc.chat_id =  self.readerlistArr[sender.tag].chat_id //self.job_detail.chat_id
            vc.dataSource = appDelegate.dataSource
            vc.session = appDelegate.session
            //        vc.callUUID = self.callUUID
            //        vc.opponentIDs = self.opponentIDs
            let navigationController = UINavigationController(rootViewController: vc)
            present(navigationController, animated: true)
        } else {
            alert.hideView()
            //            let alertview = SCLAlertView()
            //            alertview.showError("Error", subTitle: noInternetMessage, closeButtonTitle: "Ok")
            noInternetMsgDialog()
        }
        
        //        self.navigationController?.pushViewController(vc, animated: true)
        //        self.performSegue(withIdentifier: "SA_STR_SEGUE_GO_TO_CHAT", sender: nil)
        
    }
}
//extension ViewJobDetailVC : STPAddCardViewControllerDelegate {
//    // MARK: STPAddCardViewControllerDelegate
//
//    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
//        // Dismiss add card view controller
//        dismiss(animated: true)
//    }
//
//
//    func paymentCardTextFieldDidEndEditing(_ textField: STPPaymentCardTextField) {
//        print("textfield:- \(textField)")
//    }
//    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
//
//        print("card_number:- \(card_number)")
//        print("didCreateToken:- \(token)")
//        if let jobId = job_detail.job_id {
//            let user = PaymentRequestModel(job_id: jobId, source: "\(token)")
//            let data_temp = Mapper<PaymentRequestModel>().toJSON(user!)
//            print("signUp request:- \(data_temp)")
//
//            //Call api
//            alamofireManager.request(releasePayment, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
//
//                //                    let responseClass = response.result.value
//                switch response.result
//                {
//                case .success(let value):
//                    if let code = value.code {
//                        print("Code:- \(code)")
//                        switch code {
//                        case 200:
//                            alert.hideView()
//                            let alertView = SCLAlertView()
//
//                            alertView.showSuccess("Success", subTitle: (response.result.value?.message)!, closeButtonTitle: "Ok",  colorStyle: int_red)
//                            //                            dismiss(animated: true)
//                            addCardViewController.dismiss(animated: true, completion: nil)
//                            break
//                        case 202:
//                            alert.hideView()
////                            addCardViewController.dismiss(animated: true, completion: nil)
//                            if let msg = value.message {
//                                redirectWebViewDialog(title: "Info", str: msg, view: self)
//                            }
//                            break
//                        case 400:
//                            alert.hideView()
//
//                            clearSession()
//                            let story = UIStoryboard(name: "Main", bundle: nil)
//
//                            let nextVC = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//                            self.navigationController?.pushViewController(nextVC, animated: true)
//                            break
//
//                        default:
//                            alert.hideView()
//                            let appearance1 = SCLAlertView.SCLAppearance(
//                                showCloseButton: true
//                            )
//                            if let error = value.message {
//                                let alertview = SCLAlertView(appearance: appearance1)
//                                alert.addButton("OK", action: { // create button on alert
//                                    // action on click
//                                    addCardViewController.dismiss(animated: true, completion: nil)
//                                })
//                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
//                            }
//
//
//
//                            break
//                        }
//                    }
//                    break
//                case .failure(let error):
//                    print("error: \(error.localizedDescription)")
//                    let appearance1 = SCLAlertView.SCLAppearance(
//                        showCloseButton: false
//                    )
//                    alert.hideView()
//                    let alertview = SCLAlertView(appearance: appearance1)
//                    alert.addButton("OK", action: { // create button on alert
//                        // action on click
//                        addCardViewController.dismiss(animated: true, completion: nil)
//                    })
//                    alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
//                    break
//                }
//                }.responseJSON { response in
//                    print("release payment response:- \(response.result.value)")
//
//            }
//
//        }
//        //        submitTokenToBackend(token, completion: { (error: Error?) in
//        //            if let error = error {
//        //                // Show error in add card view controller
//        //                completion(error)
//        //            }
//        //            else {
//        //                // Notify add card view controller that token creation was handled successfully
//        //                completion(nil)
//        //
//        //                // Dismiss add card view controller
//        //                dismiss(animated: true)
//        //            }
//        //        })
//    }
//}

class PopupVC : UIViewController {
    
    @IBOutlet var hintView: UIView!
    @IBOutlet var hintLbl: UILabel!
    @IBOutlet var declineBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewCorners.addShadowToView(view: hintView)
        viewCorners.addCornerToView(view: hintView, value: 5)
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = UIImage(named: "ic_chat")
        //Set bound to reposition
        let imageOffsetY:CGFloat = -8.0;
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        let completeText = NSMutableAttributedString(string: "Start scheduling by clicking on the ")
        //Add image to mutable string
        completeText.append(attachmentString)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: " at the top right corner of the screen.")
        completeText.append(textAfterIcon)
        self.hintLbl.textAlignment = .center;
        self.hintLbl.attributedText = completeText;
    }
    
    @IBAction func declineBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

class PopupjobDetailsVC : UIViewController {
    
    @IBOutlet var hintView: UIView!
    @IBOutlet var hintLbl: UILabel!
    @IBOutlet var declineBtn: UIButton!
    
    override func viewDidLoad() {
        let offsetY = -7
        
        super.viewDidLoad()
        viewCorners.addShadowToView(view: hintView)
        viewCorners.addCornerToView(view: hintView, value: 5)
        
        let completeText = NSMutableAttributedString(string: "")
        
//        let imageAttachment = NSTextAttachment()
//        imageAttachment.image = UIImage(named: "ic_chat")
//        //Set bound to reposition
//        let imageOffsetY:CGFloat = -8.0;
//        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
//        //Create string with attachment
//        let attachmentString = NSAttributedString(attachment: imageAttachment)
//        //Initialize mutable string
//        
//        //Add image to mutable string
//        completeText.append(attachmentString)
//        //Add your text to mutable string
//        let  textAfterIcon = NSMutableAttributedString(string: " Chat with the reader\n")
//        completeText.append(textAfterIcon)
        
        
        let image1Attachment1 = NSTextAttachment()
        image1Attachment1.image = UIImage(named: "ic_eyeSamall")
        image1Attachment1.bounds = CGRect(x: 0, y: offsetY, width: Int((image1Attachment1.image?.size.width)!), height: Int((image1Attachment1.image?.size.height)!)) //CGRect(x: 0.0, y: offsetY, width: image1Attachment1.image?.size.width, height: image1Attachment1.image?.size.height)
       
        let image1String1 = NSAttributedString(attachment: image1Attachment1)
        
        completeText.append(image1String1)
        completeText.append(NSAttributedString(string: " Job request has been seen."))

        
        self.hintLbl.textAlignment = .center;
        self.hintLbl.attributedText = completeText;
    }
    
    @IBAction func declineBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

