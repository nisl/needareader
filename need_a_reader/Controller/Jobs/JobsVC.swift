//
//  JobsVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 1/29/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SCLAlertView
import AlamofireObjectMapper
import ObjectMapper
import ReachabilitySwift
import SDWebImage
import SwipeCellKit

class JobsVC : UIViewController {
    
    @IBOutlet var noDataView: UIView!
    @IBOutlet var tableview: UITableView!
    var jobArr : [Job_detail] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigationBar()
        tableview.delegate = self
        tableview.dataSource = self
        //        self.navigationItem.setHidesBackButton(false, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getAllJobsForUser()
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    
    
    //Set NavigationBar
    func configureNavigationBar() {
        setNavigaion(vc: self)
        
        self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        self.navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        let navigationItem = self.navigationItem
        navigationItem.title = "JOBS"

        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        let settingBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        
        if #available(iOS 11.0, *) {
            let settingBtnwidthConstraint = settingBtn.widthAnchor.constraint(equalToConstant: 44)
            let settingBtnheightConstraint = settingBtn.heightAnchor.constraint(equalToConstant: 44)
            settingBtnheightConstraint.isActive = true
            settingBtnwidthConstraint.isActive = true
            settingBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 8, 16)//UIEdgeInsetsMake(8, 0, 8, 16)
        }else {
            settingBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 8, 16)//UIEdgeInsetsMake(8, 0, 6, 12)
            //            backBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 8, 10)
        }
        
        settingBtn.setImage(UIImage(named: "ic_setting"), for: .normal)
        settingBtn.addTarget(self, action: #selector(self.settingBtnPressed(_:)), for: .touchUpInside)
        let settingButton = UIBarButtonItem(customView: settingBtn)
        navigationItem.leftBarButtonItem = settingButton
    }
    
    @objc func settingBtnPressed(_ sender: UIButton) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func loadFromJobsCache() -> Bool {
        if let v = UserDefaults.standard.dictionary(forKey: getAllNotificationForSync) {
            
            let value = Mapper<JobResponseModel>().map(JSON: v)
            print("NotificationData:- \(v)")
            if let arr = value?.job_list {
                jobArr = arr
            }
            print(jobArr.count)
        }
        
        if !jobArr.isEmpty {
            tableview.reloadData()
            noDataView.isHidden = true
            return true
        } else {
            noDataView.isHidden = false
            return false
        }
        
    }
    
    func getAllJobsForUser() {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        print("TokenHeader:- \(tokenHeader)")
        alamofireManager.request(getAllJobs, method: .post, parameters: [:], encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        //UserDefaults.standard.set(value.jobResponse?.toJSON(), forKey: getAllNotificationForSync)
                        alert.hideView()
                        print("Success")
                        print(value.jobResponse)
                        self.jobArr = (value.jobResponse?.job_list)!
                        print(self.jobArr.toJSON())
                        if !self.jobArr.isEmpty {
                            self.noDataView.isHidden = true
                            self.tableview.isHidden = false
                            self.tableview.reloadData()

                        } else {
                            self.noDataView.isHidden = false
                            self.tableview.isHidden = true
                            
                        }
                        //self.loadFromJobsCache()
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.getAllJobsForUser()
                        }else {
                            //if !self.loadFromJobsCache() {
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                                
                            //}
                        }
                        break
                    
                    default:
                        alert.hideView()
                        //if !self.loadFromJobsCache() {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                       // }
                        
                        break
                    }
                }
                break
            case .failure(let error):
                alert.hideView()
                
               // if !self.loadFromJobsCache() {
                    SCLAlertView().showError(errorTitle, subTitle: serverError)
                    //                    self.addErrorView()
                //}
                
                print("Request failed with error: \(error.localizedDescription)")
                break
                
                
            }
            
            }.responseJSON { (response) in
                print(getAllJobs+" response: \(response.result.value)")
        }
        
    }
    func deleteJobsForUser(jobID:Int?,itemIndex:Int?) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        print("TokenHeader:- \(tokenHeader)")
        let user = NotificationRequestModel(job_id: jobID)
        let data_temp = Mapper<NotificationRequestModel>().toJSON(user)
        print("parm:-\(data_temp)")
        
        alamofireManager.request(deleteJob, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        
                        if let msg = value.message {
                            self.jobArr.remove(at: itemIndex!)
                            SCLAlertView().showSuccess("Success", subTitle: msg, colorStyle: int_red)
                        }
                        self.tableview.reloadData()
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.deleteJobsForUser(jobID: jobID!, itemIndex: itemIndex)
                        }else {
                            if !self.loadFromJobsCache() {
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                            }
                        }
                        break
                    case 201:
                        alert.hideView()
                        if let msg = value.message {
                            let alertview = SCLAlertView()
                            
                            alertview.showError("Error", subTitle: msg, closeButtonTitle: "Ok")
                        }
                        
                        break
                        
                    default:
                        alert.hideView()
                        if !self.loadFromJobsCache() {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        
                        break
                    }
                }
                break
            case .failure(let error):
                alert.hideView()
                
                if !self.loadFromJobsCache() {
                    SCLAlertView().showError(errorTitle, subTitle: serverError)
                    //                    self.addErrorView()
                }
                
                print("Request failed with error: \(error.localizedDescription)")
                break
                
                
            }
            
            }.responseJSON { (response) in
                print(deleteJob+" response: \(response.result.value)")
        }
        
    }
    func getFormattedDate(date: Date) -> String{
        let dateFormattor = DateFormatter()
        dateFormattor.dateFormat = "MMM d, yyyy"
        let str = dateFormattor.string(from: date)
        return str
    }
}

extension JobsVC : UITableViewDataSource, UITableViewDelegate, SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! JobViewCell
//        viewCorners.addCornerToView(view: cell.userProfile)
        cell.delegate = self
        viewCorners.addBorderCornerTobutton(view: cell.userProfile, color: hexStringToUIColor(hex: purpleColor))
        viewCorners.addCornerToView(view: cell.countView) //addBorderCornerTobutton(view: cell.countView)
        setGradient(customView:  cell.countView)
        cell.countView.bringSubview(toFront: cell.countLBL)
        viewCorners.addCornerToView(view: cell.outerView, value: 5)
        viewCorners.addShadowToView(view: cell.outerView)
        
        print(jobArr[indexPath.row].reader_list?.count)
        if jobArr[indexPath.row].reader_list != nil {
            if (jobArr[indexPath.row].reader_list?.count)! > 1 {
                cell.countView.isHidden = false
                cell.countLBL.text = "+\((jobArr[indexPath.row].reader_list?.count)!-1)"
            }else {
                cell.countView.isHidden = true
            }
        }else {
          cell.countView.isHidden = true
        }
        //        viewCorners.addBorderCornerTobutton(view: cell.userProfile), color: hexStringToUIColor(hex: blackColor))
        cell.userProfile.sd_setShowActivityIndicatorView(true)
        cell.userProfile.sd_setIndicatorStyle(.gray)
        if let str = jobArr[indexPath.row].profile_img {
            cell.userProfile.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "ic_defaultProfile"))
        }
        
        if let description = jobArr[indexPath.row].job_description {
            cell.jobDescriptionLbl.text = description
        }
        if let name = jobArr[indexPath.row].first_name {
            cell.userNameLbl.text = name
        }
        
        if let dateTime = jobArr[indexPath.row].create_time {
            let date = convertstringToDate(str: dateTime)
            cell.dateLbl.text = getFormattedDate(date: date)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "ViewJobDetailVC") as! ViewJobDetailVC
        nextVC.isFromNotification = false
        print(jobArr[indexPath.row].job_id)
        nextVC.job_detail = jobArr[indexPath.row]
        self.navigationController?.pushViewController(nextVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            print("delete")
            if let id = jobArr[indexPath.row].job_id {
                deleteJobsForUser(jobID: id, itemIndex: indexPath.row)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
            //TODO: Delete the row at indexPath here
            //"ic_delete_not"
            let alertview = UIAlertController(title: "Alert", message: "Are you sure you want to delete this job?", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Yes", style: .default) { (action) in
    
                if let id = self.jobArr[indexPath.row].job_id {
                    self.deleteJobsForUser(jobID: id, itemIndex: indexPath.row)
                }
                
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            alertview.addAction(okAction)
            alertview.addAction(cancelAction)
            
            self.present(alertview, animated: true, completion: nil)
            
        }
        
        deleteAction.title = ""
        deleteAction.backgroundColor = hexStringToUIColor(hex: redColor)
        // customize the action appearance
        deleteAction.image = UIImage(named: "ic_delete_not")
        return [deleteAction]
    }
}
