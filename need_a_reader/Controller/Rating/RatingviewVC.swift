//
//  RatingviewVC.swift
//  need_a_reader
//
//  Created by ob_apple_3 on 17/09/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import UIKit
import SwiftyStarRatingView
import Alamofire
import SCLAlertView
import AlamofireObjectMapper
import ObjectMapper
import ReachabilitySwift
import SDWebImage


class RatingviewVC: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var avrageRatingview: UIView!
    @IBOutlet weak var QuestionView1: UIView!
    @IBOutlet weak var QuestionView2: UIView!
    @IBOutlet weak var QuestionView3: UIView!
    @IBOutlet weak var QuestionView4: UIView!
    @IBOutlet weak var tableviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var jobRatingHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var jobRatingLbl: UILabel!
    
    
   
    @IBOutlet var starRatingView: SwiftyStarRatingView!
    @IBOutlet var headerGetScoreLbl: UILabel!
    @IBOutlet var headerOutOfScoreLbl: UILabel!
    @IBOutlet var ratingHeaderLbl: UILabel!
    @IBOutlet var avgRatingView: UIView!
    
    @IBOutlet var readerOnTotalVotes: UILabel!
    @IBOutlet var readerOnTImeDislikePer: UILabel!
    @IBOutlet var readerOnTImeLikePer: UILabel!
    @IBOutlet var readerontimeQuestionLbl: UILabel!
    @IBOutlet var readerOntimeView: UIView!
    
    @IBOutlet var TotalVotes2: UILabel!
    @IBOutlet var dislikePer2: UILabel!
    @IBOutlet var likePer2: UILabel!
    @IBOutlet var questionLbl2: UILabel!
    
    
    @IBOutlet var TotalVotes3: UILabel!
    @IBOutlet var dislikePer3: UILabel!
    @IBOutlet var likePer3: UILabel!
    @IBOutlet var questionLbl3: UILabel!
    
    
    @IBOutlet var TotalVotes4: UILabel!
    @IBOutlet var dislikePer4: UILabel!
    @IBOutlet var likePer4: UILabel!
    @IBOutlet var questionLbl4: UILabel!
    
    var overAllRating :RatingOverallResponse?
    var jobRaitngArr = [RatingJobWiseResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configureNavigationBar()
        let viewArr : [UIView] = [avrageRatingview,QuestionView1,QuestionView2,QuestionView3, QuestionView4]
        setUI(viewArr: viewArr)
        self.tableviewHeightConstraint.constant = 300
        starRatingView.isUserInteractionEnabled = false
        
        self.getRatingForUser()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //Set NavigationBar
    func configureNavigationBar()
    {
        setNavigaion(vc: self)
        let navigationItem = self.navigationItem
        navigationItem.title = "VIEW RATING"
        
        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        
        
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setUI(viewArr: [UIView]) {
        for i in 0..<viewArr.count {
            //            viewCorners.addShadowToView(view: viewArr[i])
            viewCorners.addCornerToView(view: viewArr[i], value: 5)
            viewCorners.addShadowToView(view: viewArr[i], value: 2.0)
        }
    }
    func setOverAllRatingUI()  {
        
        if let avgRate =  self.overAllRating?.average_rating {
            self.headerGetScoreLbl.text = "\(avgRate)"
            self.starRatingView.value = CGFloat(Float(avgRate)!)
            
        }
        if let ratingTotal = self.overAllRating?.ratings {
            self.ratingHeaderLbl.text = "\(ratingTotal) ratings"
        }
       
        if let rating = self.overAllRating?.ratings {
           self.readerOnTotalVotes.text = "\(rating)"
        }
        if let isOneTimeYes = self.overAllRating?.is_on_time_yes {
            self.readerOnTImeLikePer.text = "\(isOneTimeYes)%"
        }
        if let isOneTimeNo = self.overAllRating?.is_on_time_no {
            self.readerOnTImeDislikePer.text = "\(isOneTimeNo)%"
        }
        
        if let isKnowledgeableYes = self.overAllRating?.is_knowledgeable_yes {
            self.dislikePer2.text = "\(isKnowledgeableYes)%"
        }
        if let isKnowledgeableNo = self.overAllRating?.is_knowledgeable_no {
            self.likePer2.text = "\(isKnowledgeableNo)%"
        }
        if let isKnowledgeablecontent  = self.overAllRating?.is_knowledgeable_on_content {
            self.TotalVotes2.text = "\(isKnowledgeablecontent)"
        }
        
        if let is_work_with_no = self.overAllRating?.work_with_no {
            self.dislikePer3.text = "\(is_work_with_no)%"
        }
        if let is_work_with_yes = self.overAllRating?.work_with_yes {
            print(is_work_with_yes)
            self.likePer3.text = "\(is_work_with_yes)%"
        }
        if let iswork_with_reader_again  = self.overAllRating?.work_with_reader_again {
            self.TotalVotes3.text = "\(iswork_with_reader_again)"
        }
        print(self.overAllRating?.job_satisfaction_no)
        print(self.overAllRating?.job_satisfaction_yes)
        if let is_job_satisfaction_no = self.overAllRating?.job_satisfaction_no {
            print(is_job_satisfaction_no)
            self.likePer4.text = "\(is_job_satisfaction_no)%"
        }
        if let is_job_satisfaction_yes = self.overAllRating?.job_satisfaction_yes {
            print(is_job_satisfaction_yes)
            self.dislikePer4.text = "\(is_job_satisfaction_yes)%"
        }
        if let isjobSatisFiedPer  = self.overAllRating?.jobSatisFiedPer {
            self.TotalVotes4.text = "\(isjobSatisFiedPer)"
        }

        
    }
    // MARK:- Server connectivity
    func getRatingForUser() {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        print("TokenHeader:- \(tokenHeader)")
        alamofireManager.request(ratingForUser, method: .post, parameters: [:], encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:

                        alert.hideView()
                        if let jobwiserating = value.viewForuserReponse?.job_wise_rates {
                            self.jobRaitngArr = jobwiserating
                        }
                        if let overallrating = value.viewForuserReponse?.overall_rating {
                            self.overAllRating = overallrating
                        }
                        print(self.jobRaitngArr.count)
                        if (self.jobRaitngArr.count) == 0 {
                            self.tableview.isHidden = true
                            self.tableviewHeightConstraint.constant = 0
                            self.jobRatingHeightConstraint.constant = 0
                            self.jobRatingLbl.isHidden = true
                        }else {
                            self.tableview.isHidden = false
                            self.jobRatingLbl.isHidden = false
                            self.jobRatingHeightConstraint.constant = 25
                            let tablviewHeight = CGFloat((self.jobRaitngArr.count) * 100)
                            self.tableviewHeightConstraint.constant = tablviewHeight
                            self.tableview.reloadData()

                        }
                    
                        self.setOverAllRatingUI()
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.getRatingForUser()
                        }else {
                            if let error = value.message {
                                SCLAlertView().showError("Error", subTitle: error)
                            }
                        }
                        break
                        
                    default:
                        alert.hideView()
                        if let error = value.message {
                            SCLAlertView().showError("Error", subTitle: error)
                        }
                        
                        break
                    }
                }
                break
            case .failure(let error):
                alert.hideView()
                break
                
                
            }
            
            }.responseJSON { (response) in
                print(ratingForUser+" response: \(response.result.value)")
        }
        
    }
}
extension RatingviewVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobRaitngArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RatingCell
        //        viewCorners.addCornerToView(view: cell.userProfile)
        
        viewCorners.addBorderCornerTobutton(view: cell.userProfile, color: hexStringToUIColor(hex: purpleColor))
        viewCorners.addCornerToView(view: cell.outerView, value: 5)
        viewCorners.addCornerToView(view: cell.viewBtn, value: 15)
        cell.viewBtn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        cell.viewBtn.tag = indexPath.row

        viewCorners.addShadowToView(view: cell.outerView)
        
        
        cell.userProfile.sd_setShowActivityIndicatorView(true)
        cell.userProfile.sd_setIndicatorStyle(.gray)
        if let str = jobRaitngArr[indexPath.row].sender_headshot_thumbnail_image {
            cell.userProfile.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "ic_defaultProfile"))
        }

        if let description = jobRaitngArr[indexPath.row].job_description {
            cell.jobDescriptionLbl.text = description
        }
        if let name = jobRaitngArr[indexPath.row].sender_first_name {
            cell.userNameLbl.text = name
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let nextVC = storyboard?.instantiateViewController(withIdentifier: "ViewJobDetailVC") as! ViewJobDetailVC
//        nextVC.isFromNotification = false
//        print(jobArr[indexPath.row].job_id)
//        nextVC.job_detail = jobArr[indexPath.row]
//        self.navigationController?.pushViewController(nextVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
  
    @objc func buttonAction(sender: UIButton!) {
        print("click")
        let vc = storyBoard.instantiateViewController(withIdentifier: "RatingUserVC") as! RatingUserVC
        vc.jobId = jobRaitngArr[sender.tag].job_id
        vc.isViewRating = true
        vc.jobRaitng = jobRaitngArr[sender.tag]
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
