//
//  RatingUserVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/26/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import AlamofireObjectMapper
import Alamofire
import SCLAlertView
import ReachabilitySwift
import ObjectMapper
import SwiftyStarRatingView
import LabelSwitch

class RatingUserVC: UIViewController, SwiftySwitchDelegate {
    
    @IBOutlet var nextBtn: UIButton!
//    @IBOutlet var workBtn: UISwitch!
//    @IBOutlet var readerBtn: UISwitch!
//    @IBOutlet var readerOnTimeBtn: UISwitch!
    @IBOutlet var workBtn: SwiftySwitch!
    @IBOutlet var readerBtn: SwiftySwitch!
    @IBOutlet var jobSatisfySwitch: SwiftySwitch!
    @IBOutlet var readerOnTimeBtn: SwiftySwitch!

    @IBOutlet var readerOnTimeLbl: UILabel!
    @IBOutlet var workLbl: UILabel!
    @IBOutlet var readerLbl: UILabel!
    @IBOutlet var jobSatisfyLbl: UILabel!

//    @IBOutlet var jobSatisfySwitch: UISwitch!
    @IBOutlet var ratingView: UIView!
    @IBOutlet var questionsView: UIView!
    @IBOutlet var swiftystarRatingview: SwiftyStarRatingView!
    var jobId : Int!
    var is_on_time : Int = 0
    var is_knowledgeable_on_content : Int = 0
    var work_with_reader_again : Int = 0
    var isJobSatisfied : Int = 0
    var isViewRating : Bool = false
    var noDataView : NoDataView!
    var errorView : NoDataView!
    
    var jobRaitng : RatingJobWiseResponse? //]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        workBtn.delegate = self
        readerBtn.delegate = self
        readerOnTimeBtn.delegate = self
        jobSatisfySwitch.delegate = self
        
        print(jobRaitng)
//        if (jobRaitng != nil) {
//            fetchUserRatingData(job_id: jobId)
//            print(jobRaitng?.is_on_time)
//            if jobRaitng?.is_on_time == 1 {
//                readerOnTimeBtn.isOn = true
//            }else {
//                readerOnTimeBtn.isOn = false
//            }
//            print(jobRaitng?.job_satisfaction)
//            if jobRaitng?.job_satisfaction == 1 {
//                jobSatisfySwitch.isOn = true
//            }else {
//                jobSatisfySwitch.isOn = false
//            }
//            if jobRaitng?.work_with_reader_again == 1 {
//                workBtn.isOn = true
//            }else {
//                workBtn.isOn = false
//            }
//            
//            if jobRaitng?.is_knowledgeable_on_content == 1 {
//                readerBtn.isOn = true
//            }else {
//                readerBtn.isOn = false
//            }
//            if let avgRate = jobRaitng?.over_all_rating {
//                self.swiftystarRatingview.value = CGFloat(Float(avgRate))
//            }
//            workBtn.isTouchedAllow = false
//            readerBtn.isTouchedAllow = false
//            jobSatisfySwitch.isTouchedAllow = false
//            readerOnTimeBtn.isTouchedAllow = false
//            swiftystarRatingview.isUserInteractionEnabled = false
//            nextBtn.isHidden = true

//        }else {
//            workBtn.isUserInteractionEnabled = true
//            readerBtn.isUserInteractionEnabled = true
//            jobSatisfySwitch.isUserInteractionEnabled = true
//            readerOnTimeBtn.isUserInteractionEnabled = true
//
//            workBtn.isTouchedAllow = true
//            readerBtn.isTouchedAllow = true
//            jobSatisfySwitch.isTouchedAllow = true
//            readerOnTimeBtn.isTouchedAllow = true
//
//            swiftystarRatingview.isUserInteractionEnabled = true
//            nextBtn.isHidden = false
            
//        }
        
        
        isSettingChanged = false
        self.navigationController?.navigationBar.isHidden = false
        
        
//
//        workBtn.setOn(false, animated: false)
//        readerBtn.setOn(false, animated: false)
//        readerOnTimeBtn.setOn(false, animated: false)
//        jobSatisfySwitch.setOn(false, animated: false)

        self.view.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
        configureNavigationBar()
        viewCorners.addCornerToView(view: nextBtn)
        viewCorners.addCornerToView(view: questionsView, value: 5.0)
        viewCorners.addCornerToView(view: ratingView, value: 5.0)
        viewCorners.addShadowToView(view: questionsView)
        viewCorners.addShadowToView(view: ratingView)
        swiftystarRatingview.accurateHalfStars = false
        swiftystarRatingview.allowsHalfStars = false
        swiftystarRatingview.continuous = true
        swiftystarRatingview.tintColor = UIColor.red
        if isViewRating {
            
            //            addNoDataView()
            nextBtn.isHidden = true
            if jobId != nil {
                fetchUserRatingData(job_id: jobId)
            }
            workBtn.isUserInteractionEnabled = false
            readerBtn.isUserInteractionEnabled = false
            jobSatisfySwitch.isUserInteractionEnabled = false
            readerOnTimeBtn.isUserInteractionEnabled = false

            swiftystarRatingview.isUserInteractionEnabled = false
            
        }
    }
    
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        isSettingChanged = true
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        if (Reachability()?.isReachable)! {
            if jobId != nil {
                callAddRatingApi(receiver_user_id: jobId, is_on_time: is_on_time, is_knowledgeable_on_content: is_knowledgeable_on_content, work_with_reader_again: work_with_reader_again, over_all_rating: Float(swiftystarRatingview.value), isJobSatisfaction: isJobSatisfied)
            }
        }else {
            alert.hideView()
            noInternetMsgDialog()
        }
    }
    
//    @IBAction func tintAnsOnOffClicked(_ sender: Any) {
    func valueChanged(sender: SwiftySwitch) {
        let switchBtn = sender as! SwiftySwitch

//        let switchBtn = sender as! UISwitch
        
        switch switchBtn {
        case readerOnTimeBtn:
            if readerOnTimeBtn.isOn {
                setBtnTextColor(currentLbl: readerOnTimeLbl, alignment: .left, color: UIColor.black, text : "Yes")
                is_on_time = 1
            }else {
                setBtnTextColor(currentLbl: readerOnTimeLbl, alignment: .right, color: UIColor.black, text : "No")
                is_on_time = 0
            }
            break
        case readerBtn:
            if readerBtn.isOn {
                setBtnTextColor(currentLbl: readerLbl, alignment: .left, color: UIColor.black, text : "Yes")
                is_knowledgeable_on_content = 1
            }else {
                setBtnTextColor(currentLbl: readerLbl, alignment: .right, color: UIColor.black, text : "No")
                is_knowledgeable_on_content = 0
            }
            break
        case workBtn:
            if workBtn.isOn {
                setBtnTextColor(currentLbl: workLbl, alignment: .left, color: UIColor.black, text : "Yes")

                work_with_reader_again = 1
            }else {
                setBtnTextColor(currentLbl: workLbl, alignment: .right, color: UIColor.black, text : "No")

                work_with_reader_again = 0
            }
            break
        default:
            if jobSatisfySwitch.isOn {
                setBtnTextColor(currentLbl: jobSatisfyLbl, alignment: .left, color: UIColor.black, text : "Yes")

                isJobSatisfied = 1
            }else {
                setBtnTextColor(currentLbl: jobSatisfyLbl, alignment: .right, color: UIColor.black, text : "No")

                isJobSatisfied = 0
            }
            break
        }
        
        
    }
    
    func callAddRatingApi(receiver_user_id: Int, is_on_time: Int, is_knowledgeable_on_content: Int, work_with_reader_again: Int, over_all_rating: Float, isJobSatisfaction: Int) {
        
        let user = RatingRequestModel(receiver_user_id: receiver_user_id, is_on_time: is_on_time, is_knowledgeable_on_content: is_knowledgeable_on_content, work_with_reader_again: work_with_reader_again, over_all_rating: over_all_rating, job_satisfaction: isJobSatisfaction)
        let data_temp = Mapper<RatingRequestModel>().toJSON(user)
        print("Rating request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(addRatingForUser, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        let alertView = SCLAlertView()
                        alertView.showSuccess("Success", subTitle: (response.result.value?.message)!, closeButtonTitle: "Ok",  colorStyle: int_red)
                        self.navigationController?.popToRootViewController(animated: true)
                        
                        
                        
                        //                        if let window = UIApplication.shared.delegate?.window {
                        //                            if let myTabController = window?.rootViewController as? MyTabBarViewController{
                        //
                        //
                        //                                if let index = self.tabBarController?.selectedIndex {
                        //                                    if index == 1 {
                        //                                        let vc = storyBoard.instantiateViewController(withIdentifier: "JobsVC") as! JobsVC
                        //
                        //                                        let nav = UINavigationController(rootViewController: vc)
                        //                                        nav.navigationBar.isTranslucent = false
                        //                                        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                        //                                        nav.navigationBar.shadowImage = UIImage()
                        //                                        nav.interactivePopGestureRecognizer?.isEnabled = false
                        //                                        self.navigationController?.present(nav, animated: true, completion: nil)
                        //                                    } else if index == 2  {
                        //                                        let vc = storyBoard.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
                        //
                        //                                        let nav = UINavigationController(rootViewController: vc)
                        //                                        nav.navigationBar.isTranslucent = false
                        //                                        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                        //                                        nav.navigationBar.shadowImage = UIImage()
                        //                                        nav.interactivePopGestureRecognizer?.isEnabled = false
                        //                                        self.navigationController?.present(nav, animated: true, completion: nil)
                        //                                    } else {
                        //                                        self.navigationController?.popViewController(animated: true)
                        //                                    }
                        //                                }
                        //                            }
                        //                        }
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.callAddRatingApi(receiver_user_id: receiver_user_id, is_on_time: is_on_time, is_knowledgeable_on_content: is_knowledgeable_on_content, work_with_reader_again: work_with_reader_again, over_all_rating: over_all_rating, isJobSatisfaction: isJobSatisfaction)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
    
    //Set NavigationBar
    func configureNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "OpenSans-Semibold", size: 18.0)]
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        let navigationItem = self.navigationItem
        if (jobRaitng != nil) {
            navigationItem.title = "VIEW A READER"
        }else {
         navigationItem.title = "RATE A READER"
        }
        
        
        self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        self.navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    
    func fetchUserRatingData(job_id: Int) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        let user = NotificationRequestModel(job_id: job_id)
        let data_temp = Mapper<NotificationRequestModel>().toJSON(user)
        print("Fatch Rating request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(getRatingByJobId, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        if let ratingData = value.viewRatingResponse {
                            self.setRateReaderData(ratingData: ratingData)
                        }
                        break
                        
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.fetchUserRatingData(job_id: job_id)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                //                let alertview = SCLAlertView()
                //                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                self.addErrorView()
                break
            }
            } .responseJSON { (response) in
                print("fetchUserData response :- \(response.result.value)")
        }
    }
    
    func setRateReaderData(ratingData: ViewRatingResponseModel) {
        if let ttlRating = ratingData.over_all_rating {
            self.swiftystarRatingview.value = CGFloat(ttlRating)
        }
        
        if let workWithReader = ratingData.work_with_reader_again {
            //                                self.workBtn.setOn(workWithReader, animated: true)
            self.workBtn.isOn = workWithReader

            if workWithReader {
                setBtnTextColor(currentLbl: self.workLbl, alignment: .left, color: UIColor.black, text : "Yes")
            } else {
                setBtnTextColor(currentLbl: self.workLbl, alignment: .right, color: UIColor.black, text : "No")
            }
            self.workBtn.isTouchedAllow = false

        }
        
        if let isKnowledeable = ratingData.is_knowledgeable_on_content {
            //                                self.readerBtn.setOn(isKnowledeable, animated: true)
            self.readerBtn.isOn = isKnowledeable

            if isKnowledeable {
                setBtnTextColor(currentLbl: self.readerLbl, alignment: .left, color: UIColor.black, text : "Yes")
                
            } else {
                setBtnTextColor(currentLbl: self.readerLbl, alignment: .right, color: UIColor.black, text : "No")
            }
            self.readerBtn.isTouchedAllow = false

        }
        
        if let isOnTime = ratingData.is_on_time {
            //                                self.readerOnTimeBtn.setOn(isOnTime, animated: true)
            self.readerOnTimeBtn.isOn = isOnTime

            if isOnTime {
                setBtnTextColor(currentLbl: self.readerOnTimeLbl, alignment: .left, color: UIColor.black, text : "Yes")
                
            } else {
                setBtnTextColor(currentLbl: self.readerOnTimeLbl, alignment: .right, color: UIColor.black, text : "No")
            }
            self.readerOnTimeBtn.isTouchedAllow = false

        }
        
        if let isJobSatisFied = ratingData.job_satisfaction {
            //                                self.readerOnTimeBtn.setOn(isOnTime, animated: true)
            self.jobSatisfySwitch.isOn = isJobSatisFied

            if isJobSatisFied {
                setBtnTextColor(currentLbl: self.jobSatisfyLbl, alignment: .left, color: UIColor.black, text : "Yes")
            } else {
                setBtnTextColor(currentLbl: self.jobSatisfyLbl, alignment: .right, color: UIColor.black, text : "No")
            }
            self.jobSatisfySwitch.isTouchedAllow = false

        }
    }
    
    func addNoDataView() {
        noDataView = NoDataView(frame: CGRect(x: 0, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height))
        noDataView.backgroundColor = hexStringToUIColor(hex: "EEEEEE")
        noDataView.errorIconsHidden = false
        noDataView.configureView(message: noDataMessage, image: UIImage(named: "ic_empty")!)
        //        noDataView.tapButton.addTarget(self, action: #selector(noDataButtonTapped(_:)), for: .touchUpInside)
        self.view.addSubview(noDataView)
        self.noDataView.isHidden = true
    }
    
    func addErrorView() {
        errorView = NoDataView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        errorView.backgroundColor = hexStringToUIColor(hex: "EEEEEE")
        errorView.errorIconsHidden = false
        errorView.configureView(message: failureViewMsg, image: UIImage(named: "ic_error")!)
        errorView.tapButton.addTarget(self, action: #selector(self.errorButtonClicked), for: .touchUpInside)
        self.view.addSubview(errorView)
        self.errorView.isHidden = false
        
    }
    
    @objc func errorButtonClicked(sender : UIButton) {
        if jobId != nil {
            fetchUserRatingData(job_id: jobId)
            
        }
        errorView.isHidden = true
        //        tableview.delegate = self
        //        tableview.dataSource = self
        //        tableview.separatorColor = hexStringToUIColor(hex: lineSeperator)
    }
}

