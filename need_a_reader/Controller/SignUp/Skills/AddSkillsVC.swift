//
//  AddSkillsVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/16/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import AlamofireObjectMapper
import Alamofire
import SCLAlertView
import ReachabilitySwift
import ObjectMapper

class AddSkillsVC: UIViewController {
    
    @IBOutlet var accentsView: UIView!
    @IBOutlet var sitcomview: UIView!
    @IBOutlet var hourDramaview: UIView!
    @IBOutlet var playsview: UIView!
    @IBOutlet var musicalsview: UIView!
    @IBOutlet var featureFilmsview: UIView!
    
    @IBOutlet var dramedyView: UIView!
    @IBOutlet var singleCameraview: UIView!
    @IBOutlet var coStarview: UIView!
    @IBOutlet var commercialview: UIView!
    @IBOutlet var shakespeareview: UIView!
    @IBOutlet var monologuesview: UIView!
    @IBOutlet var sitcomValueLbl: UILabel!
    @IBOutlet var hourDramaValueLbl: UILabel!
    @IBOutlet var playsValueLbl: UILabel!
    @IBOutlet var musicalsValueLbl: UILabel!
    @IBOutlet var featureFilmsValueLbl: UILabel!
    
    @IBOutlet var dramedyValueLbl: UILabel!
    @IBOutlet var singleCameraValueLbl: UILabel!
    @IBOutlet var coStarValueLbl: UILabel!
    @IBOutlet var commercialValueLbl: UILabel!
    @IBOutlet var shakespeareValueLbl: UILabel!
    @IBOutlet var monologuesValueLbl: UILabel!
    
    @IBOutlet var accentsValueLbl: UILabel!
    @IBOutlet var dramedySlider: UISlider!
    @IBOutlet var sitcomSlider: UISlider!
    @IBOutlet var hourDramaSlider: UISlider!
    @IBOutlet var playsValueSlider: UISlider!
    @IBOutlet var musicalsSlider: UISlider!
    @IBOutlet var featureFilmsSlider: UISlider!
    
    @IBOutlet var singleCameraSlider: UISlider!
    @IBOutlet var coStarSlider: UISlider!
    @IBOutlet var commercialSlider: UISlider!
    @IBOutlet var shakespeareSlider: UISlider!
    @IBOutlet var monologuesSlider: UISlider!
    
    @IBOutlet var accentsSlider: UISlider!
    @IBOutlet var nextBtn: UIButton!
    
    var skillData : AddSkillsRequestModel!
    var isUpdateSkill : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.isHidden = false
        configureNavigationBar()

        if UIDevice.current.userInterfaceIdiom == .pad {
            nextBtn.frame.size.height = 70
            
        }
        self.view.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
        viewCorners.addCornerToView(view: nextBtn)
        
        sitcomValueLbl.text = "0"
        hourDramaValueLbl.text = "0"
        playsValueLbl.text = "0"
        musicalsValueLbl.text = "0"
        featureFilmsValueLbl.text = "0"
        singleCameraValueLbl.text = "0"
        coStarValueLbl.text = "0"
        commercialValueLbl.text = "0"
        shakespeareValueLbl.text = "0"
        monologuesValueLbl.text = "0"
        dramedyValueLbl.text = "0"
        accentsValueLbl.text = "0"
        
        let viewArr : [UIView] = [sitcomview, hourDramaview, playsview, musicalsview, featureFilmsview, singleCameraview, coStarview, commercialview, shakespeareview, monologuesview, dramedyView, accentsView]
        let sliderArr : [UISlider] = [sitcomSlider, hourDramaSlider, playsValueSlider, musicalsSlider, featureFilmsSlider, singleCameraSlider,coStarSlider,shakespeareSlider,commercialSlider, monologuesSlider, dramedySlider, accentsSlider]
        
        changeSliderColor(view: sliderArr, minimumTrackcolor: redColor, thumbColor: redColor)
        changeButtonColor(view: [nextBtn], backgroundcolor: redColor, fontColor: "ffffff")
        let lblArr : [UILabel] = [featureFilmsValueLbl, hourDramaValueLbl,dramedyValueLbl, commercialValueLbl, sitcomValueLbl, singleCameraValueLbl, coStarValueLbl, playsValueLbl, musicalsValueLbl, monologuesValueLbl, shakespeareValueLbl, accentsValueLbl]
        changeLabelColor(view: lblArr, backcolor: redColor, fontColor: "ffffff")
        setUI(arr: viewArr)
        if isUpdateSkill {
            nextBtn.setTitle("SAVE", for: .normal)
        }
    }
    
  
    
    func setUI(arr: [UIView]) {
        for i in 0..<arr.count {
            viewCorners.addCornerToView(view: arr[i], value: 5)
            viewCorners.addShadowToView(view: arr[i])
        }
        
        if isUpdateSkill {
            if skillData != nil {
                if let sitcom = skillData.sit_com {
                    setSliderValue(slider : sitcomSlider, val : sitcom, skillLbl : sitcomValueLbl)
                }else {
                    setSliderValue(slider : sitcomSlider, val : 0, skillLbl : sitcomValueLbl)
                }
                if let hour_drama = skillData.hour_drama {
                    setSliderValue(slider : hourDramaSlider, val : hour_drama, skillLbl : hourDramaValueLbl)
                }else {
                    setSliderValue(slider : hourDramaSlider, val : 0, skillLbl : hourDramaValueLbl)
                }
                
                if let plays = skillData.plays {
                    setSliderValue(slider : playsValueSlider, val : plays, skillLbl : playsValueLbl)
                }else {
                    setSliderValue(slider : playsValueSlider, val : 0, skillLbl : playsValueLbl)
                }
                
                if let dramedy = skillData.dramedy {
                    setSliderValue(slider : dramedySlider, val : dramedy, skillLbl : dramedyValueLbl)
                }else {
                    setSliderValue(slider : dramedySlider, val : 0, skillLbl : dramedyValueLbl)
                }
                
                if let musicals = skillData.musicals {
                    setSliderValue(slider : musicalsSlider, val : musicals, skillLbl : musicalsValueLbl)
                }else {
                    setSliderValue(slider : musicalsSlider, val : 0, skillLbl : musicalsValueLbl)
                }
                
                if let feature_films = skillData.feature_films {
                    setSliderValue(slider : featureFilmsSlider, val : feature_films, skillLbl : featureFilmsValueLbl)
                }else {
                    setSliderValue(slider : featureFilmsSlider, val : 0, skillLbl : featureFilmsValueLbl)
                }
                
                if let single_camera = skillData.single_camera {
                    setSliderValue(slider : singleCameraSlider, val : single_camera, skillLbl : singleCameraValueLbl)
                }else {
                    setSliderValue(slider : singleCameraSlider, val : 0, skillLbl : singleCameraValueLbl)
                }
                
                if let co_star = skillData.co_star {
                    setSliderValue(slider : coStarSlider, val : co_star, skillLbl : coStarValueLbl)
                }else {
                    setSliderValue(slider : coStarSlider, val : 0, skillLbl : coStarValueLbl)
                }
                
                if let commercials = skillData.commercials {
                    setSliderValue(slider : commercialSlider, val : commercials, skillLbl : commercialValueLbl)
                }else {
                    setSliderValue(slider : commercialSlider, val : 0, skillLbl : commercialValueLbl)
                }
                
                if let shakespeare = skillData.shakespeare {
                    setSliderValue(slider : shakespeareSlider, val : shakespeare, skillLbl : shakespeareValueLbl)
                }else {
                    setSliderValue(slider : shakespeareSlider, val : 0, skillLbl : shakespeareValueLbl)
                }
                
                if let monologues = skillData.monologues {
                    setSliderValue(slider : monologuesSlider, val : monologues, skillLbl : monologuesValueLbl)
                }else {
                    setSliderValue(slider : monologuesSlider, val : 0, skillLbl : monologuesValueLbl)
                }
                
                if let accents = skillData.accents {
                    setSliderValue(slider : accentsSlider, val : accents, skillLbl : accentsValueLbl)
                }else {
                    setSliderValue(slider : accentsSlider, val : 0, skillLbl : accentsValueLbl)
                }
            }
            
        }
    }
    
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        if (Reachability()?.isReachable)! {
            callAddSkillsApi()
        }else {
            alert.hideView()
            noInternetMsgDialog()
        }
        if isUpdateSkill {
            defaults.set(true, forKey: "updateUserInfo")
            
        }
    }
    
    @IBAction func skillSliderValueChanged(_ sender: Any) {
        let slider = sender as! UISlider
        switch slider {
        case sitcomSlider:
            sitcomValueLbl.text = "\(Int(slider.value))"
            print("sitcomslider")
            break
        case hourDramaSlider:
            hourDramaValueLbl.text = "\(Int(slider.value))"
            break
        case dramedySlider:
            dramedyValueLbl.text = "\(Int(slider.value))"
            break
        case playsValueSlider:
            playsValueLbl.text = "\(Int(slider.value))"
            break
        case musicalsSlider:
            musicalsValueLbl.text = "\(Int(slider.value))"
            break
        case featureFilmsSlider:
            featureFilmsValueLbl.text = "\(Int(slider.value))"
            print("featureFilmsSlider")
            break
        case singleCameraSlider:
            singleCameraValueLbl.text = "\(Int(slider.value))"
            break
        case coStarSlider:
            coStarValueLbl.text = "\(Int(slider.value))"
            break
        case commercialSlider:
            commercialValueLbl.text = "\(Int(slider.value))"
            print("commercialSlider")
            break
        case shakespeareSlider:
            shakespeareValueLbl.text = "\(Int(slider.value))"
            break
        case monologuesSlider:
            monologuesValueLbl.text = "\(Int(slider.value))"
            
        default:
            accentsValueLbl.text = "\(Int(slider.value))"
            
            break
        }
    }
    
    //Set NavigationBar
    func configureNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "OpenSans-Semibold", size: 18.0)]
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        let navigationItem = self.navigationItem
        navigationItem.title = "MY SKILL SET"
        //        if let name = defaults.value(forKey: "first_name") as? String {
        ////            navigationItem.title = name + "'s skill set"
        //            //name.uppercased() + "'S SKILL SET"
        //        }
        
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func callAddSkillsApi() {
        let sit_com = Int(sitcomValueLbl.text!)
        let hour_drama = Int(hourDramaValueLbl.text!)
        let plays = Int(playsValueLbl.text!)
        let musicals = Int(musicalsValueLbl.text!)
        let feature_films = Int(featureFilmsValueLbl.text!)
        let single_camera = Int(singleCameraValueLbl.text!)
        let co_star = Int(coStarValueLbl.text!)
        let commercials = Int(commercialValueLbl.text!)
        let shakespeare = Int(shakespeareValueLbl.text!)
        let monologues = Int(monologuesValueLbl.text!)
        let dramedy = Int(dramedyValueLbl.text!)
        let accents = Int(accentsValueLbl.text!)
        
        let user = AddSkillsRequestModel(sit_com: sit_com, hour_drama: hour_drama, plays: plays, musicals: musicals, feature_films: feature_films, single_camera: single_camera, co_star: co_star, commercials: commercials, shakespeare: shakespeare, monologues: monologues, dramedy: dramedy, accents : accents)
        let data_temp = Mapper<AddSkillsRequestModel>().toJSON(user)
        print("Skill request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(addSkillForUser, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        //                        let alertView = SCLAlertView()
                        //                        alertView.showSuccess("Success", subTitle: (response.result.value?.message)!, closeButtonTitle: "Ok",  colorStyle: int_red)
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        defaults.set(true, forKey: "skillSetup")

                        if self.isUpdateSkill {
                            self.navigationController?.popViewController(animated: true)
                        }else {
//                            let nextVC = story.instantiateViewController(withIdentifier: "MyTabBarViewController")
//                            self.navigationController?.present(nextVC, animated: true)
                            let nextVC = story.instantiateViewController(withIdentifier: "TappingServiceVC") as! TappingServiceVC
                            self.navigationController?.pushViewController(nextVC, animated: true)
                        }
                        //
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.callAddSkillsApi()
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
}
