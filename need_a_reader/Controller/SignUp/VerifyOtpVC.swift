//
//  VerifyOtpVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/15/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SCLAlertView
import ReachabilitySwift

class VerifyOtpVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var timerLbl: UILabel!
    @IBOutlet var tryagainBtn: UIButton!
    @IBOutlet var otpTxt: UITextField!
    @IBOutlet var verifyOtpBtn: UIButton!
    var isForgotPasswordVC = false
    var email = ""
    var userName = ""
    var timer = Timer()
    var seconds = 60
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureNavigationBar()
        timerLbl.text = ""
        if UIDevice.current.userInterfaceIdiom == .pad {
            verifyOtpBtn.frame.size.height = 70
        }
        self.view.backgroundColor = hexStringToUIColor(hex: "F0F0F0")
        viewCorners.addBorderCornerTobutton(view: verifyOtpBtn, color: UIColor.white)
        verifyOtpBtn.backgroundColor = UIColor.white
        verifyOtpBtn.setTitleColor(hexStringToUIColor(hex: redColor), for: .normal)
        otpTxt.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        startTimer()
    }
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    //Set NavigationBar
    func configureNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "OpenSans-Semibold", size: 18.0)]
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        let navigationItem = self.navigationItem
        navigationItem.title = "VERIFY OTP"
        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: "F0F0F0")
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func startTimer() {
        seconds = 60
        tryagainBtn.isHidden = true
        timerLbl.isHidden = false
        // timerLbl.text = String(format: "00 : 00 : %02d", seconds)//"00 : 00 : \(second)"
        timer = Timer()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
        
    }
    
    @objc func updateTimer() {
        // seconds -= 1     //This will decrement(count down)the seconds.
        if seconds > 0 {
            seconds = seconds-1
            //            timerLbl.text = String(format: "00 : 00 : %02d", seconds)//"00 : 00 : \(second)"
            timerLbl.text = String(format: "%02d:%02d:%02d", seconds/3600, (seconds/60)%60, seconds%60)//"00 : 00 : \(second)"
            
            //            timerLbl.text = "\(seconds)" //This will update the label.
            
        }else {
            timer.invalidate()
            timerLbl.isHidden = true
            tryagainBtn.isHidden = false
        }
        
    }
    
    @IBAction func verifyOtpBtnclicked(_ sender: Any) {
        self.view.endEditing(true)
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        if (Reachability()?.isReachable)! {
            if isForgotPasswordVC {
                forgotPasswordForVerifyOTP()
            }else {
                verifyOtp()
            }
            
        }else {
            alert.hideView()
            noInternetMsgDialog()
        }
    }
    
    @IBAction func tryAgainBtnClicked(_ sender: Any) {
        self.view.endEditing(true)
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        if (Reachability()?.isReachable)! {
            resendOTP()
            startTimer()
            
        } else {
            alert.hideView()
            noInternetMsgDialog()
        }
    }
    
    func verifyOtp() {
        let otp_code = otpTxt.text!
        var regCode : Int?
        if  !checkLength(testStr: otp_code) {
            alert.hideView()
            let alertView = SCLAlertView()
            alertView.showError("Error", subTitle: "Please enter verification code.", closeButtonTitle: "Ok")
        } else {
            
            //Object to be serialized to JSON
            if let signUpId = defaults.value(forKey: "signUpId") {
                regCode = signUpId as? Int
            }
            
            
            let user =  CodeVerifyRequestModel(user_registration_temp_id: regCode, otp_token: Int(otp_code), device_info: deviceInfo)
            //                    CodeVerifyRequestModel(user_registration_temp_id: regCode, licence_code: licence_code!)
            
            
            //convert object to dictionary
            let data_temp = Mapper<CodeVerifyRequestModel>().toJSON(user)
            print("data:- \(data_temp)")
            alamofireManager.request(verifyOTPForRegisterUser, method: .post, parameters: data_temp, encoding: JSONEncoding(options: [])).responseObject{ (response: DataResponse<ResponseModel> )in
                
                switch response.result
                {
                case .success(let value):
                    if let code = value.code {
                        print("Code:- \(code)")
                        switch code {
                        case 200:
                            alert.hideView()
                            if let token = value.loginResponse?.token {
                                UserDefaults.standard.set(token, forKey: "token")
                            }
                            if let is_first_time = value.loginResponse?.user_detail?.is_first_time {
                                UserDefaults.standard.set(is_first_time, forKey: "isFirstSwipe")
                            }
                            if let userID = value.loginResponse?.user_detail?.user_id {
                                UserDefaults.standard.set(userID, forKey: "userId")
                                //                                registerDevice(user_id: userID)
                                //                                registerDevice()
                            }
                            if let userChatID = value.loginResponse?.user_detail?.chat_id {
                                defaults.set(userChatID, forKey: "userChatId")
                            }
                            if let user_detail = value.loginResponse?.user_detail {
                                defaults.set(user_detail.toJSON(), forKey: "user_detail")
                                QBCore.instance().qb_DEFAULT_PASSOWORD = default_password
                                appDelegate.login()
                                delete24HoursDialogs()
                            }
                            if let headshot_setup = value.loginResponse?.user_detail?.headshot_setup {
                                if headshot_setup {
                                    defaults.set(headshot_setup, forKey: "headshot_setup")
                                    //                                    if let resume_setup = value.loginResponse?.user_detail?.resume_setup {
                                    //                                        defaults.set(resume_setup, forKey: "resume_setup")
                                    //                                        if resume_setup {
                                    if let skillSetup = value.loginResponse?.user_detail?.skill_setup {
                                        defaults.set(skillSetup, forKey: "skillSetup")
                                        
                                        if !skillSetup {
                                            let story = UIStoryboard(name: "Main", bundle: nil)
                                            let nextVC = story.instantiateViewController(withIdentifier: "AddSkillsVC") as! AddSkillsVC
                                            self.navigationController?.pushViewController(nextVC, animated: true)
                                        }else {
                                            //Home
                                            let story = UIStoryboard(name: "Main", bundle: nil)
                                            //                                                    let nextVC = story.instantiateViewController(withIdentifier: "FindUserVC") as! FindUserVC
                                            //                                                    self.navigationController?.pushViewController(nextVC, animated: true)
                                            let nextVC = story.instantiateViewController(withIdentifier: "MyTabBarViewController")
                                            self.navigationController?.present(nextVC, animated: true)
                                        }
                                    }
                                    //                                        }else {
                                    //                                            let story = UIStoryboard(name: "Main", bundle: nil)
                                    //                                            let nextVC = story.instantiateViewController(withIdentifier: "AddProfileVC") as! AddProfileVC
                                    //                                            if let imgStr = value.loginResponse?.user_detail?.headshot_compressed_image {
                                    //                                                nextVC.profileImgString = imgStr
                                    //                                            }
                                    //                                            if let full_name = value.loginResponse?.user_detail?.user_name {
                                    //                                                nextVC.userName = full_name
                                    //                                                defaults.set(full_name, forKey: "user_name")
                                    //                                            }
                                    //                                            self.navigationController?.pushViewController(nextVC, animated: true)
                                    //                                        }
                                    //                                    }
                                    
                                }else {
                                    let story = UIStoryboard(name: "Main", bundle: nil)
                                    let nextVC = story.instantiateViewController(withIdentifier: "AddProfileVC") as! AddProfileVC
                                    if let imgStr = value.loginResponse?.user_detail?.headshot_compressed_image {
                                        nextVC.profileImgString = imgStr
                                    }
                                    if let full_name = value.loginResponse?.user_detail?.user_name {
                                        if let last_name = value.loginResponse?.user_detail?.last_name {
                                            nextVC.userName = full_name + " " + last_name
                                            
                                        } else {
                                            nextVC.userName = full_name
                                            
                                        }
                                        defaults.set(full_name, forKey: "first_name")
                                        defaults.set(nextVC.userName, forKey: "user_name")
                                    }
                                    nextVC.userName = self.userName
                                    self.navigationController?.pushViewController(nextVC, animated: true)
                                }
                            }
                            break
                        case 400:
                            alert.hideView()
                            //
                            //                        if let error = value.message {
                            //                            let alertview = SCLAlertView()
                            //
                            //                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            //                        }
                            clearSession()
                            let story = UIStoryboard(name: "Main", bundle: nil)
                            let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                            let nav = redirectToRootViewController(vcName: vc)
                            self.present(nav, animated: true)
                            break
                        case 401:
                            alert.hideView()
                            if let new_token = value.codeVerifyResponse?.new_token {
                                UserDefaults.standard.set(new_token, forKey: "token")
                                self.verifyOtp()
                            }else {
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                            }
                            break
                        default:
                            alert.hideView()
                            
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                            break
                        }
                    }
                    break
                case .failure(let error):
                    print("error: \(error.localizedDescription)")
                    
                    alert.hideView()
                    let alertview = SCLAlertView()
                    alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                    
                    //print("Request failed with error: \(error)")
                    break
                    
                }
                
            }
            
        }
        
    }
    
    
    //Forgot Password
    func forgotPasswordForVerifyOTP() {
        
        var otp = ""
        if let otpCode = otpTxt.text {
            if otpCode == "" {
                
                alert.hideView()
                let alertView = SCLAlertView()
                alertView.showError("Error", subTitle: "Please enter verification code.", closeButtonTitle: "Ok")
                
                
            } else {
                otp = otpCode
                
                //Object to be serialized to JSON
                
                
                let user = ForgotPasswordRequestModel(email_id: email, otp_token: Int(otp))
                
                
                //convert object to dictionary
                let data_temp = Mapper<ForgotPasswordRequestModel>().toJSON(user)
                print("data:- \(data_temp)")
                alamofireManager.request(forgotPasswordVerifyOtp, method: .post, parameters: data_temp, encoding: JSONEncoding(options: [])).responseObject{ (response: DataResponse<ResponseModel> )in
                    
                    switch response.result
                    {
                    case .success(let value):
                        if let code = value.code {
                            print("Code:- \(code)")
                            switch code {
                            case 200:
                                alert.hideView()
                                UserDefaults.standard.set(value.loginResponse?.token, forKey: "otp_token")
                                let story = UIStoryboard(name: "Main", bundle: nil)
                                let nextVC = story.instantiateViewController(withIdentifier: "AddNewPasswordVC") as! AddNewPasswordVC
                                nextVC.email = self.email
                                self.navigationController?.pushViewController(nextVC, animated: true)
                                break
                                //                        case 201:
                                //
                                //                             self.resendOTP()
                            //                            break
                            case 400:
                                alert.hideView()
                                //
                                //                        if let error = value.message {
                                //                            let alertview = SCLAlertView()
                                //
                                //                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                //                        }
                                clearSession()
                                let story = UIStoryboard(name: "Main", bundle: nil)
                                let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                let nav = redirectToRootViewController(vcName: vc)
                                self.present(nav, animated: true)
                                break
                            case 401:
                                alert.hideView()
                                if let new_token = value.signUpResponse?.new_token {
                                    UserDefaults.standard.set(new_token, forKey: "token")
                                    self.forgotPasswordForVerifyOTP()
                                }else {
                                    if let error = value.message {
                                        let alertview = SCLAlertView()
                                        
                                        alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                    }
                                }
                                break
                            default:
                                alert.hideView()
                                
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                                break
                            }
                        }
                        break
                    case .failure(let error):
                        print("error: \(error.localizedDescription)")
                        
                        alert.hideView()
                        let alertview = SCLAlertView()
                        alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                        
                        //print("Request failed with error: \(error)")
                        break
                        
                        //                    default:
                        //                        alert.hideView()
                        //                        print("Sorry")
                        //                        break
                        
                    }
                    
                }
            }
            
        }
        //        else{
        //                    alert.hideView()
        //                    let alertView = SCLAlertView()
        //                    alertView.showError("Error", subTitle: "Please enter code.", closeButtonTitle: "Ok")
        //
        //        }
        
        
        
        
    }
    
    func resendOTP() {
        var user : ForgotPasswordRequestModel!
        //        let user = ForgotPasswordRequestModel(email_id: email)
        if email != "" {
            user = ForgotPasswordRequestModel(email_id: email)
        }else {
            var regCode : Int?
            if let signUpId = defaults.value(forKey: "signUpId") {
                regCode = signUpId as? Int
                user = ForgotPasswordRequestModel(user_registration_temp_id: regCode)
            }
        }
        
        //convert object to dictionary
        let data_temp = Mapper<ForgotPasswordRequestModel>().toJSON(user)
        print("data:- \(data_temp)")
        alamofireManager.request(resendOTPForUser, method: .post, parameters: data_temp, encoding: JSONEncoding(options: [])).responseObject{ (response: DataResponse<ResponseModel> )in
            
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        if let msg = value.message {
                            let alertView = SCLAlertView()
                            alertView.showInfo("", subTitle: msg, colorStyle: int_red)
                            //                            alertView.showInfo("", subTitle: msg, colorStyle: int_red)
                        }
                        break
                    case 400:
                        alert.hideView()
                        //
                        //                        if let error = value.message {
                        //                            let alertview = SCLAlertView()
                        //
                        //                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        //                        }
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.signUpResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.resendOTP()
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                
                break
                
                
                
            }
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == otpTxt {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            
            return updatedText.count <= 4
        }
        return true
        
    }
}
