//
//  AddProfileVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/16/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SCLAlertView
import AlamofireObjectMapper
import ObjectMapper
import ReachabilitySwift
import TOCropViewController
import SDWebImage
import CoreGraphics
import ByvImagePicker


class AddProfileVC: UIViewController, TOCropViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate, UIGestureRecognizerDelegate  {
    
    @IBOutlet var hourlySlider: UISlider!
    @IBOutlet var hourlyRateLbl: UILabel!
    @IBOutlet var outerViewConstant: NSLayoutConstraint!
    @IBOutlet var scrollview: UIScrollView!
    @IBOutlet var imageouterView: UIView!
    @IBOutlet var outerview: UIView!
    @IBOutlet var profileOuterImg: UIImageView!
    @IBOutlet var filenameLbl: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var resumeBtn: UIButton!
    @IBOutlet var nextBtn: UIButton!
    @IBOutlet var profileImg: UIImageView!
    @IBOutlet var userNameLbl: UILabel!
    var userDetail : User_detail!
    var imagePicker : UIImagePickerController!
    //    var imagePicker : ByvImagePicker? = nil
    var profileImgString = ""
    var userName = ""
    var documentPicker : UIDocumentPickerViewController!
    var sendPDFData : Data!
    var resumeImage : Bool = false
    var selectedProfileImg : UIImage!
    var resumeSelectedImg : UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureNavigationBar()
        //        userNameLbl.text = ""
        self.view.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
//        self.profileImg.contentMode = .scaleAspectFit

        activityIndicator.startAnimating()
        if UIDevice.current.userInterfaceIdiom == .pad {
            resumeBtn.frame.size.height = 60
            nextBtn.frame.size.height = 60
            profileOuterImg.frame.size.height = self.outerview.frame.size.height * 0.6
            profileImg.frame.size.height = profileOuterImg.frame.size.height * 0.65
        }
        viewCorners.addBorderCornerTobutton(view: resumeBtn, color: hexStringToUIColor(hex: redColor))
        viewCorners.addCornerToView(view: nextBtn)
        viewCorners.addCornerToView(view: resumeBtn)
        //        viewCorners.addCornerToView(view: profileImg, value: 10)
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        setUIData()
        viewCorners.addShadowToView(view: profileOuterImg, value: 2.0)
        //        let img = UIImage(named: "profile_card_full")?.resizableImage(withCapInsets: edges, resizingMode: .stretch)
        //        profileOuterImg.image?.resizableImage(withCapInsets: edges, resizingMode: .stretch)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(_:)))
        profileImg.isUserInteractionEnabled = true
        profileImg.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        let img = defaults.value(forKey: "profileImage")
        let userName = defaults.value(forKey: "user_name")
        
        if userName != nil {
            userNameLbl.text = userName as! String
        }
        if img != nil {
            profileImg.sd_setShowActivityIndicatorView(true)
            profileImg.sd_setIndicatorStyle(.gray)
            profileImg.sd_setImage(with: URL(string: img as! String))
            selectedProfileImg = profileImg.image!
                self.profileImg.contentMode = .scaleToFill
            //            profileImg.contentMode = .scaleAspectFit//.scaleToFill
        }
    }
    
    override func viewDidLayoutSubviews() {
        let path = UIBezierPath(roundedRect: profileImg.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 5.0, height: 5.0))
        let mask = CAShapeLayer()
        mask.frame = profileImg.bounds
        mask.path = path.cgPath
        profileImg.layer.mask = mask
        //        outerViewConstant.constant = self.view.frame.size.height + 30
        //New
        
        imageouterView.frame.size.height = self.view.frame.size.height * 0.92
        profileImg.frame.size.height = self.imageouterView.frame.size.height * 0.6
        outerViewConstant.constant = imageouterView.frame.size.height + nextBtn.frame.size.height + 60
        print("outerviewConstant:- \(outerViewConstant.constant)")
        //        profileImg.frame.size.height = self.view.frame.size.height * 0.7
        
    }
    
    func setUIData() {
        userNameLbl.text = userName
        filenameLbl.text = ""
        //        profileImg.image = UIImage(named: "ic_defaultProfile")
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        if profileImgString.characters.count > 0 {
            profileImg.sd_setShowActivityIndicatorView(true)
            profileImg.sd_setIndicatorStyle(.gray)
            profileImg.sd_setImage(with: URL(string: profileImgString))
            selectedProfileImg = profileImg.image
//            profileImg.sd_setImage(with: URL(string: profileImgString), placeholderImage: UIImage(named: "ic_defaultProfile"))
//            if profileImg.image == UIImage(named: "ic_defaultProfile") {
//                profileImg.contentMode = .scaleAspectFit
//
//            }else{
                profileImg.contentMode = .scaleToFill
                
//            }
        }else {
            profileImg.image = UIImage(named: "ic_defaultProfile")
            profileImg.contentMode = .scaleAspectFit
        }
    }
    
    @objc func tapBlurButton(_ sender: UITapGestureRecognizer) {
        cameraClicked()
    }
    
    @IBAction func resumeBtn(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Resume From", message: "", preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: 64, width: screenWidth, height: 0)
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (success) in
            self.imagePicker.sourceType = .photoLibrary
            self.resumeImage = true
            self.present(self.imagePicker, animated: true, completion: nil)
            
            
        }))
        alert.addAction(UIAlertAction(title: "Documents/iCloud", style: .default, handler: { (success) in
            self.documentPicker = UIDocumentPickerViewController(documentTypes: ["public.content"], in: .import)
            self.documentPicker.delegate = self
            self.documentPicker.modalPresentationStyle = .fullScreen
            self.present(self.documentPicker, animated: true, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: { (success) in
            
        }))
        //        alert.view.tintColor = UIColor(constant: .BlueBlack)
        present(alert, animated: true, completion: nil)
        
        
    }
 
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        print("url:- \(url)")
        let extensionString = (url.absoluteString).suffix(4)
        let theFileName = ((url.absoluteString) as NSString).lastPathComponent
        //        if extensionString != ".pdf" {
        //            filenameLbl.text = theFileName
        ////            self.alertview.showError("Error", subTitle: "Please select pdf file.", closeButtonTitle: "Ok")
        //
        //        }else {
        filenameLbl.text = theFileName
        if controller.documentPickerMode == UIDocumentPickerMode.import {
            // This is what it should be
            //                let data = String(contentsOfFile: url.path)
            do {
                let pdfData = try Data(contentsOf: url)
                sendPDFData = pdfData
                if resumeSelectedImg != nil {
                    resumeSelectedImg = nil
                }
            } catch {
                print("Unable to load data: \(error)")
            }
        }
        //        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("document picker cancel")
        
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        
        testValidation()
    }
    
    func cameraClicked() {
        self.resumeImage = false
        let alert = UIAlertController(title: "Choose Image From", message: "", preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: 64, width: screenWidth, height: 0)
        alert.addAction(UIAlertAction(title: "Take picture", style: .default, handler: { (success) in
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
            
            
        }))
        alert.addAction(UIAlertAction(title: "Photo library", style: .default, handler: { (success) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: { (success) in
            
        }))
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func cameraBtnClicked(_ sender: Any) {
        cameraClicked()
    }
    
    func testValidation() {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        if let img = profileImg.image {
//            if img != UIImage(named: "ic_defaultProfile") {
            if selectedProfileImg != nil {
                print(img)
                setImageApi(profileImage: img)
            } else {
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: noProfileMessage, closeButtonTitle: "Ok")
            }
            
            
        } else {
            alert.hideView()
            let alertview = SCLAlertView()
            alertview.showError("Error", subTitle: noProfileMessage, closeButtonTitle: "Ok")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            print("pickedImage:- \(pickedImage.size)")
            let cropVC = TOCropViewController(croppingStyle: .default, image: pickedImage)
            cropVC.delegate = self
            if !resumeImage {
                cropVC.rotateClockwiseButtonHidden = false
                cropVC.aspectRatioPickerButtonHidden = true
                
                cropVC.resetAspectRatioEnabled = false
                cropVC.customAspectRatio = CGSize(width: profileImg.frame.size.width, height: profileImg.frame.size.height)
                cropVC.aspectRatioLockEnabled = false
                cropVC.cropView.cropBoxResizeEnabled = false
            }
            
            
            if resumeImage {
                if let imageUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
                    let imageName  = imageUrl.lastPathComponent
                    filenameLbl.text = imageName
                }
            }
            navigationController?.pushViewController(cropVC, animated: true)
            //            if resumeImage {
            //                resumeSelectedImg = pickedImage
            //                if sendPDFData != nil {
            //                    sendPDFData = nil
            //                }
            //            }else {
            //                self.profileImg.image = pickedImage
            //                profileImg.contentMode = .scaleToFill
            //
            //            }
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        
        cropViewController.navigationController?.popViewController(animated: true)
        if let imageData = UIImageJPEGRepresentation(image, 1) {
            
            let imageSize: Double = Double(imageData.count)
            let size = imageSize/(1024*1024)
            print("size of original image in MB:\((imageSize/1024)/1024)")
            if size > 1.5 {
                let compressedImage = imageCompression().compressImage(image: image)
                if resumeImage {
                    resumeSelectedImg = compressedImage
                    if sendPDFData != nil {
                        sendPDFData = nil
                    }
                }else {
                    self.profileImg.image = compressedImage
                    self.selectedProfileImg = compressedImage
                    profileImg.contentMode = .scaleToFill
                    
                }
            } else {
                if resumeImage {
                    resumeSelectedImg = image
                    if sendPDFData != nil {
                        sendPDFData = nil
                    }
                }else {
                    self.profileImg.image = image
                    self.selectedProfileImg = image
                    profileImg.contentMode = .scaleToFill
                }
            }
        }
        
    }
    
    
    //Set NavigationBar
    func configureNavigationBar()
    {
        setNavigaion(vc: self)

        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        let navigationItem = self.navigationItem
        navigationItem.title = ""
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setImageApi(profileImage: UIImage) {
        print(hourlySlider.value)
        print(tokenHeader)
        defaults.set(Int(hourlySlider.value), forKey: "hourlyRate")
        let user = ProfileSetupRequestModel(hourly_rate : Int(hourlySlider.value))
        let data_temp = Mapper<ProfileSetupRequestModel>().toJSONString(user)
        let param = ["request_data": data_temp]
        let URL = try! URLRequest(url: setHeadShotForUser, method: .post, headers: tokenHeader)
        
        alamofireManager.upload(multipartFormData:  { multipartFormData in
            if let data = UIImageJPEGRepresentation(self.selectedProfileImg, 1.0) {
                multipartFormData.append(data, withName: "file", fileName: "file.jpeg", mimeType: "image/jpeg")
            }
            for (key,value) in param
            {
                print("key:- \(key), value:- \(value)")
                multipartFormData.append(((value)?.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)))!,withName : key)
            }
        }, with: URL) { encodingResult in
            //, to: setHeadShotForUser, method: .post, headers: tokenHeader, encodingCompletion: { encodingResult in
            
            switch encodingResult
            {
            case .success(let upload,_,_):
                upload.responseObject { (response: DataResponse<ResponseModel> )in
                    switch response.result {
                    case .success(let value):
                        if let code = value.code {
                            print("Code:- \(code)")
                            switch code {
                            case 200:
                                print("Success")
                                
                                if let img = value.headShotResponse?.headshot_compressed_image {
                                    
//                                    if self.profileImg.image == UIImage(named: "ic_defaultProfile") {
//                                        self.profileImg.contentMode = .scaleAspectFit
//                                        
//                                    }else{
//                                        self.profileImg.contentMode = .scaleToFill
//                                        
//                                    }
                                    defaults.set(img, forKey: "profileImage")
                                    defaults.set(true, forKey: "headshot_setup")
                                    updateUserPhotoForCurrentUser(image: profileImage)
                                }
                                if self.sendPDFData != nil || self.resumeSelectedImg != nil {
                                    self.setResumeApi()
                                }else {
                                    alert.hideView()
                                    let story = UIStoryboard(name: "Main", bundle: nil)
                                    
                                    let nextVC = story.instantiateViewController(withIdentifier: "AddSkillsVC") as! AddSkillsVC
                                    let nav = UINavigationController(rootViewController: nextVC)
                                    nav.navigationBar.isTranslucent = false
                                    nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                                    nav.navigationBar.shadowImage = UIImage()
                                    nav.interactivePopGestureRecognizer?.isEnabled = false
                                    self.navigationController?.present(nav, animated: true, completion: nil)
                                }
                                break
                            case 400:
                                alert.hideView()
                                
                                clearSession()
                                let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                let nav = redirectToRootViewController(vcName: vc)
                                self.present(nav, animated: true)
                                break
                            case 401:
                                alert.hideView()
                                if let new_token = value.signUpResponse?.new_token {
                                    UserDefaults.standard.set(new_token, forKey: "token")
                                    self.setImageApi(profileImage: profileImage)
                                }else {
                                    if let error = value.message {
                                        let alertview = SCLAlertView()
                                        alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                    }
                                }
                                break
                                
                            default:
                                alert.hideView()
                                
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                                break
                            }
                        }
                        break
                    case .failure(let error):
                        print("error: \(error.localizedDescription)")
                        
                        alert.hideView()
                        
                        let alertview = SCLAlertView()
                        alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                        
                      //  alert.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                      //  ("Error", subTitle: serverError, closeButtonTitle: "Ok")
                        break
                    }
                    
                }
            case .failure(let encodingError):
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: encodingError.localizedDescription, closeButtonTitle: "Ok")
                //print("Request failed with error: \(error)")
                break
            }
            }
    }
    
    func setResumeApi() {
        
        let URL = try! URLRequest(url: setResumeForUser, method: .post, headers: tokenHeader)
        
        alamofireManager.upload(multipartFormData:  { multipartFormData in
            
            if self.sendPDFData != nil {
                multipartFormData.append(self.sendPDFData, withName: "file", fileName: "doc.pdf", mimeType: "application/pdf")
            }else {
                if self.resumeSelectedImg != nil {
                    if let data = UIImageJPEGRepresentation(self.resumeSelectedImg, 1.0) {
                        multipartFormData.append(data, withName: "file",  fileName: "file.jpeg", mimeType: "image/jpeg")
                    }
                }
            }
        }, with: URL) { encodingResult in
            //            , to: setResumeForUser, method: .post, headers: tokenHeader, encodingCompletion: { encodingResult in
            
            switch encodingResult
            {
            case .success(let upload,_,_):
                upload.responseObject { (response: DataResponse<ResponseModel> )in
                    switch response.result {
                    case .success(let value):
                        if let code = value.code {
                            print("Code:- \(code)")
                            switch code {
                            case 200:
                                alert.hideView()
                                if let resumeURL = value.headShotResponse?.resume_file {
                                    defaults.set(resumeURL, forKey: "ResumeURL")
                                }
                                defaults.set(true, forKey: "resume_setup")
                                let story = UIStoryboard(name: "Main", bundle: nil)
                                let nextVC = story.instantiateViewController(withIdentifier: "AddSkillsVC") as! AddSkillsVC
                                let nav = UINavigationController(rootViewController: nextVC)
                                nav.navigationBar.isTranslucent = false
                                nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                                nav.navigationBar.shadowImage = UIImage()
                                nav.interactivePopGestureRecognizer?.isEnabled = false
                                self.navigationController?.present(nav, animated: true, completion: nil)
                                //                                self.navigationController?.pushViewController(nextVC, animated: true)
                                break
                            case 400:
                                alert.hideView()
                                clearSession()
                                let story = UIStoryboard(name: "Main", bundle: nil)
                                let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                let nav = redirectToRootViewController(vcName: vc)
                                self.present(nav, animated: true)
                                break
                            case 401:
                                alert.hideView()
                                if let new_token = value.signUpResponse?.new_token {
                                    UserDefaults.standard.set(new_token, forKey: "token")
                                    self.setResumeApi()
                                }else {
                                    if let error = value.message {
                                        let alertview = SCLAlertView()
                                        alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                    }
                                }
                                break
                            default:
                                alert.hideView()
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                                break
                            }
                        }
                        break
                    case .failure(let error):
                        print("error: \(error.localizedDescription)")
                        alert.hideView()
                       
                        let alertview = SCLAlertView()
                        alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                        
                        
                        break
                    }
                    
                }
            case .failure(let encodingError):
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: encodingError.localizedDescription, closeButtonTitle: "Ok")
                //print("Request failed with error: \(error)")
                break
            }
        }
    }
    
    @IBAction func skillSliderValueChanged(_ sender: Any) {
        let slider = sender as! UISlider
        hourlyRateLbl.text = "$\(Int(slider.value))"
    }
}


