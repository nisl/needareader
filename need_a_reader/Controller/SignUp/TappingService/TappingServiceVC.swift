//
//  TappingServiceVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 5/26/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SCLAlertView
import AlamofireObjectMapper
import ObjectMapper
import ReachabilitySwift
import LabelSwitch


class TappingServiceVC : UIViewController, SwiftySwitchDelegate {
    
    @IBOutlet var btnStackHeightConstant: NSLayoutConstraint!
    @IBOutlet var tableViewConstant: NSLayoutConstraint!
    @IBOutlet var tapRatingConstant: NSLayoutConstraint!
    @IBOutlet var editRatingConstant: NSLayoutConstraint!
    
    @IBOutlet var tableViewTopCOnstant: NSLayoutConstraint!
    @IBOutlet var tapingRateTopConstant: NSLayoutConstraint!
    @IBOutlet var editingRateTopConstant: NSLayoutConstraint!
    
    @IBOutlet var tapView: UIView!
    @IBOutlet var editView: UIView!
    @IBOutlet var tapRatingVIew: UIView!
    @IBOutlet var editingRateView: UIView!
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var tapingRateBtn1: UIButton!
    @IBOutlet var tapingRateBtn2: UIButton!
    @IBOutlet var tapingRateBtn3: UIButton!
    
    @IBOutlet var tapingSwitchBtn: SwiftySwitch!
    @IBOutlet var editingSwitchBtn: SwiftySwitch!
    @IBOutlet var tapingYesNoLbl: UILabel!
    @IBOutlet var editingYesNoLbl: UILabel!

    @IBOutlet var editingRateBtn1: UIButton!
    @IBOutlet var editingRateBtn2: UIButton!
    @IBOutlet var editingRateBtn3: UIButton!
    
    @IBOutlet var skipBtn: UIButton!
    @IBOutlet var addServiceBtn: UIButton!
    
    var tapingRate : Int = 0
    var editingRate : Int = 0
    var noDataView : NoDataView!
    var errorView : NoDataView!
    var tappingSerivceData : TappingService!
    var serviceDetailArr : [ServiceDetail] = []
    var selectedServiceId: [String] = []
    var selectedTapPrice : Int = 5
    var selectedEditedPrice : Int = 20
    var isUpdateServices : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)

        tapingSwitchBtn.delegate = self
       editingSwitchBtn.delegate = self
        
        tapingRateBtn1.setTitle("$5", for: .normal)
        tapingRateBtn2.setTitle("$10", for: .normal)
        tapingRateBtn3.setTitle("$15", for: .normal)
        editingRateBtn1.setTitle("$20", for: .normal)
        editingRateBtn2.setTitle("$25", for: .normal)
        editingRateBtn3.setTitle("$30", for: .normal)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            addServiceBtn.frame.size.height = 70
            skipBtn.frame.size.height = 70
            tapingRateBtn1.frame.size.height = 70
            tapingRateBtn2.frame.size.height = 70
            tapingRateBtn3.frame.size.height = 70
            editingRateBtn1.frame.size.height = 70
            editingRateBtn2.frame.size.height = 70
            editingRateBtn3.frame.size.height = 70
            btnStackHeightConstant.constant = 70
        }

        viewCorners.addBorderCornerTobutton(view: tapingRateBtn1, color: hexStringToUIColor(hex: redColor))
        viewCorners.addBorderCornerTobutton(view: tapingRateBtn2, color: hexStringToUIColor(hex: redColor))
        viewCorners.addBorderCornerTobutton(view: tapingRateBtn3, color: hexStringToUIColor(hex: redColor))
        viewCorners.addBorderCornerTobutton(view: editingRateBtn1, color: hexStringToUIColor(hex: redColor))
        viewCorners.addBorderCornerTobutton(view: editingRateBtn2, color: hexStringToUIColor(hex: redColor))
        viewCorners.addBorderCornerTobutton(view: editingRateBtn3, color: hexStringToUIColor(hex: redColor))
        viewCorners.addCornerToView(view: addServiceBtn)
        viewCorners.addCornerToView(view: skipBtn)
        
        clearBtnColor(btns: [tapingRateBtn1, tapingRateBtn2, tapingRateBtn3, editingRateBtn1, editingRateBtn2, editingRateBtn3], titleColor: redColor)
        let viewArr : [UIView] = [tapView, tapRatingVIew, editView, editingRateView, tableView]
        addCornerView(arr: viewArr)

        if isUpdateServices {
            skipBtn.isHidden = true
            addServiceBtn.setTitle("UPDATE", for: .normal)
        }
        
        if (Reachability()?.isReachable)! {
            getAllServices()
        } else {
            noInternetMsgDialog()
            self.tableViewConstant.constant = 0
            self.tableView.isHidden = true
            addErrorView()
        }
    }
    
    func addCornerView(arr: [UIView]) {
        for i in 0..<arr.count {
            viewCorners.addCornerToView(view: arr[i], value: 5)
            viewCorners.addShadowToView(view: arr[i])
        }
    }
    
    @IBAction func tapingEditingBtnClicked(_ sender: Any) {
        let currentBtn = sender as! UIButton
        
        switch currentBtn {
        case tapingRateBtn1:
            selectedTapPrice = 5
            setSelectedBtnColors(selectedBtn: tapingRateBtn1, otherBtn: [tapingRateBtn2, tapingRateBtn3])
            break
        case tapingRateBtn2:
            selectedTapPrice = 10
            setSelectedBtnColors(selectedBtn: tapingRateBtn2, otherBtn: [tapingRateBtn1, tapingRateBtn3])
            break
        case tapingRateBtn3:
            selectedTapPrice = 15
            setSelectedBtnColors(selectedBtn: tapingRateBtn3, otherBtn: [tapingRateBtn2, tapingRateBtn1])
            break
        case editingRateBtn1:
            selectedEditedPrice = 20
            setSelectedBtnColors(selectedBtn: editingRateBtn1, otherBtn: [editingRateBtn2, editingRateBtn3])
            break
        case editingRateBtn2:
            selectedEditedPrice = 25
            setSelectedBtnColors(selectedBtn: editingRateBtn2, otherBtn: [editingRateBtn1, editingRateBtn3])
            break
        default:
            selectedEditedPrice = 30
            setSelectedBtnColors(selectedBtn: editingRateBtn3, otherBtn: [editingRateBtn1, editingRateBtn2])
            break
        }
    }
    
   
    
    func addErrorView() {
        let navHeight = self.navigationController?.navigationBar.frame.size.height
        errorView = NoDataView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height - navHeight!))
        errorView.backgroundColor = hexStringToUIColor(hex: "EEEEEE")
        errorView.errorIconsHidden = false
        errorView.configureView(message: failureViewMsg, image: UIImage(named: "ic_error")!)
        errorView.tapButton.addTarget(self, action: #selector(self.errorButtonClicked), for: .touchUpInside)
        self.view.addSubview(errorView)
        self.errorView.isHidden = false
    }
    
    @objc func errorButtonClicked(sender : UIButton) {
        if (Reachability()?.isReachable)! {
            errorView.isHidden = true
            getAllServices()
        }
    }
    
    func setServiceData() {
        if let tappingData = tappingSerivceData {
            if tappingData.is_taping! {
                hideShowEditView(isOn: true, currentView: tapRatingVIew)
                if let rate = tappingData.taping_rate {
                    switch rate{
                    case "10":
                        selectedTapPrice = 10
                        setSelectedBtnColors(selectedBtn: tapingRateBtn2, otherBtn: [tapingRateBtn1, tapingRateBtn3])
                        break
                    case "15":
                        selectedTapPrice = 15
                        setSelectedBtnColors(selectedBtn: tapingRateBtn3, otherBtn: [tapingRateBtn2, tapingRateBtn1])
                        break
                    default:
                        setSelectedBtnColors(selectedBtn: tapingRateBtn1, otherBtn: [tapingRateBtn2, tapingRateBtn3])
                        break
                    }
                }
            } else {
                setSelectedBtnColors(selectedBtn: tapingRateBtn1, otherBtn: [tapingRateBtn2, tapingRateBtn3])
                selectedTapPrice = 5
                hideShowEditView(isOn: false, currentView: tapRatingVIew)
            }
            
            if tappingData.is_edit! {
                hideShowEditView(isOn: true, currentView: editingRateView)
                if let rate = tappingData.editing_rate {
                    switch rate{
                    case "25":
                        selectedEditedPrice = 25
                        setSelectedBtnColors(selectedBtn: editingRateBtn2, otherBtn: [editingRateBtn1, editingRateBtn3])
                        break
                    case "30":
                        selectedEditedPrice = 30
                        setSelectedBtnColors(selectedBtn: editingRateBtn3, otherBtn: [editingRateBtn2, editingRateBtn1])
                        break
                    default:
//                        selectedEditedPrice = 20
                        setSelectedBtnColors(selectedBtn: editingRateBtn1, otherBtn: [editingRateBtn2, editingRateBtn3])
                        break
                    }
                }
            } else {
                setSelectedBtnColors(selectedBtn: editingRateBtn1, otherBtn: [editingRateBtn2, editingRateBtn3])
                selectedEditedPrice = 20
                hideShowEditView(isOn: false, currentView: editingRateView)
            }
            
            //            tableView.translatesAutoresizingMaskIntoConstraints = false
            
            if let serviceArr = tappingData.service_detail {
                serviceDetailArr = serviceArr
                if tappingData.is_taping! || tappingData.is_edit! {

                    let totalTableHeight = CGFloat(serviceArr.count * 50)
                    tableViewConstant.constant = totalTableHeight + 20
                    self.tableView.isHidden = false
                    tableView.reloadData()
                } else {
                    tableViewConstant.constant = 0
                    
                }
            } else {
                tableViewConstant.constant = 0
                //                tableView.heightAnchor.constraint(equalToConstant: 0).isActive = true
            }
        }
    }
    
    func getAllServices() {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        print("TokenHeader:- \(tokenHeader)")
        alamofireManager.request(getServiceNameForUser, method: .post, parameters: [:], encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        if let data = value.tappingServiceResponse {
                            self.tappingSerivceData = data
                            self.setServiceData()
                        }
                        break
                    case 400:
                        alert.hideView()
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.getAllServices()
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                alert.hideView()
                SCLAlertView().showError(errorTitle, subTitle: serverError)
                if self.errorView != nil {
                    if self.errorView.isHidden {
                        self.errorView.isHidden = false
                        self.tableViewConstant.constant = 0
                        self.tableView.isHidden = true
                    } else {
                        self.tableViewConstant.constant = 0
                        self.tableView.isHidden = true
                        self.addErrorView()
                    }
                } else {
                    self.tableViewConstant.constant = 0
                    self.tableView.isHidden = true
                    self.addErrorView()
                }
                print("Request failed with error: \(error.localizedDescription)")
                break                
            }
            }.responseJSON { response in
                print("Tapping services:- \(response.result.value)")
        }
    }
    
//    @IBAction func toggleClicked(_ sender: Any) {
    func valueChanged(sender: SwiftySwitch) {
        let btn = sender as! SwiftySwitch

//        let btn = sender as! UISwitch
        switch btn {
        case tapingSwitchBtn:
//            if tapingSwitchBtn.isOn {
//                hideShowEditView(isOn: true, currentView: tapRatingVIew)
//            } else {
//                hideShowEditView(isOn: false, currentView: tapRatingVIew)
//            }
            hideShowEditView(isOn: tapingSwitchBtn.isOn, currentView: tapRatingVIew)

            break
        default:
            if editingSwitchBtn.isOn && !tapingSwitchBtn.isOn{
                // "If you edit, you should automatically be getting paid to tape, so taping services should be included."
                SCLAlertView().showInfo("Note", subTitle: "Please include taping services if you already offer editing.", closeButtonTitle: "Ok", colorStyle: int_red)
                hideShowEditView(isOn: editingSwitchBtn.isOn, currentView: editingRateView)
                hideShowEditView(isOn: !tapingSwitchBtn.isOn, currentView: tapRatingVIew)

            } else {
                hideShowEditView(isOn: editingSwitchBtn.isOn, currentView: editingRateView)

            }
            break
        }
    }
    
    func hideShowEditView(isOn: Bool, currentView: UIView) {
        switch currentView {
        case tapRatingVIew:
            tapRatingVIew.isHidden = !isOn
//            tapingSwitchBtn.setOn(isOn, animated: true)
            self.tapingSwitchBtn.isOn = isOn

            if isOn {
                setBtnTextColor(currentLbl: self.tapingYesNoLbl, alignment: .left, color: UIColor.black, text : "Yes")

                tapingRateTopConstant.constant = 15
                if UIDevice.current.userInterfaceIdiom == .pad {
                    tapRatingConstant.constant = 140
                } else {
                    tapRatingConstant.constant = 110
                }
            } else {
                setBtnTextColor(currentLbl: self.tapingYesNoLbl, alignment: .right, color: UIColor.black, text : "No")

                tapRatingConstant.constant = 0
                tapingRateTopConstant.constant = 0
            }
        default:
            editingRateView.isHidden = !isOn
//            editingSwitchBtn.setOn(isOn, animated: true)
            self.editingSwitchBtn.isOn = isOn

            if isOn {
                setBtnTextColor(currentLbl: self.editingYesNoLbl, alignment: .left, color: UIColor.black, text : "Yes")

                editingRateTopConstant.constant = 15
                if UIDevice.current.userInterfaceIdiom == .pad {
                    editRatingConstant.constant = 140
                } else {
                    editRatingConstant.constant = 110
                }

            } else {
                setBtnTextColor(currentLbl: self.editingYesNoLbl, alignment: .right, color: UIColor.black, text : "No")

                editingRateTopConstant.constant = 0
                editRatingConstant.constant = 0
            }
        }
        
        if tapingSwitchBtn.isOn || editingSwitchBtn.isOn {
            if tableViewConstant.constant == 0 {
                tableView.isHidden = false
                tableViewTopCOnstant.constant = 15
                let totalTableHeight = CGFloat(serviceDetailArr.count * 50)
                tableViewConstant.constant = totalTableHeight + 20
                tableView.reloadData()
            }
        } else {
            tableViewConstant.constant = 0
            tableView.isHidden = true
            tableViewTopCOnstant.constant = 0
        }
    }
    
    @IBAction func addServiceBtnClicked(_ sender: Any) {
        let isTapping = tapingSwitchBtn.isOn
        let isEditing = editingSwitchBtn.isOn
        let alertView = SCLAlertView()

        if selectedServiceId.isEmpty &&  (isTapping || isEditing) {
            alertView.showError("Error", subTitle: "Please select at least one service." )
        } else {
            
            if isEditing && !isTapping{
                SCLAlertView().showInfo("Note", subTitle: "Please include taping services if you already offer editing.", closeButtonTitle: "Ok", colorStyle: int_red)

            } else {
                addTappingServiceAPI(isTapping: ((isTapping) ? 1 : 0), isEditing: ((isEditing) ? 1 : 0), tappingRate: ((isTapping) ? selectedTapPrice : 0), editingRate: ((isEditing) ? selectedEditedPrice : 0), selectedServices: ((!isEditing && !isTapping) ? "" : selectedServiceId.joined(separator: ",")))
            }
        }
        
//        if isTapping {
////            if selectedTapPrice != nil {
//                if isEditing {
//                        if selectedServiceId.isEmpty {
//                            alertView.showError("Error", subTitle: "Please select at least one service." )
//                        } else {
//                            addTappingServiceAPI(isTapping: 1, isEditing: 1, tappingRate: selectedTapPrice, editingRate: selectedEditedPrice, selectedServices: selectedServiceId.joined(separator: ","))
//                        }
//                } else {
//                    if selectedServiceId.isEmpty {
//                        alertView.showError("Error", subTitle: "Please select at least one service." )
//                    } else {
//                        addTappingServiceAPI(isTapping: 1, isEditing: 0, tappingRate: selectedTapPrice, editingRate: 0, selectedServices: selectedServiceId.joined(separator: ","))
//                    }
//                }
//        } else {
//            if isEditing {
//                    if selectedServiceId.isEmpty {
//                        alertView.showError("Error", subTitle: "Please select at least one service." )
//                    } else {
//                        //                            let services = selectedServiceId.joined(separator: ",")
//                        addTappingServiceAPI(isTapping: 0, isEditing: 1, tappingRate: 0, editingRate: selectedEditedPrice, selectedServices: selectedServiceId.joined(separator: ","))
//                    }
//            } else {
////                alertView.showError("Error", subTitle: "Please select taping or edited feature." )
//                addTappingServiceAPI(isTapping: 0, isEditing: 0, tappingRate: 0, editingRate: 0, selectedServices: "")
//
//            }
//        }
    }
    
    @IBAction func skipBtnClicked(_ sender: Any) {
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "AddVideoVC") as! AddVideoVC
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func addTappingServiceAPI(isTapping: Int, isEditing: Int, tappingRate: Int, editingRate: Int, selectedServices: String){
        
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)

        let user = TappingServiceRequestModel(is_taping: isTapping, taping_rate: tappingRate, is_edit: isEditing, editing_rate: editingRate, service_id: selectedServices)
        let data_temp = Mapper<TappingServiceRequestModel>().toJSON(user)
        print("addTappingServiceAPI request:- \(data_temp)")

        //Call api
        alamofireManager.request(addTapingForUser, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in

            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        if self.isUpdateServices {
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            let story = UIStoryboard(name: "Main", bundle: nil)
                            let nextVC = story.instantiateViewController(withIdentifier: "AddVideoVC") as! AddVideoVC
                            self.navigationController?.pushViewController(nextVC, animated: true)
                        }
                        break
                    case 400:
                        alert.hideView()
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.addTappingServiceAPI(isTapping: isTapping, isEditing: isEditing, tappingRate: tappingRate, editingRate: editingRate, selectedServices: selectedServices)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")

                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
    
    //Set NavigationBar
    func configureNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "OpenSans-Semibold", size: 18.0)]
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        let navigationItem = self.navigationItem
        navigationItem.title = "TAPING / EDITING SERVICES"
        //        if let name = defaults.value(forKey: "first_name") as? String {
        ////            navigationItem.title = name + "'s skill set"
        //            //name.uppercased() + "'S SKILL SET"
        //        }
        
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

extension TappingServiceVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serviceDetailArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TappingCell
        
        let item = serviceDetailArr[indexPath.row]
        cell.tappingLblTxt.text = item.service_name
        
        if item.is_active! {
            if !selectedServiceId.contains("\(item.service_id!)") {
                selectedServiceId.append("\(item.service_id!)")
            }
            cell.checkBoxImg.image = UIImage(named: "ic_selectedCheckBox")
        } else {
            cell.checkBoxImg.image = UIImage(named: "ic_uncheckedBox")
        }
        
        cell.tappingBtn.tag = indexPath.row
        cell.tappingBtn.addTarget(self, action: #selector(self.tapingFeatureSelection(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    @objc func tapingFeatureSelection(_ sender: UIButton) {
        print("sender tag:- \(sender.tag)")
        let tag = sender.tag
        let indexpath = IndexPath(row: tag, section: 0)
        let cell = tableView.cellForRow(at: indexpath) as! TappingCell
        let currentId = serviceDetailArr[tag].service_id!
        if selectedServiceId.contains("\(currentId)") {
            selectedServiceId.remove(at: selectedServiceId.index(of: "\(currentId)")!)
            if serviceDetailArr[tag].is_active! {
                serviceDetailArr[tag].is_active = false
            }
            cell.checkBoxImg.image = UIImage(named: "ic_uncheckedBox")
        } else {
            selectedServiceId.append("\(currentId)")
            if !serviceDetailArr[tag].is_active! {
                serviceDetailArr[tag].is_active = true
            }
            cell.checkBoxImg.image = UIImage(named: "ic_selectedCheckBox")
        }
    }
}
