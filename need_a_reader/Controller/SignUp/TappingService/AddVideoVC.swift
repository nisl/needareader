//
//  AddVideoVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 5/30/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import TOCropViewController
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import AVFoundation
import ReachabilitySwift
import SCLAlertView
import AVFoundation
import AVKit
import SDWebImage

class AddVideoVC: UIViewController, TOCropViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var scrollview: UIScrollView!
    @IBOutlet var imageouterView: UIView!
    @IBOutlet var outerview: UIView!
    @IBOutlet var profileOuterImg: UIImageView!
    @IBOutlet var noteLbl: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var paymentBtn: UIButton!
    @IBOutlet var skipBtn: UIButton!
    @IBOutlet var profileImg: UIImageView!
    @IBOutlet var addImageOuterView: UIView!
    
    var currentShapeLayer:CAShapeLayer = CAShapeLayer()
    var imagePicker : UIImagePickerController!
    var selectedVideoURL : URL!
    var isStripeSetupWithPayment : Bool = false
    var userDetail : User_detail!
    var isFromSetting: Bool = false
    //AVPlayer
    var videoPlayerVC = AVPlayerViewController()
    var videoPlayer : AVPlayer?
    var videoDetail: VideoDetailModel!
    var errorView : NoDataView!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        
        if (Reachability()?.isReachable)! {
            if userDetail != nil {
                setBtnTitle(userDetail: userDetail)
            } else {
                setBtnTitle(userDetail: current_user_detail!)
            }
        } else {
            noInternetMsgDialog()
            self.addErrorView()
        }
        
        activityIndicator.startAnimating()
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        if UIDevice.current.userInterfaceIdiom == .pad {
            paymentBtn.frame.size.height = 70
            skipBtn.frame.size.height = 70
        }
        viewCorners.addShadowToView(view: profileOuterImg, value: 2.0)
        viewCorners.addCornerToView(view: paymentBtn)
        viewCorners.addCornerToView(view: skipBtn)
        if isFromSetting {
            skipBtn.isHidden = true
        }
        
        let formattedString = NSMutableAttributedString()
        formattedString.bold("Note:").normal(" One time fee is $1.99")
        self.noteLbl.attributedText = formattedString

        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isStripeSetupWithPayment {
            paymentBtn.setTitle("SAVE", for: .normal)
            if current_user_detail != nil {
                current_user_detail?.isStripeSetup = true
                current_user_detail?.video_payment_status = true
            } else if userDetail != nil {
                userDetail?.isStripeSetup = true
                userDetail?.video_payment_status = true

            }
            isStripeSetupWithPayment = false
            addVideoByUser()
        }
    }
    
    override func viewDidLayoutSubviews() {
        let path = UIBezierPath(roundedRect: profileImg.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 5.0, height: 5.0))
        let mask = CAShapeLayer()
        mask.frame = profileImg.bounds
        mask.path = path.cgPath
        profileImg.layer.mask = mask
    }
    
    func addErrorView() {
        let navHeight = self.navigationController?.navigationBar.frame.size.height
        errorView = NoDataView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height - navHeight!))
        errorView.backgroundColor = hexStringToUIColor(hex: "EEEEEE")
        errorView.errorIconsHidden = false
        errorView.configureView(message: serverError, image: UIImage(named: "ic_error")!)
//        errorView.tapButton.addTarget(self, action: #selector(self.errorButtonClicked), for: .touchUpInside)
        self.view.addSubview(errorView)
        self.errorView.isHidden = false
    }
    
   
    
    func setBtnTitle(userDetail: User_detail) {
        if videoDetail != nil {
                if let img = videoDetail.video_thumbnail_img {
                    self.profileImg.sd_setShowActivityIndicatorView(true)
                    self.profileImg.sd_setIndicatorStyle(.gray)
                    self.profileImg.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "app_image_loader"))
                    self.addImageOuterView.isHidden = true
                }
        }
        if (userDetail.isStripeSetup)! {
            if (userDetail.video_payment_status)! {
                paymentBtn.setTitle("SAVE", for: .normal)
            } else {
                paymentBtn.setTitle("PAY $\(videoPaymentAmount)", for: .normal)
            }
        } else {
            paymentBtn.setTitle("PAY $\(videoPaymentAmount)", for: .normal)
        }
    }
    
    @IBAction func addImgVideoBtnClicked(_ sender: Any) {
        //       self.openCameraDialog(isImageSelected: false)
    }
    
    func setUI() {
        imageouterView.frame.size.height = self.view.frame.size.height * 0.7
        profileImg.frame.size.height = self.imageouterView.frame.size.height * 0.75
        
        self.addImageOuterView.frame.size.height = self.profileImg.frame.size.height * 0.6
        self.addImageOuterView.frame.size.width = self.profileImg.frame.size.width * 0.8
        self.addImageOuterView.center = self.profileImg.center
        
        self.addImageOuterView.layoutIfNeeded()
        currentShapeLayer =  self.addImageOuterView.addDashedBorder()
    }
    
    @IBAction func cameraBtnClicked(_ sender: Any) {
        choosePlayOrUpdateVideo()
    }
    
    func choosePlayOrUpdateVideo() {
        if videoDetail != nil {
            if videoDetail.video_file != nil {
                openOptionDialog(videoFile: videoDetail.video_file!)
            } else {
                openCameraDialog(isImageSelected: false)
            }
        } else {
            openCameraDialog(isImageSelected: false)
        }
    }
    
    func openOptionDialog(videoFile: String) {
        let alert = UIAlertController(title: "Please select option", message: "", preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: 64, width: screenWidth, height: 0)
        alert.addAction(UIAlertAction(title: "View Video", style: .default, handler: { (success) in
            
            self.videoPlayerVC.view.frame = self.view.bounds
            self.videoPlayer = AVPlayer(url: URL(string: videoFile)!)
            self.videoPlayerVC.player = self.videoPlayer
            self.present(self.videoPlayerVC, animated: true, completion: {
                self.videoPlayer?.play()
            })
        }))
        alert.addAction(UIAlertAction(title: "Edit Video", style: .default, handler: { (success) in
            self.openCameraDialog(isImageSelected: false)
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: { (success) in
        }))
        //        alert.view.tintColor = UIColor(constant: .BlueBlack)
        present(alert, animated: true, completion: nil)
        
    }
    
    func openCameraDialog(isImageSelected: Bool) {
        let title = "Choose Video From"
        let subTitle = "Take video"
        
        let alert = UIAlertController(title: title, message: "", preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: 64, width: screenWidth, height: 0)
        alert.addAction(UIAlertAction(title: subTitle, style: .default, handler: { (success) in
            
            self.imagePicker.sourceType = .camera
            self.imagePicker.mediaTypes = [kUTTypeMovie as NSString as String]
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "Photo library", style: .default, handler: { (success) in
            self.imagePicker.sourceType = .photoLibrary
            if !isImageSelected {
                self.imagePicker.mediaTypes = ["public.movie"]//"public.image",
            }
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: { (success) in
        }))
        //        alert.view.tintColor = UIColor(constant: .BlueBlack)
        present(alert, animated: true, completion: nil)
    }
    
    //    #MARK: ImagePicker Delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedVideoUrl = info[UIImagePickerControllerMediaURL] as? URL {
            selectedVideoURL = pickedVideoUrl as URL//"\(pickedVideoUrl)"
            generateThumbImage(url : pickedVideoUrl as URL)
            //            print("pickedVideourl:- \(pickedVideoUrl)")
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func generateThumbImage(url : URL) {
        self.addImageOuterView.isHidden = true
        
        DispatchQueue.global().async {
            
            let asset : AVAsset = AVAsset(url: url)
            
            let duration = asset.duration
            
            let durationTime = CMTimeGetSeconds(duration)
            if durationTime < 600 {
                //                self.currentShapeLayer.removeFromSuperlayer()
                let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
                assetImgGenerate.appliesPreferredTrackTransform = true
                var error : NSError? = nil
                let time = CMTimeMake(1, 2)
                let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                if img != nil {
                    
                    let frameImg  = UIImage(cgImage: img!)
                    DispatchQueue.main.async(execute: {
                        // assign your image to UIImageView
                        self.profileImg.image = frameImg
                    })
                }
            } else {
                DispatchQueue.main.async(execute: {
                    self.addImageOuterView.isHidden = false
                    let alertView = SCLAlertView()
                    alertView.showError("Error", subTitle: "Video duration must be less than or equal to 1 minute.")
                })
            }
        }
    }
    
    
    @IBAction func addVideoBtnClicked(_ sender: Any) {
        if selectedVideoURL != nil {
            if userDetail != nil {
                callVideoApi(userDetail: userDetail)
            } else {
                callVideoApi(userDetail: current_user_detail!)
            }
        } else {
            SCLAlertView().showError("Error", subTitle: "Please select Video")
        }
    }
    
    func callVideoApi(userDetail: User_detail) {
        if (userDetail.isStripeSetup)! {
            if (userDetail.video_payment_status)! {
                addVideoByUser()
            } else {
                let nextvc = storyBoard.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
                nextvc.isFromAddVideo = true
                self.navigationController?.pushViewController(nextvc, animated: true)
            }
        } else {
            addVideoByUser()
        }
    }
    
    func addVideoByUser() {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        var param : [String : String] = [:]
        param = ["request_data": ""]
        
        let URL = try! URLRequest(url: uploadVideo, method: .post, headers: tokenHeader)
        let session = URLSession(configuration: .default)
        
        alamofireManager.upload(multipartFormData:  { multipartFormData in
            do {
                
                let movieData = try Data(contentsOf: self.selectedVideoURL.absoluteURL, options: .alwaysMapped)
                multipartFormData.append(movieData, withName: "video_file", fileName: "file.mp4", mimeType: "video/mp4")
                
            }catch {
                print("error:- \(error)")
            }
            
        }, with: URL) { encodingResult in
            //, to: setHeadShotForUser, method: .post, headers: tokenHeader, encodingCompletion: { encodingResult in
            
            switch encodingResult
            {
            case .success(let upload,_,_):
                upload.responseObject { (response: DataResponse<ResponseModel> )in
                    switch response.result {
                    case .success(let value):
                        if let code = value.code {
                            switch code {
                            case 200:
                                alert.hideView()
                                if let msg = value.message {
                                    SCLAlertView().showSuccess("Success", subTitle: msg,  colorStyle: int_red)
                                }
                                if self.isFromSetting {
                                    defaults.set(true, forKey: "updateUserInfo")

                                    self.navigationController?.popViewController(animated: true)
                                } else {
                                    let nextVC = storyBoard.instantiateViewController(withIdentifier: "MyTabBarViewController")
                                    self.navigationController?.present(nextVC, animated: true)
                                }
                                break
                            case 202:
                                alert.hideView()
                                if let msg = value.message {
                                    let alertview = UIAlertController(title: "Info", message: msg, preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                        //                                        self.isFromWebView = true
                                        let story = UIStoryboard(name: "Main", bundle: nil)
                                        let nextVC = story.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                                        nextVC.vcName = "AddVideo"
                                        //                        nextVC.userId = rece_id
                                        self.navigationController?.pushViewController(nextVC, animated: true)
                                    }
                                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                                    alertview.addAction(okAction)
                                    alertview.addAction(cancelAction)
                                    self.present(alertview, animated: true, completion: nil)
                                }
                                break
                            case 400:
                                alert.hideView()
                                clearSession()
                                let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                let nav = redirectToRootViewController(vcName: vc)
                                self.present(nav, animated: true)
                                break
                            case 401:
                                alert.hideView()
                                if let new_token = value.signUpResponse?.new_token {
                                    UserDefaults.standard.set(new_token, forKey: "token")
                                    self.addVideoByUser()
                                    
                                } else {
                                    clearSession()
                                    let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                    let nav = redirectToRootViewController(vcName: vc)
                                    self.present(nav, animated: true)
                                }
                                break
                                
                            default:
                                alert.hideView()
                                if let error = value.message {
                                    SCLAlertView().showError("Error", subTitle: error)
                                }
                                break
                            }
                        }
                        break
                    case .failure(let error):
                        print("error: \(error.localizedDescription)")
                        alert.hideView()
                        SCLAlertView().showError("Error", subTitle: serverError)
                        break
                    }
                    }.responseJSON { response in
                        print("response add video:- \(response.result.value)")
                }
            case .failure(let encodingError):
                alert.hideView()
                SCLAlertView().showError("Error", subTitle: encodingError.localizedDescription)
                //print("Request failed with error: \(error)")
                break
            }
        }
    }
    
    //Set NavigationBar
    func configureNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "OpenSans-Semibold", size: 18.0)]
        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        let navigationItem = self.navigationItem
        navigationItem.title = ""
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    
    @IBAction func skipBtnClicked(_ sender: Any) {
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "MyTabBarViewController")
        self.navigationController?.present(nextVC, animated: true)

    }
    
}


extension UIView {
    func addDashedBorder() -> CAShapeLayer {
        let color = hexStringToUIColor(hex: redColor).cgColor
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1.5
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
        self.layer.addSublayer(shapeLayer)
        return shapeLayer
    }
}
