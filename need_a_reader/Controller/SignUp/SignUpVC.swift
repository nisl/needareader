//
//  SignUpVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/15/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SCLAlertView
import AlamofireObjectMapper
import ObjectMapper
import ReachabilitySwift

class SignUpVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    @IBOutlet var lastnameTxt: UITextField!
    @IBOutlet var scrollview: UIScrollView!
    @IBOutlet var cPasswordView: UIView!
    @IBOutlet var passwordView: UIView!
    @IBOutlet var loginLbl: UILabel!
    @IBOutlet var nameTxt: UITextField!
    @IBOutlet var emailTxt: UITextField!
    @IBOutlet var passwordTxt: UITextField!
    @IBOutlet var confirmPasswordTxt: UITextField!
    @IBOutlet var genderTxt: UITextField!
    @IBOutlet var phoneNoTxt: UITextField!
    //    @IBOutlet var dateOfBirthTxt: UITextField!
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var submitBtn: UIButton!
    @IBOutlet var backBtn: UIButton!
    
    @IBOutlet var loginStack: UIStackView!
    @IBOutlet var genderView: UIView!
    @IBOutlet var genderLbl: UILabel!
    @IBOutlet var passwordLbl: UILabel!
    @IBOutlet var confirmPasswordLbl: UILabel!
    
    @IBOutlet var viewConstant: NSLayoutConstraint!
    @IBOutlet var emailView: UIView!
    @IBOutlet var passwordConstant: NSLayoutConstraint!
    @IBOutlet var confirmPasswordConstant: NSLayoutConstraint!
    var isUpdateProfile : Bool = false
    var pickerView : UIPickerView?
    var genderArr = ["Male", "Female"]
    var datePicker : UIDatePicker?
    let toolBar = UIToolbar()
    var userdetail : User_detail!
    var isFromLogin : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        
        configureNavigationBar()
        //        self.view.backgroundColor = hexStringToUIColor(hex: "E4E1DA")
        if UIDevice.current.userInterfaceIdiom == .pad {
            submitBtn.frame.size.height = 70
        }
        viewCorners.addBorderCornerTobutton(view: submitBtn, color: UIColor.white)
        submitBtn.backgroundColor = UIColor.white
        submitBtn.setTitleColor(hexStringToUIColor(hex: redColor), for: .normal)
        loginLbl.textColor = hexStringToUIColor(hex: redColor)
        pickerView = UIPickerView()
        pickerView?.dataSource = self
        pickerView?.delegate = self
        
        datePicker = UIDatePicker()
        
        genderTxt.inputView = pickerView
        //        setDatePicker()
        //        dateOfBirthTxt.inputView = datePicker
        //        dateOfBirthTxt.inputAccessoryView = toolBar
        //        dateOfBirthTxt.delegate = self
        genderTxt.delegate = self
        phoneNoTxt.delegate = self
        setUI()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    func setUI() {
        if isUpdateProfile {
            submitBtn.setTitle("SAVE", for: .normal)
            changeButtonColor(view: [submitBtn], backgroundcolor: redColor, fontColor: "ffffff")
            passwordConstant.constant = 0
            passwordView.isHidden = true
            cPasswordView.isHidden = true
            confirmPasswordConstant.constant = 0
            passwordLbl.isHidden = true
            confirmPasswordLbl.isHidden = true
            genderLbl.translatesAutoresizingMaskIntoConstraints = false
            view.addConstraint(NSLayoutConstraint(item: genderLbl, attribute: .top, relatedBy: .equal, toItem: emailView, attribute: .bottom, multiplier: 1, constant: 15))
            loginStack.isHidden = true
            loginBtn.isHidden = true
            scrollview.isScrollEnabled = false
            emailTxt.isUserInteractionEnabled = false

            if userdetail != nil {
                if let name = userdetail.user_name {
                    nameTxt.text = name
                }
                if let lastName = userdetail.last_name {
                    lastnameTxt.text = lastName
                }
                if let email = userdetail.email_id {
                    emailTxt.text = email
                }
                if let phone = userdetail.contact_no {
                    phoneNoTxt.text = phone
                }
                if let gender = userdetail.gender {
                    genderTxt.text = setStringForGenderForUpdateProfile(gendertype: gender)
                    
                }
            }
        }
    }
    //    func setDatePicker() {
    ////        datePicker.frame = CGRect(x: 0, y: self.view.frame.height - 250, width: self.view.frame.width, height: 250)
    //        datePicker?.datePickerMode = .date
    //
    //        datePicker?.backgroundColor = UIColor.white
    //        datePicker?.addTarget(self, action: #selector(self.dateChangedInDate), for: .valueChanged)
    //
    //        // Creates the toolbar
    //        toolBar.barStyle = .default
    //        toolBar.isTranslucent = true
    //
    //        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
    //        toolBar.sizeToFit()
    //        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40)
    //        var doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.dismissPicker))
    //
    //        toolBar.layer.cornerRadius = 5.0
    //        toolBar.layer.shadowOpacity = 0.5
    //        toolBar.backgroundColor = hexStringToUIColor(hex: redColor)
    //        toolBar.setItems([doneButton], animated: false)
    //        doneButton.tintColor = UIColor.white
    //        toolBar.isUserInteractionEnabled = true
    //        //   toolBar.bringSubview(toFront: doneButton)
    ////        self.view.addSubview(toolBar)
    ////        self.view.addSubview(datePicker)
    //    }
    
    //    @objc func dateChangedInDate(sender:UIDatePicker){
    //        let dateFormatter = DateFormatter()
    //        dateFormatter.dateFormat = "yyyy-MM-dd"
    //        let selectedTime = dateFormatter.string(from: sender.date)
    //        print("date:- \(selectedTime)")
    //        dateOfBirthTxt.text = selectedTime
    //    }
    /*
     * MARK - dismiss the date picker value
     */
    //    @objc func dismissPicker(sender: UIButton) {
    ////        datePicker?.removeFromSuperview()
    ////        toolBar.removeFromSuperview()
    //        dateOfBirthTxt.resignFirstResponder()
    //    }
    @IBAction func submitBtnClicked(_ sender: Any) {
        //        submitBtn.backgroundColor = UIColor.white
        //        submitBtn.setTitleColor(hexStringToUIColor(hex: redColor), for: .normal)
        self.view.endEditing(true)
        if isUpdateProfile {
            defaults.set(true, forKey: "updateUserInfo")
            testUpdateProfile()
        } else {
            testValidation()
            
        }
    }
    
    @IBAction func loginBtnclicked(_ sender: Any) {
        self.view.endEditing(true)
        if isFromLogin {
            self.navigationController?.popViewController(animated: true)
        }else {
            let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            vc.isFromSignUp = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    //Set NavigationBar
    func configureNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "OpenSans-Semibold", size: 18.0)]
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        let navigationItem = self.navigationItem
        navigationItem.title = "SIGN UP"
        self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: "F5F3F4")
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        if isUpdateProfile {
            navigationItem.title = "PROFILE"
        }
    }
    
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: PickerView Delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderArr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderArr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderTxt.text = genderArr[row]
    }
    
    func testValidation()  {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        let fullname = nameTxt.text!
        let email = emailTxt.text!
        let password = passwordTxt.text!
        let cPassword = confirmPasswordTxt.text!
        let gender = genderTxt.text!
        let phoneNumber = phoneNoTxt.text!
        let lastName = lastnameTxt.text!
        
        if !checkLength(testStr: fullname) {
            alert.hideView()
            emptyFieldError(str: "first name.")
        }else if !checkLength(testStr: email){
            alert.hideView()
            emptyFieldError(str: "email.")
        } else if !checkLength(testStr: password) {
            alert.hideView()
            emptyFieldError(str: "password.")
        } else if !checkLength(testStr: gender) {
            alert.hideView()
            emptyFieldError(str: "gender.")
        } else if isValidEmail(testStr: email) {
            if validatePassword(testStr: password)
            {
                if password != cPassword {
                    alert.hideView()
                    let alertView = SCLAlertView()
                    alertView.showError("Error", subTitle: "Password mismatch!", closeButtonTitle: "Ok")
                } else {
                    if (Reachability()?.isReachable)! {
                        if phoneNumber != "" {
                            self.signUpUser(full_name: fullname, email_id: email, password: password, gender: gender, phone_no: phoneNumber, last_name: lastName)
                        }else {
                            self.signUpUser(full_name: fullname, email_id: email, password: password, gender: gender, phone_no: phoneNumber, last_name: lastName)
                        }
                        
                    } else {
                        alert.hideView()
                        noInternetMsgDialog()
                    }
                }
            }else
            {
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: passwordlengthMessage, closeButtonTitle: "Ok")
                
            }
        }else {
            alert.hideView()
            let alertview = SCLAlertView()
            alertview.showError("Error", subTitle: invalidEmailMessage, closeButtonTitle: "Ok")
        }
        
    }
    
    func testUpdateProfile() {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        let fullname = nameTxt.text!
        let password = passwordTxt.text!
        let cPassword = confirmPasswordTxt.text!
        let gender = genderTxt.text!
        let phoneNumber = phoneNoTxt.text!
        let lastName = lastnameTxt.text!
        
        if !checkLength(testStr: fullname) {
            alert.hideView()
            emptyFieldError(str: "first name.")
        }else if !checkLength(testStr: gender) {
            alert.hideView()
            emptyFieldError(str: "gender.")
        } else {
            if (Reachability()?.isReachable)! {
                updateUserProfile(full_name: fullname, gender: gender, phone_no: phoneNumber, last_name: lastName)
            } else {
                alert.hideView()
                noInternetMsgDialog()
            }
        }
    }
    
    func signUpUser(full_name: String, email_id: String, password: String, gender: String, phone_no: String, last_name: String){
        var genderValue : Int = 0
        //        if gender == "Male" {
        //            genderValue = 1
        //        } else {
        //            genderValue = 2
        //        }
        genderValue = setIntValueForGender(gendertype: gender)
        let user = SignUpRequestModel(first_name: full_name, email_id: email_id, password: password, gender: genderValue, signup_type: 1, contact_no: phone_no, last_name: last_name)
        let data_temp = Mapper<SignUpRequestModel>().toJSON(user)
        print("signUp request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(doSignUpUser, method: .post, parameters: data_temp, encoding: JSONEncoding(options: [])).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        defaults.set(value.signUpResponse?.user_registration_temp_id, forKey: "signUpId")
                        
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let nextVC = story.instantiateViewController(withIdentifier: "VerifyOtpVC") as! VerifyOtpVC
                        nextVC.userName = full_name
                        defaults.set(full_name, forKey: "user_name")
                        self.navigationController?.pushViewController(nextVC, animated: true)
                        
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.signUpUser(full_name: full_name, email_id: email_id, password: password, gender: gender, phone_no: phone_no, last_name: last_name)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNoTxt {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            
            return updatedText.count <= 13
        } else if textField == nameTxt {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            
            return updatedText.count <= 50
        } else if textField == passwordTxt {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            
            return updatedText.count <= 255
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == genderTxt {
            if textField.text == "" {
                textField.text = "Male"
            }
        }
    }
    func updateUserProfile(full_name: String, gender: String, phone_no: String,last_name: String){
        var genderValue : Int = 0
        //        if gender == "Male" {
        //            genderValue = 1
        //        } else {
        //            genderValue = 2
        //        }
        genderValue = setIntValueForGender(gendertype: gender)
        
        let user = SignUpRequestModel(first_name: full_name, gender: genderValue, contact_no: phone_no,  last_name: last_name)
        let data_temp = Mapper<SignUpRequestModel>().toJSON(user)
        print("signUp request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(updateUserProfileByUser, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        updateUserNameForCurrentUser(name: full_name+" "+last_name)
                        if let msg = value.message {
                            let alertView = SCLAlertView()
                            alertView.showSuccess("Success", subTitle: msg, closeButtonTitle: "Ok",  colorStyle: int_red)
                        }
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let nextVC = story.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
                        nextVC.userName = full_name
                        defaults.set(full_name, forKey: "user_name")
                        //                        self.navigationController?.pushViewController(nextVC, animated: true)
                        self.navigationController?.popViewController(animated: true)
                        
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.updateUserProfile(full_name: full_name, gender: gender, phone_no: phone_no,last_name: last_name)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
}
