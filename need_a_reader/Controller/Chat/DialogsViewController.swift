//
//  DialogsTableViewController.swift
//  sample-chat-swift
//
//  Created by Anton Sokolchenko on 4/1/15.
//  Copyright (c) 2015 quickblox. All rights reserved.
//

import UIKit

class DialogTableViewCellModel: NSObject {
    
    var detailTextLabelText: String = ""
    var textLabelText: String = ""
    var unreadMessagesCounterLabelText : String?
    var unreadMessagesCounterHiden = true
    var dialogIcon : UIImage?

    init(dialog: QBChatDialog) {
        super.init()
		
		switch (dialog.type){
		case .publicGroup:
			self.detailTextLabelText = "SA_STR_PUBLIC_GROUP".localized
		case .group:
			self.detailTextLabelText = "SA_STR_GROUP".localized
		case .private:
			self.detailTextLabelText = "SA_STR_PRIVATE".localized
			
			if dialog.recipientID == -1 {
				return
			}
			
			// Getting recipient from users service.
			if let recipient = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(dialog.recipientID)) {
				self.textLabelText = recipient.fullName ?? recipient.email!
			}
		}
        
        if self.textLabelText.isEmpty {
            // group chat
            
            if let dialogName = dialog.name {
                self.textLabelText = dialogName
            }
        }
        
        // Unread messages counter label
        
        if (dialog.unreadMessagesCount > 0) {
            
            var trimmedUnreadMessageCount : String
            
            if dialog.unreadMessagesCount > 99 {
                trimmedUnreadMessageCount = "99+"
            } else {
                trimmedUnreadMessageCount = String(format: "%d", dialog.unreadMessagesCount)
            }
            
            self.unreadMessagesCounterLabelText = trimmedUnreadMessageCount
            self.unreadMessagesCounterHiden = false
            
        }
        else {
            
            self.unreadMessagesCounterLabelText = nil
            self.unreadMessagesCounterHiden = true
        }
        
        // Dialog icon
        
        if dialog.type == .private {
            self.dialogIcon = UIImage(named: "user")
        }
        else {
            self.dialogIcon = UIImage(named: "group")
        }
    }
    
    
}

class DialogsViewController: UITableViewController, QMChatServiceDelegate, QMChatConnectionDelegate, QMAuthServiceDelegate {
    @IBOutlet weak var lblNoChats: UILabel!
    
    @IBOutlet weak var profilebtn: UIButton!
    @IBOutlet var navItem: UINavigationItem!
    private var didEnterBackgroundDate: NSDate?
    private var observer: NSObjectProtocol?
//    var dataSource: UsersDataSource!
//    var session: QBRTCSession!
//    var callUUID: UUID!
////    var nav: UINavigationController!
//    var backgroundTask: UIBackgroundTaskIdentifier!
//    var opponentIDs: [NSNumber] = []
//    var voipRegistry: PKPushRegistry!

    // MARK: - ViewController overrides
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // calling awakeFromNib due to viewDidLoad not being called by instantiateViewControllerWithIdentifier
//        self.backgroundTask = UIBackgroundTaskInvalid
//        self.navigationItem.title = QBCore.instance().currentUser.fullName!

//        self.navigationItem.title = "CHATS"


//        self.navigationItem.leftBarButtonItem = self.createLogoutButton()
//        QBRTCClient.instance().add(self)
//        QBCore.instance().add(self)
        ServicesManager.instance().chatService.addDelegate(self)
        
        ServicesManager.instance().authService.add(self)
//        self.voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
//        self.voipRegistry.delegate = self
//        self.voipRegistry.desiredPushTypes = [.voIP]

        self.observer = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, queue: OperationQueue.main) { (notification) -> Void in
            
            if !QBChat.instance.isConnected {
//                SVProgressHUD.show(withStatus: "SA_STR_CONNECTING_TO_CHAT".localized)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(DialogsViewController.didEnterBackgroundNotification), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        
     
//        if appDelegate.session != nil {
//            let vc = storyBoard.instantiateViewController(withIdentifier: "CallVC") as! CallVC
//            vc.session = appDelegate.session
//            vc.usersDataSource = appDelegate.dataSource
//            vc.callUUID = appDelegate.callUUID
//            let nav = UINavigationController(rootViewController: vc)
//            self.present(nav, animated: false, completion: nil)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.navigationController?.isNavigationBarHidden = true
        configureNavigationBar()
        self.view.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
        
//        let chatInfoBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
//        if #available(iOS 11.0, *) {
//            let widthConstraint = chatInfoBtn.widthAnchor.constraint(equalToConstant: 44)
//            let heightConstraint = chatInfoBtn.heightAnchor.constraint(equalToConstant: 44)
//            heightConstraint.isActive = true
//            widthConstraint.isActive = true
//            //            UIEdgeInsetsMake(, <#T##left: CGFloat##CGFloat#>, <#T##bottom: CGFloat##CGFloat#>, <#T##right: CGFloat##CGFloat#>)
//
//            chatInfoBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 0)
//        } else {
//            chatInfoBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 0)//UIEdgeInsetsMake(12, 0, 12, 24)
//        }
//        chatInfoBtn.setImage(UIImage(named: "ic_questionMark"), for: .normal)
//        chatInfoBtn.addTarget(self, action: #selector(chatInfoBtnClicked(_:)), for: .touchUpInside)
//        let chatInfoButton = UIBarButtonItem(customView: chatInfoBtn)
//
//        navigationItem.rightBarButtonItems = [chatInfoButton]

        appDelegate.dataSource = UsersDataSource(currentUser: QBCore.instance().currentUser)
        CallKitManager.instance.usersDatasource = appDelegate.dataSource
        appDelegate.getOpponentsUsers()
        
        self.tableView.reloadData()
        delete24HoursDialogs()
        if (QBChat.instance.isConnected) {
            self.getDialogs()
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//
//        if segue.identifier == "SA_STR_SEGUE_GO_TO_CHAT".localized {
//            if let chatVC = segue.destination as? ChatViewController {
//                chatVC.dialog = sender as? QBChatDialog
//            }
//        }
//    }
    
    @objc func chatBtnPressed(_ sender: UIButton) {
        let alertview = UIAlertController(title: "Chat note", message: "Chats disappear after 1 week.", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel)
        alertview.addAction(cancelAction)
        
        self.present(alertview, animated: true, completion: nil)
    }
    // MARK: - Notification handling
    
    @objc func didEnterBackgroundNotification() {
        self.didEnterBackgroundDate = NSDate()
    }
    
    // MARK: - Actions
    
    func createNewChatButton() -> UIBarButtonItem {
        
        let barButton = UIBarButtonItem(title: "New Chat", style: UIBarButtonItemStyle.plain, target: self, action: #selector(DialogsViewController.newChatAction))
        return barButton
    }

    //Set NavigationBar
    func configureNavigationBar()
    {
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        setNavigaion(vc: self)
        let navigationItem = self.navigationItem
        navigationItem.title = "CHATS"
        
        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        let chatInfoBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        let settingBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))

        if #available(iOS 11.0, *) {
            let widthConstraint = chatInfoBtn.widthAnchor.constraint(equalToConstant: 44)
            let heightConstraint = chatInfoBtn.heightAnchor.constraint(equalToConstant: 44)
            heightConstraint.isActive = true
            widthConstraint.isActive = true
            //            UIEdgeInsetsMake(, <#T##left: CGFloat##CGFloat#>, <#T##bottom: CGFloat##CGFloat#>, <#T##right: CGFloat##CGFloat#>)
            chatInfoBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 0)
            let settingBtnwidthConstraint = settingBtn.widthAnchor.constraint(equalToConstant: 44)
            let settingBtnheightConstraint = settingBtn.heightAnchor.constraint(equalToConstant: 44)
            settingBtnheightConstraint.isActive = true
            settingBtnwidthConstraint.isActive = true
            settingBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 8, 16)//UIEdgeInsetsMake(8, 0, 8, 16)

        } else {
            chatInfoBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 0)//UIEdgeInsetsMake(12, 0, 12, 24)
            settingBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 8, 16)//UIEdgeInsetsMake(8, 0, 6, 12)
        }
        chatInfoBtn.setImage(UIImage(named: "ic_questionMark"), for: .normal)
        chatInfoBtn.addTarget(self, action: #selector(chatInfoBtnClicked(_:)), for: .touchUpInside)
        let chatInfoButton = UIBarButtonItem(customView: chatInfoBtn)
        
        navigationItem.rightBarButtonItems = [chatInfoButton]
        settingBtn.setImage(UIImage(named: "ic_setting"), for: .normal)
        settingBtn.addTarget(self, action: #selector(self.settingBtnPressed(_:)), for: .touchUpInside)
        let settingButton = UIBarButtonItem(customView: settingBtn)
        navigationItem.leftBarButtonItem = settingButton

    }
    
    @objc func settingBtnPressed(_ sender: UIButton) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func newChatAction() {
//        let vc = storyBoard.instantiateViewController(withIdentifier: "NewDialogViewController") as! NewDialogViewController
//        vc.dataSource = appDelegate.dataSource
//        vc.session = appDelegate.session
////        vc.callUUID = self.callUUID
////        vc.opponentIDs = self.opponentIDs
//        self.navigationController?.pushViewController(vc, animated: true)
    }
        
    func createLogoutButton() -> UIBarButtonItem {
        
        let logoutButton = UIBarButtonItem(title: "SA_STR_LOGOUT".localized, style: UIBarButtonItemStyle.plain, target: self, action: #selector(DialogsViewController.logoutAction))
        return logoutButton
    }
    
    @IBAction func logoutAction() {
        
        QBCore.instance().logout()
        if !QBChat.instance.isConnected {

//            SVProgressHUD.showError(withStatus: "Error")
            return
        }
        
//        SVProgressHUD.show(withStatus: "SA_STR_LOGOUTING".localized)
//        ServicesManager.instance().logoutUserWithCompletion { [weak self] (boolValue) -> () in
//
//            guard let strongSelf = self else { return }
//            if boolValue {
//                NotificationCenter.default.removeObserver(strongSelf)
//
//                if strongSelf.observer != nil {
//                    NotificationCenter.default.removeObserver(strongSelf.observer!)
//                    strongSelf.observer = nil
//                }
//
//                ServicesManager.instance().chatService.removeDelegate(strongSelf)
//                ServicesManager.instance().authService.remove(strongSelf)
//
//                ServicesManager.instance().lastActivityDate = nil;
//                let _ = strongSelf.navigationController?.popToRootViewController(animated: true)
//
////                SVProgressHUD.showSuccess(withStatus: "SA_STR_COMPLETED".localized)
//            }
//        }
    }
	
    
    //QBCoreDelegate
    func coreDidLogout(_ core: QBCore) {
        NotificationCenter.default.removeObserver(self)
        
        if self.observer != nil {
            NotificationCenter.default.removeObserver(self.observer!)
            self.observer = nil
        }
        
        ServicesManager.instance().chatService.removeDelegate(self)
        ServicesManager.instance().authService.remove(self)
        
        ServicesManager.instance().lastActivityDate = nil;
        let _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    // MARK: - DataSource Action
	
    func getDialogs() {
		
        if let lastActivityDate = ServicesManager.instance().lastActivityDate {
			
			ServicesManager.instance().chatService.fetchDialogsUpdated(from: lastActivityDate as Date, andPageLimit: kDialogsPageLimit, iterationBlock: { (response, dialogObjects, dialogsUsersIDs, stop) -> Void in
				
				}, completionBlock: { (response) -> Void in
					
                    if (response.isSuccess) {
                        
                        ServicesManager.instance().lastActivityDate = NSDate()
                    }
			})
        }
        else {
            
//            SVProgressHUD.show(withStatus: "SA_STR_LOADING_DIALOGS".localized)
            ServicesManager.instance().chatService.fetchDialogsUpdated(from: Date().earlyDate, andPageLimit: kDialogsPageLimit, iterationBlock: { (response, dialogObjects, dialogsUsersIDs, stop) -> Void in
                
            }, completionBlock: { (response) -> Void in
                
                if (response.isSuccess) {
                    
                    ServicesManager.instance().lastActivityDate = NSDate()
                }
            })
//            ServicesManager.instance().chatService.allDialogs(withPageLimit: kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
//
//                }, completion: { (response: QBResponse?) -> Void in
//
//                    guard response != nil && response!.isSuccess else {
////                        SVProgressHUD.showError(withStatus: "SA_STR_FAILED_LOAD_DIALOGS".localized)
//                        return
//                    }
//
////                    SVProgressHUD.showSuccess(withStatus: "SA_STR_COMPLETED".localized)
//                    ServicesManager.instance().lastActivityDate = NSDate()
//            })
        }
    }

    // MARK: - DataSource
    
	func dialogs() -> [QBChatDialog]? {
        
        // Returns dialogs sorted by updatedAt date.
        let array = ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
        if array.count > 0 {
            self.lblNoChats.isHidden = true
        }
        print(array.count)
        print(array)
        return array
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if let dialogs = self.dialogs() {
			return dialogs.count
		}
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 64.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "dialogcell", for: indexPath) as! DialogTableViewCell
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero

        if ((self.dialogs()?.count)! < indexPath.row) {
            return cell
        }
        
        guard let chatDialog = self.dialogs()?[indexPath.row] else {
            return cell
        }
        
        cell.isExclusiveTouch = true
        cell.contentView.isExclusiveTouch = true
        
        cell.tag = indexPath.row
        cell.dialogID = chatDialog.id!
        
        let cellModel = DialogTableViewCellModel(dialog: chatDialog)
        print("occupantIds: \(chatDialog.occupantIDs)")
        var opponentId: UInt!
        if let value = chatDialog.occupantIDs {
            for item in value {
                if item.uintValue != QBCore.instance().currentUser.id {
                    opponentId = item.uintValue
                }
            }
        }
        var opponentUser = QBUUser()
        for i in AllQuickbloxUser {
            if opponentId == i.id {
               opponentUser = i
                break
            }
        }
        
        
       // let opponentUser = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: opponentId)
        print("opponentUser: \(opponentUser)")
        viewCorners.addCornerToView(view: cell.dialogTypeImage, value: 24)
        print("image url: \(imageBucketPath+"\(opponentId)")")
        if let customData = opponentUser.customData {
            cell.dialogTypeImage.sd_setShowActivityIndicatorView(true)
            cell.dialogTypeImage.sd_setIndicatorStyle(.gray)
            cell.dialogTypeImage.sd_setImage(with: URL(string: customData), placeholderImage: UIImage(named: "ic_defaultProfile"))
        }
        cell.dialogLastMessage?.text = chatDialog.lastMessageText
       // cell.dialogName?.text = opponentUser.fullName
        
        let stringName = opponentUser.fullName
        if stringName?.range(of:" ") != nil {
            print("exists")
            let fullNameArr = stringName?.components(separatedBy: " ")
            let name    = fullNameArr![0]
            let surname = fullNameArr![1]
            cell.dialogName?.text = name
        }else {
            cell.dialogName?.text = opponentUser.fullName
        }
        
//        cell.dialogTypeImage.image = cellModel.dialogIcon
        cell.unreadMessageCounterLabel.text = cellModel.unreadMessagesCounterLabelText
        cell.unreadMessageCounterHolder.isHidden = cellModel.unreadMessagesCounterHiden
        
        cell.profilebtn.tag = indexPath.row
        cell.profilebtn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if (ServicesManager.instance().isProcessingLogOut!) {
            return
        }
        
        guard let dialog = self.dialogs()?[indexPath.row] else {
            return
        }
        
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        vc.dialog =  dialog
        vc.dataSource = appDelegate.dataSource
//        self.navigationController?.pushViewController(vc, animated: true)
        let navigationController = UINavigationController(rootViewController: vc)
        present(navigationController, animated: true)
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        guard editingStyle == UITableViewCellEditingStyle.delete else {
            return
        }
        
        
        guard let dialog = self.dialogs()?[indexPath.row] else {
            return
        }
        
        _ = AlertView(title:"SA_STR_WARNING".localized , message:"SA_STR_DO_YOU_REALLY_WANT_TO_DELETE_SELECTED_DIALOG".localized , cancelButtonTitle: "SA_STR_CANCEL".localized, otherButtonTitle: ["SA_STR_DELETE".localized], didClick:{ (buttonIndex) -> Void in
            
       // _ = AlertView(title:"Notice" , message:"Do you want to delete the chat?" , cancelButtonTitle: "SA_STR_CANCEL".localized, otherButtonTitle: ["Delete"], didClick:{ (buttonIndex) -> Void in
            
            guard buttonIndex == 1 else {
                return
            }
            
//            SVProgressHUD.show(withStatus: "SA_STR_DELETING".localized)
            
            let deleteDialogBlock = { (dialog: QBChatDialog!) -> Void in
                
                // Deletes dialog from server and cache.
                ServicesManager.instance().chatService.deleteDialog(withID: dialog.id!, completion: { (response) -> Void in
                    
                    guard response.isSuccess else {
//                        SVProgressHUD.showError(withStatus: "SA_STR_ERROR_DELETING".localized)
                        print(response.error?.error)
                        return
                    }
                    
//                    SVProgressHUD.showSuccess(withStatus: "SA_STR_DELETED".localized)
                })
            }
            
            if dialog.type == QBChatDialogType.private {
                deleteDialogBlock(dialog)
                
            }
            else {
                // group
                let occupantIDs = dialog.occupantIDs!.filter({ (number) -> Bool in
                    
                    return number.uintValue != ServicesManager.instance().currentUser.id
                })
                
                dialog.occupantIDs = occupantIDs
                let userLogin = ServicesManager.instance().currentUser.fullName ?? ""
                let notificationMessage = "User \(userLogin) " + "SA_STR_USER_HAS_LEFT".localized
                // Notifies occupants that user left the dialog.
                ServicesManager.instance().chatService.sendNotificationMessageAboutLeaving(dialog, withNotificationText: notificationMessage, completion: { (error) -> Void in
                    deleteDialogBlock(dialog)
                })
            }
        })
    }
	
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        
        return "SA_STR_DELETE".localized
    }
    
    // MARK: - QMChatServiceDelegate
	
    func chatService(_ chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
		
        self.reloadTableViewIfNeeded()
    }
	
    func chatService(_ chatService: QMChatService,didUpdateChatDialogsInMemoryStorage dialogs: [QBChatDialog]){
		
        self.reloadTableViewIfNeeded()
    }
	
    func chatService(_ chatService: QMChatService, didAddChatDialogsToMemoryStorage chatDialogs: [QBChatDialog]) {
        
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddChatDialogToMemoryStorage chatDialog: QBChatDialog) {
        
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didDeleteChatDialogWithIDFromMemoryStorage chatDialogID: String) {
        
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddMessagesToMemoryStorage messages: [QBChatMessage], forDialogID dialogID: String) {
        
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String){
        
        self.reloadTableViewIfNeeded()
    }

    // MARK: QMChatConnectionDelegate
    
    func chatServiceChatDidFail(withStreamError error: Error) {
//        SVProgressHUD.showError(withStatus: error.localizedDescription)
    }
    
    func chatServiceChatDidAccidentallyDisconnect(_ chatService: QMChatService) {
//        SVProgressHUD.showError(withStatus: "SA_STR_DISCONNECTED".localized)
    }
    
    func chatServiceChatDidConnect(_ chatService: QMChatService) {
//        SVProgressHUD.showSuccess(withStatus: "SA_STR_CONNECTED".localized)
        if !ServicesManager.instance().isProcessingLogOut! {
            self.getDialogs()
        }
    }
    
    func chatService(_ chatService: QMChatService,chatDidNotConnectWithError error: Error){
//        SVProgressHUD.showError(withStatus: error.localizedDescription)
    }
	
	
    func chatServiceChatDidReconnect(_ chatService: QMChatService) {
//        SVProgressHUD.showSuccess(withStatus: "SA_STR_CONNECTED".localized)
        if !ServicesManager.instance().isProcessingLogOut! {
            self.getDialogs()
        }
    }
    
    // MARK: - Helpers
    func reloadTableViewIfNeeded() {
        if !ServicesManager.instance().isProcessingLogOut! {
            self.tableView.reloadData()
        }
    }
    
    func logInChatWithUser(user: QBUUser) {
        
        // Logging to Quickblox REST API and chat.
        ServicesManager.instance().logIn(with: user, completion:{
            [unowned self] (success, errorMessage) -> Void in
            
            guard success else {
                return
            }
            self.getDialogs()
        })
    }
    
    
    //QBCoreDelegate
//    func coreDidLogin(_ core: QBCore) {
//        self.logInChatWithUser(user: core.currentUser)
//    }

    // QBWebRTCChatDelegate
//    func didReceiveNewSession(_ session: QBRTCSession, userInfo: [String : String]? = nil) {
//        if self.session != nil {
//            let uInfo = ["reject":"busy"]
//            session.rejectCall(uInfo)
//            return
//        }
//
//        self.session = session
//
//        if CallKitManager.isCallKitAvailable {
//            self.callUUID = UUID()
//            self.opponentIDs = [session.initiatorID]
//            for userID in session.opponentsIDs {
//                if userID.uintValue != QBCore.instance().currentUser.id {
//                    self.opponentIDs.append(userID)
//                }
//            }
//
//            CallKitManager.instance.reportIncomingCall(withUserIDs: self.opponentIDs, session: session, uuid: self.callUUID, onAcceptAction: {
//                let vc = storyBoard.instantiateViewController(withIdentifier: "CallVC") as! CallVC
//                vc.session = session
//                vc.usersDataSource = appDelegate.dataSource
//                vc.callUUID = self.callUUID
//                let nav = UINavigationController(rootViewController: vc)
//                self.present(nav, animated: true, completion: nil)
//            }, completion: nil)
//        }else {
//            let vc = storyBoard.instantiateViewController(withIdentifier: "InComingCallVC") as! InComingCallVC
//            vc.delegate = self
//            vc.session = session
//            vc.usersDataSource = appDelegate.dataSource
//            let nav = UINavigationController(rootViewController: vc)
//            self.present(nav, animated: false, completion: nil)
////            if var tvc = UIApplication.shared.keyWindow?.rootViewController {
////                while let presentedViewController = tvc.presentingViewController {
////                    tvc = presentedViewController
////                }
////                tvc.present(nav, animated: false, completion: nil)
////            }
//        }
//    }
//
//    func sessionDidClose(_ session: QBRTCSession) {
//        if session == self.session {
//            if self.backgroundTask != UIBackgroundTaskInvalid {
//                UIApplication.shared.endBackgroundTask(self.backgroundTask)
//                self.backgroundTask = UIBackgroundTaskInvalid
//            }
//            DispatchQueue.main.async {
//                if UIApplication.shared.applicationState == .background && self.backgroundTask == UIBackgroundTaskInvalid {
//                    QBChat.instance.disconnect(completionBlock: nil)
//                }
//            }
//            //            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//            //            }
//            if var tvc = UIApplication.shared.keyWindow?.rootViewController {
//                while let presentedViewController = tvc.presentingViewController {
//                    tvc = presentedViewController
//                }
//                tvc.dismiss(animated: false, completion: nil)
//            }
//            self.session = nil
//
////            if self.nav != nil {
////                //                DispatchQueue.main.async {
////                self.nav.view.isUserInteractionEnabled = false
////                self.nav.dismiss(animated: false, completion: nil)
////                self.session = nil
////                self.nav = nil
////                //                }
////                //                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
////                //                }
////            }
//            if CallKitManager.isCallKitAvailable {
//                CallKitManager.instance.endCall(with: self.callUUID, completion: nil)
//                self.callUUID = nil
//                self.session = nil
//            }
//        }
//    }
//
//    func incomingCallViewControllerDidAcceptSession(vc: InComingCallVC, session: QBRTCSession) {
//        let vc = storyBoard.instantiateViewController(withIdentifier: "CallVC") as! CallVC
//        vc.session = session
//        vc.usersDataSource = appDelegate.dataSource
//        let nav = UINavigationController(rootViewController: vc)
//        if var tvc = UIApplication.shared.keyWindow?.rootViewController {
//            while let presentedViewController = tvc.presentingViewController {
//                tvc = presentedViewController
//            }
//            tvc.present(nav, animated: true, completion: nil)
//        }
////        self.nav.viewControllers = [vc]
//    }
//
//    func incomingCallViewControllerDidRejectSession(vc: InComingCallVC, session: QBRTCSession) {
//        session.rejectCall(nil)
//        vc.dismiss(animated: false, completion: nil)
////        self.nav.dismiss(animated: false, completion: nil)
////        self.nav = nil
//    }
//
//    // MARK: - PKPushRegistryDelegate protocol
//    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
//        let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString
//
//        let subscription = QBMSubscription()
//        subscription.notificationChannel = QBMNotificationChannel.APNSVOIP
//        subscription.deviceUDID = deviceIdentifier
//        subscription.deviceToken = self.voipRegistry.pushToken(for: PKPushType.voIP)
//
//        QBRequest.createSubscription(subscription, successBlock: { (response, objects) in
//            print("Create Subscription request - Success")
//        }) { (response) in
//            print("Create Subscription request - Error")
//        }
//    }
//
//    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
//        //        fatalError()
//        let deviceIdentifier = UIDevice.current.identifierForVendor!.uuidString
//        QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: deviceIdentifier, successBlock: { (response) in
//            print("Unregister Subscription request - Success")
//        }) { (error) in
//            print("Unregister Subscription request - Error")
//        }
//    }
//
//    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
//        print("payload dict: \(payload.dictionaryPayload)")
//        if CallKitManager.isCallKitAvailable {
//            if payload.dictionaryPayload[kVoipEvent] != nil {
//                let application = UIApplication.shared
//                if self.backgroundTask == UIBackgroundTaskInvalid {
//                    self.backgroundTask = application.beginBackgroundTask(expirationHandler: {
//                        application.endBackgroundTask(self.backgroundTask)
//                        self.backgroundTask = UIBackgroundTaskInvalid
//                    })
//                }
//                if !QBChat.instance.isConnected {
//                    QBCore.instance().loginWithCurrentUser()
//                }
//            }
//        }
//    }

    @IBAction func chatInfoBtnClicked(_ sender: Any) {
        let alertview = UIAlertController(title: "Chat note", message: "Chats disappear after 1 week.", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel)
        alertview.addAction(cancelAction)
        self.present(alertview, animated: true, completion: nil)
    }
    

    @objc func buttonAction(sender: UIButton!) {
        guard let dialog = self.dialogs()?[sender.tag] else {
            return
        }

        let vc = storyBoard.instantiateViewController(withIdentifier: "ViewProfileVC") as! ViewProfileVC
        if let chatId = defaults.value(forKey: "userChatId") as? Int{
            print(chatId)
            print(dialog)
            let arr = dialog.occupantIDs!
            print("arr :- \(arr)")
            for i in 0..<arr.count {
                if chatId != Int(truncating: arr[i]) {
                    print(Int(truncating: arr[i]))
                    vc.chat_id = Int(truncating: arr[i])
                }
            }
        }
        let navigationController = UINavigationController(rootViewController: vc)
        present(navigationController, animated: true)
    }

}
