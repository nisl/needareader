//
//  CallVC.swift
//  quickblox_video_call
//
//  Created by ob_apple_1 on 15/12/17.
//  Copyright © 2017 optimumbrew. All rights reserved.
//

import Foundation

class CallVC: UIViewController, QBRTCClientDelegate, QBRTCAudioSessionDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, LocalVideoViewDelegate {

    var usersDataSource: UsersDataSource!
    var session: QBRTCSession!
    var dynamicEnable: QBButton!
    var videoEnabled: QBButton!
    var audioEnabled: QBButton!
    var localVideoView: LocalVideoView!
    var statsView: StatsView!
    var cameraCapture: QBRTCCameraCapture!
    var shouldGetStats: Bool = true
    
    var callTimer: Timer!
    var beepTimer: Timer!
    var callUUID: UUID!
    var timeDuration: TimeInterval!
    @IBOutlet weak var opponentsVCallView: OpponentVideoView!
    @IBOutlet weak var usersVCallView: OpponentVideoView!
    @IBOutlet weak var connectView: UIView!
    @IBOutlet weak var lblConnect: UILabel!
    
    var videoViews: [NSNumber: UIView] = [:]
    var OUser: QBUUser!
    var UUser: QBUUser!
    var users: [QBUUser] = []
    let kOpponentCollectionViewCellIdentifier = "OpponentCollectionViewCellIdentifier"
    let kOCollectionViewCellIdentifier = "OCollectionViewCellIdentifier"
    let kUCollectionViewCellIdentifier = "UCollectionViewCellIdentifier"
    let kUnknownUserLabel = "?"
    let kQBRTCRecordingTitle = "[Recording] "
    let kRefreshTimeInterval: TimeInterval = 1.0;

    @IBOutlet weak var opponentsCollectionView: UICollectionView!
    @IBOutlet weak var OCollectionView: UICollectionView!
    @IBOutlet weak var UCollectionView: UICollectionView!
    @IBOutlet weak var toolBar: QBToolBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let audioSession = QBRTCAudioSession.instance()
        if !audioSession.isInitialized {
            audioSession.initialize(configurationBlock: { (configuration) in
                configuration.categoryOptions = .allowBluetooth
                if #available(iOS 10.0, *) {
                    configuration.categoryOptions = .allowBluetoothA2DP
                    configuration.categoryOptions = .allowAirPlay
                } else {
                    // Fallback on earlier versions
                }
                if self.session.conferenceType == .video {
                    configuration.mode = AVAudioSessionModeVideoChat
                }

            })
        }
        
        viewCorners.addCornerToView(view: self.UCollectionView, value: 10)
        let settings = Settings.instance()
//        if self.session.opponentsIDs.count == 1 && settings!.recordSettings.isEnabled {
//            if self.session.conferenceType == .video {
//                self.session.recorder?.setVideoRecording(settings!.recordSettings.videoRotation)
//                self.session.recorder?.setVideoRecordingWidth(settings!.recordSettings.width, height: settings!.recordSettings.height, bitrate: settings!.recordSettings.estimatedBitrate(), fps: settings!.recordSettings.fps)
//            }
//            let searchPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
//            let documentsPath = searchPaths.first!
//            let filePath = documentsPath+"file\(Date.timeIntervalBetween1970AndReferenceDate).mp4"
//            self.session.recorder?.startRecord(withFileURL: URL(fileURLWithPath: filePath))
//        }
        
        self.cameraCapture = QBRTCCameraCapture(videoFormat: settings!.videoFormat, position: settings!.preferredCameraPostion)
        self.cameraCapture.startSession(nil)
        self.session.localMediaStream.videoTrack.videoCapture = self.cameraCapture
        
//        if self.session.conferenceType == .video {
            self.view.backgroundColor = UIColor(red: 0.1465, green: 0.1465, blue: 0.1465, alpha: 1.0)
            self.opponentsCollectionView.backgroundColor = UIColor(red: 0.1465, green: 0.1465, blue: 0.1465, alpha: 1.0)
//            var users = [QBUUser](repeating: QBUUser(), count: self.session.opponentsIDs.count)
        var users : [QBUUser] = []
        users.reserveCapacity(self.session.opponentsIDs.count+1)
        users.insert(QBCore.instance().currentUser, at: 0)
        for uID in self.session.opponentsIDs {
            if QBCore.instance().currentUser.id == uID.uintValue {
                var initiator = self.usersDataSource.user(withID: self.session.initiatorID.uintValue)
                if initiator.fullName == nil {
                    initiator = QBUUser()
                    initiator.id = self.session.initiatorID.uintValue
                }
                users.insert(initiator, at: 0)
                continue
            }
            var user = self.usersDataSource.user(withID: uID.uintValue)
            if user.fullName == nil {
                user = QBUUser()
                user.id = uID.uintValue
            }
            users.insert(user, at: 0)
        }
        self.users = users
        for user in self.users {
            let userID = user.id
            if userID != QBSession.current.currentUser?.id {
                OUser = user
            }else {
                UUser = user
            }
        }
//        self.setVideoViews()
        let isInitiator = (QBCore.instance().currentUser.id == self.session.initiatorID.uintValue)
        isInitiator ? self.startCall() : self.acceptCall()
        self.title = "Connecting..."

        if CallKitManager.isCallKitAvailable && (self.session.initiatorID.uintValue == QBCore.instance().currentUser.id) {
            CallKitManager.instance.updateCall(with: callUUID, connectingAt: Date())
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Call"
        self.navigationController?.isNavigationBarHidden = true
        QBRTCClient.instance().add(self)
        self.configureGUI()
//        QBRTCAudioSession.instance().addDelegate(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.refreshVideoViews()
        
        if self.cameraCapture != nil && !self.cameraCapture.hasStarted {
            self.cameraCapture.startSession(nil)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setVideoViews() {
        for user in self.users {
            let userID = NSNumber(value: user.id)

            if user.id != QBSession.current.currentUser?.id {
                let title = (user.fullName != nil) ? user.fullName: kUnknownUserLabel
//                opponentsVCallView.placeholderImageView.image = PlaceholderGenerator.placeholder(with: opponentsVCallView.placeholderImageView.bounds.size, title: title!)
                opponentsVCallView.didPressMuteButton = { (isMuted) in
                    let audioTrack =  self.session.remoteAudioTrack(withUserID: userID)
                    audioTrack.isEnabled = !isMuted
                }
                opponentsVCallView.setVideoView(self.videoViewWithOpponentID(userID))
                opponentsVCallView.connectionState = self.session.connectionState(forUser: userID)
            }else {
                usersVCallView.didPressMuteButton = { (isMuted) in
                    let audioTrack =  self.session.remoteAudioTrack(withUserID: userID)
                    audioTrack.isEnabled = !isMuted
                }
                usersVCallView.setVideoView(self.videoViewWithOpponentID(userID))
                usersVCallView.connectionState = self.session.connectionState(forUser: userID)
            }
        }
    }
    
    func refreshVideoViews() {
        for viewToRefresh in self.opponentsCollectionView.visibleCells {
            let cell = (viewToRefresh as! OpponentCollectionViewCell)
            let v = cell.videoView
            cell.videoView = nil
            cell.videoView = v
        }
        
        for viewToRefresh in self.OCollectionView.visibleCells {
            let cell = (viewToRefresh as! OpponentCollectionViewCell)
            let v = cell.videoView
            cell.videoView = nil
            cell.videoView = v
        }

        for viewToRefresh in self.UCollectionView.visibleCells {
            let cell = (viewToRefresh as! OpponentCollectionViewCell)
            let v = cell.videoView
            cell.videoView = nil
            cell.videoView = v
        }

//        if let v = self.usersVCallView.videoView {
//            self.usersVCallView.videoView = v
//        }
//
//        if let v = self.opponentsVCallView.videoView {
//            self.opponentsVCallView.videoView = v
//        }
    }
    
    func startCall() {
        self.beepTimer = Timer(timeInterval: QBRTCConfig.dialingTimeInterval(), target: self, selector: #selector(playCallingSound), userInfo: nil, repeats: true)
        self.playCallingSound()
        self.session.startCall(nil)
        self.connectView.isHidden = false
    }
    
    func acceptCall() {
        QMSoundManager.instance().stopAllSounds()
        let userInfo: [String: String] = ["acceptCall":"userInfo"]
        self.session.acceptCall(userInfo)
    }
    
    func configureGUI() {
        if self.session.conferenceType == .video {
            self.videoEnabled = QBButtonsFactory.videoEnable()
            self.toolBar.add(self.videoEnabled, action: { (sender) in
                self.session.localMediaStream.videoTrack.isEnabled = !self.session.localMediaStream.videoTrack.isEnabled
                self.localVideoView.isHidden = !self.session.localMediaStream.videoTrack.isEnabled
            })
        }
        self.toolBar.add(QBButtonsFactory.circleDecline(), action: { (sender) in
            if self.callTimer != nil {
                self.callTimer.invalidate()
                self.callTimer = nil
            }
            if self.session.recorder?.state == .active {
                //                    SVProgressHUD.show(withStatus: "Saving record")
                self.session.recorder?.stopRecord({ (file) in
                    //                        SVProgressHUD.dismiss()
                })
            }
            let userInfo = ["hangup":"hang up"]
            self.session.hangUp(userInfo)
            self.dismiss(animated: true, completion: nil)
        })

        self.audioEnabled = QBButtonsFactory.auidoEnable()
        self.toolBar.add(self.audioEnabled) { (sender) in
            self.session.localMediaStream.audioTrack.isEnabled = !self.session.localMediaStream.audioTrack.isEnabled
            self.session.recorder?.isMicrophoneMuted = !self.session.localMediaStream.audioTrack.isEnabled
        }
        
        if session.conferenceType == .audio {
            self.dynamicEnable = QBButtonsFactory.dynamicEnable()
            self.toolBar.add(self.dynamicEnable, action: { (sender) in
                let device = QBRTCAudioSession.instance().currentAudioDevice
                QBRTCAudioSession.instance().currentAudioDevice = (device == QBRTCAudioDevice.speaker) ? QBRTCAudioDevice.receiver : QBRTCAudioDevice.speaker
            })
        }
        
//        if self.session.conferenceType == .video {
//        }
        self.toolBar.updateItems()
        self.statsView = StatsView(frame: self.view.bounds)
        self.statsView.isHidden = true
        self.view.addSubview(self.statsView)
        
        let statsButton = UIBarButtonItem(title: "Stats", style: .plain, target: self, action: #selector(updateStatsView))
        self.navigationItem.rightBarButtonItem = statsButton
    }
    
    
    func videoViewWithOpponentID(_ opponentID: NSNumber) -> UIView {
        if session.conferenceType == .audio {
            return UIView()
        }
        var result = self.videoViews[opponentID]
        
        if QBCore.instance().currentUser.id == opponentID.uintValue {
            if result == nil {
                let localVideoView = LocalVideoView(previewlayer: self.cameraCapture.previewLayer)
                self.videoViews[opponentID] = localVideoView
                localVideoView?.delegate = self
                self.localVideoView = localVideoView
                result = localVideoView
                return localVideoView!
            }
        }else {
            var remoteVideoView = QBRTCRemoteVideoView()
            let remoteVideoTrack = self.session.remoteVideoTrack(withUserID: opponentID)
            if result == nil {
                remoteVideoView = QBRTCRemoteVideoView(frame: CGRect(x: 2, y: 2, width: 2, height: 2))
                remoteVideoView.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue
                self.videoViews[opponentID] = remoteVideoView
                result = remoteVideoView
            }else {
                remoteVideoView.setVideoTrack(remoteVideoTrack)
                result = remoteVideoView
            }
            return result!
        }
        return result!
    }
    
    //CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.OCollectionView:
            return 1
        case self.UCollectionView:
            return 1
        default:
            return self.users.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.OCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kOCollectionViewCellIdentifier, for: indexPath) as! OpponentCollectionViewCell
            let userID = NSNumber(value: self.OUser.id)
            cell.didPressMuteButton = { (isMuted) in
                let audioTrack =  self.session.remoteAudioTrack(withUserID: userID)
                audioTrack.isEnabled = !isMuted
            }
            
            cell.videoView = self.videoViewWithOpponentID(userID)
            cell.connectionState = self.session.connectionState(forUser: userID)
            
            if self.OUser.id != QBSession.current.currentUser?.id {
                let title = (self.OUser.fullName != nil) ? self.OUser.fullName: kUnknownUserLabel
//                cell.placeholderImageView.image = PlaceholderGenerator.placeholder(with: cell.placeholderImageView.bounds.size, title: title!)
            }
            return cell
        case self.UCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kUCollectionViewCellIdentifier, for: indexPath) as! OpponentCollectionViewCell
            let userID = NSNumber(value: self.UUser.id)
            cell.didPressMuteButton = { (isMuted) in
                let audioTrack =  self.session.remoteAudioTrack(withUserID: userID)
                audioTrack.isEnabled = !isMuted
            }
            
            cell.videoView = self.videoViewWithOpponentID(userID)
            cell.connectionState = self.session.connectionState(forUser: userID)
            
            if self.UUser.id != QBSession.current.currentUser?.id {
                let title = (self.UUser.fullName != nil) ? self.UUser.fullName: kUnknownUserLabel
//                cell.placeholderImageView.image = PlaceholderGenerator.placeholder(with: cell.placeholderImageView.bounds.size, title: title!)
            }
            return cell
       default:
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kOpponentCollectionViewCellIdentifier, for: indexPath) as! OpponentCollectionViewCell
        let user = self.users[indexPath.row]
        let userID = NSNumber(value: user.id)
        cell.didPressMuteButton = { (isMuted) in
            let audioTrack =  self.session.remoteAudioTrack(withUserID: userID)
            audioTrack.isEnabled = !isMuted
        }
        
        cell.videoView = self.videoViewWithOpponentID(userID)
        cell.connectionState = self.session.connectionState(forUser: userID)
        
        if user.id != QBSession.current.currentUser?.id {
            let title = (user.fullName != nil) ? user.fullName: kUnknownUserLabel
//            cell.placeholderImageView.image = PlaceholderGenerator.placeholder(with: cell.placeholderImageView.bounds.size, title: title!)
        }
        return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { (context) in
            self.refreshVideoViews()
        }, completion: nil)
    }
    
    @objc func updateStatsView() {
        self.statsView.isHidden = true
    }
    
    func indexPathAtUserID(userID: NSNumber) -> IndexPath {
        var user = self.usersDataSource.user(withID: userID.uintValue)
        if user.fullName == nil {
            user = QBUUser()
            user.id = userID.uintValue
        }
        let index = self.users.index(where: {$0.id == user.id})
        let indexPath = IndexPath(item: index!, section: 0)
        return indexPath
    }
    
    func performUpdateUserID(userID: NSNumber, block: (OpponentCollectionViewCell) -> Void) {
        if userID.uintValue != QBSession.current.currentUser?.id {
            let cell = self.OCollectionView.cellForItem(at: IndexPath(item: 0, section: 0))
            block(cell as! OpponentCollectionViewCell)
        }else {
            let cell = self.UCollectionView.cellForItem(at: IndexPath(item: 0, section: 0))
            block(cell as! OpponentCollectionViewCell)
        }

//        let indexPath = self.indexPathAtUserID(userID: userID)
//        let cell = self.opponentsCollectionView.cellForItem(at: indexPath)
//        block(cell as! OpponentCollectionViewCell)
    }
    
    func performUpdateStateUserID(userID: NSNumber, block: (UILabel) -> Void) {
        block(self.lblConnect)
    }
    
    @objc func playCallingSound() {
        QMSoundManager.playCallingSound()
    }
    
    func localVideoView(_ localVideoView: LocalVideoView!, pressedSwitch sender: UIButton!) {
        let position = self.cameraCapture.position
        let newPosition = (position == AVCaptureDevice.Position.back) ? AVCaptureDevice.Position.front : AVCaptureDevice.Position.back
        
        if self.cameraCapture.hasCamera(for: newPosition) {
            let animation = CATransition()
            animation.duration = 0.75
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            animation.type = "oglFlip"
            
            if position == .front {
                animation.subtype = kCATransitionFromRight
            }else if position == .back {
                animation.subtype = kCATransitionFromLeft
            }
            localVideoView.superview?.layer.add(animation, forKey: nil)
            self.cameraCapture.position = newPosition
        }
    }
    
    @objc func refreshCallTime(_ sender: Timer) {
        self.timeDuration = self.timeDuration+kRefreshTimeInterval
        var extraTitle = ""
        if self.session.recorder?.state == QBRTCRecorderState.active {
            extraTitle = kQBRTCRecordingTitle
        }
        self.title = extraTitle+"Call time - \(self.timeDuration)"
    }
    
    func setConnectionStateLabel(_ label: UILabel, userID: NSNumber) {
        switch self.session.connectionState(forUser: userID) {
            
        case .new:
            
            label.text = "Ringing..."
            self.connectView.isHidden = false

        case .pending:
            
            label.text = "Ringing..."
            self.connectView.isHidden = false

        case .checking:
            
            label.text = "Ringing..."
            self.connectView.isHidden = false

        case .connecting:
            
            label.text = "Ringing..."
            self.connectView.isHidden = false

        case .connected:
            
            label.text = "Connected"
            self.connectView.isHidden = true
        case .closed:
            
            label.text = "Closed"
            self.connectView.isHidden = true

        case .hangUp:
            
            label.text = "Hung Up"
            self.connectView.isHidden = true

        case .rejected:
            
            label.text = "Rejected"
            self.connectView.isHidden = true

        case .noAnswer:
            
            label.text = "No Answer"
            self.connectView.isHidden = true

        case .disconnectTimeout:
            
            label.text = "Time out"
            self.connectView.isHidden = true

        case .disconnected:
            
            label.text = "Disconnected"
            self.connectView.isHidden = true

        default:
            break;
        }
    }
    
    func session(_ session: QBRTCBaseSession, updatedStatsReport report: QBRTCStatsReport, forUserID userID: NSNumber) {
        let result = report.statsString()
        if shouldGetStats {
            self.statsView.setStats(result)
            self.view.setNeedsLayout()
        }
    }
    
    func session(_ session: QBRTCSession, userDidNotRespond userID: NSNumber) {
        if session == self.session {
//            self.performUpdateUserID(userID: userID, block: { (cell) in
//                cell.connectionState = self.session.connectionState(forUser: userID)
//            })
            self.performUpdateStateUserID(userID: userID, block: { (label) in
                self.setConnectionStateLabel(label, userID: userID)
            })
        }
    }
    
    func session(_ session: QBRTCSession, acceptedByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        if session == self.session {
//            self.performUpdateUserID(userID: userID, block: { (cell) in
//                cell.connectionState = self.session.connectionState(forUser: userID)
//            })
            self.performUpdateStateUserID(userID: userID, block: { (label) in
                self.setConnectionStateLabel(label, userID: userID)
            })
        }
    }
    
    func session(_ session: QBRTCSession, rejectedByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        if session == self.session {
//            self.performUpdateUserID(userID: userID, block: { (cell) in
//                cell.connectionState = self.session.connectionState(forUser: userID)
//            })
            self.performUpdateStateUserID(userID: userID, block: { (label) in
                self.setConnectionStateLabel(label, userID: userID)
            })
        }
    }
    
    func session(_ session: QBRTCSession, hungUpByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        if session == self.session {
//            self.performUpdateUserID(userID: userID, block: { (cell) in
//                cell.connectionState = self.session.connectionState(forUser: userID)
//            })
            self.performUpdateStateUserID(userID: userID, block: { (label) in
                self.setConnectionStateLabel(label, userID: userID)
            })
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func session(_ session: QBRTCBaseSession, receivedRemoteVideoTrack videoTrack: QBRTCVideoTrack, fromUser userID: NSNumber) {
        if session == self.session {
            self.performUpdateUserID(userID: userID, block: { (cell) in
                let opponentVideoView = self.videoViewWithOpponentID(userID)
                cell.videoView = opponentVideoView
            })
        }
    }
    
    func session(_ session: QBRTCBaseSession, connectedToUser userID: NSNumber) {
        if session == self.session {
            if self.beepTimer != nil {
                self.beepTimer.invalidate()
                self.beepTimer = nil
            }
            QMSoundManager.instance().stopAllSounds()

            if self.callTimer == nil {
                if CallKitManager.isCallKitAvailable && self.session.initiatorID.uintValue == QBCore.instance().currentUser.id {
                    CallKitManager.instance.updateCall(with: callUUID, connectedAt: Date())
                }
                self.callTimer = Timer(timeInterval: kRefreshTimeInterval, target: self, selector: #selector(refreshCallTime(_:)), userInfo: nil, repeats: true)
            }
//            self.performUpdateUserID(userID: userID, block: { (cell) in
//                cell.connectionState = self.session.connectionState(forUser: userID)
//            })
            self.performUpdateStateUserID(userID: userID, block: { (label) in
                self.setConnectionStateLabel(label, userID: userID)
            })
        }
    }
    
    func session(_ session: QBRTCBaseSession, connectionClosedForUser userID: NSNumber) {
        if session == self.session {
            self.performUpdateUserID(userID: userID, block: { (cell) in
//                cell.connectionState = self.session.connectionState(forUser: userID)
                self.videoViews.removeValue(forKey: userID)
                cell.videoView = nil
            })
            self.performUpdateStateUserID(userID: userID, block: { (label) in
                self.setConnectionStateLabel(label, userID: userID)
            })
        }
    }
    
    func session(_ session: QBRTCBaseSession, disconnectedFromUser userID: NSNumber) {
        if session == self.session {
//            self.performUpdateUserID(userID: userID, block: { (cell) in
//                cell.connectionState = self.session.connectionState(forUser: userID)
//            })
            self.performUpdateStateUserID(userID: userID, block: { (label) in
                self.setConnectionStateLabel(label, userID: userID)
            })
        }
    }
    
    func session(_ session: QBRTCBaseSession, connectionFailedForUser userID: NSNumber) {
        if session == self.session {
//            self.performUpdateUserID(userID: userID, block: { (cell) in
//                cell.connectionState = self.session.connectionState(forUser: userID)
//            })
            self.performUpdateStateUserID(userID: userID, block: { (label) in
                self.setConnectionStateLabel(label, userID: userID)
            })
        }
    }
    
    func sessionDidClose(_ session: QBRTCSession) {
        if session == self.session {
            if CallKitManager.isCallKitAvailable {
                CallKitManager.instance.endCall(with: self.callUUID, completion: nil)
            }
            
            if self.session.recorder?.state == QBRTCRecorderState.active {
//                SVProgressHUD.show(withStatus: "Saving record")
                self.session.recorder?.stopRecord({ (file) in
//                    SVProgressHUD.dismiss()
                })
            }
            self.cameraCapture.stopSession(nil)
            let audioSession = QBRTCAudioSession.instance()
            if audioSession.isInitialized {
                audioSession.deinitialize()
            }
            
            if self.beepTimer != nil {
                self.beepTimer.invalidate()
                self.beepTimer = nil
            }
            QMSoundManager.instance().stopAllSounds()

            if self.callTimer != nil {
                self.callTimer.invalidate()
                self.callTimer = nil
            }
            
            self.toolBar.isUserInteractionEnabled = false
            UIView.animate(withDuration: 0.5, animations: {
                self.toolBar.alpha = 0.4
            })
            self.title = "End"
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func audioSession(_ audioSession: QBRTCAudioSession, didChangeCurrentAudioDevice updatedAudioDevice: QBRTCAudioDevice) {
        let isSpeaker = updatedAudioDevice == QBRTCAudioDevice.speaker
        if self.dynamicEnable.pressed != isSpeaker {
            self.dynamicEnable.pressed = isSpeaker
        }
    }
}
