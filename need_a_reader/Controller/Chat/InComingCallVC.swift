//
//  InComingCallVC.swift
//  quickblox_video_call
//
//  Created by ob_apple_1 on 15/12/17.
//  Copyright © 2017 optimumbrew. All rights reserved.
//

import Foundation
import UIKit

protocol InComingCallDelegate {
    func incomingCallViewControllerDidAcceptSession(vc: InComingCallVC, session: QBRTCSession)
    func incomingCallViewControllerDidRejectSession(vc: InComingCallVC, session: QBRTCSession)
}

class InComingCallVC: UIViewController, QBRTCClientDelegate {

    @IBOutlet weak var callStatusLabel: UILabel!
    @IBOutlet weak var colorMarker: CornerView!
    @IBOutlet weak var callInfoTextView: UITextView!
    @IBOutlet weak var toolBar: QBToolBar!
    var usersDataSource: UsersDataSource!
    var session: QBRTCSession!
    var delegate: InComingCallDelegate!
    var users: [QBUUser] = []
    var dialingTimer: Timer!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        QMSoundManager.playRingtoneSound()
        var users: [QBUUser] = []
        for uID in self.session.opponentsIDs {
            var user = self.usersDataSource.user(withID: uID.uintValue)
            if let item = user.fullName {
                user = QBUUser()
                user.id = uID.uintValue
            }
            users.append(user)
        }
        self.users = users
        self.configureGUI()
        
        self.dialingTimer = Timer.scheduledTimer(timeInterval: QBRTCConfig.dialingTimeInterval(), target: self, selector: #selector(dialing(_:)), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "InComing Call"
        QBRTCClient.instance().add(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func dialing(_ sender: Timer) {
        QMSoundManager.playRingtoneSound()
    }
    
    func configureGUI() {
        self.configureToolBar()
        self.updateOfferInfo()
        self.updateCallInfo()
        
        self.title = "Logged in as \(QBChat.instance.currentUser!.fullName)"
    }
    
    func configureToolBar() {
        self.toolBar.backgroundColor = UIColor.clear
        self.toolBar.add(QBButtonsFactory.circleDecline()) { (sender) in
            self.cleanUp()
            self.delegate.incomingCallViewControllerDidRejectSession(vc: self, session: self.session)
        }
        
        self.toolBar.add(QBButtonsFactory.answer()) { (sender) in
            self.cleanUp()
            self.delegate.incomingCallViewControllerDidAcceptSession(vc: self, session: self.session)
        }
        self.toolBar.updateItems()
    }
    
    func updateOfferInfo() {
        let caller = self.usersDataSource.user(withID: self.session.initiatorID.uintValue)
        
        self.colorMarker.bgColor = UIColor.gray
        self.colorMarker.title = caller.fullName
        self.colorMarker.fontSize = 30.0
    }
    
    func updateCallInfo() {
        var info: [String] = []
        for user in self.users {
//            info.append("\(user.fullName!)(ID \(user.id)")
        }
        self.callInfoTextView.text = info.joined(separator: ", ")
        self.callInfoTextView.font = UIFont.systemFont(ofSize: 19)
        self.callInfoTextView.textAlignment = .center
        
        let text = self.session.conferenceType == .video ? "Incoming video call" : "Incoming audio call"
        self.callStatusLabel.text = text
    }
    
    func cleanUp() {
        if self.dialingTimer != nil {
            self.dialingTimer.invalidate()
            self.dialingTimer = nil
        }
        QBRTCClient.instance().remove(self)
        QMSoundManager.instance().stopAllSounds()
    }
    
    func sessionDidClose(_ session: QBRTCSession) {
        if self.session == session {
            self.cleanUp()
        }
    }
}
