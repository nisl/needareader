//
//  FullScreenVC.swift
//  need_a_reader
//
//  Created by ob_apple_3 on 18/10/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import UIKit
import SDWebImage
class FullScreenVC: UIViewController {

    @IBOutlet weak var imageview: UIImageView!
    
    var imageString = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        self.imageview.sd_setShowActivityIndicatorView(true)
        self.imageview.sd_setIndicatorStyle(.gray)
        self.imageview.sd_setImage(with: URL(string: imageString), placeholderImage: UIImage(named: "ic_defaultProfile"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    func configureNavigationBar()
    {
        setNavigaion(vc: self)
        let navigationItem = self.navigationItem
        navigationItem.title = ""
        
        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
//        let addBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
//        // let backBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
//
//        // backBtn.addTarget(self, action: #selector(backBtnPressed(_:)), for: .touchUpInside)
//        addBtn.setImage(UIImage(named: "ic_resume"), for: .normal)
//        addBtn.addTarget(self, action: #selector(addBtnPressed(_:)), for: .touchUpInside)
        
     //   let addButton = UIBarButtonItem(customView: addBtn)
        //   let backButton = UIBarButtonItem(customView: backBtn)
        // navigationItem.leftBarButtonItem = backButton
      //  navigationItem.rightBarButtonItems = [addButton]
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
