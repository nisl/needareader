//
//  ViewProfileVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 1/17/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView
import SwiftyStarRatingView
import SDWebImage
import ReachabilitySwift
import UserNotifications
import ObjectMapper
import Alamofire
import AlamofireObjectMapper
import AVFoundation
import AVKit

class ViewProfileVC : UIViewController, UIGestureRecognizerDelegate, UIPopoverPresentationControllerDelegate,UIWebViewDelegate {
    
    @IBOutlet var showVideoBtn: UIButton!
    @IBOutlet var showServiceBtn: UIButton!
    @IBOutlet var hourLbl: UILabel!
    @IBOutlet var pdfsubview: UIView!
    @IBOutlet var skillSetTitle: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var ratingSubview: UIView!
    @IBOutlet var starRatingView: SwiftyStarRatingView!
    @IBOutlet var headerGetScoreLbl: UILabel!
    @IBOutlet var headerOutOfScoreLbl: UILabel!
    @IBOutlet var ratingHeaderLbl: UILabel!
    @IBOutlet var tableview: UITableView!
    @IBOutlet var avgRatingView: UIView!
    
    @IBOutlet var readerOnTotalVotes: UILabel!
    @IBOutlet var readerOnTImeDislikePer: UILabel!
    @IBOutlet var readerOnTImeLikePer: UILabel!
    @IBOutlet var readerontimeQuestionLbl: UILabel!
    @IBOutlet var readerOntimeView: UIView!
    
    @IBOutlet var TotalVotes2: UILabel!
    @IBOutlet var dislikePer2: UILabel!
    @IBOutlet var likePer2: UILabel!
    @IBOutlet var questionLbl2: UILabel!
    @IBOutlet var questionView2: UIView!
    
    @IBOutlet var TotalVotes3: UILabel!
    @IBOutlet var dislikePer3: UILabel!
    @IBOutlet var likePer3: UILabel!
    @IBOutlet var questionLbl3: UILabel!
    @IBOutlet var questionView3: UIView!
    
    @IBOutlet var TotalVotes4: UILabel!
    @IBOutlet var dislikePer4: UILabel!
    @IBOutlet var likePer4: UILabel!
    @IBOutlet var questionLbl4: UILabel!
    @IBOutlet var questionView4: UIView!

    @IBOutlet var showSkillView: UIView!
    
    @IBOutlet var outerView: UIView!
    @IBOutlet var hintView: UIView!
    @IBOutlet var profileImg: UIImageView!
    //ShowSkillView
    @IBOutlet var sitcomview: UIView!
    @IBOutlet var hourDramaview: UIView!
    @IBOutlet var playsview: UIView!
    @IBOutlet var musicalsview: UIView!
    @IBOutlet var featureFilmsview: UIView!
    
    @IBOutlet var dramedyView: UIView!
    @IBOutlet var singleCameraview: UIView!
    @IBOutlet var coStarview: UIView!
    @IBOutlet var commercialview: UIView!
    @IBOutlet var shakespeareview: UIView!
    @IBOutlet var monologuesview: UIView!
    @IBOutlet var accentsView: UIView!
    
    @IBOutlet var sitcomValueLbl: UILabel!
    @IBOutlet var hourDramaValueLbl: UILabel!
    @IBOutlet var playsValueLbl: UILabel!
    @IBOutlet var musicalsValueLbl: UILabel!
    @IBOutlet var featureFilmsValueLbl: UILabel!
    
    @IBOutlet var profileName: UILabel!
    @IBOutlet var priceLbl: UILabel!
    @IBOutlet var dramedyValueLbl: UILabel!
    @IBOutlet var singleCameraValueLbl: UILabel!
    @IBOutlet var coStarValueLbl: UILabel!
    @IBOutlet var commercialValueLbl: UILabel!
    @IBOutlet var shakespeareValueLbl: UILabel!
    @IBOutlet var monologuesValueLbl: UILabel!
    @IBOutlet var accentsValueLbl: UILabel!
    
    @IBOutlet var dramedySlider: UISlider!
    @IBOutlet var sitcomSlider: UISlider!
    @IBOutlet var hourDramaSlider: UISlider!
    @IBOutlet var playsValueSlider: UISlider!
    @IBOutlet var musicalsSlider: UISlider!
    @IBOutlet var featureFilmsSlider: UISlider!
    
    @IBOutlet var singleCameraSlider: UISlider!
    @IBOutlet var coStarSlider: UISlider!
    @IBOutlet var commercialSlider: UISlider!
    @IBOutlet var shakespeareSlider: UISlider!
    @IBOutlet var monologuesSlider: UISlider!
    @IBOutlet var accentsSlider: UISlider!
    
    @IBOutlet var webview: UIWebView!
    @IBOutlet var showServicesView: UIView!
    @IBOutlet var showServiceOuterView: UIView!
    @IBOutlet var serviceTableView: UITableView!
    @IBOutlet var mutualFndLbl: UILabel!
    
    var skillData : AddSkillsRequestModel!
    var ratingData : RatingResponseModel!
    var readerArrList : [User_detail] = []
    var readerArr : User_detail!
    var questionArr : [String] = []
    var timer = Timer()
    var seconds = 3
    var hintviewDisplay : Bool = false
    var tapCard : Int = 0
    var chat_id : Int!
    var errorView : NoDataView!
    var serviceNameArr : [ServiceDetail] = []
    //AVPlayer
    var videoPlayerVC = AVPlayerViewController()
    var videoPlayer : AVPlayer?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        
        serviceTableView.delegate = self
        serviceTableView.dataSource = self

        self.view.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
        questionArr = ["Was the reader on time?","Was the reader knowledgeable and skilled in this genre?","Would you work with this reader again?"]
        viewCorners.addShadowToView(view: showSkillView)
        viewCorners.addCornerToView(view: showSkillView, value: 10)
        viewCorners.addShadowToView(view: pdfsubview)
        viewCorners.addCornerToView(view: pdfsubview, value: 10)
        viewCorners.addShadowToView(view: ratingSubview)
        viewCorners.addCornerToView(view: ratingSubview, value: 10)
        viewCorners.addCornerToView(view: avgRatingView, value: 5)
        viewCorners.addShadowToView(view: avgRatingView)
        viewCorners.addCornerToView(view: readerOntimeView, value: 5)
        viewCorners.addShadowToView(view: readerOntimeView)
        viewCorners.addCornerToView(view: questionView2, value: 5)
        viewCorners.addShadowToView(view: questionView2)
        viewCorners.addCornerToView(view: questionView3, value: 5)
        viewCorners.addShadowToView(view: questionView3)
        viewCorners.addCornerToView(view: questionView4, value: 5)
        viewCorners.addShadowToView(view: questionView4)

        viewCorners.addCornerToView(view: outerView, value: 10)
        viewCorners.addCornerToView(view: hintView, value: 10)
        viewCorners.addShadowToView(view: outerView, value: 2.0)
        viewCorners.addShadowToView(view: pdfsubview, value: 2.0)
        viewCorners.addShadowToView(view: showSkillView, value: 2.0)
        viewCorners.addShadowToView(view: ratingSubview, value: 2.0)
        viewCorners.addShadowToView(view: showServicesView)
        viewCorners.addCornerToView(view: showServicesView, value: 10)
        viewCorners.addShadowToView(view: showServiceOuterView)
        viewCorners.addCornerToView(view: showServiceOuterView, value: 5)

        //        profileName.isHidden = true
        //        priceLbl.isHidden = true
        //        hourLbl.isHidden = true
        let viewArr : [UIView] = [sitcomview, hourDramaview, playsview, musicalsview, featureFilmsview, singleCameraview, coStarview, commercialview, shakespeareview, monologuesview, dramedyView, accentsView]
        setUI(arr: viewArr)
        let sliderArr : [UISlider] = [sitcomSlider, hourDramaSlider, playsValueSlider, musicalsSlider, featureFilmsSlider, singleCameraSlider,coStarSlider,shakespeareSlider,commercialSlider, monologuesSlider, dramedySlider, accentsSlider]
        setSliderEnabled(slider: sliderArr)
        
        changeSliderColor(view: sliderArr, minimumTrackcolor: redColor, thumbColor: redColor)
        let lblArr : [UILabel] = [featureFilmsValueLbl, hourDramaValueLbl, commercialValueLbl, sitcomValueLbl, singleCameraValueLbl, coStarValueLbl, playsValueLbl, musicalsValueLbl, monologuesValueLbl, shakespeareValueLbl, dramedyValueLbl, accentsValueLbl]
        changeLabelColor(view: lblArr, backcolor: redColor, fontColor: "ffffff")
        
        if chat_id != nil {
            print(chat_id)
            getuserProfileByChatId(receiver_user_id: chat_id)
        }
        //        loadFromActorCatCache()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCard(_:)))
        tapGesture.delegate = self
        profileImg.addGestureRecognizer(tapGesture)
        profileImg.isUserInteractionEnabled = true
        webview.delegate = self
        self.startTimer()
        
        showSkillView.isHidden = true
        pdfsubview.isHidden = true
        ratingSubview.isHidden = true
        starRatingView.isEnabled = false
        showServicesView.isHidden = true
        self.showVideoBtn.isHidden = true
        self.showServiceBtn.isHidden = true

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layer.layoutIfNeeded()
        let path = UIBezierPath(roundedRect: profileImg.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 5.0, height: 5.0))
        let mask = CAShapeLayer()
        mask.frame = profileImg.bounds
        mask.path = path.cgPath
        profileImg.layer.mask = mask
        //        imageouterView.frame.size.height = self.view.frame.size.height * 0.8
        //        profileImg.frame.size.height = self.view.frame.size.height * 0.7
        let cardMaxY = hintView.frame.maxY
        let midY = cardMaxY - (cardMaxY * 0.3)
        let midX = hintView.frame.size.width / 2
        drawLine(scaleX: midX, scaleY: 0, width: 1, height: midY, lineType: "vertical")
        drawLine(scaleX: 0, scaleY: midY, width: hintView.frame.size.width, height: 1, lineType: "horizontal")
    }
    
    func drawLine(scaleX: CGFloat, scaleY: CGFloat, width: CGFloat, height: CGFloat, lineType: String)
    {
        //        let path = UIBezierPath(rect: CGRect(x: scaleX, y: scaleY, width: width, height: height))
        switch lineType {
        case "vertical":
            let path = CGMutablePath()
            
            path.addLines(between: [CGPoint(x: scaleX, y: scaleY), CGPoint(x: scaleX, y: height)])
            let layer = CAShapeLayer()
            layer.path = path//.cgPath
            layer.strokeColor = UIColor.white.cgColor
            layer.fillColor = UIColor.clear.cgColor
            layer.lineDashPattern = [5,5]
            layer.lineJoin = kCALineCapRound
            layer.lineWidth = 1.0
            self.hintView.layer.addSublayer(layer)
        default:
            let path = CGMutablePath()
            
            path.addLines(between: [CGPoint(x: scaleX, y: scaleY), CGPoint(x: width, y: scaleY)])
            let layer = CAShapeLayer()
            layer.path = path//.cgPath
            layer.strokeColor = UIColor.white.cgColor
            layer.fillColor = UIColor.clear.cgColor
            layer.lineDashPattern = [5,5]
            layer.lineJoin = kCALineCapRound
            layer.lineWidth = 1.0
            self.hintView.layer.addSublayer(layer)
        }
        
    }
    
    func startTimer() {
        seconds = 3
        
        hintView.isHidden = false
        // timerLbl.text = String(format: "00 : 00 : %02d", seconds)//"00 : 00 : \(second)"
        timer = Timer()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        if seconds > 0 {
            seconds = seconds-1
            
        }else {
            timer.invalidate()
            hintView.isHidden = true
        }
        
    }
    
    //Set NavigationBar
    func configureNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "OpenSans-Semibold", size: 18.0)]
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        let navigationItem = self.navigationItem
        navigationItem.title = "Profile"
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        let backBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        
        if #available(iOS 11.0, *) {
            let chatwidthConstraint = backBtn.widthAnchor.constraint(equalToConstant: 44)
            let chatheightConstraint = backBtn.heightAnchor.constraint(equalToConstant: 44)
            chatheightConstraint.isActive = true
            chatwidthConstraint.isActive = true
            backBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 8, 16)
        }else {
            backBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 6, 12)
        }
        backBtn.setImage(UIImage(named: "ic_back"), for: .normal)
        backBtn.addTarget(self, action: #selector(backBtnPressed(_:)), for: .touchUpInside)
        let backButton = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func backBtnPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tapCard(_ sender: UITapGestureRecognizer) {
        let viewTag = sender.view as! UIImageView
        tapCard = 0//readerArrTemp.count - 1
        let co_ordinate = sender.location(in: profileImg)
        
        let originX = profileImg.frame.origin.x
        let originY = profileImg.frame.origin.y
        
        let cardMaxX = profileImg.frame.maxX
        let midX = profileImg.frame.midX
        print("maxY:- \(profileImg.frame.maxY)")
        print("calc maxY:- \(profileImg.frame.size.height * 0.85)")
        let cardMaxY = profileImg.frame.maxY
        let midY = cardMaxY - (cardMaxY * 0.4)
        if let name = readerArr.user_name {
            skillSetTitle.text = name.uppercased() + "'S SKILL SET"
        }
        identifyTapArea(x: originX, y: originY, max_x: cardMaxX, max_y: cardMaxY, midX: midX, midY: midY, tapX: co_ordinate.x, tapY: co_ordinate.y)
        
        //        identifyTapArea(x: originX, y: originY, max_x: cardMaxX, max_y: cardMaxY, midX: midX, midY: midY)
        print("x:- \(co_ordinate.x)")
        print("y:- \(co_ordinate.y)")
        
    }
    
    func identifyTapArea(x: CGFloat, y :CGFloat, max_x: CGFloat, max_y : CGFloat, midX: CGFloat, midY: CGFloat, tapX: CGFloat, tapY: CGFloat) {
        
        if ((tapX < midX) && (tapY < midY)) {
            print("Open Rating")
            
            if setRatingData() {
                ratingSubview.isHidden = false
                
            }else {
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: "No rating details found for selected reader")
                
            }
            show(animated: true, displayView: ratingSubview)
            
        } else if ((tapX > midX) && (tapY < midY)){
            
            if skillData != nil {
                if let sitcom = skillData.sit_com {
                    setSliderValue(slider : sitcomSlider, val : sitcom, skillLbl : sitcomValueLbl)
                }else {
                    setSliderValue(slider : sitcomSlider, val : 0, skillLbl : sitcomValueLbl)
                }
                if let hour_drama = skillData.hour_drama {
                    setSliderValue(slider : hourDramaSlider, val : hour_drama, skillLbl : hourDramaValueLbl)
                }else {
                    setSliderValue(slider : hourDramaSlider, val : 0, skillLbl : hourDramaValueLbl)
                }
                
                if let dramedy = skillData.dramedy {
                    setSliderValue(slider : dramedySlider, val : dramedy, skillLbl : dramedyValueLbl)
                }else {
                    setSliderValue(slider : dramedySlider, val : 0, skillLbl : dramedyValueLbl)
                }
                
                if let plays = skillData.plays {
                    setSliderValue(slider : playsValueSlider, val : plays, skillLbl : playsValueLbl)
                }else {
                    setSliderValue(slider : playsValueSlider, val : 0, skillLbl : playsValueLbl)
                }
                
                if let musicals = skillData.musicals {
                    setSliderValue(slider : musicalsSlider, val : musicals, skillLbl : musicalsValueLbl)
                }else {
                    setSliderValue(slider : musicalsSlider, val : 0, skillLbl : musicalsValueLbl)
                }
                
                if let feature_films = skillData.feature_films {
                    setSliderValue(slider : featureFilmsSlider, val : feature_films, skillLbl : featureFilmsValueLbl)
                }else {
                    setSliderValue(slider : featureFilmsSlider, val : 0, skillLbl : featureFilmsValueLbl)
                }
                
                if let single_camera = skillData.single_camera {
                    setSliderValue(slider : singleCameraSlider, val : single_camera, skillLbl : singleCameraValueLbl)
                }else {
                    setSliderValue(slider : singleCameraSlider, val : 0, skillLbl : singleCameraValueLbl)
                }
                
                if let co_star = skillData.co_star {
                    setSliderValue(slider : coStarSlider, val : co_star, skillLbl : coStarValueLbl)
                }else {
                    setSliderValue(slider : coStarSlider, val : 0, skillLbl : coStarValueLbl)
                }
                
                if let commercials = skillData.commercials {
                    setSliderValue(slider : commercialSlider, val : commercials, skillLbl : commercialValueLbl)
                }else {
                    setSliderValue(slider : commercialSlider, val : 0, skillLbl : commercialValueLbl)
                }
                
                if let shakespeare = skillData.shakespeare {
                    setSliderValue(slider : shakespeareSlider, val : shakespeare, skillLbl : shakespeareValueLbl)
                }else {
                    setSliderValue(slider : shakespeareSlider, val : 0, skillLbl : shakespeareValueLbl)
                }
                
                if let monologues = skillData.monologues {
                    setSliderValue(slider : monologuesSlider, val : monologues, skillLbl : monologuesValueLbl)
                }else {
                    setSliderValue(slider : monologuesSlider, val : 0, skillLbl : monologuesValueLbl)
                }
                
                if let accents = skillData.accents {
                    setSliderValue(slider : accentsSlider, val : accents, skillLbl : accentsValueLbl)
                }else {
                    setSliderValue(slider : accentsSlider, val : 0, skillLbl : accentsValueLbl)
                }
                
            }else {
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: "No skill details found for selected reader.")
                
            }
            showSkillView.isHidden = false
            
            show(animated: true, displayView: showSkillView)
            //            self.getSkill()
            print("Open Skill")
        } else {
            
            if let url = URL(string: "about:blank") {
                let myRequest = NSURLRequest(url: url)//URLRequest(url: url)
                //                pdfWebView.load(myRequest as URLRequest)
                webview.loadRequest(URLRequest(url: url))
            }else {
                print("Error")
            }
            
            if let resumeFile = readerArr.resume_file {
                if resumeFile == "" {
                    let alertview = SCLAlertView()
                    alertview.showError("Error", subTitle: "No resume found for selected reader.")
                } else {
                    pdfsubview.isHidden = false
                    activityIndicator.isHidden = false
                    activityIndicator.startAnimating()
                    openPdf()
                    show(animated: true, displayView: pdfsubview)
                    print("Open resume")
                }
            }else {
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: "No resume found for selected reader.")
            }
            
        }
    }
    
    func setRatingData() -> Bool {
        if ratingData != nil {
            if let avgRate = ratingData.average_rating {
                headerGetScoreLbl.text = "\(avgRate)"
                starRatingView.value = CGFloat(Float(avgRate)!)
            }
            
            if let ratingTotal = ratingData.ratings {
                ratingHeaderLbl.text = "\(ratingTotal) ratings"
            }
            
            if let likePercent = ratingData.on_time_yes {
                readerOnTImeLikePer.text = "\(likePercent)%"
            }
            
            if let dislikePercent = ratingData.on_time_no {
                readerOnTImeDislikePer.text = "\(dislikePercent)%"
            }
            
            if let votes = ratingData.is_on_time {
                readerOnTotalVotes.text = "\(votes)"
            }
            
            if let likePercent = ratingData.is_knowledgeable_yes {
                likePer2.text = "\(likePercent)%"
            }
            
            if let dislikePercent = ratingData.is_knowledgeable_no {
                dislikePer2.text = "\(dislikePercent)%"
            }
            
            if let votes = ratingData.is_knowledgeable_on_content {
                TotalVotes2.text = "\(votes)"
            }
            
            if let likePercent = ratingData.work_with_yes {
                likePer3.text = "\(likePercent)%"
            }
            
            if let dislikePercent = ratingData.work_with_no {
                dislikePer3.text = "\(dislikePercent)%"
            }
            
            if let votes = ratingData.work_with_reader_again {
                TotalVotes3.text = "\(votes)"
            }
            
            if let likePercent = ratingData.job_satisfaction_yes {
                likePer4.text = "\(likePercent)%"
            }
            if let dislikePercent = ratingData.job_satisfaction_no {
                dislikePer4.text = "\(dislikePercent)%"
            }
            if let votes = ratingData.jobSatisFiedPer {
                TotalVotes4.text = "\(votes)"
            }
            return true
        }else {
            return false
        }
    }
    
    func openPdf() {
        if let resumeFile = readerArr.resume_file {
            let pdfUrl = URL(string: resumeFile)
            if let url = pdfUrl {
                let myRequest = NSURLRequest(url: url)//URLRequest(url: url)
                //                pdfWebView.load(myRequest as URLRequest)
                webview.loadRequest(URLRequest(url: url))
            }else {
                print("Error")
            }
        }
        
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        //        showSkillView.isHidden = true
        if !showSkillView.isHidden {
            UIView.animate(withDuration: 0.25, animations: {
                self.showSkillView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                self.showSkillView.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.showSkillView.isHidden = true
                }
            });
        }
        if !ratingSubview.isHidden {
            UIView.animate(withDuration: 0.25, animations: {
                self.ratingSubview.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                self.ratingSubview.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.ratingSubview.isHidden = true
                }
            });
        }
        
        if !pdfsubview.isHidden {
            
            UIView.animate(withDuration: 0.25, animations: {
                self.pdfsubview.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                self.pdfsubview.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.pdfsubview.isHidden = true
                    
                }
            });
        }
        
        if !showServicesView.isHidden {
            UIView.animate(withDuration: 0.25, animations: {
                self.showServicesView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                self.showServicesView.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)  {
                    self.showServicesView.isHidden = true
                }
            });
        }
        
    }
    
    func show(animated:Bool, displayView: UIView){
        if animated {
            
            //            readerListView.alpha = 0.5
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let popOverVC = story.instantiateViewController(withIdentifier: "ViewProfileVC") as! ViewProfileVC
            self.addChildViewController(popOverVC)
            //            popOverVC.view.frame = self.view.frame
            popOverVC.modalPresentationStyle = .popover
            
            self.view.addSubview(displayView)
            popOverVC.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
            popOverVC.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "Arial", size: 18)!]
            
            popOverVC.didMove(toParentViewController: self)
            
            //animate
            //            displayView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            //            self.view.alpha = 0.7;
            //            readerListView.alpha = 0.6
            
            UIView.animate(withDuration: 0.25, animations: {
                displayView.alpha = 1.0
                displayView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            });
            
        }else{
        }
    }
    
    func loadFromActorCatCache() -> Bool {
        if let v = UserDefaults.standard.dictionary(forKey: "getuserProfileByChatId"+"\(self.chat_id)") {
            //            print("v: \(v)")
            //            let value = Mapper<FindingActorResponseModel>().map(JSON: v)
            
            if let readerData = Mapper<User_detail>().map(JSON: v) {
                //let readerData = value?.readerData {
                self.readerArr = readerData
                if let skillData = readerData.skill_detail {
                    self.skillData = skillData
                }
                
                if let ratingData = readerData.rating_detail {
                    self.ratingData = ratingData
                }
                
                if let serviceArr = readerData.service_detail {
                    self.serviceNameArr = serviceArr
                    self.serviceTableView.reloadData()
                }
                
                if let img = self.readerArr.headshot_compressed_image {
                    profileImg.sd_setShowActivityIndicatorView(true)
                    profileImg.sd_setIndicatorStyle(.gray)
                    profileImg.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
                } else {
                    profileImg.image = UIImage(named: "ic_defaultProfile")
                }
                if let name = self.readerArr.user_name {
                    self.profileName.text = name
                    
                }
                if let price = self.readerArr.hourly_rate {
                    self.priceLbl.text = " $\(price)"
                    //                    profileName.isHidden = false
                    //                    priceLbl.isHidden = false
                    //                    hourLbl.isHidden = false
                }
                self.hourLbl.text = "/Hour"
                if let isSlat = self.readerArr.is_slate {
                    if isSlat {
                        self.showVideoBtn.isHidden = false
                    } else {
                        self.showVideoBtn.isHidden = true
                    }
                }
                
                if let serviceArr = self.readerArr.service_detail {
                    if !serviceArr.isEmpty {
                        serviceNameArr = serviceArr
                        self.showServiceBtn.isHidden = false
//                        UIView.animate(withDuration: 1, delay: 0, options: [.autoreverse, .repeat, .curveEaseInOut, .allowUserInteraction], animations: {
//                                        self.showServiceBtn.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//                        }, completion:nil)
                    } else {
                        self.showServiceBtn.isHidden = true
                    }
                }
                
                if let totalFriend = self.readerArr.mutual_friend {
                    if totalFriend != 0 {
                        mutualFndLbl.text = "\(totalFriend) Mutual Friends"
                    } else {
                        mutualFndLbl.isHidden = true
                    }
                } else {
                    mutualFndLbl.isHidden = true
                }
            }
            
            
            //            if let readerData = value?.reader_list {
            //                if !readerData.isEmpty {
            //                    self.readerArrList = readerData
            //                    var skillList : [AddSkillsRequestModel] = []
            //                    var ratingList : [RatingResponseModel] = []
            //                    for i in 0..<readerArrList.count {
            //                        if readerArrList[i].chat_id == chat_id {
            //                            readerArr = readerArrList[i]
            //                            print(readerArrList[i])
            //                            if let img = self.readerArrList[i].headshot_compressed_image {
            //                                profileImg.sd_setShowActivityIndicatorView(true)
            //                                profileImg.sd_setIndicatorStyle(.gray)
            //                                profileImg.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
            //                            }
            //                            if let name = self.readerArrList[i].user_name {
            //                                self.profileName.text = name
            //                            }
            //                            if let skill_list = self.readerArrList[i].skill_detail {
            //                                //                    self.skillData.append(skill_list)
            //                                skillList.append(skill_list)
            //                            }
            //                            if let rating_list = self.readerArrList[i].rating_detail {
            //                                //                    self.ratingData.append(rating_list)
            //                                ratingList.append(rating_list)
            //                            }
            //                        }
            //
            //                    }
            //                    skillData = skillList
            //                    ratingData = ratingList
            //                }
            //            }
        }
        if readerArr != nil {
            return true
        } else {
            return false
        }
    }
    
    func getuserProfileByChatId(receiver_user_id: Int) {
        
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        let user = RatingRequestModel(chat_id: receiver_user_id)
        let data_temp = Mapper<RatingRequestModel>().toJSON(user)
        print("TokenHeader:- \(tokenHeader)")
        print("getuserProfileByChatId request:- \(data_temp)")
        alamofireManager.request(getUserProfileByChatId, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                       UserDefaults.standard.set(value.userDetailResponse?.toJSON(), forKey: "getuserProfileByChatId"+"\(self.chat_id)")
                        alert.hideView()
                        self.loadFromActorCatCache()
                        
                        break
                    case 201:
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.loadFromActorCatCache()
                        }else {
                            if !self.loadFromActorCatCache() {
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                                
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if !self.loadFromActorCatCache() {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        
                        break
                    }
                }
                break
            case .failure(let error):
                alert.hideView()
                
                if !self.loadFromActorCatCache() {
                    //                    SCLAlertView().showError(errorTitle, subTitle: serverError)
                    self.addErrorView()
                }
                print("Request failed with error: \(error.localizedDescription)")
                break
            }
            }.responseJSON { response in
                print("View user profile response:- \(response.result.value)")
        }
    }
    
    func addErrorView() {
        errorView = NoDataView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        errorView.backgroundColor = hexStringToUIColor(hex: "EEEEEE")
        errorView.errorIconsHidden = false
        errorView.configureView(message: failureViewMsg, image: UIImage(named: "ic_error")!)
        errorView.tapButton.addTarget(self, action: #selector(self.errorButtonClicked), for: .touchUpInside)
        self.view.addSubview(errorView)
        self.errorView.isHidden = false
        
    }
    
    @objc func errorButtonClicked(sender : UIButton) {
        if chat_id != nil {
            getuserProfileByChatId(receiver_user_id: chat_id)
        }
        errorView.isHidden = true
        //        tableview.delegate = self
        //        tableview.dataSource = self
        //        tableview.separatorColor = hexStringToUIColor(hex: lineSeperator)
    }
    
    @IBAction func showVideoBtnClicked(_ sender: Any) {
        if let videoUrl = readerArr.video_detail?.video_file {
            if videoUrl.count > 0 {
                videoPlayerVC.view.frame = self.view.bounds
                videoPlayer = AVPlayer(url: URL(string: videoUrl)!)
                videoPlayerVC.player = videoPlayer
                self.present(videoPlayerVC, animated: true, completion: {
                    self.videoPlayer?.play()
                })
            }
        }
    }
    
    @IBAction func showServiceBtnClicked(_ sender: Any) {
        
//        serviceNameArr = readerArr.service_detail!
//        serviceTableView.reloadData()
        showServicesView.isHidden = false
        show(animated: true, displayView: showServicesView)
    }
    
    
}

extension ViewProfileVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serviceNameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TappingCell
        if let name = serviceNameArr[indexPath.row].service_name {
            cell.tappingLblTxt.text = name
        }
        return cell
    }
}
