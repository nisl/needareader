//
//  NotificationsVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 1/29/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SCLAlertView
import AlamofireObjectMapper
import ObjectMapper
import ReachabilitySwift
import SDWebImage
import Stripe
import SwipeCellKit


class NotificationsVC : UIViewController {
    
    @IBOutlet var noDataView: UIView!
    @IBOutlet var tableview: UITableView!
    
    var notificationArr : [Notification_list] = []
    var job_detail : Job_detail!
//    var defaultOptions = SwipeOptions()
//    var isSwipeRightEnabled = true
//    var buttonDisplayMode: ButtonDisplayMode = .titleAndImage
//    var buttonStyle: ButtonStyle = .backgroundColor
//    var usesTallCells = false

    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        //        self.navigationItem.setHidesBackButton(true, animated: true)
        tableview.delegate = self
        tableview.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        getNotification()
    }
    
    //Set NavigationBar
    func configureNavigationBar() {
        setNavigaion(vc: self)
        let navigationItem = self.navigationItem
        navigationItem.title = "NOTIFICATIONS"
        
        self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        self.navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        let settingBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        
        if #available(iOS 11.0, *) {
            let settingBtnwidthConstraint = settingBtn.widthAnchor.constraint(equalToConstant: 44)
            let settingBtnheightConstraint = settingBtn.heightAnchor.constraint(equalToConstant: 44)
            settingBtnheightConstraint.isActive = true
            settingBtnwidthConstraint.isActive = true
            settingBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 8, 16)//UIEdgeInsetsMake(8, 0, 8, 16)
        }else {
            settingBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 8, 16)//UIEdgeInsetsMake(8, 0, 6, 12)
        }
        
        settingBtn.setImage(UIImage(named: "ic_setting"), for: .normal)
        settingBtn.addTarget(self, action: #selector(self.settingBtnPressed(_:)), for: .touchUpInside)
        let settingButton = UIBarButtonItem(customView: settingBtn)
        navigationItem.leftBarButtonItem = settingButton

    }
    
    @objc func settingBtnPressed(_ sender: UIButton) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func loadFromNotificationCache() -> Bool {
        if let v = UserDefaults.standard.dictionary(forKey: getAllNotificationForSync) {
            
            let value = Mapper<NotificationResponseModel>().map(JSON: v)
            print("NotificationData:- \(v)")
            if let arr = value?.notification_list {
                notificationArr = arr
            }
        }
        
        if !notificationArr.isEmpty {
            noDataView.isHidden = true
            tableview.reloadData()
            return true
        } else {
            noDataView.isHidden = false
            return false
        }
        
    }
    
    
    func getNotification() {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        print("TokenHeader:- \(tokenHeader)")
        alamofireManager.request(getAllNotificationForSync, method: .post, parameters: [:], encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        UserDefaults.standard.set(value.notificationResponse?.toJSON(), forKey: getAllNotificationForSync)
                        alert.hideView()
                        print("Success")
                        self.loadFromNotificationCache()
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.getNotification()
                        }else {
                            if !self.loadFromNotificationCache() {
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                                
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if !self.loadFromNotificationCache() {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        
                        break
                    }
                }
                break
            case .failure(let error):
                alert.hideView()
                
                if !self.loadFromNotificationCache() {
                    SCLAlertView().showError(errorTitle, subTitle: serverError)
                    //                    self.addErrorView()
                }
                
                print("Request failed with error: \(error.localizedDescription)")
                break
                
                
            }
            
        }
        
    }
    func readJobsForUser(Notificationid:Int?,jobDetail:Job_detail?) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        print("TokenHeader:- \(tokenHeader)")
        let user = NotificationReadtModel(notificationId: Notificationid)
        let data_temp = Mapper<NotificationReadtModel>().toJSON(user)
        print("parm:-\(data_temp)")
        
        alamofireManager.request(setReadMarkPhase3, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        
                        let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "ViewJobDetailVC") as! ViewJobDetailVC
                        nextvc.job_detail = jobDetail
                        nextvc.notification_id = Notificationid
                        self.navigationController?.pushViewController(nextvc, animated: true)
                        
//                        if let msg = value.message {
//
//                            SCLAlertView().showSuccess("Success", subTitle: msg, colorStyle: int_red)
//                        }
                       
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.readJobsForUser(Notificationid:Notificationid,jobDetail:jobDetail)
                        }else {
                           // if !self.loadFromJobsCache() {
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                            //}
                        }
                        break
                        
                    default:
                        alert.hideView()
                        //if !self.loadFromJobsCache() {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                       // }
                        
                        break
                    }
                }
                break
            case .failure(let error):
                alert.hideView()
                
                //if !self.loadFromJobsCache() {
                    SCLAlertView().showError(errorTitle, subTitle: serverError)
                    //                    self.addErrorView()
               // }
                print("Request failed with error: \(error.localizedDescription)")
                break

            }
            
            }.responseJSON { (response) in
                print(deleteJob+" response: \(response.result.value)")
        }
        
    }
}

extension NotificationsVC : UITableViewDataSource, UITableViewDelegate, SwipeTableViewCellDelegate {
    
    func createSelectedBackgroundView() -> UIView {
        let view = UIView()
        view.backgroundColor = hexStringToUIColor(hex: redColor)
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = notificationArr[indexPath.row]
        let cell = tableview.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! notificationViewCell
        cell.delegate = self
        cell.selectedBackgroundView = createSelectedBackgroundView()

        viewCorners.addCornerToView(view: cell.outerView, value: 5)
        viewCorners.addShadowToView(view: cell.outerView)
//        viewCorners.addCornerToView(view: cell.userProfile)
        viewCorners.addBorderCornerTobutton(view: cell.userProfile, color: hexStringToUIColor(hex: purpleColor))

        viewCorners.addCornerToView(view: cell.viewBtn)
        viewCorners.addCornerToView(view: cell.declineBtn)
        cell.declineBtn.isHidden = true
        cell.viewBtn.isHidden = true
        cell.userProfile.sd_setShowActivityIndicatorView(true)
        cell.userProfile.sd_setIndicatorStyle(.gray)

        if let quote = item.ntf_message {
            cell.userStatus.text = quote
        }
        if let time = item.create_time {
            let date = convertstringToDate(str: time)
            let dateFormatter = DateFormatter()
            let dateTime = dateFormatter.timeSince(from: date as NSDate, numericDates: true)
            print("date:- \(date)")
            cell.timeLbl.text = dateTime
        }
            //        viewCorners.addBorderCornerTobutton(view: cell.userProfile, color: hexStringToUIColor(hex: blackColor))
            
//            if let endTime = item.job_detail?.end_at {
//                let date = convertstringToDate(str: endTime)
                let status = item.notification_type
                print(status)
                switch status {
                case 1?:
                    //                if let approvalStatus = notificationArr[indexPath.row].job_detail?.approval_status {
                    //                    if approvalStatus == 0 {
                    //                        cell.viewBtn.isHidden = true
                    //                    } else {
                    //                        cell.viewBtn.isHidden = false
                    //
                    //                    }
                    //                } else {
                    //                    cell.viewBtn.isHidden = false
                    //                }
                    cell.viewBtn.isHidden = false
                    cell.viewBtn.setTitle("VIEW", for: .normal)
                    cell.viewBtn.tag = indexPath.row
                    cell.viewBtn.addTarget(self, action: #selector(self.viewPressed(_:)), for: .touchUpInside)
                    if let img = item.job_detail?.profile_img {
                        cell.userProfile.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
                    }
                    
                    if let metTime = item.job_detail?.meeting_time {
                        if metTime.count > 0 {
                            let date = convertMeetingTimeStringToDate(str: metTime)
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "MMM dd, yyyy 'at' hh:mm a"
                            let selectedDate = dateFormatter.string(from: date)
                            print("date:- \(selectedDate)")

                            cell.userStatus.text = cell.userStatus.text! + " on\n" + selectedDate //+ "."
                            
                        

                        }
                    }
                    if let meetType = item.job_detail?.meeting_type {
                        if meetType == 1 {
                            cell.userStatus.text = cell.userStatus.text! + " (In Person)" + "."
                        }else {
                            cell.userStatus.text = cell.userStatus.text! + " (Video Chat)" + "."
                        }
                    }
                    
                //                }
                case 3?:
                    cell.viewBtn.isHidden = true
                    if let img = item.job_detail?.profile_img {
                        cell.userProfile.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
                    }
                    break
                case 7?:
                    if let approvalStatus = item.friend_detail?.approval_status {
                        switch approvalStatus {
                        case 0:
                            cell.viewBtn.isHidden = false
                            cell.declineBtn.isHidden = false
                            cell.declineBtn.tag = indexPath.row
                            cell.declineBtn.addTarget(self, action: #selector(self.declineBtnPressed(_:)), for: .touchUpInside)

                            cell.viewBtn.setTitle("ACCEPT", for: .normal)
                            cell.viewBtn.tag = indexPath.row
                            cell.viewBtn.addTarget(self, action: #selector(self.viewPressed(_:)), for: .touchUpInside)
                            break
                        default:
                            cell.declineBtn.isHidden = true
                            cell.viewBtn.isHidden = true
                            break
                        }
                    }
                    if let img = item.friend_detail?.profile_img {
                        cell.userProfile.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
                    }
                    break
                case 8?, 9?, 10?:
                    cell.viewBtn.isHidden = true
                    cell.declineBtn.isHidden = true
                    if status == 10 {
                        if let img = item.job_detail?.profile_img {
                            cell.userProfile.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
                        }
                    } else {
                        if let img = item.friend_detail?.profile_img {
                            cell.userProfile.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
                        }
                    }
                    
                    break
                case 4?:
                    cell.viewBtn.setTitle("VIEW", for: .normal)
                    cell.viewBtn.isHidden = false
                    cell.viewBtn.tag = indexPath.row
                    cell.viewBtn.addTarget(self, action: #selector(self.viewPressed(_:)), for: .touchUpInside)
                    if let img = item.job_detail?.profile_img {
                        cell.userProfile.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
                    }
                    setNotificationForPayment(jobDetail: item.job_detail!)
                case 2?,5?,6?:
                    cell.viewBtn.setTitle("VIEW", for: .normal)
                    cell.viewBtn.isHidden = false
                    cell.viewBtn.tag = indexPath.row
                    cell.viewBtn.addTarget(self, action: #selector(self.viewPressed(_:)), for: .touchUpInside)
                    if let img = item.job_detail?.profile_img {
                        cell.userProfile.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
                    }

                    break
                default:
                    cell.viewBtn.isHidden = true
                    cell.declineBtn.isHidden = true
                    if let img = item.job_detail?.profile_img {
                        cell.userProfile.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
                    }
                    break
                }
        return cell
    }
    
    func setNotificationForPayment(jobDetail: Job_detail) {
        let jobCreationTime = jobDetail.create_time
        let date = convertstringToDate(str: jobCreationTime!)
        var dateComponent = DateComponents()
        dateComponent.day = -3

        let currentDate = Calendar.current.date(byAdding: dateComponent, to: Date())
        
//        let notificationDate = Calendar.current.date(byAdding: dateComponent, to: date)
        print("3 days before notificaiton:- \(date) && now:- \(date)")
        print("3 days before current date:- \(Date()) && now:- \(currentDate)")
        if let approvalStatus = jobDetail.approval_status {
            if approvalStatus == 4 {
//                if !(notificationDate! < currentDate!) {
                if ((date >= currentDate!)) {
                    setPaymentNotification(jobId: (jobDetail.job_id!), notificationDate: date)

                }
            } else {
                deleteNotificationByJobId(jobId: (jobDetail.job_id!), isFromAppdelegate: false)
            }
        }
    }
    // MARK:- VIew button presss
    @objc func viewPressed(_ sender: UIButton) {
        let sender = sender as! UIButton
        let indexpath = sender.tag
        if sender.titleLabel?.text == "VIEW" {
            let approvalStatus = notificationArr[indexpath].job_detail?.approval_status
            job_detail = notificationArr[indexpath].job_detail
            let notificationStatus = notificationArr[indexpath].notification_type
            let fullfillStatus =  notificationArr[indexpath].is_fulfill
            print(fullfillStatus)
            if fullfillStatus == 1 {
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: "This job has been filled.", closeButtonTitle: "Ok") //fulfilled
            }else {
                if notificationStatus == 1 {
                    if approvalStatus == 0 {
                        let alertview = SCLAlertView()
                        alertview.showError("Error", subTitle: "This job has been canceled.", closeButtonTitle: "Ok")
                    } else {
//                        let nextvc = storyboard?.instantiateViewController(withIdentifier: "ViewJobDetailVC") as! ViewJobDetailVC
//                        nextvc.job_detail = notificationArr[indexpath].job_detail
//                        nextvc.notification_id = notificationArr[indexpath].id
//                        self.navigationController?.pushViewController(nextvc, animated: true)
                        if approvalStatus == 1 {
                            let read = notificationArr[indexpath].is_read
                            print(read)
                            if read == 1 {
                                let nextvc = storyboard?.instantiateViewController(withIdentifier: "ViewJobDetailVC") as! ViewJobDetailVC
                                nextvc.job_detail = notificationArr[indexpath].job_detail
                                nextvc.notification_id = notificationArr[indexpath].id
                                self.navigationController?.pushViewController(nextvc, animated: true)
                            }else {
                                self.readJobsForUser(Notificationid: notificationArr[indexpath].id, jobDetail: notificationArr[indexpath].job_detail)
                            }
                        }else {
                            let nextvc = storyboard?.instantiateViewController(withIdentifier: "ViewJobDetailVC") as! ViewJobDetailVC
                            nextvc.job_detail = notificationArr[indexpath].job_detail
                            nextvc.notification_id = notificationArr[indexpath].id
                            self.navigationController?.pushViewController(nextvc, animated: true)
                        }

                    }
                }else {
                    let nextvc = storyboard?.instantiateViewController(withIdentifier: "ViewJobDetailVC") as! ViewJobDetailVC
                    nextvc.job_detail = notificationArr[indexpath].job_detail
                    nextvc.notification_id = notificationArr[indexpath].id
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }

            }
        } else {
            let friendDetail = notificationArr[indexpath].friend_detail
            acceptRejectNotification(friendId: (friendDetail?.friend_id)!, isAccept: true)
            
        }
        
    }
    
//    @objc func acceptBtnPressed(_ sender: UIButton) {
//        let sender = sender as! UIButton
//        let indexpath = sender.tag
//        let friendDetail = notificationArr[indexpath].friend_detail
//        acceptRejectNotification(friendId: (friendDetail?.friend_id)!, isAccept: true)
//    }
    
    @objc func declineBtnPressed(_ sender: UIButton) {
        let sender = sender as! UIButton
        let indexpath = sender.tag
        let friendDetail = notificationArr[indexpath].friend_detail
        acceptRejectNotification(friendId: (friendDetail?.friend_id)!, isAccept: false)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
        
    }
    
    //    Editing table row
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            self.deleteNotification(notificationId: self.notificationArr[indexPath.row].id!, indexPath: indexPath)
        }
    }
    
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
////        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (rowAction, indexPath) in
//        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
//            //TODO: Delete the row at indexPath here
//            //"ic_delete_not"
//        }
//        
//        deleteAction.backgroundColor = hexStringToUIColor(hex: redColor)
//        // customize the action appearance
//        deleteAction.image = UIImage(named: "ic_delete_not")
//        return [deleteAction]
//
//    }
//    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }

        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
            //TODO: Delete the row at indexPath here
            //"ic_delete_not"
            let alertview = UIAlertController(title: "Alert", message: "Are you sure you want to delete this notification?", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                self.deleteNotification(notificationId: self.notificationArr[indexPath.row].id!, indexPath: indexPath)

            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            alertview.addAction(okAction)
            alertview.addAction(cancelAction)
            
            self.present(alertview, animated: true, completion: nil)

        }
        
        deleteAction.title = ""
        deleteAction.backgroundColor = hexStringToUIColor(hex: redColor)
        // customize the action appearance
        deleteAction.image = UIImage(named: "ic_delete_not")
        return [deleteAction]
    }

    
    func acceptRejectNotification(friendId : Int, isAccept : Bool) {
        var user : NotificationRequestModel!
        var apiName = ""
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        //Object to be serialized to JSON
            user = NotificationRequestModel(friend_id: friendId)
        if isAccept {
            apiName = acceptFriendRequest
        } else {
            apiName = rejectFriendRequest
        }
        //convert object to dictionary
        let data_temp = Mapper<NotificationRequestModel>().toJSON(user)
        print("data:- \(data_temp)")
        alamofireManager.request(apiName, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        defaults.set(true, forKey: "changeStatus")
                        if let msg = value.message {
                            SCLAlertView().showSuccess("Success", subTitle: msg, colorStyle: int_red)
                        }
                        self.getNotification()
                        print("success")
                        break
                    case 400:
                        alert.hideView()
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.acceptRejectNotification(friendId: friendId, isAccept: isAccept)
                        }else {
                            if let error = value.message {
                                SCLAlertView().showError("Error", subTitle: error)
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            SCLAlertView().showError("Error", subTitle: error)
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                print("error:- \(error.localizedDescription)")
                alert.hideView()
                SCLAlertView().showError("Error", subTitle: serverError)
                break
                
            }
            
            }.responseJSON { response in
                print("response acceptReject notification:- \(response.result.value)")
        }
    }
    
    func deleteNotification(notificationId: Int, indexPath: IndexPath) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        let user = NotificationRequestModel(notificationId: notificationId)
        let data_temp = Mapper<NotificationRequestModel>().toJSON(user)
        print("Delete Notification request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(deleteNotificationById, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        self.notificationArr.remove(at: indexPath.row)
                        self.tableview.deleteRows(at: [indexPath], with: .fade)
                        self.tableview.reloadData()
                        break
                    case 400:
                        alert.hideView()
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
//                        let nav = UINavigationController(rootViewController: vc)
//                        nav.navigationBar.isTranslucent = false
//                        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
//                        nav.navigationBar.shadowImage = UIImage()
//                        nav.interactivePopGestureRecognizer?.isEnabled = false
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.deleteNotification(notificationId: notificationId, indexPath: indexPath)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
}


