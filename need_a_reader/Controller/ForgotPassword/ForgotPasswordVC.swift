//
//  ForgotPasswordVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/15/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SCLAlertView
import AlamofireObjectMapper
import ObjectMapper
import ReachabilitySwift

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet var emailTxt: UITextField!

    @IBOutlet var nextBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.navigationBar.isHidden = false
        configureNavigationBar()
        self.view.backgroundColor = hexStringToUIColor(hex: "F5F3F4")
        if UIDevice.current.userInterfaceIdiom == .pad {
            nextBtn.frame.size.height = 70
        }
        viewCorners.addBorderCornerTobutton(view: nextBtn, color: UIColor.white)
        nextBtn.backgroundColor = UIColor.white
        nextBtn.setTitleColor(hexStringToUIColor(hex: redColor), for: .normal)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //Set NavigationBar
    func configureNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "OpenSans-Semibold", size: 18.0)]
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        let navigationItem = self.navigationItem
        navigationItem.title = "FORGOT PASSWORD"
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        self.view.endEditing(true)
        testValidation()
    }
    
    func testValidation()  {
        let email = emailTxt.text!
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)

        if !checkLength(testStr: email){
            alert.hideView()
            emptyFieldError(str: "email.")
        } else if isValidEmail(testStr: email) {
                if (Reachability()?.isReachable)! {

                    self.forgotPwdForSendOtp(email_id: email)
                    
                } else {
                    alert.hideView()
                    noInternetMsgDialog()
                }
        }else {
            alert.hideView()
            let alertview = SCLAlertView()
            alertview.showError("Error", subTitle: invalidEmailMessage, closeButtonTitle: "Ok")
        }
        
    }
    
    func forgotPwdForSendOtp(email_id: String) {
       
            
            //Object to be serialized to JSON
            let user = ForgotPasswordRequestModel(email_id: email_id)
            
            
            //convert object to dictionary
            let data_temp = Mapper<ForgotPasswordRequestModel>().toJSON(user)
            print("data:- \(data_temp)")
            alamofireManager.request(forgotPasswordForSendOTP, method: .post, parameters: data_temp, encoding: JSONEncoding(options: [])).responseObject{ (response: DataResponse<ResponseModel> )in
                switch response.result
                {
                case .success(let value):
                    if let code = value.code {
                        print("Code:- \(code)")
                        switch code {
                        case 200:
                            alert.hideView()
                            
                            let alertView = SCLAlertView()
                            alertView.showInfo("", subTitle: value.message!, colorStyle: int_red)
                            
                            let story = UIStoryboard(name: "Main", bundle: nil)
                            let nextVC = story.instantiateViewController(withIdentifier: "VerifyOtpVC") as! VerifyOtpVC
                            nextVC.isForgotPasswordVC = true
                            
                            nextVC.email = email_id
                            self.navigationController?.pushViewController(nextVC, animated: true)
                            break
                        case 400:
                            alert.hideView()
                            
                            clearSession()
                            let story = UIStoryboard(name: "Main", bundle: nil)
                            let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                            let nav = redirectToRootViewController(vcName: vc)
                            self.present(nav, animated: true)
                            break
                            
                        case 401:
                            alert.hideView()
                            if let new_token = value.signUpResponse?.new_token {
                                UserDefaults.standard.set(new_token, forKey: "token")
                                self.forgotPwdForSendOtp(email_id: email_id)
                            }else {
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                            }
                            break
                        default:
                            alert.hideView()
                            
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                            break
                        }
                    }
                    break
                case .failure(let error):
                    print("error: \(error.localizedDescription)")
                    
                    alert.hideView()
                    let alertview = SCLAlertView()
                    alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                    break
                default:
                    alert.hideView()
                    break
                }
            }
    }
}
