//
//  AddNewPasswordVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/15/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SCLAlertView
import AlamofireObjectMapper
import ObjectMapper
import ReachabilitySwift

class AddNewPasswordVC: UIViewController {
    
    @IBOutlet var infoLbl: UILabel!
    @IBOutlet var newPasswordLbl: UILabel!
    @IBOutlet var currPasswordtxt: UITextField!
    @IBOutlet var currPasswordViewConstant: NSLayoutConstraint!
    @IBOutlet var currentPasswordView: UIView!
    @IBOutlet var currentPasswordLbl: UILabel!
    @IBOutlet var newPwdTxt: UITextField!
    @IBOutlet var confirmPwdTxt: UITextField!
    @IBOutlet var submitBtn: UIButton!
    var email = ""
    var isChangePassword : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        self.view.backgroundColor = hexStringToUIColor(hex: "F5F3F4")
        viewCorners.addBorderCornerTobutton(view: submitBtn, color: UIColor.white)
        submitBtn.backgroundColor = UIColor.white
        submitBtn.setTitleColor(hexStringToUIColor(hex: redColor), for: .normal)
        // Do any additional setup after loading the view, typically from a nib.
        if isChangePassword {
            submitBtn.setTitle("SAVE", for: .normal)
            changeButtonColor(view: [submitBtn], backgroundcolor: redColor, fontColor: "ffffff")
        }else {
            currPasswordViewConstant.constant = 0
            currentPasswordLbl.isHidden = true
            currentPasswordView.isHidden = true
            newPasswordLbl.translatesAutoresizingMaskIntoConstraints = false
            
            view.addConstraint(NSLayoutConstraint(item: newPasswordLbl, attribute: .top, relatedBy: .equal, toItem: infoLbl, attribute: .bottom, multiplier: 1, constant: 20))
        }
    }
    
    //Set NavigationBar
    func configureNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "OpenSans-Semibold", size: 18.0)]
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        let navigationItem = self.navigationItem
        navigationItem.title = "RESET PASSWORD"
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        if isChangePassword {
            self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: "F5F3F4")
            navigationItem.title = "CHANGE PASSWORD"
        }
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        self.view.endEditing(true)
        testValidation()
    }
    
    func testValidation() {
        let password = newPwdTxt.text!
        let cPassword = confirmPwdTxt.text!
        let curr_password = currPasswordtxt.text!
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        if !currentPasswordView.isHidden {
            if !checkLength(testStr: curr_password) {
                alert.hideView()
                emptyFieldError(str: "current password.")
            }else if !checkLength(testStr: password) {
                alert.hideView()
                emptyFieldError(str: "password.")
            }else {
                if validatePassword(testStr: password)
                {
                    if password != cPassword {
                        alert.hideView()
                        let alertView = SCLAlertView()
                        alertView.showError("Error", subTitle: "Password mismatch!", closeButtonTitle: "Ok")
                    } else {
                        
                        if (Reachability()?.isReachable)! {
                            changePasswordForUser(password: password, curr_password: curr_password)
                        } else {
                            alert.hideView()
                            noInternetMsgDialog()
                        }
                    }
                } else {
                    alert.hideView()
                    let alertview = SCLAlertView()
                    alertview.showError("Error", subTitle: passwordlengthMessage, closeButtonTitle: "Ok")
                    
                }
            }
        }else if !checkLength(testStr: password) {
            alert.hideView()
            emptyFieldError(str: "password.")
        }else {
            if validatePassword(testStr: password)
            {
                if password != cPassword {
                    alert.hideView()
                    let alertView = SCLAlertView()
                    alertView.showError("Error", subTitle: "Password mismatch!", closeButtonTitle: "Ok")
                } else {
                    
                    if (Reachability()?.isReachable)! {
                        addNewPassword(email: email, password: password)
                        
                    } else {
                        alert.hideView()
                        noInternetMsgDialog()
                    }
                }
            } else {
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: passwordlengthMessage, closeButtonTitle: "Ok")
                
            }
        }
    }
    
    func addNewPassword(email: String, password: String) {
        //let email = oldPwdTxt.text
        
        let token = defaults.value(forKey: "otp_token")
        //Object to be serialized to JSON
        let user = ChangePasswordModel(email_id: email, new_password: password, token: token as! String)
        
        
        //convert object to dictionary
        let data_temp = Mapper<ChangePasswordModel>().toJSON(user)
        alamofireManager.request(addNewPasswordForUser, method: .post, parameters: data_temp, encoding: JSONEncoding(options: [])).responseObject{ (response: DataResponse<ResponseModel> )in
            
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    switch code {
                    case 200:
                        alert.hideView()
                        let alertView = SCLAlertView()
                        alertView.showSuccess("Success", subTitle: passwordChangeMessage, closeButtonTitle: "Ok",  colorStyle: int_red)
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let nextVC = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(nextVC, animated: true)
                        //                                let nav = UINavigationController(rootViewController: nextVC)
                        //
                        //                                self.present(nav, animated: true, completion: nil)
                        
                        
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                        
                    case 401:
                        alert.hideView()
                        if let new_token = value.signUpResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.addNewPassword(email: email, password: password)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                
                break
                
            default:
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
            
        }
        
        
    }
    
    func changePasswordForUser(password: String, curr_password: String) {
        //let email = oldPwdTxt.text
        
        let token = defaults.value(forKey: "otp_token")
        //Object to be serialized to JSON
        let user = ChangePasswordModel(new_password: password, current_password: curr_password)
        
        
        //convert object to dictionary
        let data_temp = Mapper<ChangePasswordModel>().toJSON(user)
        alamofireManager.request(changePassword, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    switch code {
                    case 200:
                        alert.hideView()
                        let alertView = SCLAlertView()
                        alertView.showSuccess("Success", subTitle: passwordChangeMessage, closeButtonTitle: "Ok",  colorStyle: int_red)
                        //                        let story = UIStoryboard(name: "Main", bundle: nil)
                        //                        let nextVC = story.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
                        //                        self.navigationController?.pushViewController(nextVC, animated: true)
                        self.navigationController?.popViewController(animated: true)
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                        
                    case 401:
                        alert.hideView()
                        if let new_token = value.signUpResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.changePasswordForUser(password: password, curr_password: curr_password)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                
                break
            }
            
        }
    }
}

