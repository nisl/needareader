
//
//  ContactVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 1/26/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import ReachabilitySwift
import SCLAlertView
import MessageUI

class ContactVC: UIViewController, UISearchBarDelegate {
    
    @IBOutlet var collectionviewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var nextBtnBottomConstant: NSLayoutConstraint!
    @IBOutlet var contactNodataView: UIView!
    @IBOutlet var noDataFoundView: UIView!
    @IBOutlet var tableview: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var contactListView: UIView!
    @IBOutlet var contactSearchView: UIView!
    @IBOutlet var nextBtn: UIButton!
    @IBOutlet var collViewBottomConstant: NSLayoutConstraint!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var comingSoonView: UIView!

    @IBOutlet var tapHereBtn: UIButton!
    @IBOutlet var tapHereLbl: UILabel!
    @IBOutlet var nextBtnHeightConstant: NSLayoutConstraint!
    var userDetails : [User_detail] = []
    var addedContactDetails : [User_detail] = []
    let backBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
    let addBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
    var contactArr : [String] = []
    var inviteUserIdArr : [Int] = []
    var isFromHome : Bool = false
    var isFirstTime : Bool = true
    var isContactPickerOpen = false
    var isFromTransferToken = false
    var selectedIndexPath : IndexPath!
    var noDataView : NoDataView!
    var errorView : NoDataView!

    var cellWidth : CGFloat {
        let collWidth = screenWidth - 40
        let height = collWidth
        print("height :- \(height)")
        if UIDevice.current.userInterfaceIdiom == .pad {
            return (collWidth) / 4.5
        } else {
            return (collWidth) / 3.5
        }
//        let width = (collWidth) / 3.5
//        return  width
    }
    
    var is_running : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        // Do any additional setup after loading the view, typically from a nib.
        collectionView.delegate = self
        collectionView.dataSource = self
        tableview.delegate = self
        tableview.dataSource = self
        searchBar.delegate = self

        if UIDevice.current.userInterfaceIdiom == .pad {
            tapHereBtn.frame.size.height = 70
            tapHereBtn.frame.size.width = 300

        }
        viewCorners.addCornerToView(view: tapHereBtn)

//        addNoDataView()

        searchBar.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
        searchBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        searchBar.layer.borderColor = hexStringToUIColor(hex: lightGrayColor).cgColor
        searchBar.layer.borderWidth = 1

        for view in searchBar.subviews {
            for subview in view.subviews {
                if subview is UITextField {
                    let textField: UITextField = subview as! UITextField
                    print("textfield height:- \(textField.frame.size.height)")
                    textField.frame.size.height = 45
                    print("textfield height1:- \(textField.frame.size.height)")

                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
            contactSearchView.isHidden = true
            contactNodataView.isHidden = true
            contactListView.isHidden = false
            comingSoonView.isHidden = true
            self.addBtn.isHidden = false
            collectionviewBottomConstraint.constant = 10
            callFriendFunc()
            setBackBtn()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        if !contactSearchView.isHidden {
            contactSearchView.isHidden = true
            addBtn.isHidden = false
        }
    }
    
    func callFriendFunc() {
        if (Reachability()?.isReachable)! {
            fetchFriends()
        } else {
            hideContactWhenNoData()
            noInternetMsgDialog()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        let str = searchBar.text!
//        if str.count == 1 {
//            if isFirstTime {
//                alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
//            }
//        }
//        if str.count == 0 {
//            isFirstTime = true
//        } else if str.count > 1 {
//            isFirstTime = false
//        }
//        if str != "" {
////            noDataFoundView.isHidden = true
//            if !is_running {
//
//                searchUserContact(searchStr: str)
//            }
//        } else {
//            noDataFoundView.isHidden = false
//        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_violet)
        if let str = searchBar.text {
            if str == "" {
                
            }else {
                 searchUserContact(searchStr: str)
            }
          
        }
        self.view.endEditing(true)
    }
    
    func refreshUIWhenNotificationDelivered() {
        if contactSearchView.isHidden {
            callFriendFunc()
//            fetchFriends()
        }
    }
    
    func displayFailureView() {
        self.noDataView.isHidden = true
        if self.errorView != nil {
            self.errorView.isHidden = false
        } else {
            self.addErrorView()
        }
    }
    
    func searchUserContact(searchStr: String) {
        is_running = true

        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        print("searchStr:- \(searchStr)")
        //Object to be serialized to JSON
        let user = ContactRequestModel(search_query: searchStr)
        //convert object to dictionary
        let data_temp = Mapper<ContactRequestModel>().toJSON(user)
        print("data:- \(data_temp)")
        alamofireManager.request(searchUser, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            //           
            let responseClass = response.result.value
            self.is_running = false

            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        if let data = value.contactReponse?.searchUserDetails {
                            self.userDetails = data
                        }
                        
                        if self.userDetails.count > 0 {
                            self.noDataFoundView.isHidden = true

                            self.addBtn.isHidden = true
                            self.tableview.reloadData()
                        } else {
                            self.noDataFoundView.isHidden = false
                        }
                        print("success")
                        break
                    case 400:
                        alert.hideView()
                        self.view.endEditing(true)
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                        
                    case 401:
                        alert.hideView()
                        self.view.endEditing(true)
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.searchUserContact(searchStr: searchStr)
                        }else {
                            if let error = value.message {
                                SCLAlertView().showError("Error", subTitle: error)
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        self.view.endEditing(true)
                        if let error = value.message {
                            SCLAlertView().showError("Error", subTitle: error)
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                print("error:- \(error.localizedDescription)")
                alert.hideView()
                self.view.endEditing(true)
                SCLAlertView().showError("Error", subTitle: serverError)
                self.displayFailureView()
                break
                
            default:
                alert.hideView()
                self.view.endEditing(true)
                break
                
            }
            
            }.responseJSON { response in
                print("response signUp:- \(response.result.value)")
                
        }
    }
    
    
    
    
    //Set NavigationBar
    func configureNavigationBar()
    {
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        let navigationItem = self.navigationItem
        navigationItem.title = "MY FRIENDS"//"Going with"
        setNavigaion(vc: self)
        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setBackBtn()

        if #available(iOS 11.0, *) {
            let widthConstraint = backBtn.widthAnchor.constraint(equalToConstant: 44)
            let heightConstraint = backBtn.heightAnchor.constraint(equalToConstant: 44)
            heightConstraint.isActive = true
            widthConstraint.isActive = true
//            UIEdgeInsetsMake(CGFloat, <#T##left: CGFloat##CGFloat#>, <#T##bottom: CGFloat##CGFloat#>, <#T##right: CGFloat##CGFloat#>)
//            backBtn.imageEdgeInsets = UIEdgeInsetsMake(8, 0, 8, 10)
//            backBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 8, 16)
            let addBtnwidthConstraint = addBtn.widthAnchor.constraint(equalToConstant: 44)
            let addBtnheightConstraint = addBtn.heightAnchor.constraint(equalToConstant: 44)
            addBtnheightConstraint.isActive = true
            addBtnwidthConstraint.isActive = true
            addBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 0)//UIEdgeInsetsMake(8, 0, 8, 16)
        }else {
            addBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 0)//UIEdgeInsetsMake(8, 0, 6, 12)
//            backBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 8, 10)
        }

        backBtn.addTarget(self, action: #selector(backBtnPressed(_:)), for: .touchUpInside)
        addBtn.setImage(UIImage(named: "ic_addFriend"), for: .normal)
        addBtn.addTarget(self, action: #selector(addBtnPressed(_:)), for: .touchUpInside)
        let backButton = UIBarButtonItem(customView: backBtn)
        let addButton = UIBarButtonItem(customView: addBtn)
        navigationItem.leftBarButtonItem = backButton
        navigationItem.rightBarButtonItems = [addButton]
    }
    
    
    func setBackBtn() {
        backBtn.contentEdgeInsets = UIEdgeInsets.zero
        backBtn.imageEdgeInsets = UIEdgeInsets.zero
        if contactSearchView.isHidden {
            backBtn.setImage(UIImage(named: "ic_setting"), for: .normal)
            backBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 8, 16)//UIEdgeInsetsMake(8, 0, 6, 12)
        } else {
            backBtn.setImage(UIImage(named: "ic_backArrow"), for: .normal)
            backBtn.contentEdgeInsets = UIEdgeInsetsMake(12,-10,12,30)
            
        }
    }
    
    @objc func backBtnPressed(_ sender: UIButton) {
            if !contactSearchView.isHidden {
//                self.navigationItem.titleView = nil
//                self.navigationItem.title = "Going with"
                self.view.endEditing(true)
                contactSearchView.isHidden = true
                contactListView.isHidden = false
                self.setBackBtn()
                self.addBtn.isHidden = false
            } else {
//                self.navigationController?.popViewController(animated: true)
                let vc = storyBoard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
    }
    
    @objc func addBtnPressed(_ sender: UIButton) {
       openSearchContactView()
    }
    
    func openSearchContactView() {
        contactSearchView.isHidden = false
        contactListView.isHidden = true
        searchBar.text = ""
        addBtn.isHidden = true
        setBackBtn()
//        backBtn.isHidden = false
    }
    
    func addFriend(userId: Int, selectedRow: Int) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        //Object to be serialized to JSON
        let user = ContactRequestModel(receiver_user_id: userId)
        //convert object to dictionary
        let data_temp = Mapper<ContactRequestModel>().toJSON(user)
        print("data:- \(data_temp)")
        alamofireManager.request(sendFriendRequest, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        let indexpath = IndexPath(row: selectedRow, section: 0)
                        self.userDetails.remove(at: selectedRow)
                        self.tableview.deleteRows(at: [indexpath], with: .fade)
                        self.tableview.reloadData()
//                        self.tableview.reloadRows(at: [indexpath], with: .fade)
                        SCLAlertView().showSuccess("Success", subTitle: value.message!, colorStyle: int_red)
//                        displayError(str: value.message!, title: "Success")
                        print("success")
                        break
                    case 400:
                        alert.hideView()
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.addFriend(userId: userId, selectedRow: selectedRow)
                        }else {
                            if let error = value.message {
                                SCLAlertView().showError("Error", subTitle: error)
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            SCLAlertView().showError("Error", subTitle: error)
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                print("error:- \(error.localizedDescription)")
                alert.hideView()
                SCLAlertView().showError("Error", subTitle: serverError)
                break
            default:
                alert.hideView()
                break
            }
            
            }.responseJSON { response in
                print("response signUp:- \(response.result.value)")
                
        }
    }
    
    

    func fetchFriends() {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        print("token:- \(tokenHeader)")
        alamofireManager.request(getMyFriendList, method: .post, parameters: [:], encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        if let data = value.contactReponse?.myContactLists {
                            self.addedContactDetails = data
                        }
                        if self.addedContactDetails.count > 0{
                            self.contactNodataView.isHidden = true
//                            self.nextBtn.isHidden = false
                             self.collectionView.isHidden = false
                            self.collectionView.reloadData()

                        } else {
                             self.contactNodataView.isHidden = false
                            self.collectionView.isHidden = true

                        }
                        
                        print("success")
                        break
                        
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                        
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.fetchFriends()
                        }else {
                            if let error = value.message {
                                SCLAlertView().showError("Error", subTitle: error)
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        self.hideContactWhenNoData()
                        if let error = value.message {
                            SCLAlertView().showError("Error", subTitle: error)
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                print("error:- \(error.localizedDescription)")
                alert.hideView()
                self.hideContactWhenNoData()
                SCLAlertView().showError("Error", subTitle: serverError)
                break
                
            default:
                alert.hideView()
                break
                
            }
            
            }.responseJSON { response in
                print("response fetch Friend:- \(response.result.value)")
                
        }
    }
    
    func hideContactWhenNoData() {
        self.contactNodataView.isHidden = false
        self.contactListView.isHidden = false

    }
    
}

extension ContactVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return addedContactDetails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ContactCellVC
        print("height:- \(cell.frame.size.height)")
        print("width:- \(cell.frame.size.width)")
//        viewCorners.addCornerToView(view: cell.profileImgview)
//        cell.profileImgview.frame.size.height = (cellWidth + 20) * 0.8
        cell.profileImgview.frame.size.height = cellWidth
        cell.profileImgview.frame.size.width = cell.profileImgview.frame.size.height
        
        print("width:- \(cell.profileImgview.frame.size.width), height:- \(cell.profileImgview.frame.size.height)---\(cellWidth)")
        
        viewCorners.addBorderCornerTobutton(view: cell.profileImgview, color: hexStringToUIColor(hex: purpleColor))
//        viewCorners.addCornerToView(view: cell.profileImgview, value: (cell.profileImgview.frame.size.height) / 2)
        cell.userNameLbl.text = addedContactDetails[indexPath.row].user_name
        cell.selectedImgView.isHidden = true

        cell.profileImgview.sd_setShowActivityIndicatorView(true)
        cell.profileImgview.sd_setIndicatorStyle(.gray)
        cell.profileImgview.sd_setImage(with: URL(string: addedContactDetails[indexPath.row].headshot_compressed_image!), placeholderImage: UIImage(named: "ic_defaultProfile"))
        if self.selectedIndexPath != nil && indexPath == selectedIndexPath {
            cell.selectedImgView.isHidden = false
        }
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(self.removeUserBtnPressed(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func removeUserBtnPressed(_ sender: UIButton) {
        let tag = sender.tag
//        let indexpath = IndexPath(row: tag, section: 0)
        let alertview = UIAlertController(title: "Alert", message: "Are you sure you want to unfriend this user?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            self.removeFriend(userId: self.addedContactDetails[tag].user_id!, selectedRow: tag)

        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alertview.addAction(okAction)
        alertview.addAction(cancelAction)
        
        self.present(alertview, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let cell = collectionView.cellForItem(at: indexPath) as! ContactCellVC

        let nextVC = storyboard?.instantiateViewController(withIdentifier: "OneHireReaderVC") as! OneHireReaderVC
        nextVC.userDetail = addedContactDetails[indexPath.row]
        self.navigationController?.pushViewController(nextVC, animated: true)
        
    }
    
    
//    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
//        if isFromTransferToken {
//            let cell = collectionView.cellForItem(at: indexPath) as! ContactCellVC
//            cell.selectedImgView.isHidden = true
//            self.selectedIndexPath = nil
//
//        }
//    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
//        return CGSize(width: 100, height: 120)
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: cellWidth, height: cellWidth + 35)

        } else {
            return CGSize(width: cellWidth, height: cellWidth + 25)

        }
    }
    
    @IBAction func tapHereBtnClicked(_ sender: Any) {
        openSearchContactView()
        
    }
    
    func removeFriend(userId: Int, selectedRow: Int) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        //Object to be serialized to JSON
        let user = ClearSession(user_id: userId)
        //convert object to dictionary
        let data_temp = Mapper<ClearSession>().toJSON(user)
        print("data:- \(data_temp)")
        alamofireManager.request(disconnectFriendByUser, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        let indexpath = IndexPath(row: selectedRow, section: 0)
                        DispatchQueue.main.async {
                            print("before self.addedContactDetails:- \(self.addedContactDetails.count)")
                            self.addedContactDetails.remove(at: selectedRow)
                            print("self.addedContactDetails:- \(self.addedContactDetails.count)")
//                            self.collectionView.deleteItems(at: [indexpath])
//                            self.collectionView.dele
                            if self.addedContactDetails.isEmpty {
                                self.contactNodataView.isHidden = false
                                self.collectionView.isHidden = true
                            }
                            self.collectionView.reloadData()

                        }

                        //                        self.tableview.reloadRows(at: [indexpath], with: .fade)
                        //                        SCLAlertView().showSuccess("Success", subTitle: value.message!, colorStyle: int_red)
                        //                        displayError(str: value.message!, title: "Success")
                        print("success")
                        break
                    case 400:
                        alert.hideView()
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.addFriend(userId: userId, selectedRow: selectedRow)
                        }else {
                            if let error = value.message {
                                SCLAlertView().showError("Error", subTitle: error)
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            SCLAlertView().showError("Error", subTitle: error)
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                print("error:- \(error.localizedDescription)")
                alert.hideView()
                SCLAlertView().showError("Error", subTitle: serverError)
                break
            default:
                alert.hideView()
                break
            }
            
            }.responseJSON { response in
                print("response signUp:- \(response.result.value)")
                
        }
    }
    
//    @IBAction func inviteFriendsBtnClicked(_ sender: Any) {
//        self.isContactPickerOpen = true
//
//        let contactPickerScene = EPContactsPicker(delegate: self, multiSelection:true, subtitleCellType: SubtitleCellValue.phoneNumber)
//        let navigationController = UINavigationController(rootViewController: contactPickerScene)
//        self.present(navigationController, animated: true, completion: nil)
//    }
    
//    func inviteYourFriends(receiverUserId: [Int]) {
//        var receiverUserIdArr : [InviteUser] = []
//        
//        for i in 0..<receiverUserId.count {
//            //            let tempArr = ["receiver_user_id": receiverUserId[i]]
//            //            receiverUserIdArr.append(receiverUserId[i])
//            let tmp = InviteUser(receiver_user_id: receiverUserId[i])
//            let data_temp = Mapper<InviteUser>().toJSON(tmp)
//            receiverUserIdArr.append(tmp)
//        }
//        
//        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_violet)
//        //Object to be serialized to JSON
//        let user = InviteContactRequestModel(invite_user: receiverUserIdArr)
//        //convert object to dictionary
//        let data_temp = Mapper<InviteContactRequestModel>().toJSON(user)
//        print("data:- \(data_temp)")
//        alamofireManager.request(inviteUser, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
//            //                    let responseClass = response.result.value
//            switch response.result
//            {
//            case .success(let value):
//                if let code = value.code {
//                    print("Code:- \(code)")
//                    switch code {
//                    case 200:
//                        alert.hideView()
//                        
//                        defaults.set(true, forKey: "changeStatus")
//                        if let groupDetail = value.inviteContactReponse?.group_detail {
//                            defaults.set(groupDetail.toJSON(), forKey: "groupDetail")
//                        }
//                        //                        self.searchUserContact(searchStr: self.searchBar.text!)
//                        
//                        let story = UIStoryboard.init(name: "Main", bundle: nil)
//                        let nextVc = story.instantiateViewController(withIdentifier: "ChangeLocationVC") as! ChangeLocationVC
//                        self.navigationController?.pushViewController(nextVc, animated: true)
//                        
//                        //                        self.navigationController?.popViewController(animated: true)
//                        if let msg = value.message {
//                            displayError(str: msg, view: self, title: "Success")
//                        }
//                        print("success")
//                        break
//                        
//                    case 400:
//                        alert.hideView()
//                        
//                        clearSession()
//                        let story = UIStoryboard.init(name: "Main", bundle: nil)
//                        let nextVc = story.instantiateViewController(withIdentifier: "LandingScreenVC") as! LandingScreenVC
//                        let nav = redirectToViewController(vcName: nextVc)
//                        self.navigationController?.present(nav, animated: true, completion: nil)
//                        break
//                        
//                    case 401:
//                        alert.hideView()
//                        if let new_token = value.loginResponse?.new_token {
//                            UserDefaults.standard.set(new_token, forKey: "token")
//                            self.inviteYourFriends(receiverUserId: receiverUserId)
//                        }else {
//                            if let error = value.message {
//                                displayError(str: error, view: self)
//                            }
//                        }
//                        break
//                    default:
//                        alert.hideView()
//                        
//                        if let error = value.message {
//                            displayError(str: error, view: self)
//                        }
//                        break
//                    }
//                }
//                break
//            case .failure(let error):
//                print("error:- \(error.localizedDescription)")
//                alert.hideView()
//                displayError(str: serverError, view: self)
//                
//                break
//                
//            default:
//                alert.hideView()
//                break
//                
//            }
//            
//            }.responseJSON { response in
//                print("response signUp:- \(response.result.value)")
//                
//        }
//    }
    
}

extension ContactVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! AddContactTableCellVC
        viewCorners.addCornerToView(view: cell.profileImgview)
        viewCorners.addBorderCornerTobutton(view: cell.profileImgview, color: hexStringToUIColor(hex: purpleColor))
        cell.userNameLbl.text = userDetails[indexPath.row].user_name
        cell.addBtnImg.image = UIImage(named: "ic_searchAdd")
        cell.profileImgview.sd_setShowActivityIndicatorView(true)
        cell.profileImgview.sd_setIndicatorStyle(.gray)
        cell.profileImgview.sd_setImage(with: URL(string: userDetails[indexPath.row].headshot_compressed_image!), placeholderImage: UIImage(named: "ic_defaultProfile"))
        cell.contactAddBtn.tag = indexPath.row
        cell.contactAddBtn.addTarget(self, action: #selector(addContactPressed(_:)), for: .touchUpInside)
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    @objc func addContactPressed(_ sender: UIButton) {
        let btn = sender as! UIButton
        let indexpath = btn.tag
        if let userId = userDetails[indexpath].user_id {
            addFriend(userId: userId, selectedRow: indexpath)
        }
    }
    
    func addNoDataView() {
        let navHeight = self.navigationController?.navigationBar.frame.size.height
        
        noDataView = NoDataView(frame: CGRect(x: 0, y: self.tableview.frame.origin.y + self.tableview.frame.size.height , width: self.view.frame.size.width, height: self.view.frame.size.height - self.tableview.frame.height - navHeight!))
        noDataView.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
        noDataView.errorIconsHidden = false
        noDataView.configureView(message: "No data found.", image: UIImage(named: "ic_empty")!)
        //        noDataView.tapButton.addTarget(self, action: #selector(self.noDataButtonClicked), for: .touchUpInside)
        self.view.addSubview(noDataView)
        //        self.noDataView.isHidden = true
    }
    
    func addErrorView() {
        let navHeight = self.navigationController?.navigationBar.frame.size.height
        errorView = NoDataView(frame: CGRect(x: 0, y: self.tableview.frame.origin.y , width: self.view.frame.size.width, height: self.view.frame.size.height -  navHeight!))
        errorView.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
        errorView.errorIconsHidden = false
        errorView.configureView(message: failureViewMsg, image: UIImage(named: "ic_error")!)
        errorView.tapButton.addTarget(self, action: #selector(self.errorButtonClicked), for: .touchUpInside)
        self.view.addSubview(errorView)
        self.errorView.isHidden = false
    }
    
    @objc func errorButtonClicked(sender : UIButton) {
        if (Reachability()?.isReachable)! {
            errorView.isHidden = true
//            activityIndicatorView.startAnimating()
            searchUserContact(searchStr: searchBar.text!)
        } else {
            errorView.isHidden = false
        }
    }
}


