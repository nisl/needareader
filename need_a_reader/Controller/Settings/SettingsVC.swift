//
//  SettingsVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/19/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SCLAlertView
import AlamofireObjectMapper
import ObjectMapper
import ReachabilitySwift
import TOCropViewController
import SDWebImage
import CoreLocation
import AddressBookUI
import ByvImagePicker
import LabelSwitch


class SettingsVC: UIViewController, TOCropViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate, UIGestureRecognizerDelegate, QBCoreDelegate, SwiftySwitchDelegate {
    
    @IBOutlet var scrollview: UIScrollView!
    var skillData : AddSkillsRequestModel!
    var ratingData: RatingResponseModel!
    var userData : User_detail!
    var current_location: CLLocation!
    var videoDetail : VideoDetailModel!
    @IBOutlet var contentViewHeightConstant: NSLayoutConstraint!
    @IBOutlet var profileViewConstant: NSLayoutConstraint!
    @IBOutlet var locationView: UILabel!
    @IBOutlet var genderLbl: UILabel!
    
    @IBOutlet var logoutView: UIView!
    @IBOutlet var hourlyRateSlider: UISlider!
    
    @IBOutlet var hourlyRateLbl: UILabel!
    
    @IBOutlet var distanceSlider: UISlider!
    @IBOutlet var maxDistanceLbl: UILabel!
    @IBOutlet var profileLbl: UILabel!
    @IBOutlet var profileImg: UIImageView!
    
    @IBOutlet var logoutBtn: UIButton!
    @IBOutlet var uploadResumebtn: UIButton!
    @IBOutlet var profileView: UIView!
    @IBOutlet var displaySettingView: UIView!
    @IBOutlet var changePasswordView: UIView!
    @IBOutlet var updateSkillsView: UIView!
    @IBOutlet var updateProfileView: UIView!
    @IBOutlet var updateResumeView: UIView!
    @IBOutlet var showmeOnOffView: UIView!
    @IBOutlet var updateSlateView: UIView!
    @IBOutlet var updateServicesView: UIView!
    @IBOutlet var showmeOnOffSwitch: SwiftySwitch!
    @IBOutlet var showmeOnOffLbl: UILabel!
    @IBOutlet weak var viewRatingView: UIView!
    @IBOutlet weak var HowtouseView: UIView!
    
    
    var sendPDFData : Data!
    var documentPicker : UIDocumentPickerViewController!
    var imagePicker : UIImagePickerController!
    
    var userName = ""
    var navColor : UIColor = hexStringToUIColor(hex: lightGrayColor)
    var location_coordinate : CLLocationCoordinate2D!
    var resumeImage : Bool = false
    
    var resumeSelectedImg : UIImage!
    var userlocation : String = ""
    var imagePickerOpen : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defaults.set(false, forKey: "loadFirstTimeSetting")
        defaults.set(false, forKey: "updateUserInfo")
        if #available(iOS 11.0, *) {
            //            scrollview.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
            self.scrollview.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
        if UIDevice.current.userInterfaceIdiom == .pad {
            logoutView.frame.size.height = 70
            logoutBtn.frame.size.height = 70
        }
        //        navigationController?.delegate = self
        isSettingChanged = false
        //        self.navigationController?.navigationBar.isHidden = false
        configureNavigationBar()
        profileLbl.text = userName
        
        // Do any additional setup after loading the view, typically from a nib.
        QBCore.instance().add(self)
        let viewArr : [UIView] = [profileView , displaySettingView,changePasswordView,updateSkillsView, updateProfileView,updateResumeView, showmeOnOffView, logoutView, updateSlateView, updateServicesView,viewRatingView,HowtouseView]
        setUI(viewArr: viewArr)
        getuserProfile()
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        showmeOnOffSwitch.delegate = self

        //        imagePicker = ByvImagePicker()
        
        //        viewCorners.addRoundCornerToUpperSide(corners: [.topLeft, .topRight], radius: 5.0, view: profileImg)
        
        viewCorners.addCornerToView(view: logoutBtn)
        changeButtonColor(view: [logoutBtn], backgroundcolor: redColor, fontColor: "ffffff")
        logoutView.isHidden = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(_:)))
        profileImg.isUserInteractionEnabled = true
        profileImg.addGestureRecognizer(tapGesture)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureNavigationBar()
        //        getuserProfile()
        if !imagePickerOpen {
            let isUpdateUserInfo = defaults.bool(forKey: "updateUserInfo")
            if isUpdateUserInfo {
                let isUpdateUserInfo = defaults.set(false, forKey: "updateUserInfo")
                getuserProfile()
            }
        }
        if userName != "" {
            profileLbl.text = userName
        }
        
        //        if location_coordinate != nil {
        if let location = defaults.value(forKey: "locationAddress") as? String {
            print("location_coordinate:- \(location)")
            self.locationView.text = location
            // Create Location
            
            //            let location = CLLocation(latitude: location_coordinate.latitude, longitude: location_coordinate.longitude)
            //            // Create Location
            //
            //            CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            //                print("placemark:- \(placemarks)")
            //                if error == nil {
            //                    if let address = placemarks?.first?.addressDictionary {
            //
            //                        print("address:- \(address)")
            //                        let formattedAddress = ABCreateStringWithAddressDictionary(address, true)
            //                        self.locationView.text = "\(formattedAddress)"
            //                    }
            //                }
            //
            //
            //            }
        } else if let data =  defaults.value(forKey: "locationCoordinatesArr") as?  [String:CLLocationDegrees]{
            let location = CLLocation(latitude: data["latitude"]!, longitude: data["longtitude"]!)
            // Create Location
            
            CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
                print("placemark:- \(placemarks)")
                if let address = placemarks?.first?.addressDictionary {
                    print("address:- \(address)")
                    let formattedAddress = ABCreateStringWithAddressDictionary(address, true)
                    //                    if let city = address["City"] as? String {
                    //                        self.locationView.text = city
                    //                    }
                    //
                    self.locationView.text = "\(formattedAddress)"
                }
            }
        }else {
            if current_location != nil {
                // Create Location
                
                let location = CLLocation(latitude: current_location.coordinate.latitude, longitude: current_location.coordinate.longitude)
                // Create Location
                CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
                    if let address = placemarks?.first?.addressDictionary {
                        //                let formattedAddress = ABCreateStringWithAddressDictionary(address, true)
                        self.locationView.text = address["City"] as! String
                        
                    }
                }
            }
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if location_coordinate != nil {
            if self.isMovingFromParentViewController {
                let cnt = self.navigationController?.viewControllers.count
                if cnt == 1 {
                    let vc = self.navigationController?.viewControllers[0] as! FindUserVC
                    vc.location_coordinate = location_coordinate
                }else if cnt == 3 {
                    let vc = self.navigationController?.viewControllers[2] as! FindUserVC
                    vc.location_coordinate = location_coordinate
                }else {
                    let vc = self.navigationController?.viewControllers[1] as! FindUserVC
                    vc.location_coordinate = location_coordinate
                }
                
            }
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        let path = UIBezierPath(roundedRect: profileImg.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 5.0, height: 5.0))
        let mask = CAShapeLayer()
        mask.frame = profileImg.bounds
        mask.path = path.cgPath
        profileImg.layer.mask = mask
        //        if defaults.value(forKey: "locationCoordinatesArr") as?  [String:CLLocationDegrees] == nil{
        let isFirstTimeSetting = defaults.bool(forKey: "loadFirstTimeSetting")
        if !isFirstTimeSetting {
            defaults.set(true, forKey: "loadFirstTimeSetting")
            setProfileView()
            
        }
        //        }
    }
    
    func setUI(viewArr: [UIView]) {
        for i in 0..<viewArr.count {
            //            viewCorners.addShadowToView(view: viewArr[i])
            viewCorners.addCornerToView(view: viewArr[i], value: 5)
            viewCorners.addShadowToView(view: viewArr[i], value: 2.0)
        }
        setProfileView()
    }
    
    func setProfileView() {
        if UIDevice.current.userInterfaceIdiom == .phone {
//            contentViewHeightConstant.constant = 1145 - 350
            let viewHeight = (self.view.frame.size.height * 0.8)
            let lblHeight = (viewHeight * 0.7) * 0.2
            profileViewConstant.constant = (viewHeight * 0.6) + lblHeight + 80//self.view.frame.size.width * 1.18
//            contentViewHeightConstant.constant = contentViewHeightConstant.constant + profileViewConstant.constant
        } else {
//            contentViewHeightConstant.constant = 1595 - 800
            let viewHeight = (self.view.frame.size.height * 0.85)
            let lblHeight = (viewHeight * 0.7) * 0.2
            profileViewConstant.constant = (viewHeight * 0.6) + lblHeight + 80//self.view.frame.size.width * 1.18
//            contentViewHeightConstant.constant = contentViewHeightConstant.constant + profileViewConstant.constant
        }
    }
    
    //Set NavigationBar
    func configureNavigationBar()
    {
        setNavigaion(vc: self)
        let navigationItem = self.navigationItem
        navigationItem.title = "SETTINGS"
        
        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @objc func tapBlurButton(_ sender: UITapGestureRecognizer) {
        cameraClicked()
    }
    
    func loadFromuserDetailCache() -> Bool {
        if let v = UserDefaults.standard.dictionary(forKey: getUserProfileByUser) {
            print("v: \(v)")
            let value = Mapper<SettingResponseModel>().map(JSON: v)
            
            if let user_list = value?.user_detail {
                userData = user_list
                
                if let showMeOnOff = userData.show_me_on {
                        showmeOnOffSwitch.isOn = showMeOnOff
                    showMeOnOff ? (setBtnTextColor(currentLbl: showmeOnOffLbl, alignment: .left, color: UIColor.black, text : "Yes")) :             (setBtnTextColor(currentLbl: showmeOnOffLbl, alignment: .right, color: UIColor.black, text : "No"))
                }
                
                if let img = userData.headshot_compressed_image {
                    profileImg.sd_setShowActivityIndicatorView(true)
                    profileImg.sd_setIndicatorStyle(.gray)
                    profileImg.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
                    
                    //                    if self.profileImg.image == UIImage(named: "ic_defaultProfile") {
                    //                        self.profileImg.contentMode = .scaleAspectFit
                    //
                    //                    }else{
                    //                        self.profileImg.contentMode = .scaleToFill
                    //
                    //                    }
                    
                    //                    self.profileImg.contentMode = .scaleToFill
                } else {
                    profileImg.image = UIImage(named: "ic_defaultProfile")
                }
                
                if let userName = userData.user_name {
                    if let lastName = userData.last_name {
                        profileLbl.text = userName + " " + lastName
                        
                    }else {
                        profileLbl.text = userName
                    }
                    
                }
                if let gender = userData.search_by_gender {
                    genderLbl.text = setStringValueForGender(gendertype: gender)
                }
                if let distance = userData.maximum_distance {
                    maxDistanceLbl.text = "\(distance)mi"
                    distanceSlider.value = Float(distance)
                }
                //                if let city = userData.city {
                //                    locationView.text = city
                //                }
                
                if let hourlyRate = userData.hourly_rate {
                    hourlyRateLbl.text = "$\(hourlyRate)"
                    hourlyRateSlider.value = Float(hourlyRate)
                    defaults.set(hourlyRate, forKey: "hourlyRate")

                }
                
                
            }
            
            if let skill_data = value?.skill_detail {
                skillData = skill_data
            }
            
            if let video_detail = value?.video_detail {
                videoDetail = video_detail
            }
            
            if let rating_data = value?.rating_detail {
                ratingData = rating_data
            }
        } else {
            if let userDetail = current_user_detail {
                userData = userDetail
                
                if let showMeOnOff = userData.show_me_on {
                    showmeOnOffSwitch.isOn = showMeOnOff
                    showMeOnOff ? (setBtnTextColor(currentLbl: showmeOnOffLbl, alignment: .left, color: UIColor.black, text : "Yes")) :             (setBtnTextColor(currentLbl: showmeOnOffLbl, alignment: .right, color: UIColor.black, text : "No"))
                }
                
                if let img = userDetail.headshot_compressed_image {
                    profileImg.sd_setShowActivityIndicatorView(true)
                    profileImg.sd_setIndicatorStyle(.gray)
                    profileImg.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
                } else {
                    profileImg.image = UIImage(named: "ic_defaultProfile")
                }
                
                if let userName = userDetail.user_name {
                    if let lastName = userDetail.last_name {
                        profileLbl.text = userName + " " + lastName
                        
                    }else {
                        profileLbl.text = userName
                    }
                    
                }
                if let gender = userDetail.search_by_gender {
                    genderLbl.text = setStringValueForGender(gendertype: gender)
                }
                
                if let distance = userDetail.maximum_distance {
                    maxDistanceLbl.text = "\(distance)mi"
                    distanceSlider.value = Float(distance)
                }
                //                if let city = userData.city {
                //                    locationView.text = city
                //                }
                
                if let hourlyRate = userDetail.hourly_rate {
                    hourlyRateLbl.text = "$\(hourlyRate)"
                    hourlyRateSlider.value = Float(hourlyRate)
                    defaults.set(hourlyRate, forKey: "hourlyRate")
                }
                
                if let skill_data = userDetail.skill_detail {
                    skillData = skill_data
                }
                
                if let rating_data = userDetail.rating_detail {
                    ratingData = rating_data
                }
                
                if let video_detail = userDetail.video_detail {
                    videoDetail = video_detail
                }
            }
            
        }
        
        if userData != nil {
            return true
        }else {
            return false
        }
    }
    
    func getuserProfile() {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        print("TokenHeader:- \(tokenHeader)")
        alamofireManager.request(getUserProfileByUser, method: .post, parameters: [:], encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        UserDefaults.standard.set(value.settingResponse?.toJSON(), forKey: getUserProfileByUser)
                        alert.hideView()
                        self.loadFromuserDetailCache()
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.loadFromuserDetailCache()
                        }else {
                            if !self.loadFromuserDetailCache() {
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                                
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if !self.loadFromuserDetailCache() {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                alert.hideView()
                
                if !self.loadFromuserDetailCache() {
                    SCLAlertView().showError(errorTitle, subTitle: serverError)
                    //                    self.addErrorView()
                }
                
                print("Request failed with error: \(error.localizedDescription)")
                break
                
                
            }
            
            }.responseJSON { response in
                print("Setting response:- \(response.result.value)")
                
        }
        
    }
    
    @IBAction func profileImgBtnClicked(_ sender: Any) {
        cameraClicked()
    }
    
    @IBAction func locationBtnClicked(_ sender: Any) {
        imagePickerOpen = false
        isSettingChanged = true
        let vc = storyboard?.instantiateViewController(withIdentifier: "ChangeLocationVC") as! ChangeLocationVC
        
        if current_location != nil {
            vc.current_location = current_location
            
        }
        vc.isFromSetting = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func genderBtnClicked(_ sender: Any) {
        imagePickerOpen = false
        
        var genderValue : Int = 0
        let alert = UIAlertController(title: "Read With", message: "", preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: 64, width: screenWidth, height: 0)
        alert.addAction(UIAlertAction(title: "Actor", style: .default, handler: { (success) in
            isSettingChanged = true
            
            genderValue = 1
            self.genderLbl.text = "Actor"
            self.setGender(genderType: genderValue)
            
        }))
        alert.addAction(UIAlertAction(title: "Actress", style: .default, handler: { (success) in
            isSettingChanged = true
            
            genderValue = 2
            self.genderLbl.text = "Actress"
            self.setGender(genderType: genderValue)
            
        }))
        alert.addAction(UIAlertAction(title: "All", style: .default, handler: { (success) in
            isSettingChanged = true
            
            genderValue = 0
            self.genderLbl.text = "All"
            self.setGender(genderType: genderValue)
        }))
        
        
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: { (success) in
        }))
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func touchEnd(_ sender: Any) {
        let slider = sender as! UIEvent
        
        
    }
    @IBAction func sliderChanged(_ sender: UISlider, event: UIEvent) {
        isSettingChanged = true
        let slider = sender as! UISlider
        //        if let touchevent = slider
        //        setMaxDistance(maxDistance: Double(slider.value))
        switch slider{
        case distanceSlider:
            maxDistanceLbl.text = "\(Int(slider.value))mi"
            
            if let touchEvent = event.allTouches?.first {
                switch touchEvent.phase {
                case .began:
                    // handle drag began
                    break
                case .moved:
                    // handle drag moved
                    break
                case .ended:
                    // handle drag ended
                    setMaxDistance(maxDistance: Int(slider.value))
                    
                    break
                default:
                    break
                }
            }
            break
        default :
            hourlyRateLbl.text = "$\(Int(slider.value))"
            
            if let touchEvent = event.allTouches?.first {
                switch touchEvent.phase {
                case .began:
                    // handle drag began
                    break
                case .moved:
                    // handle drag moved
                    break
                case .ended:
                    // handle drag ended
                    //                    setMaxDistance(maxDistance: Int(slider.value))
                    setPrice(rate: Int(slider.value))
                    break
                default:
                    break
                }
            }
            break
            
        }
        
    }
    
   
//    @IBAction func showmeOnOff(_ sender: Any) {
    func valueChanged(sender: SwiftySwitch) {
        let sender = sender as! SwiftySwitch
        var setValue : Int = 0
//        let sender = sender as! UISwitch
        if sender.isOn {
            setValue = 1
            setBtnTextColor(currentLbl: showmeOnOffLbl, alignment: .left, color: UIColor.black, text : "Yes")

        } else {
            setValue = 0
            setBtnTextColor(currentLbl: showmeOnOffLbl, alignment: .right, color: UIColor.black, text : "No")

        }
        setShowMeOnOff(showValue: setValue)
    }
    
    @IBAction func updateProfileClicked(_ sender: Any) {
        imagePickerOpen = false
        defaults.set(false, forKey: "updateUserInfo")
        
        let vc = storyBoard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        vc.isUpdateProfile = true
        vc.userdetail = userData
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func updateResumeBtnClicked(_ sender: Any) {
        
//        let alert = UIAlertController(title: "Choose Resume From", message: "", preferredStyle: .actionSheet)
//        alert.popoverPresentationController?.sourceView = self.view
//        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: 64, width: screenWidth, height: 0)
//        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (success) in
//            self.resumeImage = true
//            self.imagePicker.sourceType = .photoLibrary
//            self.present(self.imagePicker, animated: true, completion: nil)
//            self.imagePickerOpen = true
//
//
//        }))
//        alert.addAction(UIAlertAction(title: "Documents/iCloud", style: .default, handler: { (success) in
//            self.documentPicker = UIDocumentPickerViewController(documentTypes: ["public.content"], in: .import)
//            self.documentPicker.delegate = self
//            self.documentPicker.modalPresentationStyle = .fullScreen
//            self.present(self.documentPicker, animated: true, completion: nil)
//
//        }))
//
//        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: { (success) in
//
//        }))
//
//        present(alert, animated: true, completion: nil)
        
        let vc = storyBoard.instantiateViewController(withIdentifier: "ViewresumeVC") as! ViewresumeVC
        vc.userData = self.userData
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func cameraClicked() {
        self.resumeImage = false
        let alert = UIAlertController(title: "Choose Image From:", message: "", preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: 64, width: screenWidth, height: 0)
        alert.addAction(UIAlertAction(title: "Take picture", style: .default, handler: { (success) in
            self.imagePickerOpen = true
            
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "Photo library", style: .default, handler: { (success) in
            self.imagePickerOpen = true
            
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
            //            let viewHeight = (self.view.frame.size.height * 0.75)
            //            let lblHeight = (viewHeight * 0.65) * 0.2
            //            let imgheight = self.profileViewConstant.constant - lblHeight
            //            let viewWidth = self.view.frame.size.width  - 20
            //            let from = ByvFrom(controller: self, from:self.view.frame, inView:self.view, arrowDirections:.any)
            
            //            self.imagePicker?.getLibraryImage(from: from, editable: true, isCircle: false, customAspectRatio: CGSize(width: viewWidth, height: imgheight), completion: { image in
            //                if let imageData = UIImageJPEGRepresentation(image!, 1) {
            //                    let imageSize: Double = Double(imageData.count)
            //                    print("size of original image in MB:\((imageSize/1024)/1024)")
            //                }
            //
            //                //getEditableImage(from: from, completion: { image in
            //                let compressedImage = imageCompression().compressImage(image: image!)
            //                if compressedImage != nil {
            //                    if self.resumeImage {
            //                        self.resumeSelectedImg = compressedImage
            //                        if self.sendPDFData != nil {
            //                            self.sendPDFData = nil
            //                        }
            //                        self.setResumeApi()
            //                    }else {
            //                        self.profileImg.image = compressedImage
            //                        self.profileImg.contentMode = .scaleToFill
            //                        self.setImageApi(profileImage: compressedImage!)
            //                    }
            //                }
            //            })
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: { (success) in
            
        }))
        //        alert.view.tintColor = UIColor(constant: .BlueBlack)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func updateSkillsBtnClicked(_ sender: Any) {
        imagePickerOpen = false
        defaults.set(false, forKey: "updateUserInfo")
        
        let vc = storyBoard.instantiateViewController(withIdentifier: "AddSkillsVC") as! AddSkillsVC
        vc.isUpdateSkill = true
        vc.skillData = skillData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func changePasswordClicked(_ sender: Any) {
        imagePickerOpen = false
        defaults.set(false, forKey: "updateUserInfo")
        
        let vc = storyBoard.instantiateViewController(withIdentifier: "AddNewPasswordVC") as! AddNewPasswordVC
        vc.isChangePassword = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func logoutBtnClicked(_ sender: Any) {
        imagePickerOpen = false
        
        if (Reachability()?.isReachable)! {
            callLogoutApi()
            
        }else {
            clearSession()
            let story = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = story.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            let nav = UINavigationController(rootViewController: nextVC)
            nav.navigationBar.isTranslucent = false
            nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
            nav.navigationBar.shadowImage = UIImage()
            nav.interactivePopGestureRecognizer?.isEnabled = false
            self.navigationController?.present(nav, animated: true, completion: nil)
        }
        QBCore.instance().logout()
        //        ServicesManager.instance().lastActivityDate = nil;
        ServicesManager.instance().chatService.disconnect { (error) in
            
        }
        ServicesManager.instance().logoutUserWithCompletion { (boolValue) -> () in
            
            if boolValue {
                
                ServicesManager.instance().lastActivityDate = nil;
            }
        }
    }
    
    func callLogoutApi() {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        print("TokenHeader:- \(tokenHeader)")
        alamofireManager.request(doLogout, method: .post, parameters: [:], encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        //                        UserDefaults.standard.removeObject(forKey: "token")
                        //                        UserDefaults.standard.set(nil, forKey: "token")
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let nextVC = story.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        let nav = UINavigationController(rootViewController: nextVC)
                        nav.navigationBar.isTranslucent = false
                        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                        nav.navigationBar.shadowImage = UIImage()
                        nav.interactivePopGestureRecognizer?.isEnabled = false
                        self.navigationController?.present(nav, animated: true, completion: nil)
                        
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let nextVC = story.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        let nav = redirectToRootViewController(vcName: nextVC)
                        //                        self.navigationController?.present(nav, animated: true, completion: nil)
                        self.present(nav, animated: true)
                        
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.signUpResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.callLogoutApi()
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let nextVC = story.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        let nav = UINavigationController(rootViewController: nextVC)
                        nav.navigationBar.isTranslucent = false
                        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                        nav.navigationBar.shadowImage = UIImage()
                        nav.interactivePopGestureRecognizer?.isEnabled = false
                        self.navigationController?.present(nav, animated: true, completion: nil)
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                clearSession()
                let story = UIStoryboard(name: "Main", bundle: nil)
                let nextVC = story.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                let nav = UINavigationController(rootViewController: nextVC)
                nav.navigationBar.isTranslucent = false
                nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                nav.navigationBar.shadowImage = UIImage()
                nav.interactivePopGestureRecognizer?.isEnabled = false
                self.navigationController?.present(nav, animated: true, completion: nil)
                
                //print("Request failed with error: \(error)")
                break
            }
            
        }
    }
    
    func setResumeApi() {
        
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        let URL = try! URLRequest(url: setResumeForUser, method: .post, headers: tokenHeader)
        
        alamofireManager.upload(multipartFormData:  { multipartFormData in
            
            if self.sendPDFData != nil {
                multipartFormData.append(self.sendPDFData, withName: "file", fileName: "doc.pdf", mimeType: "application/pdf")
            }else {
                if self.resumeSelectedImg != nil {
                    if let data = UIImageJPEGRepresentation(self.resumeSelectedImg, 1.0) {
                        multipartFormData.append(data, withName: "file",  fileName: "file.jpeg", mimeType: "image/jpeg")
                    }
                }
            }
        }, with: URL) { encodingResult in
            //            , to: setResumeForUser, method: .post, headers: tokenHeader, encodingCompletion: { encodingResult in
            
            switch encodingResult
            {
            case .success(let upload,_,_):
                upload.responseObject { (response: DataResponse<ResponseModel> )in
                    switch response.result {
                    case .success(let value):
                        if let code = value.code {
                            print("Code:- \(code)")
                            switch code {
                            case 200:
                                alert.hideView()
                                if let msg = value.message {
                                    let alertView = SCLAlertView()
                                    alertView.showSuccess("Success", subTitle: msg, closeButtonTitle: "Ok",  colorStyle: int_red)
                                }
                                if let resumeURL = value.headShotResponse?.resume_file {
                                    
                                    defaults.set(resumeURL, forKey: "ResumeURL")
                                    
                                }
                                defaults.set(true, forKey: "resume_setup")
                                
                                //                                self.navigationController?.pushViewController(nextVC, animated: true)
                                break
                            case 400:
                                alert.hideView()
                                
                                clearSession()
                                let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                let nav = redirectToRootViewController(vcName: vc)
                                self.present(nav, animated: true)
                                break
                            case 401:
                                alert.hideView()
                                if let new_token = value.signUpResponse?.new_token {
                                    UserDefaults.standard.set(new_token, forKey: "token")
                                    self.setResumeApi()
                                }else {
                                    if let error = value.message {
                                        let alertview = SCLAlertView()
                                        alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                    }
                                }
                                break
                                
                            default:
                                alert.hideView()
                                
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                                break
                            }
                        }
                        break
                    case .failure(let error):
                        print("error: \(error.localizedDescription)")
                        
                        alert.hideView()
                       
                        let alertview = SCLAlertView()
                        alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                        break
                    }
                    
                }
            case .failure(let encodingError):
                alert.hideView()
                let alertview = SCLAlertView()
                
                alertview.showError("Error", subTitle: encodingError.localizedDescription, closeButtonTitle: "Ok")
                //print("Request failed with error: \(error)")
                break
            }
            
            
        }
        
    }
    
    func setImageApi(profileImage: UIImage,rate : Int) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        let user = ProfileSetupRequestModel(hourly_rate : rate)
        let data_temp = Mapper<ProfileSetupRequestModel>().toJSONString(user)
        let param = ["request_data": data_temp]
        let URL = try! URLRequest(url: setHeadShotForUser, method: .post, headers: tokenHeader)
        alamofireManager.upload(multipartFormData: { (multipartFormData) in
            if let imageData = UIImageJPEGRepresentation(profileImage, 1) {
                
                let imageSize: Double = Double(imageData.count)
                let size = imageSize/(1024*1024)
                print("size of compressed image in MB:\(size)")
                //                print("size of compressed image in MB:\((imageSize/1024)/1024)")
                multipartFormData.append(imageData, withName: "file", fileName: "file.jpeg", mimeType: "image/jpeg")
            }
            for (key,value) in param
            {
                print("key:- \(key), value:- \(value)")
                multipartFormData.append(((value)?.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)))!,withName : key)
            }
        }, with: URL) { encodingResult in
            
            //        }
            //        alamofireManager.upload(multipartFormData:  { multipartFormData in
            //            if let data = UIImageJPEGRepresentation(profileImage, 1.0) {
            //                let imageSize: Double = Double(data.count)
            //                print("size of image in MB:\((imageSize/1024)/1024)")
            //                multipartFormData.append(data, withName: "file", fileName: "file.jpeg", mimeType: "image/jpeg")
            //            }
            //        }, to: setHeadShotForUser, method: .post, headers: tokenHeader, encodingCompletion: { encodingResult in
            switch encodingResult
            {
            case .success(let upload,_,_):
                upload.responseObject { (response: DataResponse<ResponseModel> )in
                    switch response.result {
                    case .success(let value):
                        if let code = value.code {
                            print("Code:- \(code)")
                            switch code {
                            case 200:
                                alert.hideView()
                                print("Success")
                                if let msg = value.message {
                                    let alertView = SCLAlertView()
                                    alertView.showSuccess("Success", subTitle: msg, closeButtonTitle: "Ok",  colorStyle: int_red)
                                }
                                if let img = value.headShotResponse?.headshot_compressed_image {
                                    //                                    SDImageCache.shared().removeImage(forKey: img, withCompletion: nil)
                                    defaults.set(img, forKey: "profileImage")
                                    defaults.set(true, forKey: "headshot_setup")
                                    updateUserPhotoForCurrentUser(image: profileImage)
                                }
                                break
                            case 400:
                                alert.hideView()
                                
                                clearSession()
                                let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                let nav = redirectToRootViewController(vcName: vc)
                                self.present(nav, animated: true)
                                break
                            case 401:
                                alert.hideView()
                                if let new_token = value.signUpResponse?.new_token {
                                    UserDefaults.standard.set(new_token, forKey: "token")
                                    self.setImageApi(profileImage: profileImage, rate: rate)
                                }else {
                                    if let error = value.message {
                                        let alertview = SCLAlertView()
                                        alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                    }
                                }
                                break
                                
                            default:
                                alert.hideView()
                                
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                                break
                            }
                        }
                        break
                    case .failure(let error):
                        print("error: \(error.localizedDescription)")
                        
                        alert.hideView()

                        let alertview = SCLAlertView()
                        alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                        break
                    }
                    
                }
            case .failure(let encodingError):
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: encodingError.localizedDescription, closeButtonTitle: "Ok")
                //print("Request failed with error: \(error)")
                break
            }
            
            
        }
        
    }
    
    func setMaxDistance(maxDistance: Int) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        let user = SettingRequestModel(maximum_distance: maxDistance)
        let data_temp = Mapper<SettingRequestModel>().toJSON(user)
        print("Max Destance request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(setMaximumDistance, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        //                        let alertView = SCLAlertView()
                        //                        alertView.showSuccess("Success", subTitle: (response.result.value?.message)!, closeButtonTitle: "Ok",  colorStyle: int_red)
                        print("Success")
                        //
                        if let msg = value.message {
                            let alertView = SCLAlertView()
                            alertView.showSuccess("Success", subTitle: msg, closeButtonTitle: "Ok",  colorStyle: int_red)
                        }
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.setMaxDistance(maxDistance: maxDistance)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
    
    func setPrice(rate: Int) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        let user = ProfileSetupRequestModel(hourly_rate : rate)
        let data_temp = Mapper<ProfileSetupRequestModel>().toJSON(user)
        print("setPrice request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(setHourlyRate, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        defaults.set(rate, forKey: "hourlyRate")
                        //                        let alertView = SCLAlertView()
                        //                        alertView.showSuccess("Success", subTitle: (response.result.value?.message)!, closeButtonTitle: "Ok",  colorStyle: int_red)
                        print("Success")
                        //
                        if let msg = value.message {
                            let alertView = SCLAlertView()
                            alertView.showSuccess("Success", subTitle: msg, closeButtonTitle: "Ok",  colorStyle: int_red)
                        }
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.setPrice(rate: rate)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
    func setShowMeOnOff(showValue: Int) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        let user = SettingRequestModel(show_me_on: showValue)
        let data_temp = Mapper<SettingRequestModel>().toJSON(user)
        print("On Off request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(setShowMeOnForUser, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        //                        let alertView = SCLAlertView()
                        //                        alertView.showSuccess("Success", subTitle: (response.result.value?.message)!, closeButtonTitle: "Ok",  colorStyle: int_red)
                        print("Success")
                        //
                        if let msg = value.message {
                            let alertView = SCLAlertView()
                            alertView.showSuccess("Success", subTitle: msg, closeButtonTitle: "Ok",  colorStyle: int_red)
                        }
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.setShowMeOnOff(showValue: showValue)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
    
    func setGender(genderType: Int) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        let user = SettingRequestModel(search_by_gender: genderType)
        let data_temp = Mapper<SettingRequestModel>().toJSON(user)
        print("Gender request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(setGenderType, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        //                        let alertView = SCLAlertView()
                        //                        alertView.showSuccess("Success", subTitle: (response.result.value?.message)!, closeButtonTitle: "Ok",  colorStyle: int_red)
                        if let msg = value.message {
                            let alertView = SCLAlertView()
                            alertView.showSuccess("Success", subTitle: msg, closeButtonTitle: "Ok",  colorStyle: int_red)
                        }
                        //
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.setGender(genderType: genderType)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
    //DocumentPicker Delegate
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        print("url:- \(url)")
        let extensionString = (url.absoluteString).suffix(4)
        let theFileName = ((url.absoluteString) as NSString).lastPathComponent
        //        filenameLbl.text = theFileName
        if controller.documentPickerMode == UIDocumentPickerMode.import {
            // This is what it should be
            //                let data = String(contentsOfFile: url.path)
            do {
                let pdfData = try Data(contentsOf: url)
                sendPDFData = pdfData
                if resumeSelectedImg != nil {
                    resumeSelectedImg = nil
                }
                self.setResumeApi()
            } catch {
                print("Unable to load data: \(error)")
            }
        }
        //        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        tabBarController?.tabBar.isHidden = false
        
        print("document picker cancel")
        
    }
    
    //ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //        UIImagePickerControllerEditedImage
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let cropVC = TOCropViewController(croppingStyle: .default, image: pickedImage)
            cropVC.delegate = self
            if !resumeImage {
                cropVC.rotateClockwiseButtonHidden = false
                cropVC.resetAspectRatioEnabled = false
                cropVC.aspectRatioPickerButtonHidden = true
                //            cropVC.aspectRatioPreset = .preset4x3
                cropVC.customAspectRatio = CGSize(width: profileImg.frame.size.width, height: profileImg.frame.size.height)
                cropVC.cropView.cropBoxResizeEnabled = false
            }
            
            
            
            //            cropVC.deviceOrientation = "Landscape"
            tabBarController?.tabBar.isHidden = true
            navigationController?.pushViewController(cropVC, animated: true)
            //            if resumeImage {
            //                resumeSelectedImg = pickedImage
            //                if sendPDFData != nil {
            //                    sendPDFData = nil
            //                }
            //                self.setResumeApi()
            //            }else {
            //                self.profileImg.image = pickedImage
            //                profileImg.contentMode = .scaleToFill
            //                setImageApi(profileImage: pickedImage)
            //            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        tabBarController?.tabBar.isHidden = false
        
        dismiss(animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        
        cropViewController.navigationController?.popViewController(animated: true)
        if let imageData = UIImageJPEGRepresentation(image, 1) {
            
            let imageSize: Double = Double(imageData.count)
            //            print("size of original image in MB:\((imageSize/1024)/1024)")
            let size = imageSize/(1024*1024)
            print("size of original image in MB:\(size)")
            if size > 1.5 {
                let compressedImage = imageCompression().compressImage(image: image)
                
                if compressedImage != nil {
                    if self.resumeImage {
                        self.resumeSelectedImg = compressedImage
                        if self.sendPDFData != nil {
                            self.sendPDFData = nil
                        }
                        self.setResumeApi()
                    }else {
                        self.profileImg.image = compressedImage
                        self.profileImg.contentMode = .scaleToFill
                        let rate = defaults.integer(forKey: "hourlyRate")
                        self.setImageApi(profileImage: compressedImage!, rate: rate)
                    }
                }
            } else {
                
                if self.resumeImage {
                    self.resumeSelectedImg = image
                    if self.sendPDFData != nil {
                        self.sendPDFData = nil
                    }
                    self.setResumeApi()
                }else {
                    self.profileImg.image = image
                    self.profileImg.contentMode = .scaleToFill
                    let rate = defaults.integer(forKey: "hourlyRate")
                    self.setImageApi(profileImage: image, rate: rate)
                }
                
            }
        }
        
        
        tabBarController?.tabBar.isHidden = false
    }
    
    //QBCoreDelegate
    func coreDidLogout(_ core: QBCore) {
        
        //        ServicesManager.instance().chatService.removeDelegate(self)
        //        ServicesManager.instance().authService.remove(self)
        
        ServicesManager.instance().lastActivityDate = nil;
    }
    
    @IBAction func notificationBtnPressed(_ sender: Any) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
        
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nav.navigationBar.shadowImage = UIImage()
        nav.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.present(nav, animated: true, completion: nil)
        
    }
    
    @IBAction func jobsBtnPressed(_ sender: Any) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "JobsVC") as! JobsVC
        
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nav.navigationBar.shadowImage = UIImage()
        nav.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func homeBtnPressed(_ sender: Any) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "FindUserVC") as! FindUserVC
        
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nav.navigationBar.shadowImage = UIImage()
        nav.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func chatBtnPressed(_ sender: Any) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "DialogsViewController") as! DialogsViewController
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nav.navigationBar.shadowImage = UIImage()
        nav.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    
    @IBAction func serviceBtnClicked(_ sender: Any) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "TappingServiceVC") as! TappingServiceVC
        vc.isUpdateServices = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func needSlateBtnClicked(_ sender: Any) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "AddVideoVC") as! AddVideoVC
        if let userDetail = userData {
            vc.userDetail = userDetail
        } else {
            vc.userDetail = current_user_detail
        }
        vc.videoDetail = videoDetail
        vc.isFromSetting = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func viewRatingBtnClick(_ sender: Any) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "RatingviewVC") as! RatingviewVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func Howtouseapptutorial(_ sender: UIButton) {
        if let requestUrl = URL(string: howToUseURL) {
            UIApplication.shared.openURL(requestUrl)
        }
    }
    
}


extension CLPlacemark {
    
    var compactAddress: String? {
        if let name = name {
            var result = name
            
            if let street = thoroughfare {
                result += ", \(street)"
            }
            
            if let city = locality {
                result += ", \(city)"
            }
            
            if let country = country {
                result += ", \(country)"
            }
            
            return result
        }
        
        return nil
    }
    
}
