//
//  HireReaderVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 1/29/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import AlamofireObjectMapper
import Alamofire
import SCLAlertView
import ReachabilitySwift
import ObjectMapper
import SDWebImage
import LabelSwitch

class HireReaderVC: UIViewController, UITextViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var videoChatMeetIMGV: UIImageView!
    @IBOutlet weak var personalMeetIMGV: UIImageView!
    @IBOutlet weak var readerouterView: UIView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var inviteBtn: UIButton!
    @IBOutlet weak var tablviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var hireReaderYesNoLbl: UILabel!
    @IBOutlet var hireReaderView: SwiftySwitch!
    
    @IBOutlet var editingSwitchLbl: UILabel!
    @IBOutlet var editingSwitchBtn: SwiftySwitch!
    
    @IBOutlet var tappingSwitchLbl: UILabel!
    @IBOutlet var tappingSwitchBtn: SwiftySwitch!

//    @IBOutlet var editingSwitchBtn: UISwitch!
//    @IBOutlet var tappingSwitchBtn: UISwitch!

//    @IBOutlet var hireReaderView: LabelSwitch!
    @IBOutlet var dateLblHeightConstant: NSLayoutConstraint!
    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var datePickerBTn: UIButton!
    @IBOutlet var profileImgHeightConstant: NSLayoutConstraint!
    @IBOutlet var totalMutualFrndLbl: UILabel!
    @IBOutlet var mutualLblHeightConstant: NSLayoutConstraint!
    @IBOutlet var collectionHeightConstant: NSLayoutConstraint!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var timeStackView: UIStackView!
    @IBOutlet var bottomView: UIView!
    
    @IBOutlet var minute30Btn: UIButton!
    @IBOutlet var minute60Btn: UIButton!
    @IBOutlet var minute90Btn: UIButton!
    @IBOutlet var editingLbl: UILabel!
//    @IBOutlet var editingSwitchBtn: UISwitch!
//    @IBOutlet var tappingSwitchBtn: UISwitch!
    @IBOutlet var tappingLbl: UILabel!
    
    @IBOutlet var finalRatingLbl: UILabel!
    @IBOutlet var scrollviewOuterView: UIView!
    @IBOutlet var scrollviewBottomConstant: NSLayoutConstraint!
    @IBOutlet var sendBtn: UIButton!
    @IBOutlet var textView: UITextView!
//    @IBOutlet var hireReaderView: UISwitch!
    @IBOutlet var imageOuterViewConstant: NSLayoutConstraint!
    @IBOutlet var scrollview: UIScrollView!
    @IBOutlet var imageouterView: UIView!
    @IBOutlet var outerview: UIView!
    @IBOutlet var profileOuterImg: UIImageView!
    @IBOutlet var filenameLbl: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var resumeBtn: UIButton!
    @IBOutlet var nextBtn: UIButton!
    @IBOutlet var profileImg: UIImageView!
    @IBOutlet var userNameLbl: UILabel!
    @IBOutlet var hourlyRateLbl: UILabel!
    
    @IBOutlet var tappingViewConstant: NSLayoutConstraint!
    @IBOutlet var tappingView: UIView!
    @IBOutlet var editingViewConstant: NSLayoutConstraint!
    @IBOutlet var editingView: UIView!
    
    @IBOutlet var tapingTopConstant: NSLayoutConstraint!
    @IBOutlet var editingTopConstant: NSLayoutConstraint!

    var userDetail : User_detail!
    var isFromWebView : Bool = false
    var isSuccesFromHireReader : Bool = false
    var mutualFrndResponse : contactResponseModel!
    var mutualFrndList : [User_detail] = []
    var readerlist : [User_detail] = []
    var hourlyRate : Int = 0
    var tappingRate : Int = 0
    var editingRate : Int = 0
    var finalRate : Float = 0.0
    var selectedTime : Int = 60
    var datePicker : UIDatePicker = UIDatePicker()
    let toolBar = UIToolbar()
    var currentDate : String!
    let layout = UICollectionViewFlowLayout()
    var meeting_type = 0
    var multiplerequestArr : [MultipleHireReaderRequestModel] = []
    
    var is_editing = 0
    var is_tapping = 0
    var is_removeDisableUser = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        //getUserMutualFriends()
        layout.itemSize = CGSize(width: 100, height: 120)
        layout.scrollDirection = .horizontal
        // Then initialize collectionView
        collectionView.collectionViewLayout = layout
//        let collectionView = UICollectionView(frame: CGRect(x:20, y: 0, width: self.view.frame.size.width - 40, height: 120), collectionViewLayout: layout)

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isPagingEnabled = true
        textView.delegate = self
        textView.text = "Please enter job description"
        textView.textColor = UIColor.lightGray
        textView.layer.cornerRadius = 10
        textView.clipsToBounds = true
        textView.layer.borderWidth = 1.0//1
        textView.layer.borderColor = UIColor.darkGray.cgColor
//        hireReaderView.setOn(false, animated: true)
        
       // tabBarController?.tabBar.isHidden = true
        setSelectedBtnColors(selectedBtn: minute60Btn, otherBtn: [minute30Btn, minute90Btn])
        //        clearBtnColor(btns: [minute30Btn, minute90Btn], titleColor: redColor)
        if userDetail != nil {
            if !userDetail.is_taping! {
//                profileImgHeightConstant.constant = profileImgHeightConstant.constant - 60
                tappingView.isHidden = true
                tappingViewConstant.constant = 0
                tapingTopConstant.constant = 0
            }
            
            if !userDetail.is_edit! {
//                profileImgHeightConstant.constant = profileImgHeightConstant.constant - 60
                editingView.isHidden = true
                editingViewConstant.constant = 0
                editingTopConstant.constant = 0
            }
        }
        hideMutualFrndView()
        if UIDevice.current.userInterfaceIdiom == .pad {
            sendBtn.frame.size.height = 70
            minute30Btn.frame.size.height = 70
            minute60Btn.frame.size.height = 70
            minute90Btn.frame.size.height = 70

            //            imageouterView.frame.size.height = self.view.frame.size.height * 0.92
            //            profileImg.frame.size.height = self.imageouterView.frame.size.height * 0.6
            profileImgHeightConstant.constant = 0 //self.view.frame.size.height * 0.55
            self.bottomView.frame.origin.y = profileImgHeightConstant.constant + self.profileImg.frame.origin.y
            self.imageouterView.frame.size.height = self.imageouterView.frame.size.height + (profileImgHeightConstant.constant) - 190
        } else {
            profileImgHeightConstant.constant = 0 //self.view.frame.size.height * 0.55
            self.bottomView.frame.origin.y = profileImgHeightConstant.constant + self.profileImg.frame.origin.y
            self.imageouterView.frame.size.height = self.imageouterView.frame.size.height + (profileImgHeightConstant.constant) - 220
        }
        viewCorners.addCornerToView(view: sendBtn)
        sendBtn.backgroundColor = hexStringToUIColor(hex: redColor)
        viewCorners.addBorderCornerTobutton(view: minute30Btn, color: hexStringToUIColor(hex: redColor))
        viewCorners.addBorderCornerTobutton(view: minute60Btn, color: hexStringToUIColor(hex: redColor))
        viewCorners.addBorderCornerTobutton(view: minute90Btn, color: hexStringToUIColor(hex: redColor))

        viewCorners.addCornerToView(view: imageouterView, value: 10)
        viewCorners.addShadowToView(view: profileOuterImg, value: 2.0)
        viewCorners.addCornerToView(view: readerouterView, value: 10)
        viewCorners.addCornerToView(view: inviteBtn, value: 20)
        dateLblHeightConstant.constant = 0
        setData()
        hireReaderView.delegate = self
//        hireReaderView.isHidden = true
        editingSwitchBtn.delegate = self
        tappingSwitchBtn.delegate = self
        setTotalPrice(isTappingOn: tappingSwitchBtn.isOn, isEditingOn: editingSwitchBtn.isOn, selectedTime: selectedTime)
        
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tablviewHeightConstraint.constant = CGFloat(readerlist.count*100)
        
        print(readerlist.toJSON())

    }
    
    override func viewWillAppear(_ animated: Bool) {
//        if isSuccesFromHireReader {
//            alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
//            callHireReaderApi(rece_id: userDetail.user_id!, str: textView.text)
//        }
        tabBarController?.tabBar.isHidden = true
        self.setReaderInfo()
    }
    func setReaderInfo() {
        if UserDefaults.standard.value(forKey: "setHireReaderInfo") != nil {
            
            if let v = UserDefaults.standard.dictionary(forKey: "setHireReaderInfo") {
                print(v)
                let value = Mapper<ReaderRequestModel>().map(JSON: v)
                print(value)
                if let selectTime = value?.time_to_hire {
                    self.selectedTime = selectTime
                    if self.selectedTime == 30 {
                        setSelectedBtnColors(selectedBtn: minute30Btn, otherBtn: [minute60Btn, minute90Btn])
                    }else if self.selectedTime == 60 {
                        setSelectedBtnColors(selectedBtn: minute60Btn, otherBtn: [minute30Btn, minute90Btn])
                    }else {
                        setSelectedBtnColors(selectedBtn: minute90Btn, otherBtn: [minute30Btn, minute60Btn])
                    }
                }
                if let meetingType = value?.meeting_type {
                    self.meeting_type = meetingType
                    if meetingType == 1 {
                        self.personalMeetIMGV.image = UIImage(named:"ic_selectRadio")
                        self.videoChatMeetIMGV.image = UIImage(named:"ic_radio")
                       // self.meeting_type = 1
                    }else {
                        self.personalMeetIMGV.image = UIImage(named:"ic_radio")
                        self.videoChatMeetIMGV.image = UIImage(named:"ic_selectRadio")
                      //  self.meeting_type = = 2
                    }
                }
                if let is_tapping = value?.is_taping {
                    if is_tapping == 1{
                        self.tappingSwitchBtn.isOn = true
                        
                        setBtnTextColor(currentLbl: tappingSwitchLbl, alignment: .left, color: UIColor.black, text : "Yes")
                        self.is_tapping = 1
                    }else {
                        self.tappingSwitchBtn.isOn = false
                        setBtnTextColor(currentLbl: tappingSwitchLbl, alignment: .right, color: UIColor.black, text : "No")
                        self.is_tapping = 0
                    }
                }
                
                if let is_editing = value?.is_editing {
                    if is_editing == 1{
                        self.editingSwitchBtn.isOn = true
                        setBtnTextColor(currentLbl: editingSwitchLbl, alignment: .left, color: UIColor.black, text : "Yes")
                        self.is_editing = 1
                    }else {
                        self.editingSwitchBtn.isOn = false
                        setBtnTextColor(currentLbl: editingSwitchLbl, alignment: .right, color: UIColor.black, text : "No")
                        self.is_editing = 0
                    }
                }
                if let job_des = value?.job_description {
                    if job_des == "Please enter job description" {
                        self.textView.text = job_des
                        self.textView.textColor = UIColor.lightGray
                    }else {
                        self.textView.text = job_des
                        self.textView.textColor = UIColor.black
                    }
                }
                print(value?.meeting_time)
                if let time = value?.meeting_time {
                    self.currentDate = value?.meeting_time
                    self.dateLbl.text = time
                    self.dateLblHeightConstant.constant = 20
                }else {
                    self.dateLbl.text = nil
                    self.dateLblHeightConstant.constant = 0
                }
                
            }
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //        imageouterView.frame.size.height = self.view.frame.size.height * 0.92
        //        profileImg.frame.size.height = self.view.frame.size.height * 0.5
        //        self.bottomView.frame.origin.y = self.profileImg.frame.size.height + self.profileImg.frame.origin.y
        //        self.imageouterView.frame.size.height = self.imageouterView.frame.size.height + (self.profileImg.frame.size.height - 200)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if !isFromWebView {
            tabBarController?.tabBar.isHidden = false
        }
    }
    
    func setData() {
        if userDetail != nil {
            if let name = userDetail.user_name {
                userNameLbl.text = name
            }
            
            if let hourRate = userDetail.hourly_rate {
                finalRatingLbl.text = "$" + String(format: "%.2f", Float(hourRate))//"$\(hourRate)"
                hourlyRateLbl.text = " $\(hourRate)"
                hourlyRate = hourRate
            } else {
                hourlyRateLbl.text = "$" + String(format: "%.2f", 0)
            }
            
            if let img = userDetail.headshot_compressed_image {
                self.profileImg.sd_setShowActivityIndicatorView(true)
                self.profileImg.sd_setIndicatorStyle(.gray)
                self.profileImg.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
            }
            
            if let taping_rate = userDetail.taping_rate {
                tappingRate = Int(taping_rate)!
                self.tappingLbl.text = "Can tape for an additional rate"
            } else {
                self.tappingLbl.text = "Can tape for an additional rate0"
            }
            
            if let editing_rate = userDetail.editing_rate {
                editingRate = Int(editing_rate)!
                self.editingLbl.text = "Can edit for an additional rate"
            } else {
                self.editingLbl.text = "Can edit for an additional rate"
            }
        }
    }
    
    @IBAction func inPersonMeetBtnClick(_ sender: Any) {
        self.personalMeetIMGV.image = UIImage(named: "ic_selectRadio")
        self.videoChatMeetIMGV.image = UIImage(named: "ic_radio")
        self.meeting_type = 1
        self.saveReaderInfo()
    }
    @IBAction func videoChatMeetBtnClick(_ sender: Any) {
        self.videoChatMeetIMGV.image = UIImage(named: "ic_selectRadio")
        self.personalMeetIMGV.image = UIImage(named: "ic_radio")
        self.meeting_type = 2
        
        SCLAlertView().showInfo("Info", subTitle: "Taping and editing services are only available in person, not by video chat.", closeButtonTitle: "Ok", colorStyle: int_red)
        if tappingSwitchBtn.isOn {
            setBtnTextColor(currentLbl: tappingSwitchLbl, alignment: .right, color: UIColor.black, text : "No")
            tappingSwitchBtn.isOn = false
        }
        if editingSwitchBtn.isOn {
            setBtnTextColor(currentLbl: editingSwitchLbl, alignment: .right, color: UIColor.black, text : "No")
            editingSwitchBtn.isOn = false
        }
        self.saveReaderInfo()
    }
    
    @IBAction func inviteBtnClick(_ sender: Any) {
        self.saveReaderInfo()
        _ = navigationController?.popViewController(animated: true)
    }
    func saveReaderInfo()  {
        print(self.currentDate)
        let obj = ReaderRequestModel(job_description: textView.text, is_taping: self.is_tapping, is_editing: self.is_editing, time_to_hire: self.selectedTime, meeting_time: self.currentDate, meeting_type: self.meeting_type)
        print(obj.toJSON())
        defaults.set(obj.toJSON(), forKey: "setHireReaderInfo")
    }
    @IBAction func nextBtnClicked(_ sender: Any) {
        print(readerlist.count)
        if self.readerlist.count != 0 {
            //   alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
            print(meeting_type)
            if self.meeting_type != 0 {
            if textView.textColor == UIColor.lightGray {
                alert.hideView()
                let alerview = SCLAlertView()
                alerview.showError("Error", subTitle: "Please enter job description")
            } else {
                print(dateLbl.text)
                if dateLbl.text == "" {
                    dateLbl.text = nil
                }
                print(dateLbl.text)
                if dateLbl.text != nil {
                    //                    alert.hideView()
                    if !tappingView.isHidden {
                        if !tappingSwitchBtn.isOn && editingSwitchBtn.isOn {
                            alert.hideView()
                            SCLAlertView().showInfo("Info", subTitle: "Editors must include taping in total.", closeButtonTitle: "Ok", colorStyle: int_red)
                        }
                        else if tappingSwitchBtn.isOn || editingSwitchBtn.isOn { //|| self.meeting_type == 2 {
                            print(self.meeting_type)
                            if meeting_type == 2 {
                                alert.hideView()
                                if tappingSwitchBtn.isOn {
                                    setBtnTextColor(currentLbl: tappingSwitchLbl, alignment: .right, color: UIColor.black, text : "No")
                                    tappingSwitchBtn.isOn = false
                                }
                                if editingSwitchBtn.isOn {
                                    setBtnTextColor(currentLbl: editingSwitchLbl, alignment: .right, color: UIColor.black, text : "No")
                                    editingSwitchBtn.isOn = false
                                }
                                SCLAlertView().showInfo("Info", subTitle: "Taping and editing services are only available in person, not by video chat.", closeButtonTitle: "Ok", colorStyle: int_red)
                            }else {
                                if self.tappingSwitchBtn.isOn || self.editingSwitchBtn.isOn {
                                    if is_removeDisableUser == 0 {
                                        let appearance = SCLAlertView.SCLAppearance(showCloseButton:false)
                                        let alert = SCLAlertView(appearance: appearance)
                                        alert.addButton("OK") {
                                            self.removeUserTaping()
                                        }
                                        alert.showInfo("Info", subTitle:"Readers who do not offer a taping or editing option will automatically be removed from this request.",colorStyle:int_red)
                                    }else {
                                        self.madeArray()
                                    }
                                }else {
                                    self.madeArray()
                                }
                                
                            }
                        }else {
//                            if self.tappingSwitchBtn.isOn || self.editingSwitchBtn.isOn {
//                                if is_removeDisableUser == 0 {
//                                    let appearance = SCLAlertView.SCLAppearance(showCloseButton:false)
//                                    let alert = SCLAlertView(appearance: appearance)
//                                    alert.addButton("OK") {
//                                        self.removeUserTaping()
//                                    }
//                                    alert.showInfo("Info", subTitle: "Readers who do not offer a taping or editing option will automatically be removed from this request.",colorStyle:int_red)
//                                }else {
//                                    self.madeArray()
//                                }
//                            }else {
//                                self.madeArray()
//                            }
                            self.madeArray()
                        }
                    }
                } else {
                    alert.hideView()
                    SCLAlertView().showError("Error", subTitle: "Please select date and time to meet.")
                }
            }
            }else {
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: "Please select meeting type.", closeButtonTitle: "Ok")
            }
        } else {
            let alertview = SCLAlertView()
            alertview.showError("Error", subTitle: "Please select at least one reader.", closeButtonTitle: "Ok")
        }
    }
    
    func setTotalPrice(isTappingOn: Bool, isEditingOn: Bool, selectedTime : Int) {
        var totalPrice : Float = 0
        var currentTimeRate : Float = 0
        print("half = \(Float(hourlyRate) / 2)")
        switch selectedTime{
        case 30:
            currentTimeRate = Float(hourlyRate) / 2
            break
        case 60:
            currentTimeRate = Float(hourlyRate)
            break
        default:
            currentTimeRate = Float(hourlyRate) + (Float(hourlyRate) / 2)
            break
        }
        //        print("currentTimeRate:- \(currentTimeRate)")
        if isTappingOn && isEditingOn {
            totalPrice = currentTimeRate + Float(tappingRate) + Float(editingRate)
        } else if isEditingOn {
            totalPrice = currentTimeRate + Float(editingRate)
        } else if isTappingOn{
            totalPrice = currentTimeRate + Float(tappingRate)
        } else {
            totalPrice = currentTimeRate
        }
        //        print("totalPrice:- \(totalPrice)")
        finalRate = totalPrice

        finalRatingLbl.text = "$" + String(format: "%.2f", totalPrice)

    }
    

//    @IBAction func hireSwitchClicked(_ sender: Any) {
//        let sender = sender as! UISwitch
//        switch sender {
//        case tappingSwitchBtn:
//            setTotalPrice(isTappingOn: tappingSwitchBtn.isOn, isEditingOn: editingSwitchBtn.isOn, selectedTime: selectedTime)
//            break
//        case editingSwitchBtn:
//            if editingSwitchBtn.isOn {
//                if !tappingSwitchBtn.isOn {
//                    SCLAlertView().showError("Error", subTitle: "Editors must include taping in total.")
//                    setTotalPrice(isTappingOn: true, isEditingOn: editingSwitchBtn.isOn, selectedTime: selectedTime)
//                } else {
//                    setTotalPrice(isTappingOn: tappingSwitchBtn.isOn, isEditingOn: editingSwitchBtn.isOn, selectedTime: selectedTime)
//                }
//            } else {
//                setTotalPrice(isTappingOn: tappingSwitchBtn.isOn, isEditingOn: editingSwitchBtn.isOn, selectedTime: selectedTime)
//            }
//
//            break
//        default:
//            if sender.isOn {
//                sendBtn.isUserInteractionEnabled = true
//            } else {
//                //            sendBtn.isUserInteractionEnabled = false
//                sendBtn.isUserInteractionEnabled = true
//            }
//            break
//        }
//    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
//        datePicker.removeFromSuperview()
        removePickerView()
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        self.saveReaderInfo()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Please enter job description"
            textView.textColor = UIColor.lightGray
        }
        self.saveReaderInfo()
    }
    
    func callHireReaderApi(rece_id : Int, str: String) {
//        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        let user = HireReaderRequestModel(receiver_user_id: rece_id, job_description: str, is_taping: ((tappingSwitchBtn.isOn) ? 1 : 0), is_editing: ((editingSwitchBtn.isOn) ? 1 : 0), final_amount: finalRate, time_to_hire: selectedTime, meeting_time : currentDate) //HireReaderRequestModel(receiver_user_id: rece_id, job_description: str)
        let data_temp = Mapper<HireReaderRequestModel>().toJSON(user)
        print("Hire readers request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(sendRequestToHireReader, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        let alertView = SCLAlertView()
                        alertView.showSuccess("Success", subTitle: (response.result.value?.message)!, closeButtonTitle: "Ok",  colorStyle: int_red)
                        let calendar = Calendar.current
                        defaults.set("\(rece_id)" , forKey: "receiverUserId")
//                        if let waitingTime = value.hireReaderResponse.waiting_time {
//                            waitingDuration = waitingTime
//                        }
//                        let date = calendar.date(byAdding: .minute, value: waitingDuration, to: Date())
//                        defaults.set(date , forKey: "terminationTime")
//                        defaults.set(false , forKey: "userAcceptedYourRequest")
//                        defaults.set(true, forKey: "isWaiting")
                        
                        self.navigationController?.popViewController(animated: true)

                        break
                    case 203:
                        alert.hideView()
                        if let msg = value.message {
                            print(msg)
                            let alertview = SCLAlertView()
                            alertview.addButton("Yes", action: {
                                if let jobId = value.hireReaderResponse.job_id {
                                    self.deleteJob(job_id: jobId)
                                }
                            })
                            alertview.showInfo("Delete Job?", subTitle: msg,closeButtonTitle: "Cancel", colorStyle: int_red)
                        }
                        break
                    case 202:
                        alert.hideView()
                        //                        self.isFromWebView = true
                        //                        let story = UIStoryboard(name: "Main", bundle: nil)
                        //
                        //                        let nextVC = story.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                        ////                        nextVC.userId = rece_id
                        //                        self.navigationController?.pushViewController(nextVC, animated: true)
                        if let msg = value.message {
                            let alertview = UIAlertController(title: "Info", message: msg, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                self.isFromWebView = true
                                let story = UIStoryboard(name: "Main", bundle: nil)
                                let nextVC = story.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                                nextVC.vcName = "HireReaderVC"
                                //                        nextVC.userId = rece_id
                                self.navigationController?.pushViewController(nextVC, animated: true)
                            }
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alertview.addAction(okAction)
                            alertview.addAction(cancelAction)
                            self.present(alertview, animated: true, completion: nil)
                        }
                        break
                    case 400:
                        alert.hideView()
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.callHireReaderApi(rece_id: rece_id, str: str)
                        }else {
                            alert.hideView()

                            if let error = value.message {
                                let alertview = SCLAlertView()
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
    
    func callMultipleHireReaderApi() {
            alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        let user = MultipleHireReader(reader_list: self.multiplerequestArr)
        let data_temp = Mapper<MultipleHireReader>().toJSON(user)
        print("sendMultipleRequestToHireReaderPhase3 request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(sendMultipleRequestToHireReaderPhase3, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        UserDefaults.standard.removeObject(forKey: "readerSelected")
                        UserDefaults.standard.removeObject(forKey: "setHireReaderInfo")
                        //defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        defaults.synchronize()
                        
                      
                       
                        alert.hideView()
                        let alertView = SCLAlertView()
                        alertView.showSuccess("Success", subTitle: (response.result.value?.message)!, closeButtonTitle: "Ok",  colorStyle: int_red)
                        let calendar = Calendar.current
                        
                        
                        
                        //defaults.set("\(rece_id)" , forKey: "receiverUserId")
                        //                        if let waitingTime = value.hireReaderResponse.waiting_time {
                        //                            waitingDuration = waitingTime
                        //                        }
                        //                        let date = calendar.date(byAdding: .minute, value: waitingDuration, to: Date())
                        //                        defaults.set(date , forKey: "terminationTime")
                        //                        defaults.set(false , forKey: "userAcceptedYourRequest")
                        //                        defaults.set(true, forKey: "isWaiting")
                        
                        self.navigationController?.popViewController(animated: true)
                        
                        break
                    case 203:
                        alert.hideView()
                        if let msg = value.message {
                            let alertview = SCLAlertView()
                            alertview.addButton("Yes", action: {
                                if let jobId = value.hireReaderResponse.job_id {
                                    self.deleteJob(job_id: jobId)
                                }
                            })
                            alertview.showInfo("Delete Job?", subTitle: msg,closeButtonTitle: "Cancel", colorStyle: int_red)
                        }
                        break
                    case 202:
                        alert.hideView()
                        //                        self.isFromWebView = true
                        //                        let story = UIStoryboard(name: "Main", bundle: nil)
                        //
                        //                        let nextVC = story.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                        ////                        nextVC.userId = rece_id
                        //                        self.navigationController?.pushViewController(nextVC, animated: true)
                        if let msg = value.message {
                            let alertview = UIAlertController(title: "Info", message: msg, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                self.isFromWebView = true
                                let story = UIStoryboard(name: "Main", bundle: nil)
                                let nextVC = story.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                                nextVC.vcName = "HireReaderVC"
                                //                        nextVC.userId = rece_id
                                self.navigationController?.pushViewController(nextVC, animated: true)
                            }
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alertview.addAction(okAction)
                            alertview.addAction(cancelAction)
                            self.present(alertview, animated: true, completion: nil)
                        }
                        break
                    case 400:
                        alert.hideView()
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.callMultipleHireReaderApi()
                        }else {
                            alert.hideView()
                            
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
    //Set NavigationBar
    func configureNavigationBar()
    {
        setNavigaion(vc: self)
        
        let navigationItem = self.navigationItem
        navigationItem.title = "HIRE READER"
        
        self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        self.navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        let chatBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        if #available(iOS 11.0, *) {
            let chatwidthConstraint = chatBtn.widthAnchor.constraint(equalToConstant: 44)
            let chatheightConstraint = chatBtn.heightAnchor.constraint(equalToConstant: 44)
            chatheightConstraint.isActive = true
            chatwidthConstraint.isActive = true
            chatBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 0)//UIEdgeInsetsMake(8, 0, 8, 16)
        }else {
            chatBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 0)//UIEdgeInsetsMake(8, 0, 6, 12)
        }
        chatBtn.setImage(UIImage(named: "ic_questionMark"), for: .normal)
        chatBtn.addTarget(self, action: #selector(chatBtnPressed(_:)), for: .touchUpInside)
        let chatButton = UIBarButtonItem(customView: chatBtn)
        navigationItem.rightBarButtonItems = [chatButton]
    }
    @objc func chatBtnPressed(_ sender: UIButton) {
        
        let alertview = UIAlertController(title: "Hiring note", message: "First reader to confirm is hired", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel)
        alertview.addAction(cancelAction)
        
        self.present(alertview, animated: true, completion: nil)
    }
    func deleteJob(job_id: Int) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        let user = NotificationRequestModel(job_id: job_id)
        let data_temp = Mapper<NotificationRequestModel>().toJSON(user)
        print("Delete Job request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(cancelJob, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        //                        alert.hideView()
                        //self.callHireReaderApi(rece_id: self.userDetail.user_id!, str: self.textView.text)
                        //                        self.navigationController?.popViewController(animated: true)
                        self.callMultipleHireReaderApi()
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.deleteJob(job_id: job_id)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
    
    func hideMutualFrndView() {
        collectionHeightConstant.constant = 0
        collectionView.isHidden = true
        totalMutualFrndLbl.isHidden = true
        mutualLblHeightConstant.constant = 0
    }
    
    func getUserMutualFriends() {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        let user = NotificationRequestModel(receiver_user_id: userDetail.user_id)
        let data_temp = Mapper<NotificationRequestModel>().toJSON(user)
        
        alamofireManager.request(getMutualFriendList, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        if let data = value.contactReponse {
                            self.mutualFrndResponse = data
                            if let arr = data.mutualFrndList {
                                if !arr.isEmpty {
                                    self.mutualFrndList = arr
                                    self.collectionHeightConstant.constant = 120
                                    self.mutualLblHeightConstant.constant = 15
                                    self.collectionView.isHidden = false
                                    self.totalMutualFrndLbl.isHidden = false
                                    self.totalMutualFrndLbl.text = "\(arr.count) MUTUAL FRIENDS"
                                    self.collectionView.reloadData()
                                } else {
                                    self.hideMutualFrndView()
                                }
                            } else {
                                self.hideMutualFrndView()
                            }
                        } else {
                            self.hideMutualFrndView()
                        }
                        print("Success")
                        break
                    case 400:
                        alert.hideView()
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.getUserMutualFriends()
                        }else {
                            clearSession()
                            let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                            let nav = redirectToRootViewController(vcName: vc)
                            self.present(nav, animated: true)
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        self.hideMutualFrndView()

                        break
                    }
                }
                break
            case .failure(let error):
                alert.hideView()
                SCLAlertView().showError(errorTitle, subTitle: serverError)
                //                    self.addErrorView()
                print("Request failed with error: \(error.localizedDescription)")
                self.hideMutualFrndView()

                break
            }
            }.responseJSON { response in
                print("response mutual Friend:- \(response.result.value)")
        }
    }
    
    
    @IBAction func timeBtnsClicked(_ sender: Any) {
        let btn = sender as! UIButton
        switch btn {
        case minute30Btn:
            selectedTime = 30
            setSelectedBtnColors(selectedBtn: minute30Btn, otherBtn: [minute60Btn, minute90Btn])
            

            print("30 min rate")
            
            self.saveReaderInfo()
            
            break
        case minute60Btn:
            setSelectedBtnColors(selectedBtn: minute60Btn, otherBtn: [minute30Btn, minute90Btn])
           
            selectedTime = 60
            print("60 min rate")
            self.saveReaderInfo()
            
            break
        default:
            setSelectedBtnColors(selectedBtn: minute90Btn, otherBtn: [minute30Btn, minute60Btn])
            
            selectedTime = 90
            print("90 min rate")
            self.saveReaderInfo()
            
            break
        }
        tableview.reloadData()
        setTotalPrice(isTappingOn: tappingSwitchBtn.isOn, isEditingOn: editingSwitchBtn.isOn, selectedTime: selectedTime)
        
    }
    
    @IBAction func datePickerBtnClicked(_ sender: Any) {
        setDatePicker()
    }
    
    func setDatePicker() {
        self.view.endEditing(true)
        
        datePicker.datePickerMode = .dateAndTime
        datePicker.addTarget(self, action: #selector(self.dateChanged), for: .valueChanged)
        datePicker.backgroundColor = hexStringToUIColor(hex: darkGrayColor)
        datePicker.frame = CGRect(x: 0, y: self.view.frame.size.height - 250, width: self.view.frame.size.width, height: 250)
        // Creates the toolbar
        toolBar.barStyle = .default
        toolBar.isTranslucent = false
//        toolBar.backgroundColor = hexStringToUIColor(hex: redColor)
        toolBar.barTintColor = hexStringToUIColor(hex: redColor)
//        toolBar.tintColor = hexStringToUIColor(hex: redColor)
        toolBar.sizeToFit()
        toolBar.frame = CGRect(x: 0, y: self.view.frame.height - 290, width: self.view.frame.width, height: 40)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.dismissPicker))
        
        toolBar.layer.cornerRadius = 5.0
        toolBar.layer.shadowOpacity = 0.5
        toolBar.setItems([doneButton], animated: false)
        doneButton.tintColor = UIColor.white
        toolBar.isUserInteractionEnabled = true
        //   toolBar.bringSubview(toFront: doneButton)
        self.view.addSubview(toolBar)
        self.view.addSubview(datePicker)
//        self.datePicker.bringSubview(toFront: scrollview)
//        self.toolBar.bringSubview(toFront: scrollview)
    }
    
    @objc func dateChanged(sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.dateFormat = "MMM dd, yyyy 'at' hh:mm a"
        let selectedDate = dateFormatter.string(from: sender.date)
        
//        dateFormatter.dateFormat = "hh:mm a"
//        let selectedTime = dateFormatter.string(from: sender.date)
        
        ///        scheduleNotification()
        
//        if dateLbl.text = "" {
//            dateLblHeightConstant.constant = 20
//            self.imageouterView.frame.size.height = self.imageouterView.frame.size.height + 20
//        }
        dateLblHeightConstant.constant = 20
        dateLbl.text = selectedDate //+ " at " + selectedTime
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

//        currentDate = sender.date
        currentDate = dateFormatter.string(from: sender.date)
        self.saveReaderInfo()
    }
    
    /*
     * MARK - dismiss the date picker value
     */
    @objc func dismissPicker(sender: UIButton) {
        removePickerView()
    }
    
    func removePickerView() {
        datePicker.removeFromSuperview()
        toolBar.removeFromSuperview()

    }
    func madeArray()  {
        if UserDefaults.standard.value(forKey: "setHireReaderInfo") != nil {
            if let v = UserDefaults.standard.dictionary(forKey: "setHireReaderInfo") {
                print(v)
                let obj = Mapper<ReaderRequestModel>().map(JSON: v)
                print(obj)
                self.multiplerequestArr.removeAll()
                for items in readerlist {
                    print(items.toJSON())
                    //if items.editing_rate != "0" || items.taping_rate != "0" {
                        let obje = MultipleHireReaderRequestModel(receiver_user_id: items.user_id, job_description: obj?.job_description, is_taping: obj?.is_taping, is_editing: obj?.is_editing, final_amount: items.final_rate, time_to_hire: obj?.time_to_hire,meeting_time : obj?.meeting_time, meeting_type:obj?.meeting_type)
                        self.multiplerequestArr.append(obje)
                    //}
                }
                print(self.multiplerequestArr.count)
                print(self.multiplerequestArr.toJSON())
                if multiplerequestArr.count > 0 {
                    self.callMultipleHireReaderApi()
                }else {
                    
                }
                
            }
        }else {
            print(self.currentDate)
            let obj = ReaderRequestModel(job_description: textView.text, is_taping: self.is_tapping, is_editing: self.is_editing, time_to_hire: self.selectedTime, meeting_time: self.currentDate, meeting_type: self.meeting_type)
            print(obj)
            print(readerlist.count)
            print(self.multiplerequestArr.count)
            self.multiplerequestArr.removeAll()
            for items in readerlist {
               // if items.editing_rate != "0" || items.taping_rate != "0" {
                    let obje = MultipleHireReaderRequestModel(receiver_user_id: items.user_id, job_description: obj.job_description, is_taping: obj.is_taping, is_editing: obj.is_editing, final_amount: items.final_rate, time_to_hire: obj.time_to_hire,meeting_time : obj.meeting_time, meeting_type:obj.meeting_type)
                    self.multiplerequestArr.append(obje)
               // }
            }
            print(self.multiplerequestArr.count)
            print(self.multiplerequestArr.toJSON())
            if multiplerequestArr.count > 0 {
                self.callMultipleHireReaderApi()
            }else {
                
            }
           
        }
    }
    func removeUserTaping()  {
        var temparr : [User_detail] = []
        print(temparr.count)
        for i in 0..<readerlist.count {
            
            if readerlist[i].editing_rate != "0" || readerlist[i].taping_rate != "0" {
                temparr.append(readerlist[i])
            }
        }
        self.readerlist = temparr
        print(self.readerlist.toJSON())
        self.tablviewHeightConstraint.constant = CGFloat(self.readerlist.count * 100)
        self.is_removeDisableUser = 1
        defaults.set(self.readerlist.toJSON(), forKey: "readerSelected")
        
        self.scrollview.layoutIfNeeded()
        self.tableview.reloadData()
    }
    
}

extension HireReaderVC : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mutualFrndList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! FriendCellVC
        cell.profileImgview.sd_setShowActivityIndicatorView(true)
        cell.profileImgview.sd_setIndicatorStyle(.gray)
//        viewCorners.addCornerToView(view: cell.profileImgview)
        viewCorners.addBorderCornerTobutton(view: cell.profileImgview, color: hexStringToUIColor(hex: purpleColor))

        if let img = mutualFrndList[indexPath.row].headshot_compressed_image {
            cell.profileImgview.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
        }
        
        if let name = mutualFrndList[indexPath.row].user_name {
            cell.userNameLbl.text = name
        } else {
            cell.userNameLbl.text = ""
        }
        
        //        cell.profileImgview.image = UIImage(named: "ic_setting")
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
}

extension HireReaderVC: SwiftySwitchDelegate {
    //add this function inside the desired viewController
    func valueChanged(sender: SwiftySwitch) {
        let sender = sender as! SwiftySwitch
        switch sender {
        case tappingSwitchBtn:
            if !tappingSwitchBtn.isOn {
                setBtnTextColor(currentLbl: tappingSwitchLbl, alignment: .right, color: UIColor.black, text : "No")
                setTotalPrice(isTappingOn: tappingSwitchBtn.isOn, isEditingOn: editingSwitchBtn.isOn, selectedTime: selectedTime)
                self.is_tapping = 0
                self.saveReaderInfo()
                
            } else {
                if !editingSwitchBtn.isOn {
                setBtnTextColor(currentLbl: tappingSwitchLbl, alignment: .left, color: UIColor.black, text : "Yes")

                setTotalPrice(isTappingOn: tappingSwitchBtn.isOn, isEditingOn: editingSwitchBtn.isOn, selectedTime: selectedTime)
                self.is_tapping = 1
              //  self.is_editing  = 1
                self.saveReaderInfo()
//                editingSwitchBtn.isOn = true
//                setBtnTextColor(currentLbl: editingSwitchLbl, alignment: .left, color: UIColor.black, text : "Yes")
//                    setBtnTextColor(currentLbl: editingSwitchLbl, alignment: .left, color: UIColor.black, text : "Yes")
//                SCLAlertView().showInfo("Info", subTitle: "Editors must include editing in total.", closeButtonTitle: "Ok", colorStyle: int_red)
                }else {
                   setTotalPrice(isTappingOn: tappingSwitchBtn.isOn, isEditingOn: editingSwitchBtn.isOn, selectedTime: selectedTime)
                    self.is_tapping = 1
                    setBtnTextColor(currentLbl: tappingSwitchLbl, alignment: .left, color: UIColor.black, text : "Yes")
                    self.saveReaderInfo()

                }
            }
            break
        case editingSwitchBtn:
            if editingSwitchBtn.isOn {
//                editingSwitchBtn.myColor = UIColor(red: 240 / 255, green: 65 / 255, blue: 0 / 255, alpha: 0.44)
                setBtnTextColor(currentLbl: editingSwitchLbl, alignment: .left, color: UIColor.black, text : "Yes")

                if !tappingView.isHidden {
                    if !tappingSwitchBtn.isOn {
                        SCLAlertView().showInfo("Info", subTitle: "Editors must include taping in total.", closeButtonTitle: "Ok", colorStyle: int_red)
                        tappingSwitchBtn.isOn = true
                        setBtnTextColor(currentLbl: tappingSwitchLbl, alignment: .left, color: UIColor.black, text : "Yes")
                        
                        setTotalPrice(isTappingOn: true, isEditingOn: editingSwitchBtn.isOn, selectedTime: selectedTime)
                        self.is_tapping = 1
                        self.is_editing = 1
                        self.saveReaderInfo()
                        
                    } else {
                        setTotalPrice(isTappingOn: tappingSwitchBtn.isOn, isEditingOn: editingSwitchBtn.isOn, selectedTime: selectedTime)
                        self.is_editing = 1
                        self.saveReaderInfo()
                    }
                } else {
                    setTotalPrice(isTappingOn: tappingSwitchBtn.isOn, isEditingOn: editingSwitchBtn.isOn, selectedTime: selectedTime)
                }
                
            } else {
                setBtnTextColor(currentLbl: editingSwitchLbl, alignment: .right, color: UIColor.black, text : "No")

                setTotalPrice(isTappingOn: tappingSwitchBtn.isOn, isEditingOn: editingSwitchBtn.isOn, selectedTime: selectedTime)
                self.is_editing = 0
                self.saveReaderInfo()
            }
            
            break
        default:
            if sender.isOn {
                setBtnTextColor(currentLbl: hireReaderYesNoLbl, alignment: .left, color: UIColor.black, text : "Yes")
                
//                sendBtn.isUserInteractionEnabled = true
            } else {
//                //            sendBtn.isUserInteractionEnabled = false
//                sendBtn.isUserInteractionEnabled = true
             
                setBtnTextColor(currentLbl: hireReaderYesNoLbl, alignment: .right, color: UIColor.black, text : "No")
            }
            break
        }
        self.tableview.reloadData()
    }
    
   
}

extension HireReaderVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return readerlist.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RatingCell
        //        viewCorners.addCornerToView(view: cell.userProfile)
        
        viewCorners.addBorderCornerTobutton(view: cell.userProfile, color: hexStringToUIColor(hex: purpleColor))
       // viewCorners.addCornerToView(view: cell.outerView, value: 5)
        viewCorners.addCornerToView(view: cell.viewBtn, value: 15)
       // viewCorners.addCornerToView(view: cell.cameraImageView)
        cell.viewBtn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        cell.viewBtn.tag = indexPath.row
        
       // viewCorners.addShadowToView(view: cell.outerView)
        cell.cameraImageView.image = UIImage(named:"ic_showServices")
        if readerlist[indexPath.row].taping_rate != "0" || readerlist[indexPath.row].editing_rate != "0" {
            cell.cameraImageView.isHidden = false
        }else {
            cell.cameraImageView.isHidden = true
        }
        
  
        cell.userProfile.sd_setShowActivityIndicatorView(true)
        cell.userProfile.sd_setIndicatorStyle(.gray)
        if let str = readerlist[indexPath.row].headshot_thumbnail_image {
            cell.userProfile.sd_setImage(with: URL(string: str), placeholderImage: UIImage(named: "ic_defaultProfile"))
        }
        if selectedTime == 30 {
            if self.is_editing == 1 && self.is_tapping == 1 {
                if let rate = readerlist[indexPath.row].hourly_rate {
                    let editing = readerlist[indexPath.row].editing_rate
                    let tapping = readerlist[indexPath.row].taping_rate
                    let final = (Float(rate)/2)+Float(editing!)!+Float(tapping!)!
                    //cell.jobDescriptionLbl.text = "Final Reader Rate:$\(final)"
                    cell.jobDescriptionLbl.text = "Final Reader Rate:\(" $" + String(format: "%.2f", Float(final)))"
                }
            }else if self.is_editing == 1 {
                if let rate = readerlist[indexPath.row].hourly_rate {
                    let editing = readerlist[indexPath.row].editing_rate
                    
                    let final = (Float(rate)/2)+Float(editing!)!
                    //cell.jobDescriptionLbl.text = "Final Reader Rate:$\(final)"
                    cell.jobDescriptionLbl.text = "Final Reader Rate:\(" $" + String(format: "%.2f", Float(final)))"
                }
            }
            else if self.is_tapping == 1 {
                if let rate = readerlist[indexPath.row].hourly_rate {
                    let tapping = readerlist[indexPath.row].taping_rate
                    let final = (Float(rate)/2)+Float(tapping!)!
                    //cell.jobDescriptionLbl.text = "Final Reader Rate:$\(final)"
                    cell.jobDescriptionLbl.text = "Final Reader Rate:\(" $" + String(format: "%.2f", Float(final)))"
                }
            }else {
                if let rate = readerlist[indexPath.row].hourly_rate {
                    let final = (Float(rate)/2)
                    //cell.jobDescriptionLbl.text = "Final Reader Rate:$\(final)"
                    cell.jobDescriptionLbl.text = "Final Reader Rate:\(" $" + String(format: "%.2f", Float(final)))"
                }
            }
            
        }else if selectedTime == 60 {
//            if let rate = readerlist[indexPath.row].hourly_rate {
//                cell.jobDescriptionLbl.text = "Final Reader Rate:$\(rate)"
//            }
            if self.is_editing == 1 && self.is_tapping == 1 {
                if let rate = readerlist[indexPath.row].hourly_rate {
                    let editing = readerlist[indexPath.row].editing_rate
                    let tapping = readerlist[indexPath.row].taping_rate
                    let final = (Float(rate))+Float(editing!)!+Float(tapping!)!
                    
                    cell.jobDescriptionLbl.text = "Final Reader Rate:\(" $" + String(format: "%.2f", Float(final)))"
                }
            }else if self.is_editing == 1 {
                if let rate = readerlist[indexPath.row].hourly_rate {
                    let editing = readerlist[indexPath.row].editing_rate
                    
                    let final = (Float(rate))+Float(editing!)!
                    cell.jobDescriptionLbl.text = "Final Reader Rate:\(" $" + String(format: "%.2f", Float(final)))"
                }
            }
            else if self.is_tapping == 1 {
                if let rate = readerlist[indexPath.row].hourly_rate {
                    let tapping = readerlist[indexPath.row].taping_rate
                    let final = (Float(rate))+Float(tapping!)!
                    
                    cell.jobDescriptionLbl.text = "Final Reader Rate:\(" $" + String(format: "%.2f", Float(final)))"
                }
            }else {
                if let rate = readerlist[indexPath.row].hourly_rate {
                    let final = (Float(rate))
                    cell.jobDescriptionLbl.text = "Final Reader Rate:\(" $" + String(format: "%.2f", Float(final)))"
                }
            }
            
        }else {
            if self.is_editing == 1 && self.is_tapping == 1 {
                if let rate = readerlist[indexPath.row].hourly_rate {
                    let editing = readerlist[indexPath.row].editing_rate
                    let tapping = readerlist[indexPath.row].taping_rate
                    let final = (Float(rate/2)+Float(rate))+Float(editing!)!+Float(tapping!)!
                    cell.jobDescriptionLbl.text = "Final Reader Rate:\(" $" + String(format: "%.2f", Float(final)))"
                }
            }else if self.is_editing == 1 {
                if let rate = readerlist[indexPath.row].hourly_rate {
                    let editing = readerlist[indexPath.row].editing_rate
                    
                    let final = (Float(rate/2)+Float(rate))+Float(editing!)!
                    cell.jobDescriptionLbl.text = "Final Reader Rate:\(" $" + String(format: "%.2f", Float(final)))"
                }
            }
            else if self.is_tapping == 1 {
                if let rate = readerlist[indexPath.row].hourly_rate {
                    let tapping = readerlist[indexPath.row].taping_rate
                    let final = (Float(rate/2)+Float(rate))+Float(tapping!)!
                    
                    cell.jobDescriptionLbl.text = "Final Reader Rate:\(" $" + String(format: "%.2f", Float(final)))"
                }
            }else {
                if let rate = readerlist[indexPath.row].hourly_rate {
                    let final = (Float(rate/2)+Float(rate))
                    cell.jobDescriptionLbl.text = "Final Reader Rate:\(" $" + String(format: "%.2f", Float(final)))"
                }
            }
        }
        
        if let name = readerlist[indexPath.row].user_name {
            cell.userNameLbl.text = name
        }
        print(cell.jobDescriptionLbl.text)
        let myStringArr = cell.jobDescriptionLbl.text?.components(separatedBy: "$")
        print(myStringArr)
        let Finalrate : String = String(format: "%.2f", Float(myStringArr![1])!)//myStringArr![1]
        print("Final Rate is: \(Finalrate)")
        readerlist[indexPath.row].final_rate = Float(Finalrate)
       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "ReaderprofileVC") as! ReaderprofileVC
        nextVC.userDetail = readerlist[indexPath.row]
        //self.navigationController?.pushViewController(nextVC, animated: true)
      //  self.present(nextVC, animated: true, completion: nil)
        let navigationController = UINavigationController(rootViewController: nextVC)
        present(navigationController, animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    @objc func buttonAction(sender: UIButton!) {
        print("click")
        
       
            let alertview = UIAlertController(title: "Alert", message: "Are you sure you want to delete this reader?", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                
                self.readerlist.remove(at: sender.tag)
                defaults.set(self.readerlist.toJSON(), forKey: "readerSelected")
                self.tablviewHeightConstraint.constant = CGFloat(self.readerlist.count * 100)
                self.tableview.reloadData()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            alertview.addAction(okAction)
            alertview.addAction(cancelAction)
            
            self.present(alertview, animated: true, completion: nil)
            
    }
    
}
