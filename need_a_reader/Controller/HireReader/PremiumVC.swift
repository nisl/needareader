//
//  PremiumVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 2/2/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import Stripe
import Alamofire
import SCLAlertView
import AlamofireObjectMapper
import ObjectMapper

class PremiumVC : UIViewController, STPPaymentCardTextFieldDelegate {
    
    
    @IBOutlet var payOrder: UIButton!
    @IBOutlet var outerView: UIView!
    
    var job_detail : Job_detail!
    let cardParams = STPCardParams()
    let paymentField = STPPaymentCardTextField()
    var isFromAddVideo : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if job_detail != nil {
            if let finalRate = job_detail.finalRate {
                payOrder.setTitle("PAY $" + finalRate, for: .normal)
            } else if let rate = job_detail.hourly_rate {
                payOrder.setTitle("PAY $\(rate)", for: .normal)

            }
        }
        
        if isFromAddVideo {
            payOrder.setTitle("PAY $\(videoPaymentAmount)", for: .normal)
        }
        
        paymentField.frame = CGRect(x: 15, y: 30, width:self.view.frame.size.width - 30, height: 45)
//         let paymentField = STPPaymentCardTextField(frame: CGRect(x: 15, y: 25, width:self.view.frame.size.width - 30, height: 45))
        
        paymentField.delegate = self
        
        paymentField.backgroundColor = UIColor.black
        paymentField.textColor = UIColor.white
        paymentField.textErrorColor = UIColor.orange
        self.view.addSubview(paymentField)
        configureNavigationBar()
        viewCorners.addCornerToView(view: payOrder)
//        viewCorners.addCornerToView(view: paymentField)
        
        viewCorners.addShadowToView(view: paymentField, value: 2.0)
//        paymentField.backgroundColor = UIColor.white
        paymentField.borderColor = UIColor.clear
        paymentField.placeholderColor = hexStringToUIColor(hex: "666666")
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        payOrder.translatesAutoresizingMaskIntoConstraints = false
//        payOrder.frame = CGRect(x: 15, y: (paymentField.frame.maxY + 30), width:self.view.frame.size.width - 30, height: 45)
    }
    
    
    func paymentCardTextFieldDidChange(textField: STPPaymentCardTextField) {
        print("Card number: \(textField.cardParams.number) Exp Month: \(textField.cardParams.expMonth) Exp Year: \(textField.cardParams.expYear) CVC: \(textField.cardParams.cvc)")
        let validData = textField.isValid
        if validData {
//            payOrder.backgroundColor = UIColor.blue
            cardParams.number = textField.cardParams.number
            cardParams.expMonth = textField.cardParams.expMonth
            cardParams.expYear = textField.cardParams.expYear
            cardParams.cvc = textField.cardParams.cvc
        }
    }
    

    @IBAction func payBtnClicked(_ sender: Any) {
        self.view.endEditing(true)
        let validData = paymentField.isValid
//        self.payOrder.isEnabled = paymentField.isValid
        if validData {
            cardParams.number = paymentField.cardParams.number
            cardParams.expMonth = paymentField.cardParams.expMonth
            cardParams.expYear = paymentField.cardParams.expYear
            cardParams.cvc = paymentField.cardParams.cvc
            var releaseTokenId = ""
            if let number = cardParams.number {
                switch number {
                case "4242424242424242":
                    releaseTokenId = "tok_visa";
                    break;
                case "4000056655665556":
                    releaseTokenId = "tok_visa_debit";
                    break;
                case "5555555555554444":
                    releaseTokenId = "tok_mastercard";
                    break;
                case "5200828282828210":
                    releaseTokenId = "tok_mastercard_debit";
                    break;
                case "5105105105105100":
                    releaseTokenId = "tok_mastercard_prepaid";
                    break;
                case "378282246310005": break
                case "371449635398431":
                    releaseTokenId = "tok_amex";
                    break;
                case "6011111111111117": break
                case "6011000990139424":
                    releaseTokenId = "tok_discover";
                    break;
                default:
                    break
                }
            }
            if releaseTokenId != "" {
                createToken(testedToken: releaseTokenId)
            } else {
                createToken()
            }
        } else {
            let alertView = SCLAlertView()
            alertView.showError("Error", subTitle: "Please enter valid card info.")
        }
    }
    
    func createToken(testedToken: String? = nil) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        STPAPIClient.shared().createToken(withCard: cardParams) { (token, error) in
            if let error = error {
                alert.hideView()
                let alertView = SCLAlertView()
                alertView.showError("Failure", subTitle: error.localizedDescription)
//                print("error:- \(error)")
            } else if let token = token {
                if self.isFromAddVideo {
                    self.videoPayment(token: token, testedToken: testedToken)
                } else {
                    self.jobPayment(token: token, testedToken: testedToken)

                }
//                print("token:- \(token)")

            } else {
                alert.hideView()

                print("Nothing")
            }
        }
    }
    
    func jobPayment(token: STPToken, testedToken: String? = nil) {
        let user : PaymentRequestModel!

        if let jobId = self.job_detail.job_id {
            if testedToken != nil {
                user = PaymentRequestModel(job_id: jobId, source: testedToken)
            } else {
                user = PaymentRequestModel(job_id: jobId, source: "\(token)")
            }
            
            let data_temp = Mapper<PaymentRequestModel>().toJSON(user!)
           print("job payment request:- \(data_temp)")
            
            //Call api
            alamofireManager.request(releasePayment, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
                
                //                    let responseClass = response.result.value
                switch response.result
                {
                case .success(let value):
                    if let code = value.code {
                        print("Code:- \(code)")
                        switch code {
                        case 200:
                            alert.hideView()
                            self.navigationController?.popViewController(animated: true)
                            let alertView = SCLAlertView()
                            
                            alertView.showSuccess("Success", subTitle: (response.result.value?.message)!, closeButtonTitle: "Ok",  colorStyle: int_red)
                            self.navigationController?.popToRootViewController(animated: true)
                            break
                        case 202:
                            alert.hideView()
                            //                            self.isFromWebView = true
                            //                                    let story = UIStoryboard(name: "Main", bundle: nil)
                            //
                            //                                    let nextVC = story.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                            //                                    //                            nextVC.userId = self.job_detail.sender_user_id
                            //                                    self.navigationController?.pushViewController(nextVC, animated: true)
                            if let msg = value.message {
                                redirectWebViewDialog(title: "Info", str: msg, view: self)
                            }
                            break
                        case 400:
                            alert.hideView()
                            
                            clearSession()
                            let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                            let nav = redirectToRootViewController(vcName: vc)
                            self.present(nav, animated: true)
                            break
                        case 401:
                            alert.hideView()
                            if let new_token = value.loginResponse?.new_token {
                                UserDefaults.standard.set(new_token, forKey: "token")
                                self.createToken(testedToken: testedToken)
                            }else {
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                            }
                            break
                        default:
                            alert.hideView()
//                            let appearance1 = SCLAlertView.SCLAppearance(
//                                showCloseButton: true
//                            )
                            if let error = value.message {
                                let alertview = SCLAlertView()//SCLAlertView(appearance: appearance1)
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                            break
                        }
                    }
                    break
                case .failure(let error):
                    print("error: \(error.localizedDescription)")
                    
                    alert.hideView()
                    let alertview = SCLAlertView()
                    alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                    break
                }
                }.responseJSON { response in
                    print("stripe payment response:- \(response.result.value)")
            }
        }
    }
    
    func videoPayment(token: STPToken, testedToken: String? = nil) {
        let user : PaymentRequestModel!
        
            if testedToken != nil {
                user = PaymentRequestModel(amount: videoPaymentAmount, source: testedToken)
            } else {
                user = PaymentRequestModel(amount: videoPaymentAmount, source: "\(token)")
            }
            
            let data_temp = Mapper<PaymentRequestModel>().toJSON(user!)
            //                    print("signUp request:- \(data_temp)")
            
            //Call api
            alamofireManager.request(releasePaymentForVideo, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
                
                //                    let responseClass = response.result.value
                switch response.result
                {
                case .success(let value):
                    if let code = value.code {
                        print("Code:- \(code)")
                        switch code {
                        case 200:
                            alert.hideView()
//                            let alertView = SCLAlertView()
//                            alertView.showSuccess("Success", subTitle: , closeButtonTitle: "Ok",  colorStyle: int_red)
                            let alertview = UIAlertController(title: "Success", message: (response.result.value?.message)!, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                current_user_detail?.video_payment_status = true
                                let cnt = self.navigationController?.viewControllers.count
                                if let currentvc = self.navigationController?.viewControllers[cnt! - 1] as? AddVideoVC {
                                    print("add video is on 1")
                                    currentvc.isStripeSetupWithPayment = true
                                    self.navigationController?.popToViewController(currentvc, animated: true)
                                } else if let currentvc = self.navigationController?.viewControllers[cnt! - 2] as? AddVideoVC {
                                    print("add video is on 2")
                                    currentvc.isStripeSetupWithPayment = true
                                    self.navigationController?.popToViewController(currentvc, animated: true)
                                } else if let currentvc = self.navigationController?.viewControllers[cnt! - 3] as? AddVideoVC {
                                    print("add video is on 3")
                                    currentvc.isStripeSetupWithPayment = true
                                    self.navigationController?.popToViewController(currentvc, animated: true)
                                } else {
                                    print("not found")
//                                    let nextvc = storyBoard.instantiateViewController(withIdentifier: "AddVideoVC") as! AddVideoVC
//                                    nextvc.isStripeSetupWithPayment = true
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alertview.addAction(okAction)
                            alertview.addAction(cancelAction)
                            self.present(alertview, animated: true, completion: nil)
//                            self.navigationController?.popToRootViewController(animated: true)
                            break
                        case 202:
                            alert.hideView()
                            //                            self.isFromWebView = true
                            //                                    let story = UIStoryboard(name: "Main", bundle: nil)
                            //
                            //                                    let nextVC = story.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                            //                                    //                            nextVC.userId = self.job_detail.sender_user_id
                            //                                    self.navigationController?.pushViewController(nextVC, animated: true)
                            if let msg = value.message {
                                redirectWebViewDialog(title: "Info", str: msg, view: self)
                            }
                            break
                        case 400:
                            alert.hideView()
                            
                            clearSession()
                            let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                            let nav = redirectToRootViewController(vcName: vc)
                            self.present(nav, animated: true)
                            break
                        case 401:
                            alert.hideView()
                            if let new_token = value.loginResponse?.new_token {
                                UserDefaults.standard.set(new_token, forKey: "token")
                                self.createToken(testedToken: testedToken)
                            }else {
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                            }
                            break
                        default:
                            alert.hideView()
//                            let appearance1 = SCLAlertView.SCLAppearance(
//                                showCloseButton: true
//                            )
                            if let error = value.message {
                                let alertview = SCLAlertView()//SCLAlertView(appearance: appearance1)
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                            break
                        }
                    }
                    break
                case .failure(let error):
                    print("error: \(error.localizedDescription)")
                    alert.hideView()
                    let alertview = SCLAlertView()
                    alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                    break
                }
                }.responseJSON { response in
                    print("video payment response:- \(response.result.value)")
        }
            
        
    }
    
    //Set NavigationBar
    func configureNavigationBar() {
        setNavigaion(vc: self)
        let navigationItem = self.navigationItem
        if isFromAddVideo {
            navigationItem.title = "VIDEO PAYMENT"

        } else {
            navigationItem.title = "PAY READER"

        }
        
        self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        self.navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
   
}
