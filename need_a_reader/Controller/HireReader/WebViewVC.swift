//
//  WebViewVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 2/1/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit

class WebViewVC : UIViewController, UIWebViewDelegate {
    
//    live stripeURl
//    2) https://tconnect.stripe.com/express/oauth/authorize?redirect_uri=https://needareader.com/api/public/api/getAuthorizationCode&client_id=ca_CCKvpnZXTZPUsELbGiOaFbTIyrg8OPYD&state={user_id}
    
    //Local url :- 1) test server 2) local url
    //1)  https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://needareader.com/test/need_a_reader_backend/api/public/api/getAuthorizationCode&client_id=ca_CCKvCCh0ahiuT3YyEYyrorKV6UIq0XwT&state=%7Buser_id%7D
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var webview: UIWebView!
    
    let mainURL = "https://connect.stripe.com/express/oauth/authorize?redirect_uri="//"https://connect.stripe.com/express/oauth/authorize?redirect_uri="//
    let redirectURL =  "https://needareader.com/api/public/api/getAuthorizationCode&client_id=" //"https://needareader.com/test/need_a_reader_backend/api/public/api/getAuthorizationCode&client_id="
    let client_id = "ca_CCKvpnZXTZPUsELbGiOaFbTIyrg8OPYD&state=" //"ca_CCKvCCh0ahiuT3YyEYyrorKV6UIq0XwT&state="
    let successUrl =  "https://needareader.com/api/resources/views/success.blade.php" //"https://needareader.com/test/need_a_reader_backend/api/resources/views/success.blade.php"
    let failUrl = "https://needareader.com/api/resources/views/failure.blade.php" // "https://needareader.com/test/need_a_reader_backend/api/resources/views/failure.blade.php"
    
    var userId : Int!
    var vcName : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webview.isHidden = true
        activityIndicator.isHidden = true
        configureNavigationBar()
        let userId = defaults.integer(forKey: "userId")
        if userId != nil {
            
            let redirectStr = mainURL + redirectURL + client_id + "\(userId)"
            print("redirectStr:- \(redirectStr)")
            if let url = URL(string: redirectStr) {
                //                if UIApplication.shared.canOpenURL(url) {
                //                    UIApplication.shared.openURL(url)
                //                }
                let myRequest = NSURLRequest(url: url)//URLRequest(url: url)
                //                pdfWebView.load(myRequest as URLRequest)
                webview.delegate = self
                print("webview url:- \(url)")
                webview.loadRequest(URLRequest(url: url))
                webview.isHidden = false
                activityIndicator.isHidden = false
                activityIndicator.startAnimating()
            } else {
                print("Invalid url")
            }
        }
        
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator.isHidden = false
//        activityIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        print("loadedurl1:- \(webView.request?.mainDocumentURL)")

        //        hudProggess(view: self.view, Show: false)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let currentURL = request.url?.absoluteString

        print("loadedurl needareader:- \(currentURL)")
        if let requrl = request.url {
            if requrl.absoluteString.contains(successUrl) { //|| requrl.absoluteString.contains(failUrl) {
                //                guard let url = URLComponents(string: requrl.absoluteString) else{return}
                //                url.queryItems?.first(where: { $0.name == "hdnemp" })?.value
                
                let alertview = UIAlertController(title: "Success", message: "Your stripe account has been created successfully.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                    let story = UIStoryboard(name: "Main", bundle: nil)
                    if self.vcName == "HireReaderVC" {
                        let nextvc = story.instantiateViewController(withIdentifier: "HireReaderVC") as! HireReaderVC
                        nextvc.isSuccesFromHireReader = true
                        self.navigationController?.popViewController(animated: true)
                    } else if self.vcName == "ViewJobDetailVC" {
                        let nextvc = story.instantiateViewController(withIdentifier: "ViewJobDetailVC") as! ViewJobDetailVC
                        nextvc.isUserRequestPayment = true
                        self.navigationController?.popViewController(animated: true)
                    } else {
//                        let nextvc = story.instantiateViewController(withIdentifier: "AddVideoVC") as! AddVideoVC
//                        nextvc.isStripeSetup = true
                        let nextvc = storyBoard.instantiateViewController(withIdentifier: "PremiumVC") as! PremiumVC
                        current_user_detail?.isStripeSetup = true
                        nextvc.isFromAddVideo = true
                        self.navigationController?.pushViewController(nextvc, animated: true)
                    }

                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                alertview.addAction(okAction)
                alertview.addAction(cancelAction)
                self.present(alertview, animated: true, completion: nil)
            } else  if requrl.absoluteString.contains(failUrl) {
                let alertview = UIAlertController(title: "Failure", message: "Need A Reader is unable to create your stripe account.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                    self.navigationController?.popViewController(animated: true)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                alertview.addAction(okAction)
                alertview.addAction(cancelAction)
                self.present(alertview, animated: true, completion: nil)
            }
        }
        return true
    }
    //Set NavigationBar
    func configureNavigationBar()
    {
        setNavigaion(vc: self)
        let navigationItem = self.navigationItem
        navigationItem.title = "CONNECT WITH STRIPE"
        
        self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        self.navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

