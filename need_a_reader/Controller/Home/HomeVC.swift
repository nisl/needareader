//
//  HomeVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/14/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func nsRange(from range: Range<Index>) -> NSRange {
        return NSRange(range, in: self)
    }
}

extension NSMutableAttributedString {
    
    public func setAsLink(value: String, textToFind:String) {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSAttributedStringKey.link, value: value, range: foundRange)
        }
    }
    
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont(name: "OpenSans-Bold", size: 14)!] //AvenirNext-Medium  Arial Bold
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont(name: "OpenSans", size: 14)!] //AvenirNext-Medium  Arial Bold

        let normal = NSMutableAttributedString(string:text, attributes: attrs)//NSAttributedString(string: text)
        append(normal)
        return self
    }
}

class HomeVC: UIViewController {
    
    @IBOutlet var becomeAReaderBtn: UIButton!
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var joinNowBtn: UIButton!
    @IBOutlet var txtView: UITextView!
    @IBOutlet var landingImgBg: UIImageView!
    
//    @IBOutlet var loginBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
//        self.view.backgroundColor = hexStringToUIColor(hex: redColor)
       setTextViewLinkText()
        if UIDevice.current.userInterfaceIdiom == .pad {
            loginBtn.frame.size.height = 70
            joinNowBtn.frame.size.height = 70
            becomeAReaderBtn.frame.size.height = 70
        }
        viewCorners.addBorderCornerTobutton(view: loginBtn, color: UIColor.white)
        viewCorners.addBorderCornerTobutton(view: joinNowBtn, color: UIColor.white)
        viewCorners.addBorderCornerTobutton(view: becomeAReaderBtn, color: UIColor.white)

//        joinNowBtn.backgroundColor = UIColor.white
//        joinNowBtn.setTitleColor(hexStringToUIColor(hex: redColor), for: .normal)
        changeButtonColor(view: [joinNowBtn, becomeAReaderBtn], backgroundcolor: "ffffff", fontColor: redColor)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true

    }
    
    //Set NavigationBar
    func configureNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont(name: "Arial", size: 18)!]
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        let navigationItem = self.navigationItem
        navigationItem.title = ""
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func findRange(str: String, searchTxt: String)->Int {
        var startPos : Int = 0
        var endPos : Int = 0
        // Search for one string in another.
        if let result = str.range(of: searchTxt) {
            startPos = str.distance(from: str.startIndex, to: result.lowerBound)
//            endPos = str.distance(from: str.startIndex, to: result.upperBound)
            print(startPos, endPos) // 3 7
           
        }
         return startPos
    }
    
    func setTextViewLinkText() {
        // Do any additional setup after loading the view, typically from a nib.
        let text = "By signing up you agree to our TERMS OF USE and PRIVACY POLICY."
//        let attributedString = NSMutableAttributedString(string: text, attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 14.0)])
        let attributedString = NSMutableAttributedString(string: text, attributes: [NSAttributedStringKey.font:UIFont(name: "OpenSans-Italic", size: 14.0)])
        //        "OpenSansLight-Italic"
        let termsStartPosition = findRange(str: text, searchTxt: "TERMS OF USE")
        //        let (privacyStartPosition,privacyLength) = findRange(str: text, searchTxt: "PRIVACY POLICY")
        let privacyStartPosition = findRange(str: text, searchTxt: "PRIVACY POLICY")
//        let termsLength = "TERMS OF USE".count
//        let privacyLength = "PRIVACY POLICY".count
        print("termsStartPosition:- \(termsStartPosition)")
        print("privacyStartPosition:- \(privacyStartPosition)")
        
        let boldFontAttribute = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 16.0)]
        attributedString.addAttributes(boldFontAttribute, range: text.nsRange(from: text.range(of: "TERMS OF USE")!))
        attributedString.addAttributes(boldFontAttribute, range: text.nsRange(from: text.range(of: "PRIVACY POLICY")!))
        attributedString.setAsLink(value:privacyPolicyURL, textToFind: "PRIVACY POLICY")
        attributedString.setAsLink(value:termsOfUseURL, textToFind: "TERMS OF USE")
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: NSRange.init(location: 0, length: text.count))
//        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: NSRange.init(location: privacyStartPosition, length: privacyLength))
        self.txtView.attributedText = attributedString
        self.txtView.textAlignment = .center
        self.txtView.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.white]
    }
    
    @IBAction func loginBtnClicked(_ sender: Any) {
//        loginBtn.backgroundColor = UIColor.white
//        loginBtn.setTitleColor(hexStringToUIColor(hex: redColor), for: .normal)
//        joinNowBtn.backgroundColor = UIColor.clear
//        joinNowBtn.setTitleColor(UIColor.white, for: .normal)
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func joinNowBtnClicked(_ sender: Any) {
//        joinNowBtn.backgroundColor = UIColor.white
//        joinNowBtn.setTitleColor(hexStringToUIColor(hex: redColor), for: .normal)
//        loginBtn.backgroundColor = UIColor.clear
//        loginBtn.setTitleColor(UIColor.white, for: .normal)
        let vc = storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
