//
//  MyTabBarViewController.swift
//  Gredient
//
//  Created by ob_apple_3 on 27/01/18.
//  Copyright © 2018 ob_apple_3. All rights reserved.
//

import UIKit


class MyTabBarViewController: UITabBarController , UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.delegate = self
        self.tabBar.barTintColor = UIColor.white
        self.tabBarController?.tabBar.isHidden = true
//        self.tabBar.unselectedItemTintColor = UIColor.white
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let index = tabBar.items?.index(of: item)
        switch index {
        case 0?:
            item.selectedImage = UIImage(named: "ic_tabSelectedCard")
            break
        case 1?:
            item.selectedImage = UIImage(named: "ic_selectedJobs")
            break
        case 2?:
            item.selectedImage = UIImage(named: "ic_selectedNotifications")
            break
        case 3?:
            item.selectedImage = UIImage(named: "ic_chat")
            break
        case 4?:
            item.selectedImage = UIImage(named: "ic_selectedfndTab")
        default:
            item.selectedImage = UIImage(named: "ic_setting")
            break
        }
    }
}
