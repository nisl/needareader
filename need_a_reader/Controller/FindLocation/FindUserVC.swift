//
//  FindUserVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/18/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import Pulsator
import Alamofire
import SCLAlertView
import AlamofireObjectMapper
import ObjectMapper
import ReachabilitySwift
import SDWebImage
import Koloda
import UserNotifications
import WebKit
import SwiftyStarRatingView
import SwiftyStoreKit
import StoreKit
import KDCircularProgress
import ChameleonFramework
import AVFoundation
import AVKit
import EasyTipView


class FindUserVC: UIViewController, CLLocationManagerDelegate, KolodaViewDelegate, KolodaViewDataSource,  UIGestureRecognizerDelegate, UIPopoverPresentationControllerDelegate, WKUIDelegate, UIWebViewDelegate, QBRTCClientDelegate, QBCoreDelegate, InComingCallDelegate, PKPushRegistryDelegate, EasyTipViewDelegate,setReaderlist ,moveNextview{
    
    
    @IBOutlet var waitingTimerView: UIView!
    
    @IBOutlet weak var selectedCollectionView: UICollectionView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var selctedReaderView: UIView!
   // @IBOutlet var collectionView: UICollectionView!
    @IBOutlet weak var kolodaviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var selectedReaderViewHeightConstarint: NSLayoutConstraint!
    @IBOutlet weak var collectionviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var kolodaviewBottoamConstraint: NSLayoutConstraint!
    @IBOutlet weak var resumeHintView: UIView!
    
    @IBOutlet var settingsBtn: UIButton!
    @IBOutlet var customProgressView: KDCircularProgress!
    //  @IBOutlet var customProgressView: UICircularProgressRingView!
    //    @IBOutlet var customProgressView: TimeFractionProgressView!
    @IBOutlet var btn2: UIButton!
    @IBOutlet var btn1: UIButton!
    @IBOutlet var buttonBackView: UIView!
    @IBOutlet var playPauseImg: UIImageView!
    
    @IBOutlet var gradientImg: UIImageView!
    @IBOutlet var durationLbl: UILabel!
    
    
    @IBOutlet var headerTitle: UILabel!
    @IBOutlet var headerView: UIView!
    @IBOutlet var webview: UIWebView!
    @IBOutlet var pdfsubview: UIView!
    @IBOutlet var pdfView: UIView!
    
    @IBOutlet var skillSetTitle: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var comingSoonView: UIView!
    @IBOutlet var hintView: UIView!
    //    @IBOutlet var pdfWebView: WKWebView!
    
    @IBOutlet var monthlyPurchaseBtn: UIButton!
    @IBOutlet var yearlyPurchaseBtn: UIButton!
    @IBOutlet var dailyPurchaseBtn: UIButton!
    @IBOutlet var paymentSubview: UIView!
    
    @IBOutlet var ratingSubview: UIView!
    @IBOutlet var starRatingView: SwiftyStarRatingView!
    @IBOutlet var headerGetScoreLbl: UILabel!
    @IBOutlet var headerOutOfScoreLbl: UILabel!
    @IBOutlet var ratingHeaderLbl: UILabel!
    @IBOutlet var tableview: UITableView!
    @IBOutlet var avgRatingView: UIView!
    
    @IBOutlet var readerOnTotalVotes: UILabel!
    @IBOutlet var readerOnTImeDislikePer: UILabel!
    @IBOutlet var readerOnTImeLikePer: UILabel!
    @IBOutlet var readerontimeQuestionLbl: UILabel!
    @IBOutlet var readerOntimeView: UIView!
    
    @IBOutlet var TotalVotes2: UILabel!
    @IBOutlet var dislikePer2: UILabel!
    @IBOutlet var likePer2: UILabel!
    @IBOutlet var questionLbl2: UILabel!
    @IBOutlet var questionView2: UIView!
    
    @IBOutlet var TotalVotes3: UILabel!
    @IBOutlet var dislikePer3: UILabel!
    @IBOutlet var likePer3: UILabel!
    @IBOutlet var questionLbl3: UILabel!
    @IBOutlet var questionView3: UIView!
    
    @IBOutlet var TotalVotes4: UILabel!
    @IBOutlet var dislikePer4: UILabel!
    @IBOutlet var likePer4: UILabel!
    @IBOutlet var questionLbl4: UILabel!
    @IBOutlet var questionView4: UIView!

    @IBOutlet var showSkillView: UIView!
    @IBOutlet var showServicesView: UIView!
    @IBOutlet var showServiceOuterView: UIView!
    @IBOutlet var serviceTableView: UITableView!
    
    @IBOutlet var showServiceTitleLbl: UILabel!
    //    @IBOutlet var profileImg: UIImageView!
    @IBOutlet var profileNameWithAge: UILabel!
    @IBOutlet var readerListView: UIView!
    @IBOutlet var kolodaView: KolodaView!
    @IBOutlet var changeLocationBtn: UIButton!
    @IBOutlet var locationMsg: UILabel!
    @IBOutlet var animatedView: UIView!
    @IBOutlet var launcherImg: UIImageView!
    @IBOutlet var searchNearByUserView: UIView!
    @IBOutlet var locationOffErrorView: UIView!
    
    //ShowSkillView
    @IBOutlet var sitcomview: UIView!
    @IBOutlet var hourDramaview: UIView!
    @IBOutlet var playsview: UIView!
    @IBOutlet var musicalsview: UIView!
    @IBOutlet var featureFilmsview: UIView!
    
    @IBOutlet var dramedyView: UIView!
    @IBOutlet var singleCameraview: UIView!
    @IBOutlet var coStarview: UIView!
    @IBOutlet var commercialview: UIView!
    @IBOutlet var shakespeareview: UIView!
    @IBOutlet var monologuesview: UIView!
    @IBOutlet var accentsView: UIView!
    
    @IBOutlet var sitcomValueLbl: UILabel!
    @IBOutlet var hourDramaValueLbl: UILabel!
    @IBOutlet var playsValueLbl: UILabel!
    @IBOutlet var musicalsValueLbl: UILabel!
    @IBOutlet var featureFilmsValueLbl: UILabel!
    
    @IBOutlet var dramedyValueLbl: UILabel!
    @IBOutlet var singleCameraValueLbl: UILabel!
    @IBOutlet var coStarValueLbl: UILabel!
    @IBOutlet var commercialValueLbl: UILabel!
    @IBOutlet var shakespeareValueLbl: UILabel!
    @IBOutlet var monologuesValueLbl: UILabel!
    @IBOutlet var accentsValueLbl: UILabel!
    
    @IBOutlet var dramedySlider: UISlider!
    @IBOutlet var sitcomSlider: UISlider!
    @IBOutlet var hourDramaSlider: UISlider!
    @IBOutlet var playsValueSlider: UISlider!
    @IBOutlet var musicalsSlider: UISlider!
    @IBOutlet var featureFilmsSlider: UISlider!
    
    @IBOutlet var singleCameraSlider: UISlider!
    @IBOutlet var coStarSlider: UISlider!
    @IBOutlet var commercialSlider: UISlider!
    @IBOutlet var shakespeareSlider: UISlider!
    @IBOutlet var monologuesSlider: UISlider!
    @IBOutlet var accentsSlider: UISlider!
    
    @IBOutlet var headerViewHeightConstant: NSLayoutConstraint!
    @IBOutlet var locationErrorDialog: UIView!
    
    //AVPlayer
    var videoPlayerVC = AVPlayerViewController()
    var videoPlayer : AVPlayer?

    //TimerView
    
    var startProgress = false
    var diff = 0.0
    var stopAngle = 0.0
    var currentCount = 0.0
    //    var is_waiting : Bool = false
    //TIme countDown vars
//    var swiftTimer = Timer()
//    var swiftCounter =  Double(waitingDuration * 60)//600.0
    //
    var locationManager : CLLocationManager!
    var current_location: CLLocation!
    var skillData : [AddSkillsRequestModel] = []
    var ratingData : [RatingResponseModel] = []
    var readerArr : [User_detail] = []
    var selectedreaderArr : [User_detail] = []
    var readerArrTemp : [User_detail] = []
    var pdfWebView: WKWebView!
    
    let pulsator = Pulsator()
    let kMaxDuration: TimeInterval = 10
    var kMaxRadius: CGFloat = screenWidth / 1//.25
    let settingBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
    let chatBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
    var tapCard = 0
    var questionArr : [String] = []
    var timer = Timer()
    var timerSeconds : TimeInterval?
    
    var seconds = 3
    var hintviewDisplay : Bool = false
    var navColor : UIColor!
    var location_coordinate : CLLocationCoordinate2D!
    var orderInfo : Order_info?
    var tempRestoreArr = [Purchase]()
    //    var dialog: QBChatDialog?
    var dataSource: UsersDataSource!
    var session: QBRTCSession!
    var callUUID: UUID!
    var backgroundTask: UIBackgroundTaskIdentifier!
    var opponentIDs: [NSNumber] = []
    var voipRegistry: PKPushRegistry!
    var updateUI : Bool = false
    var serviceNameArr : [ServiceDetail] = []

    weak var tipView: EasyTipView?

    @IBOutlet var backgroundLogoView: UIView!
    //    var is_running: Bool = false
    //    var is_next_page: Bool = true
    
    let layout = UICollectionViewFlowLayout()
    
    required init?(coder aDecoder: NSCoder) {
        self.pdfWebView = WKWebView(frame: CGRect.zero)
        CLLocationManager().requestWhenInUseAuthorization()
        super.init(coder: aDecoder)!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        serviceTableView.delegate = self
        serviceTableView.dataSource = self
        
        //Quickblox
        waitingTimerView.isHidden = true
        self.backgroundTask = UIBackgroundTaskInvalid
        QBRTCClient.instance().add(self)
        //        QBCore.instance().add(self)
        
        self.voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
        self.voipRegistry.delegate = self
        self.voipRegistry.desiredPushTypes = [.voIP]
        
        //Location
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        //        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startMonitoringSignificantLocationChanges()
        configureNavigationBar()
        readerListView.frame.size.width = self.view.frame.size.width
        searchNearByUserView.frame.size.width = self.view.frame.size.width
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                print("iPhone X")
                if #available(iOS 11, *) {
                    //                    headerView.translatesAutoresizingMaskIntoConstraints = false
                    headerViewHeightConstant.constant = 88
                    let guide = view.safeAreaLayoutGuide
                    let height = (self.tabBarController?.tabBar.frame.size.height)! + 88
                    readerListView.frame.size.height = self.view.frame.size.height - height
                    searchNearByUserView.frame.size.height = self.view.frame.size.height - (self.tabBarController?.tabBar.frame.size.height)!
                    waitingTimerView.frame.size.height = self.view.frame.size.height - height
                    //                    NSLayoutConstraint.activate([
                    //                        headerView.topAnchor.constraint(equalTo: guide.topAnchor),//, constant: 40).isActive = true
                    //                        headerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
                    //                        headerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
                    //                        headerView.heightAnchor.constraint(equalToConstant: 64)
                    //                        ])
                }
                break
            default:
                let height = (self.tabBarController?.tabBar.frame.size.height)! + 64
                readerListView.frame.size.height = self.view.frame.size.height - height
                searchNearByUserView.frame.size.height = (self.tabBarController?.tabBar.frame.size.height)!//self.view.frame.size.height - height
                waitingTimerView.frame.size.height = self.view.frame.size.height - height
                break
            }
        }else if UIDevice.current.userInterfaceIdiom == .pad {
            kMaxRadius = screenWidth / 2
            let height = (self.tabBarController?.tabBar.frame.size.height)! + 64
            
            readerListView.frame.size.height = self.view.frame.size.height - height
            searchNearByUserView.frame.size.height = (self.tabBarController?.tabBar.frame.size.height)!//self.view.frame.size.height - height
            waitingTimerView.frame.size.height = self.view.frame.size.height - height
            showSkillView.frame.size.height = self.view.frame.size.height * 0.92
            pdfsubview.frame.size.height = self.view.frame.size.height * 0.92
            ratingSubview.frame.size.height = self.view.frame.size.height * 0.92
//            dailyPurchaseBtn.frame.size.height = 60
//            monthlyPurchaseBtn.frame.size.height = 60
//            yearlyPurchaseBtn.frame.size.height = 60
        }
        
        self.readerListView.isHidden = true
        questionArr = ["Was the reader on time?","Was the reader knowledgeable and skilled in this genre?","Would you work with this reader again?"]
        viewCorners.addShadowToView(view: showSkillView)
        viewCorners.addCornerToView(view: showSkillView, value: 10)
        viewCorners.addShadowToView(view: pdfsubview)
        viewCorners.addCornerToView(view: pdfsubview, value: 10)
        viewCorners.addShadowToView(view: ratingSubview)
        viewCorners.addCornerToView(view: ratingSubview, value: 10)
        viewCorners.addCornerToView(view: avgRatingView, value: 5)
        viewCorners.addShadowToView(view: avgRatingView)
        viewCorners.addCornerToView(view: locationErrorDialog, value: 5)
        viewCorners.addCornerToView(view: readerOntimeView, value: 5)
        viewCorners.addShadowToView(view: readerOntimeView)
        viewCorners.addCornerToView(view: questionView2, value: 5)
        viewCorners.addShadowToView(view: questionView2)
        viewCorners.addCornerToView(view: questionView3, value: 5)
        viewCorners.addShadowToView(view: questionView3)
        viewCorners.addCornerToView(view: questionView4, value: 5)
        viewCorners.addShadowToView(view: questionView4)

        viewCorners.addShadowToView(view: showServicesView)
        viewCorners.addCornerToView(view: showServicesView, value: 10)
        viewCorners.addShadowToView(view: showServiceOuterView)
        viewCorners.addCornerToView(view: showServiceOuterView, value: 5)
        
        //        viewCorners.addCornerToView(view: dailyPurchaseBtn)
        //        viewCorners.addCornerToView(view: monthlyPurchaseBtn)
        //        viewCorners.addCornerToView(view: yearlyPurchaseBtn)
        //        viewCorners.addShadowToView(view: paymentSubview)
        //        viewCorners.addCornerToView(view: paymentSubview, value: 10)
        
        showSkillView.isHidden = true
        pdfsubview.isHidden = true
        ratingSubview.isHidden = true
        showServicesView.isHidden = true
        //        paymentSubview.isHidden = true
        //        viewCorners.addCornerToView(view: kolodaView, value: 10)
        //        viewCorners.addBorderCornerTobutton(view: changeLocationBtn, color: UIColor.white)
        if UIDevice.current.userInterfaceIdiom == .pad {
            changeLocationBtn.frame.size.height = 70
        }
        viewCorners.addCornerToView(view: changeLocationBtn)
        addanimation()
        
        if !CLLocationManager.locationServicesEnabled() {
            locationOffErrorView.isHidden = false
            self.settingsBtn.setImage(UIImage(named: "ic_whiteSetting"), for: .normal)

            //            self.tabBarController?.tabBar.isHidden = true
            headerTitle.isHidden = true
            headerView.backgroundColor = hexStringToUIColor(hex: "8F8F8F")
            self.view.backgroundColor = hexStringToUIColor(hex: "8F8F8F")
            //            self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: "8F8F8F")
            //            self.navigationItem.leftBarButtonItems = []
            //            self.navigationItem.rightBarButtonItems = []
            //            navigationItem.title = ""
            //            self.navigationItem.setHidesBackButton(true, animated: true)
            searchNearByUserView.isHidden = true
        }else {
            //            switch CLLocationManager.authorizationStatus() {
            //            case .notDetermined, .restricted, .denied:
            //                print("No access")
            //                self.navigationController?.navigationBar.isHidden = false
            //                locationOffErrorView.isHidden = false
            //                self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: "8F8F8F")
            //                self.navigationItem.leftBarButtonItems = []
            //                self.navigationItem.rightBarButtonItems = []
            //                navigationItem.title = ""
            //                self.navigationItem.setHidesBackButton(true, animated: true)
            //                searchNearByUserView.isHidden = true
            //
            //            case .authorizedAlways, .authorizedWhenInUse:
            //                print("Access")
            //                self.navigationController?.navigationBar.isHidden = false
            //                locationOffErrorView.isHidden = true
            //                searchNearByUserView.isHidden = false
            //                current_location = locationManager.location
            //
            ////                findNearByUser(latitude: Double(current_location.coordinate.latitude), longitude: Double(current_location.coordinate.longitude))
            //                self.callSearchNearApi()
            //            }
        }
        let viewArr : [UIView] = [sitcomview, hourDramaview, playsview, musicalsview, featureFilmsview, singleCameraview, coStarview, commercialview, shakespeareview, monologuesview, dramedyView, accentsView]
        setUI(arr: viewArr)
        let sliderArr : [UISlider] = [sitcomSlider, hourDramaSlider, playsValueSlider, musicalsSlider, featureFilmsSlider, singleCameraSlider,coStarSlider,shakespeareSlider,commercialSlider, monologuesSlider, dramedySlider, accentsSlider]
        setSliderEnabled(slider: sliderArr)
        
        changeSliderColor(view: sliderArr, minimumTrackcolor: redColor, thumbColor: redColor)
        let lblArr : [UILabel] = [featureFilmsValueLbl, hourDramaValueLbl, commercialValueLbl, sitcomValueLbl, singleCameraValueLbl, coStarValueLbl, playsValueLbl, musicalsValueLbl, monologuesValueLbl, shakespeareValueLbl, dramedyValueLbl, accentsValueLbl]
        changeLabelColor(view: lblArr, backcolor: redColor, fontColor: "ffffff")
        
        kolodaView.delegate = self
        kolodaView.dataSource = self
        pdfWebView.uiDelegate = self
        pdfWebView.frame = CGRect(x: 0, y: 65, width: self.pdfsubview.frame.size.width, height: self.pdfsubview.frame.size.height - 50)
        kolodaView.countOfVisibleCards = 4
        //        pdfsubview.addSubview(pdfWebView)
        pdfWebView.sizeToFit()
        webview.delegate = self
        setGradient(customView: readerListView)
        readerListView.addSubview(kolodaView)
        readerListView.addSubview(hintView)
        readerListView.addSubview(selctedReaderView)
        viewCorners.addShadowToView(view: selctedReaderView)
        viewCorners.addCornerToView(view: selctedReaderView, value: 5)
        viewCorners.addCornerToView(view: nextBtn, value: 15)
        
        animatedView.frame.size.height =  self.view.frame.size.height * 0.52
        animatedView.frame.size.width =  animatedView.frame.size.height
        waitingTimerView.frame.size.width = self.view.frame.size.width
        
        btn1.frame.size.width = waitingTimerView.frame.size.width * 0.88
        btn1.frame.size.height = btn1.frame.size.width
//        customProgressView.frame.size.width = waitingTimerView.frame.size.width * 0.75//btn1.frame.size.width - 70
//        customProgressView.frame.size.height =  customProgressView.frame.size.width
//        customProgressView.progressInsideFillColor =  UIColor(gradientStyle:UIGradientStyle.topToBottom, withFrame:customProgressView.frame, andColors:[hexStringToUIColor(hex: redColor), hexStringToUIColor(hex: yellowcolor)])
//        customProgressView.glowMode = .noGlow
//        buttonBackView.frame.size.width = customProgressView.frame.size.width - 40//customProgressView.frame.size.width - 20
//        buttonBackView.frame.size.height =  buttonBackView.frame.size.width
        //        setTimerView()
        

        selectedCollectionView.collectionViewLayout = layout
        layout.scrollDirection = .horizontal
        selectedCollectionView.delegate = self
        selectedCollectionView.dataSource = self
        selectedCollectionView.showsHorizontalScrollIndicator = false
        selectedCollectionView.showsVerticalScrollIndicator = false
        selectedCollectionView.isPagingEnabled = true
         self.selectedCollectionView.collectionViewLayout.invalidateLayout()

    }
    
    func showSelectedReaderView() {
        if UserDefaults.standard.value(forKey: "readerSelected") != nil {
            self.selectedreaderArr.removeAll()
            if let v = UserDefaults.standard.array(forKey: "readerSelected") {
                print(v.count)
                for item in v {
                    let value = Mapper<User_detail>().map(JSON: item as! [String : Any])
                    self.selectedreaderArr.append(value!)
                }
            }
            print(self.selectedreaderArr.count)
            if selectedreaderArr.count > 0 {
                //self.selectedReaderViewHeightConstarint.constant = 60
                //self.kolodaviewBottoamConstraint.constant = 80
                selctedReaderView.isHidden = false

                self.selectedCollectionView.collectionViewLayout.invalidateLayout()
                self.selectedCollectionView.reloadData()
  
            }else {
                self.selectedCollectionView.collectionViewLayout.invalidateLayout()
                self.selectedCollectionView.reloadData()
               // self.kolodaviewBottoamConstraint.constant = 80
                selctedReaderView.isHidden = true

            }
        
        }else {
            self.selectedreaderArr.removeAll()
           // self.selectedReaderViewHeightConstarint.constant = 60
           // self.kolodaviewBottoamConstraint.constant = 80
            selctedReaderView.isHidden = true
          //  self.kolodaView.reloadData()

        }
    }
   
    override func viewWillAppear(_ animated: Bool) {
      //  self.view.layoutSubviews()
        self.view.layer.layoutIfNeeded()
        //self.view.layer.setNeedsLayout()
        
        self.navigationController?.navigationBar.isHidden = true
        //        self.dialog = nil
        appDelegate.dataSource = UsersDataSource(currentUser: QBCore.instance().currentUser)
        CallKitManager.instance.usersDatasource = appDelegate.dataSource
        appDelegate.getOpponentsUsers()
        starRatingView.isEnabled = false
        if !pulsator.isPulsating {
            addanimation()
        }
        if isSettingChanged {
            callSearchNearApi()
            isSettingChanged = false
        }
       // callSearchNearApi()
        backgroundLogoView.frame.size.height = animatedView.frame.size.height * 0.48
        backgroundLogoView.frame.size.width =  backgroundLogoView.frame.size.height
        //setGradient(customView: backgroundLogoView)
        
        
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
//        kolodaView.reloadInputViews()
//        if !self.readerListView.isHidden {
//            kolodaView.resetCurrentCardIndex()
//            self.kolodaView.reloadData()
//        }
        
        self.showSelectedReaderView()
    
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        btn1.layer.removeAllAnimations()
//        customProgressView.stopAnimation()
//        swiftTimer.invalidate()
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layer.layoutIfNeeded()
        backgroundLogoView.frame.size.height = self.animatedView.frame.size.height * 0.48
        backgroundLogoView.frame.size.width = backgroundLogoView.frame.size.height
        
        backgroundLogoView.layer.cornerRadius = backgroundLogoView.frame.size.width / 2
        backgroundLogoView.clipsToBounds = true
        pulsator.position = launcherImg.layer.position
        setGradient(customView: backgroundLogoView)
       
    
        
        let cardMaxY = hintView.frame.maxY
        let midY = cardMaxY - (cardMaxY * 0.3)
        let midX = hintView.frame.size.width / 2
        print(" hintView.frame.midX:- \(hintView.frame.midX)---- midX:- \(midX)")
        drawLine(scaleX: midX, scaleY: 0, width: 1, height: midY, lineType: "vertical")
        drawLine(scaleX: 0, scaleY: 0, width: hintView.frame.size.width, height: 1, lineType: "horizontal")
        if UIDevice.current.userInterfaceIdiom == .pad {
            showSkillView.frame.size.height = self.view.frame.size.height * 0.88
            pdfsubview.frame.size.height = self.view.frame.size.height * 0.88
            ratingSubview.frame.size.height = self.view.frame.size.height * 0.88
            showServicesView.frame.size.height = self.view.frame.size.height * 0.88

        }
        //collectionView.collectionViewLayout.invalidateLayout()
        layout.invalidateLayout()
    }
    

    
    @objc func appMovedToForeground() {
        if !CLLocationManager.locationServicesEnabled() {
            locationOffErrorView.isHidden = false
            self.settingsBtn.setImage(UIImage(named: "ic_whiteSetting"), for: .normal)

            headerTitle.isHidden = true
            headerView.backgroundColor = hexStringToUIColor(hex: "8F8F8F")
            self.view.backgroundColor = hexStringToUIColor(hex: "8F8F8F")
            searchNearByUserView.isHidden = true
        }else {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                locationOffErrorView.isHidden = false
                self.settingsBtn.setImage(UIImage(named: "ic_whiteSetting"), for: .normal)

                headerTitle.isHidden = true
                headerView.backgroundColor = hexStringToUIColor(hex: "8F8F8F")
                self.view.backgroundColor = hexStringToUIColor(hex: "8F8F8F")
                searchNearByUserView.isHidden = true
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationOffErrorView.isHidden = true
                if readerListView.isHidden {
                    searchNearByUserView.isHidden = false
                    self.settingsBtn.setImage(UIImage(named: "ic_setting"), for: .normal)
                    headerView.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
                    self.view.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
                }
                current_location = locationManager.location
            }
        }
    }
    
    func addanimation() {
        backgroundLogoView.layer.superlayer?.insertSublayer(pulsator, below: backgroundLogoView.layer)
        pulsator.numPulse = 4
        pulsator.radius = CGFloat(1.0) * kMaxRadius
        pulsator.animationDuration = Double(0.5) * kMaxDuration
        pulsator.backgroundColor = hexStringToUIColor(hex: redColor).cgColor
        pulsator.start()
    }
    
    @IBAction func openSettingBtnClicked(_ sender: Any) {
        let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
        if let url = settingsUrl {
            UIApplication.shared.openURL(url)
        }
    }
    
    //Set NavigationBar
    func configureNavigationBar()   {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "OpenSans-Semibold", size: 18.0)]
        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        
        let navigationItem = self.navigationItem
        navigationItem.title = ""
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        if #available(iOS 11.0, *) {
            let widthConstraint = settingBtn.widthAnchor.constraint(equalToConstant: 44)
            let heightConstraint = settingBtn.heightAnchor.constraint(equalToConstant: 44)
            heightConstraint.isActive = true
            widthConstraint.isActive = true
            settingBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 8, 16)
            let chatwidthConstraint = chatBtn.widthAnchor.constraint(equalToConstant: 44)
            let chatheightConstraint = chatBtn.heightAnchor.constraint(equalToConstant: 44)
            chatheightConstraint.isActive = true
            chatwidthConstraint.isActive = true
            chatBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 0)//UIEdgeInsetsMake(8, 0, 8, 16)
        }else {
            chatBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 0)//UIEdgeInsetsMake(8, 0, 6, 12)
            settingBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 8, 16)
        }
        settingBtn.setImage(UIImage(named: "ic_logoutRed"), for: .normal)
        chatBtn.setImage(UIImage(named: "ic_chat"), for: .normal)
        
        let settingButton = UIBarButtonItem(customView: settingBtn)
        let chatButton = UIBarButtonItem(customView: chatBtn)
        
    }
    
    @IBAction func nextBtnClick(_ sender: Any) {
        print("next click")
        
        defaults.set(self.selectedreaderArr.toJSON(), forKey: "readerSelected")
        var tempArr = [User_detail]()
        
         if let v = UserDefaults.standard.array(forKey: "readerSelected") {
            print(v)
            for item in v {
                let value = Mapper<User_detail>().map(JSON: item as! [String : Any])
                tempArr.append(value!)
            }
        }
        print(tempArr.toJSON())
        self.goToRider(user_detail: tempArr)

    }
    @IBAction func settingPressed(_ sender: Any) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        self.navColor = (navigationController?.navigationBar.barTintColor)!
        vc.current_location = current_location
        if location_coordinate != nil {
            vc.location_coordinate = location_coordinate
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func notificationBtnPressed(_ sender: Any) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nav.navigationBar.shadowImage = UIImage()
        nav.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func jobsBtnPressed(_ sender: Any) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "JobsVC") as! JobsVC
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nav.navigationBar.shadowImage = UIImage()
        nav.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func chatbtnPressed(_ sender: Any) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "DialogsViewController") as! DialogsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func locationNotFoundRetryClicked(_ sender: Any) {
        callSearchNearApi()
    }
    
    func callSearchNearApi() {
        if (Reachability()?.isReachable)! {
            if location_coordinate != nil {
                searchNearByUserView.isHidden = false
                self.settingsBtn.setImage(UIImage(named: "ic_setting"), for: .normal)
                headerTitle.isHidden = true
                headerView.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
                readerListView.isHidden = true
                self.changeLocationBtn.isHidden = true
                findNearByUser(latitude: location_coordinate.latitude, longitude: location_coordinate.longitude)
            }else if let data =  defaults.value(forKey: "locationCoordinatesArr") as?  [String:CLLocationDegrees] {
                searchNearByUserView.isHidden = false
                self.settingsBtn.setImage(UIImage(named: "ic_setting"), for: .normal)
                headerTitle.isHidden = true
                readerListView.isHidden = true
                self.changeLocationBtn.isHidden = true
                headerView.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
                findNearByUser(latitude: data["latitude"]!, longitude: data["longtitude"]!)
            }else if current_location != nil {
                searchNearByUserView.isHidden = false
                self.settingsBtn.setImage(UIImage(named: "ic_setting"), for: .normal)

                headerTitle.isHidden = true
                readerListView.isHidden = true
                self.changeLocationBtn.isHidden = true
                headerView.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
                
                let latitude = current_location.coordinate.latitude
                let longtitude = current_location.coordinate.longitude
                if latitude != nil  && longtitude != nil {
                    findNearByUser(latitude: latitude, longitude: longtitude)
                }
            } else {
                //findNearByUser(latitude: 21.1702, longitude: 72.8311)
                self.changeLocationBtn.isUserInteractionEnabled = true
                self.changeLocationBtn.setTitle("CHANGE LOCATION", for: .normal)
            }
        }else{
            if !self.loadFromActorCatCache() {
                noInternetMsgDialog()
            }
        }
    }
    
    @IBAction func changeLocationBtnClicked(_ sender: Any) {
        if changeLocationBtn.title(for: .normal) == "RETRY" {
            changeLocationBtn.isUserInteractionEnabled = false
            callSearchNearApi()
        }else {
            let vc = storyboard?.instantiateViewController(withIdentifier: "ChangeLocationVC") as! ChangeLocationVC
            if current_location != nil {
                vc.current_location = current_location
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func loadFromActorCatCache() -> Bool {
        if let v = UserDefaults.standard.dictionary(forKey: searchNearByReader) {
            //            print("v: \(v)")
            let value = Mapper<FindingActorResponseModel>().map(JSON: v)
            if let readerData = value?.reader_list {
                if !readerData.isEmpty {
                    self.readerArr = readerData
                    self.readerArrTemp = self.readerArr
                    self.searchNearByUserView.isHidden = true
                    self.locationOffErrorView.isHidden = true
                    self.readerListView.isHidden = false
                    self.settingsBtn.setImage(UIImage(named: "ic_whiteSetting"), for: .normal)

                    let firstSwipe = defaults.bool(forKey: "isDisplayedHint")
                    if !firstSwipe{
                        defaults.set(true, forKey: "isDisplayedHint")
                        self.startTimer()
                    }else {
                        hintView.isHidden = true
                    }
                    headerTitle.isHidden = false
                    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont(name: "Arial", size: 18)!]
                    
                    headerView.backgroundColor = hexStringToUIColor(hex: redColor)
                    self.view.backgroundColor = hexStringToUIColor(hex: redColor)
                    var skillList : [AddSkillsRequestModel] = []
                    var ratingList : [RatingResponseModel] = []
                    
                    for i in 0..<self.readerArr.count {
                        if let skill_list = self.readerArr[i].skill_detail {
                            skillList.append(skill_list)
                        }
                        if let rating_list = self.readerArr[i].rating_detail {
                            ratingList.append(rating_list)
                        }
                    }
                    skillData = skillList
                    ratingData = ratingList
                    kolodaView.resetCurrentCardIndex()
                    self.kolodaView.reloadData()
                }else {
                    self.locationMsg.text = "No actors near your location"
                    self.changeLocationBtn.isHidden = false
                    changeLocationBtn.isUserInteractionEnabled = true
                    self.locationOffErrorView.isHidden = true
                    //                        self.tabBarController?.tabBar.isHidden = false
                    searchNearByUserView.isHidden = false
                    self.settingsBtn.setImage(UIImage(named: "ic_setting"), for: .normal)

                    self.changeLocationBtn.setTitle("CHANGE LOCATION", for: .normal)
                    
                    self.readerListView.isHidden = true
                    headerView.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
                    self.view.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
                    headerTitle.isHidden = true
                }
            }else {
                self.locationMsg.text = "No actors near your location"
                self.changeLocationBtn.isHidden = false
                changeLocationBtn.isUserInteractionEnabled = true
                searchNearByUserView.isHidden = false
                self.settingsBtn.setImage(UIImage(named: "ic_setting"), for: .normal)

                self.changeLocationBtn.setTitle("CHANGE LOCATION", for: .normal)
                self.locationOffErrorView.isHidden = true
                self.readerListView.isHidden = true
                headerView.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
                self.view.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
                headerTitle.isHidden = true
            }
        } else {
            self.locationMsg.text = "No actors near your location"
            self.changeLocationBtn.isHidden = false
            changeLocationBtn.isUserInteractionEnabled = true
            self.locationOffErrorView.isHidden = true
            self.readerListView.isHidden = true
            self.changeLocationBtn.setTitle("CHANGE LOCATION", for: .normal)
            changeLocationBtn.isUserInteractionEnabled = true
            headerView.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
            self.view.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
            headerTitle.isHidden = true
        }
        
        if !self.readerArr.isEmpty {
            return true
        }else {
            return false
        }
    }
    
    func findNearByUser(latitude: Double, longitude: Double) {
        //        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        let obj = FindingActorRequestModel(latitude: latitude, longitude: longitude)
        
        let param = Mapper().toJSON(obj)
        print(param)
        print("tokenHeader:- \(tokenHeader)")
        alamofireManager.request(searchNearByReader, method: .post, parameters: param, encoding: JSONEncoding.default, headers: tokenHeader).responseObject { (response: DataResponse<ResponseModel> )in
            
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        print("success")
                        UserDefaults.standard.set(value.findingActorResponse?.toJSON(), forKey: searchNearByReader)
                        self.loadFromActorCatCache()
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.findNearByUser(latitude: latitude, longitude: longitude)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if !self.loadFromActorCatCache() {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                alert.hideView()
                if !self.loadFromActorCatCache() {
                    SCLAlertView().showError(errorTitle, subTitle: serverError)
                }
                break
            }
            }.responseJSON { response in
                print("search near by user response:- \(response.result.value)")
        }
    }
    
    @IBAction func settingsBtnclicked(_ sender: Any) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //    #MARK:- Protocall delegate
    func readerList(user_details: User_detail) {
        
        print(user_details.toJSON())
        
        defaults.set(self.selectedreaderArr.toJSON(), forKey: "readerSelected")
        var tempArr = [User_detail]()
        
        if let v = UserDefaults.standard.array(forKey: "readerSelected") {
            print(v)
            for item in v {
                let value = Mapper<User_detail>().map(JSON: item as! [String : Any])
                tempArr.append(value!)
            }
            print(tempArr.count)
            if tempArr.count == 0 {
                self.rederSeleted(user_detail: user_details)
            }else {
                
                var categories = [Int]()
                for i in tempArr {
                    categories.append(i.user_id!)
                }
        
                let contains = categories.contains(where: { $0 == (user_details.user_id) })
                print(contains)
                if contains == true {
                    print("already user added")
                }else {
                   self.rederSeleted(user_detail: user_details)
                }
               
            }
        }else {
            self.rederSeleted(user_detail: user_details)
        }
        //self.selectedreaderArr = tempArr
        print(tempArr.count)
        print(selectedreaderArr.count)
        self.layout.invalidateLayout()
        self.selectedCollectionView.collectionViewLayout.invalidateLayout()
        self.selectedCollectionView.reloadData()
        
        
    }
    
    func nextview(user_details: User_detail) {
        //defaults.set(self.selectedreaderArr.toJSON(), forKey: "readerSelected")
        
        var tempArr = [User_detail]()
        
        if let v = UserDefaults.standard.array(forKey: "readerSelected") {
            print(v)
            for item in v {
                let value = Mapper<User_detail>().map(JSON: item as! [String : Any])
                tempArr.append(value!)
            }
            print(tempArr.count)
            print(tempArr.toJSON())
            if tempArr.count == 0 {
                self.rederSeleted(user_detail: user_details)
            }else {
                var categories = [Int]()
                for i in tempArr {
                    categories.append(i.user_id!)
                }
                
                let contains = categories.contains(where: { $0 == (user_details.user_id) })
                print(contains)
                if contains == true {
                    
                }else {
                    self.rederSeleted(user_detail: user_details)
                }
            }
        }
        else {
            self.rederSeleted(user_detail: user_details)
            tempArr.append(user_details)
        }
        
        print(tempArr.count)
        print(selectedreaderArr.count)
        
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "HireReaderVC") as! HireReaderVC
        nextVC.readerlist = self.selectedreaderArr
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
//    #MARK:- KolodaView delegate
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        self.locationMsg.text = "No actors near your location"
        self.changeLocationBtn.isHidden = false
        changeLocationBtn.isUserInteractionEnabled = true
        headerView.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
        self.view.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
        searchNearByUserView.isHidden = false
        self.settingsBtn.setImage(UIImage(named: "ic_setting"), for: .normal)

        headerTitle.isHidden = true
        self.locationOffErrorView.isHidden = true
        self.readerListView.isHidden = true
        kolodaView.resetCurrentCardIndex()
        self.changeLocationBtn.setTitle("RETRY", for: .normal)
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        print("Selected Index:- \(index)")
    }
    
    func kolodaShouldTransparentizeNextCard(_ koloda: KolodaView) -> Bool {
        return false
    }
    
    func kolodaShouldMoveBackgroundCard(_ koloda: KolodaView) -> Bool {
        return false
    }
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return readerArr.count
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .default
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        print("kolodo index:- \(index)")
        let subview = UIView()
        subview.frame = kolodaView.bounds

        
        
        let kolodaSubView = UIView()
        kolodaSubView.frame = subview.frame
        
        kolodaSubView.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
        let imgview : UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: kolodaSubView.frame.size.width, height: kolodaSubView.frame.size.height * 0.85))
        imgview.backgroundColor = UIColor.clear
        
        let nameView = UIView(frame: CGRect(x: 0, y: imgview.frame.size.height, width: kolodaSubView.frame.size.width, height: kolodaSubView.frame.size.height - imgview.frame.size.height))
        nameView.backgroundColor = UIColor.white
        
        let stackview = UIStackView()
        let priceLbl = UILabel()
        priceLbl.textColor = hexStringToUIColor(hex: redColor)
        priceLbl.font = UIFont(name: "OpenSans", size: 16)
        if let price = readerArr[index].hourly_rate {
            priceLbl.text = " $\(price)"
        } else {
            priceLbl.text = " $5"
        }
        
        let hourLbl = UILabel()
        hourLbl.textColor = UIColor.lightGray
        hourLbl.font = UIFont(name: "OpenSans", size: 12)
        hourLbl.text = "/Hour"
        
        let nameLbl = UILabel()//frame: CGRect(x: 0, y: imgview.frame.size.height, width: kolodaSubView.frame.size.width, height: kolodaSubView.frame.size.height - imgview.frame.size.height))
        if let firstName = readerArr[index].user_name {
            nameLbl.text =  firstName
        }
        nameLbl.textAlignment = .center
        nameLbl.textColor = hexStringToUIColor(hex: "6E6C8A")
        nameLbl.font = UIFont(name: "OpenSans", size: 16)
        
        stackview.axis = .horizontal
        stackview.alignment = .center
        stackview.distribution = .fillProportionally
        
        imgview.sd_setShowActivityIndicatorView(true)
        imgview.sd_setIndicatorStyle(.gray)
        
        if let img  = readerArr[index].headshot_compressed_image {
            imgview.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
        }
        imgview.contentMode = .scaleAspectFill
        imgview.clipsToBounds = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCard(_:)))
        tapGesture.delegate = self
        imgview.tag = index
        imgview.addGestureRecognizer(tapGesture)
        imgview.isUserInteractionEnabled = true
        
        viewCorners.addCornerToView(view: hintView, value: 10)
        nameView.addSubview(stackview)
        let serviceBtn = UIButton(frame: CGRect(x: 10, y: 10, width: 60, height: 60))
        let videoBtn = UIButton(frame: CGRect(x: imgview.frame.size.width - 70, y: (imgview.frame.size.height - 30), width: 60, height: 60))
        let hintBtn = UIButton(frame: CGRect(x: 10, y: 10, width: 60, height: 60))

        kolodaSubView.addSubview(imgview)
        kolodaSubView.addSubview(nameView)
        
        if readerArr[index].is_slate! {
            videoBtn.setImage(UIImage(named: "ic_addVideo"), for: .normal)
            videoBtn.addTarget(self, action: #selector(self.videoBtnPressed(_:)), for: .touchDown)
            videoBtn.tag = index
            kolodaSubView.addSubview(videoBtn)
        }
        
        if !(readerArr[index].service_detail?.isEmpty)! {
            serviceBtn.setImage(UIImage(named: "ic_showServices"), for: .normal)

//            serviceBtn.addTarget(self, action: #selector(self.showToolTip(_:)), for: .touchDragEnter)
            serviceBtn.addTarget(self, action: #selector(self.showServicesBtnPressed(_:)), for: .touchUpInside)

            serviceBtn.tag = index
            kolodaSubView.addSubview(serviceBtn)
//            UIView.animate(withDuration: 1, delay: 0, options: [.autoreverse, .repeat, .curveEaseInOut, .allowUserInteraction],
//                                       animations: {
//                                        serviceBtn.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//            }, completion:nil)
            
                hintBtn.frame = serviceBtn.bounds
                hintBtn.tag = index

                hintBtn.addTarget(self, action: #selector(self.hintBtnPressed(_:)), for: .touchUpInside)
                hintBtn.setTitle("", for: .normal)
                kolodaSubView.addSubview(hintBtn)

        }
        viewCorners.addShadowToView(view: serviceBtn)

        stackview.addArrangedSubview(nameLbl)
        stackview.addArrangedSubview(priceLbl)
        stackview.addArrangedSubview(hourLbl)
        stackview.translatesAutoresizingMaskIntoConstraints = false
        //Layout for Stack View
        stackview.centerXAnchor.constraint(equalTo: nameView.centerXAnchor).isActive = true
//        stackview.centerYAnchor.constraint(equalTo: nameView.centerYAnchor).isActive = true
//        stackview.topAnchor.constraint(equalTo: nameView.topAnchor, constant: 10).isActive = true
        subview.addSubview(kolodaSubView)
        
        
        if let totalFnd = readerArr[index].mutual_friend {
            if totalFnd != 0 {
                let mutualFndLbl = UILabel()//frame: CGRect(x: 0, y: stackview.frame.size.height + 10, width: stackview.frame.size.width, height: 15)
                mutualFndLbl.textColor = hexStringToUIColor(hex: "6E6C8A")
                mutualFndLbl.textAlignment = .center
                mutualFndLbl.font = UIFont(name: "OpenSans", size: 12)
                mutualFndLbl.text = "\(totalFnd) Mutual Friends"
                nameView.addSubview(mutualFndLbl)
                mutualFndLbl.translatesAutoresizingMaskIntoConstraints = false
                mutualFndLbl.topAnchor.constraint(equalTo: stackview.bottomAnchor, constant: 0).isActive = true
                mutualFndLbl.centerXAnchor.constraint(equalTo: nameView.centerXAnchor).isActive = true
                stackview.topAnchor.constraint(equalTo: nameView.topAnchor, constant: 10).isActive = true
                
            } else {
                stackview.centerYAnchor.constraint(equalTo: nameView.centerYAnchor).isActive = true
            }
         } else {
//            mutualFndLbl.text = "No Mutual Friends"
            stackview.centerYAnchor.constraint(equalTo: nameView.centerYAnchor).isActive = true
        }
        
        
        viewCorners.addShadowToView(view: subview, value: 1.0)
        viewCorners.addCornerToView(view: kolodaSubView, value: 10)
     
        subview.backgroundColor = UIColor.clear
        

     //   subview.layoutIfNeeded()
       
        
        return subview
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        switch direction {
        case .topRight, .right,.bottomRight:
            
            var categories = [Int]()
            for i in self.selectedreaderArr {
                categories.append(i.user_id!)
            }
            
            let contains = categories.contains(where: { $0 == (readerArr[index].user_id) })
            print(contains)
            if contains == true {
                print("already user added")
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: "This reader has already been requested", closeButtonTitle: "Ok")
            }else {
                let nextVC = storyboard?.instantiateViewController(withIdentifier: "PopupDialogVC") as! PopupDialogVC
                nextVC.userDetail = readerArr[index]
                nextVC.readerlistdelegate = self
                nextVC.nextviewdelegate = self
                self.present(nextVC, animated: true, completion: nil)
            }
            
           
            
//            let firstSwipe = defaults.integer(forKey: "isFirstSwipe")
//            print(firstSwipe)
//            if firstSwipe == 1 {
//
//                defaults.set(2, forKey: "isFirstSwipe")
//
//                 let nextVC = storyboard?.instantiateViewController(withIdentifier: "PopupDialogVC") as! PopupDialogVC
//                 nextVC.userDetail = readerArr[index]
//                nextVC.readerlistdelegate = self
//                nextVC.nextviewdelegate = self
//                 self.present(nextVC, animated: true, completion: nil)
//
//
//            }else {
//
//                if let expDate = defaults.object(forKey: "expiration_time") as? Date {
//
//                    if expDate < Date() {
//                        //self.goToChat(user_detail: readerArr[index])
//
//                        self.rederSeleted(user_detail: readerArr[index])
//                    }else {
//                       // self.goToChat(user_detail: readerArr[index])
//                        self.rederSeleted(user_detail: readerArr[index])
//                    }
//                }else {
//                   // self.goToChat(user_detail: readerArr[index])
//                    self.rederSeleted(user_detail: readerArr[index])
//                }
//            }
            break
        default:
            print("leftSwipe")
            break
        }
        if tipView != nil {
            if !(tipView?.isHidden)! {
                tipView?.isHidden = true
            }
        }
        if !readerArrTemp.isEmpty {
            readerArrTemp.remove(at: 0)
            print("tapCard:- \(tapCard)")
            print("Koloda card Index:- \(kolodaView.currentCardIndex)")
            tapCard += 1
        }
    }
    
    func goToChat(user_detail: User_detail) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "HireReaderVC") as! HireReaderVC
        nextVC.userDetail = user_detail
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    func goToRider(user_detail: [User_detail]) {
        let nextVC = storyboard?.instantiateViewController(withIdentifier: "HireReaderVC") as! HireReaderVC
        nextVC.readerlist = user_detail
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    func rederSeleted(user_detail: User_detail) {
        print(self.selectedreaderArr.count)
        self.selectedreaderArr.append(user_detail)
        print(self.selectedreaderArr.count)
        defaults.set(self.selectedreaderArr.toJSON(), forKey: "readerSelected")
        self.showSelectedReaderView()
       
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        return Bundle.main.loadNibNamed("OverlayView", owner: self, options: nil)?[0] as? OverlayView
    }
    
    func displayTip(_ sender: UIButton) {
        
        var preferences = EasyTipView.Preferences()
        preferences.drawing.font = UIFont(name: "OpenSans-Semibold", size: 14)!
        preferences.drawing.backgroundColor = UIColor.white
        preferences.drawing.foregroundColor = hexStringToUIColor(hex: redColor)
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.left
        preferences.animating.dismissDuration = 0.3
        
//        Change fileprivate lazy var contentSize: CGSize = {} prperty in EasyTipView.swift to make this thinner tip view
        let easyTipView = EasyTipView(text: "On Tape?", preferences: preferences, delegate: self)
//        EasyTipView.show(forView: sender,
//                         withinSuperview: self.kolodaView,
//                         text: "On Tape / Slate",
//                         preferences: preferences,
//                         delegate: self)
        tipView = easyTipView
//        tipView?.show(forView: sender)
        if tipView != nil {
            viewCorners.addShadowToView(view: tipView!)

        }
        tipView?.show(animated: true, forView: sender, withinSuperview: self.kolodaView)
    }
    
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        print("\(tipView) did dismiss!")
        
        showServicesView.isHidden = false
//        show(animated: true, displayView: showServicesView)
        show(animated: true, displayView: showServicesView, tag: kolodaView.currentCardIndex)

    }
    
    @objc func hintBtnPressed(_ sender: UIButton) {
        let tag = sender.tag
        serviceNameArr = readerArr[tag].service_detail!
        serviceTableView.reloadData()

        displayTip(sender)
        sender.removeFromSuperview()
    }
    
    @objc func showServicesBtnPressed(_ sender: UIButton) {
                let tag = sender.tag
//        print("showServicePressed:- \(tag)")
        if let name = readerArr[tag].user_name {
            showServiceTitleLbl.text = name.uppercased()
        }
        serviceNameArr = readerArr[tag].service_detail!
        serviceTableView.reloadData()

        if tipView != nil {
            tipView?.dismiss()
        }
        showServicesView.isHidden = false
        show(animated: true, displayView: showServicesView, tag: tag)
    }
    
    @objc func videoBtnPressed(_ sender: UIButton) {
        let tag = sender.tag
         if let videoUrl = readerArr[tag].video_detail?.video_file {
            if videoUrl.count > 0 {
                videoPlayerVC.view.frame = self.view.bounds
                videoPlayer = AVPlayer(url: URL(string: videoUrl)!)
                videoPlayerVC.player = videoPlayer
                self.present(videoPlayerVC, animated: true, completion: {
                    self.videoPlayer?.play()
            })
        }
    }
    }
    
    @objc func tapCard(_ sender: UITapGestureRecognizer) {
        print("kolodoCount:- \(kolodaView.countOfCards)")
        print("kolodoCount:- \(kolodaView.countOfVisibleCards)")
        print("Tapped")
        let viewTag = sender.view as! UIView
        tapCard = 0//readerArrTemp.count - 1
        print("current Index:- \(kolodaView.currentCardIndex)")
        let co_ordinate = sender.location(in: kolodaView.subviews[tapCard])
        let originX = kolodaView.subviews[tapCard].frame.origin.x
        let originY = kolodaView.subviews[tapCard].frame.origin.y
        let cardMaxX = kolodaView.subviews[tapCard].frame.maxX
        let midX = kolodaView.subviews[tapCard].frame.midX
        print("maxY:- \(kolodaView.subviews[tapCard].frame.maxY)")
        print("calc maxY:- \(kolodaView.subviews[tapCard].frame.size.height * 0.85)")
        let cardMaxY = kolodaView.subviews[tapCard].frame.maxY
        let midY = cardMaxY - (cardMaxY * 0.4)
        tapCard = kolodaView.currentCardIndex
        if let name = readerArr[tapCard].user_name {
            skillSetTitle.text = name.uppercased() + "'S SKILL SET"
        }
        identifyTapArea(x: originX, y: originY, max_x: cardMaxX, max_y: cardMaxY, midX: midX, midY: midY, tapX: co_ordinate.x, tapY: co_ordinate.y)
        print("x:- \(co_ordinate.x)")
        print("y:- \(co_ordinate.y)")
    }
    
    func identifyTapArea(x: CGFloat, y :CGFloat, max_x: CGFloat, max_y : CGFloat, midX: CGFloat, midY: CGFloat, tapX: CGFloat, tapY: CGFloat) {
        if ((tapX < midX) && (tapY < midY)) {
            print("Open Rating")
            
            if setRatingData() {
                ratingSubview.isHidden = false
            }else {
                alert.showError("Error", subTitle: "No rating details found for selected reader")
            }
            show(animated: true, displayView: ratingSubview)
        } else if ((tapX > midX) && (tapY < midY)){
            
            if !skillData.isEmpty {
                setSliderValue(slider : sitcomSlider, val : ((skillData[tapCard].sit_com != nil) ? skillData[tapCard].sit_com! : 0 ), skillLbl : sitcomValueLbl)
                setSliderValue(slider : hourDramaSlider, val : ((skillData[tapCard].hour_drama != nil) ? skillData[tapCard].hour_drama! : 0 ), skillLbl : hourDramaValueLbl)
                setSliderValue(slider : dramedySlider, val : ((skillData[tapCard].dramedy != nil) ? skillData[tapCard].dramedy! : 0 ), skillLbl : dramedyValueLbl)
                setSliderValue(slider : playsValueSlider, val : ((skillData[tapCard].plays != nil) ? skillData[tapCard].plays! : 0 ), skillLbl : playsValueLbl)
                setSliderValue(slider : musicalsSlider, val : ((skillData[tapCard].musicals != nil) ? skillData[tapCard].musicals! : 0 ), skillLbl : musicalsValueLbl)
                setSliderValue(slider : featureFilmsSlider, val : ((skillData[tapCard].feature_films != nil) ? skillData[tapCard].feature_films! : 0 ), skillLbl : featureFilmsValueLbl)
                setSliderValue(slider : singleCameraSlider, val : ((skillData[tapCard].single_camera != nil) ? skillData[tapCard].single_camera! : 0 ), skillLbl : singleCameraValueLbl)
                setSliderValue(slider : coStarSlider, val : ((skillData[tapCard].co_star != nil) ? skillData[tapCard].co_star! : 0 ), skillLbl : coStarValueLbl)
                setSliderValue(slider : commercialSlider, val : ((skillData[tapCard].commercials != nil) ? skillData[tapCard].commercials! : 0 ), skillLbl : commercialValueLbl)
                setSliderValue(slider : shakespeareSlider, val : ((skillData[tapCard].shakespeare != nil) ? skillData[tapCard].shakespeare! : 0 ), skillLbl : shakespeareValueLbl)
                setSliderValue(slider : monologuesSlider, val : ((skillData[tapCard].monologues != nil) ? skillData[tapCard].monologues! : 0 ), skillLbl : monologuesValueLbl)
                setSliderValue(slider : accentsSlider, val : ((skillData[tapCard].accents != nil) ? skillData[tapCard].accents! : 0 ), skillLbl : accentsValueLbl)
//                if let accents = skillData[tapCard].accents {
//                    setSliderValue(slider : accentsSlider, val : accents, skillLbl : accentsValueLbl)
//                }else {
//                    setSliderValue(slider : accentsSlider, val : 0, skillLbl : accentsValueLbl)
//                }
            }
            showSkillView.isHidden = false
            show(animated: true, displayView: showSkillView)
            print("Open Skill")
        } else {
            if let url = URL(string: "about:blank") {
                let myRequest = NSURLRequest(url: url)//URLRequest(url: url)
                webview.loadRequest(URLRequest(url: url))
            }else {
                print("Error")
            }
            
            if let resumeFile = readerArr[tapCard].resume_file {
                if resumeFile == "" {
                    let alertview = SCLAlertView()
                    alertview.showError("Error", subTitle: "No resume found for selected reader.")
                } else {
                    pdfsubview.isHidden = false
                    activityIndicator.isHidden = false
                    activityIndicator.startAnimating()
                    openPdf()
                    show(animated: true, displayView: pdfsubview)
                }
            }else {
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: "No resume found for selected reader.")
            }
        }
    }
    
    func openPdf() {
        if let resumeFile = readerArr[tapCard].resume_file {
            let pdfUrl = URL(string: resumeFile)
            if let url = pdfUrl {
                let myRequest = NSURLRequest(url: url)//URLRequest(url: url)
                webview.loadRequest(URLRequest(url: url))
            }else {
                print("Error")
            }
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        print("start")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }

    @IBAction func cancelBtnClicked(_ sender: Any) {
        readerListView.alpha = 1.0
        if !showSkillView.isHidden {
            UIView.animate(withDuration: 0.25, animations: {
                self.showSkillView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                self.showSkillView.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished) {
                    self.showSkillView.isHidden = true
                }
            });
        }
        if !ratingSubview.isHidden {
            UIView.animate(withDuration: 0.25, animations: {
                self.ratingSubview.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                self.ratingSubview.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished) {
                    self.ratingSubview.isHidden = true
                }
            });
        }
        
        if !pdfsubview.isHidden {
            UIView.animate(withDuration: 0.25, animations: {
                self.pdfsubview.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                self.pdfsubview.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)  {
                    self.pdfsubview.isHidden = true
                }
            });
        }
       
        if !showServicesView.isHidden {
            UIView.animate(withDuration: 0.25, animations: {
                self.showServicesView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                self.showServicesView.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)  {
                    self.showServicesView.isHidden = true
                }
            });
        }
        
        if !comingSoonView.isHidden {
            comingSoonView.isHidden = true
        }
        settingsBtn.isUserInteractionEnabled = true
    }
    
    func show(animated:Bool, displayView: UIView, tag : Int? = nil){
        if animated {
            let popOverVC = storyBoard.instantiateViewController(withIdentifier: "FindUserVC") as! FindUserVC
            self.addChildViewController(popOverVC)
            popOverVC.modalPresentationStyle = .none
            self.view.addSubview(displayView)
            popOverVC.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: redColor)
            popOverVC.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont(name: "Arial", size: 18)!]
            popOverVC.didMove(toParentViewController: self)
            
            UIView.animate(withDuration: 0.25, animations: {
                displayView.alpha = 1.0
                displayView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            });
            if tag != nil {
                if let name = readerArr[tag!].user_name {
                    showServiceTitleLbl.text = name.uppercased()
                }
            }
        }else{
        }
        settingBtn.isEnabled = false
        chatBtn.isEnabled = false
        settingsBtn.isUserInteractionEnabled = false
    }
    
    func setRatingData() -> Bool {
        if !ratingData.isEmpty {
            if let avgRate = ratingData[tapCard].average_rating {
                headerGetScoreLbl.text = "\(avgRate)"
                starRatingView.value = CGFloat(Float(avgRate)!)
            }
            if let ratingTotal = ratingData[tapCard].ratings {
                ratingHeaderLbl.text = "\(ratingTotal) ratings"
            }
            if let likePercent = ratingData[tapCard].on_time_yes {
                readerOnTImeLikePer.text = "\(likePercent)%"
            }
            if let dislikePercent = ratingData[tapCard].on_time_no {
                readerOnTImeDislikePer.text = "\(dislikePercent)%"
            }
            if let votes = ratingData[tapCard].is_on_time {
                readerOnTotalVotes.text = "\(votes)"
            }
            if let likePercent = ratingData[tapCard].is_knowledgeable_yes {
                likePer2.text = "\(likePercent)%"
            }
            if let dislikePercent = ratingData[tapCard].is_knowledgeable_no {
                dislikePer2.text = "\(dislikePercent)%"
            }
            if let votes = ratingData[tapCard].is_knowledgeable_on_content {
                TotalVotes2.text = "\(votes)"
            }
            if let likePercent = ratingData[tapCard].work_with_yes {
                likePer3.text = "\(likePercent)%"
            }
            if let dislikePercent = ratingData[tapCard].work_with_no {
                dislikePer3.text = "\(dislikePercent)%"
            }
            if let votes = ratingData[tapCard].work_with_reader_again {
                TotalVotes3.text = "\(votes)"
            }
            if let likePercent = ratingData[tapCard].job_satisfaction_yes {
                likePer4.text = "\(likePercent)%"
            }
            if let dislikePercent = ratingData[tapCard].job_satisfaction_no {
                dislikePer4.text = "\(dislikePercent)%"
            }
            if let votes = ratingData[tapCard].jobSatisFiedPer {
                TotalVotes4.text = "\(votes)"
            }
            return true
        }else {
            return false
        }
    }
    
    func startTimer() {
        seconds = 3
        hintView.isHidden = false
        timer = Timer()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        if seconds > 0 {
            seconds = seconds-1
        }else {
            timer.invalidate()
            hintView.isHidden = true
        }
    }
    
    func drawLine(scaleX: CGFloat, scaleY: CGFloat, width: CGFloat, height: CGFloat, lineType: String)
    {
        let path = CGMutablePath()
        let layer = CAShapeLayer()

        switch lineType {
        case "vertical":
            path.addLines(between: [CGPoint(x: scaleX, y: scaleY), CGPoint(x: scaleX, y: height)])
            layer.path = path//.cgPath
        default:
            path.addLines(between: [CGPoint(x: scaleX, y: scaleY), CGPoint(x: width, y: scaleY)])
            layer.path = path//.cgPath
        }
        layer.strokeColor = UIColor.white.cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.lineDashPattern = [5,5]
        layer.lineJoin = kCALineCapRound
        layer.lineWidth = 1.0
        switch lineType {
        case "vertical":
           self.hintView.layer.addSublayer(layer)
        default:
           self.resumeHintView.layer.addSublayer(layer)
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .denied,.notDetermined,.restricted:
            print("No access")
            locationOffErrorView.isHidden = false
            self.settingsBtn.setImage(UIImage(named: "ic_whiteSetting"), for: .normal)

            headerTitle.isHidden = true
            headerView.backgroundColor = hexStringToUIColor(hex: "8F8F8F")
            self.view.backgroundColor = hexStringToUIColor(hex: "8F8F8F")
            searchNearByUserView.isHidden = true
            readerListView.isHidden = true
            comingSoonView.isHidden = true
            showSkillView.isHidden = true
            showServicesView.isHidden = true
            pdfsubview.isHidden = true
            ratingSubview.isHidden = true
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
        default:
            locationOffErrorView.isHidden = true
            self.view.layoutSubviews()
            current_location = locationManager.location
            headerView.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
            self.view.backgroundColor = hexStringToUIColor(hex: lightGrayColor)
            searchNearByUserView.isHidden = false
            self.settingsBtn.setImage(UIImage(named: "ic_setting"), for: .normal)

            headerTitle.isHidden = true
            if !pulsator.isPulsating {
                addanimation()
            }
            self.callSearchNearApi()
            configureNavigationBar()
            self.view.layoutSubviews()
            self.view.layoutIfNeeded()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        current_location = locationManager.location
        self.view.layoutSubviews()
    }
    
    // QBWebRTCChatDelegate
    func didReceiveNewSession(_ session: QBRTCSession, userInfo: [String : String]? = nil) {
        if self.session != nil {
            let uInfo = ["reject":"busy"]
            session.rejectCall(uInfo)
            return
        }
        self.session = session
        
        if CallKitManager.isCallKitAvailable {
            self.callUUID = UUID()
            self.opponentIDs = [session.initiatorID]
            for userID in session.opponentsIDs {
                if userID.uintValue != QBCore.instance().currentUser.id {
                    self.opponentIDs.append(userID)
                }
            }
            
            CallKitManager.instance.reportIncomingCall(withUserIDs: self.opponentIDs, session: session, uuid: self.callUUID, onAcceptAction: {
                let vc = storyBoard.instantiateViewController(withIdentifier: "CallVC") as! CallVC
                vc.session = session
                vc.usersDataSource = appDelegate.dataSource
                vc.callUUID = self.callUUID
                let nav = UINavigationController(rootViewController: vc)
                //                self.present(nav, animated: true, completion: nil)
                if var tvc = UIApplication.shared.keyWindow?.rootViewController {
                    while let presentedViewController = tvc.presentedViewController {
                        tvc = presentedViewController
                    }
                    tvc.present(nav, animated: false, completion: nil)
                }
            }, completion: nil)
        }else {
            let vc = storyBoard.instantiateViewController(withIdentifier: "InComingCallVC") as! InComingCallVC
            vc.delegate = self
            vc.session = session
            vc.usersDataSource = appDelegate.dataSource
            let nav = UINavigationController(rootViewController: vc)
            //            self.present(nav, animated: false, completion: nil)
            if var tvc = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = tvc.presentedViewController {
                    tvc = presentedViewController
                }
                tvc.present(nav, animated: false, completion: nil)
            }
        }
    }
    
    func sessionDidClose(_ session: QBRTCSession) {
        if session == self.session {
            if self.backgroundTask != UIBackgroundTaskInvalid {
                UIApplication.shared.endBackgroundTask(self.backgroundTask)
                self.backgroundTask = UIBackgroundTaskInvalid
            }
            DispatchQueue.main.async {
                if UIApplication.shared.applicationState == .background && self.backgroundTask == UIBackgroundTaskInvalid {
                    QBChat.instance.disconnect(completionBlock: nil)
                }
            }
            if var tvc = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = tvc.presentedViewController {
                    tvc = presentedViewController
                }
                if tvc is CallVC {
                    tvc.dismiss(animated: false, completion: nil)
                }
            }
            self.session = nil
            
            if CallKitManager.isCallKitAvailable {
                CallKitManager.instance.endCall(with: self.callUUID, completion: nil)
                self.callUUID = nil
                self.session = nil
            }
        }
    }
    
    func incomingCallViewControllerDidAcceptSession(vc: InComingCallVC, session: QBRTCSession) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "CallVC") as! CallVC
        vc.session = session
        vc.usersDataSource = self.dataSource
        let nav = UINavigationController(rootViewController: vc)
        if var tvc = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = tvc.presentedViewController {
                tvc = presentedViewController
            }
            tvc.present(nav, animated: true, completion: nil)
        }
    }
    
    func incomingCallViewControllerDidRejectSession(vc: InComingCallVC, session: QBRTCSession) {
        session.rejectCall(nil)
        vc.dismiss(animated: false, completion: nil)
    }
    
    // MARK: - PKPushRegistryDelegate protocol
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString
        
        let subscription = QBMSubscription()
        subscription.notificationChannel = QBMNotificationChannel.APNSVOIP
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = self.voipRegistry.pushToken(for: PKPushType.voIP)
        
        QBRequest.createSubscription(subscription, successBlock: { (response, objects) in
            print("Create Subscription request - Success")
        }) { (response) in
            print("Create Subscription request - Error")
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        //        fatalError()
        let deviceIdentifier = UIDevice.current.identifierForVendor!.uuidString
        QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: deviceIdentifier, successBlock: { (response) in
            print("Unregister Subscription request - Success")
        }) { (error) in
            print("Unregister Subscription request - Error")
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        print("payload dict: \(payload.dictionaryPayload)")
        if CallKitManager.isCallKitAvailable {
            if payload.dictionaryPayload[kVoipEvent] != nil {
                let application = UIApplication.shared
                if self.backgroundTask == UIBackgroundTaskInvalid {
                    self.backgroundTask = application.beginBackgroundTask(expirationHandler: {
                        application.endBackgroundTask(self.backgroundTask)
                        self.backgroundTask = UIBackgroundTaskInvalid
                    })
                }
                if !QBChat.instance.isConnected {
                    QBCore.instance().loginWithCurrentUser()
                }
                ServicesManager.instance().chatService.connect(completionBlock: nil)
            }
        }
    }
    
    func showMessage(_ message: String) {
        let alert = UIAlertController(title: "Info", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: false, completion: nil)
    }
}

extension Date {
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
}

extension FindUserVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serviceNameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TappingCell
        if let name = serviceNameArr[indexPath.row].service_name {
            cell.tappingLblTxt.text = name
        }
        return cell
    }
}
extension FindUserVC : UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedreaderArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print(indexPath)
    
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ReaderCell
        
        viewCorners.addBorderCornerTobutton(view: cell, color: hexStringToUIColor(hex: purpleColor))
        
        cell.profileImgview.sd_setShowActivityIndicatorView(true)
        cell.profileImgview.sd_setIndicatorStyle(.gray)
        if let img = selectedreaderArr[indexPath.row].headshot_compressed_image {
            cell.profileImgview.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat((45)), height: CGFloat(45))
    }
}
