//
//  ChangeLocationVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/25/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import CoreLocation
import GooglePlaces
import AddressBookUI
import Alamofire
import SCLAlertView
import AlamofireObjectMapper
import ObjectMapper
import ReachabilitySwift

class ChangeLocationVC : UIViewController, GMSAutocompleteResultsViewControllerDelegate, GMSMapViewDelegate, UISearchResultsUpdating, UINavigationControllerDelegate, CLLocationManagerDelegate {
    
    @IBOutlet var mapView: GMSMapView!
    var current_location: CLLocation!
    var resultVC : GMSAutocompleteResultsViewController?
    var searchController : UISearchController?
    var resultView : UITextView?
    var marker: GMSMarker!
    var location_coordinate : CLLocationCoordinate2D!
    var isFromSetting : Bool = false
    var locationManager : CLLocationManager!
    var userLocation : String = ""
    @IBOutlet var subView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !(Reachability()?.isReachable)! {
            noInternetMsgDialog()
        }
        
        //Location
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startMonitoringSignificantLocationChanges()
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 10.
        navigationController?.delegate = self
        self.navigationController?.navigationBar.isHidden = false
        configureNavigationBar()
        marker = GMSMarker()
        if let data =  defaults.value(forKey: "locationCoordinatesArr") as?  [String:CLLocationDegrees]{
            //            let camera = GMSCameraPosition.camera(withLatitude: data["latitude"]!, longitude: data["longtitude"]!, zoom: 10.0)
            setLocation(latitude: data["latitude"]!, longitude: data["longtitude"]!)
            
        }else  if current_location != nil {
            setLocation(latitude: current_location.coordinate.latitude, longitude: current_location.coordinate.longitude)
            
        }else {
            current_location = locationManager.location
            if current_location != nil {
                //                let camera = GMSCameraPosition.camera(withLatitude: current_location.coordinate.latitude, longitude: current_location.coordinate.longitude, zoom: 10.0)
                setLocation(latitude: current_location.coordinate.latitude, longitude: current_location.coordinate.longitude)
                //                mapView.camera = camera
                //                mapView.delegate = self
                //                // Creates a marker in the center of the map.
                //                marker.isDraggable = true
                //                marker.position = CLLocationCoordinate2D(latitude: current_location.coordinate.latitude, longitude: current_location.coordinate.longitude)
                //                //        marker.title = "Sydney"
                //                //        marker.snippet = "Australia"
                //                marker.map = self.mapView
            }else {
                print("No location found")
            }
        }
        
        //        loadMap(current_location: current_location.coordinate, viewFrame: self.mapView.frame)
        
        resultVC  = GMSAutocompleteResultsViewController()
        resultVC?.delegate = self
        //        resultVC?.view.frame = self.view.frame
        
        searchController = UISearchController(searchResultsController: resultVC)
        searchController?.searchResultsUpdater = resultVC
        searchController?.view.frame = self.view.bounds
        let searchsubView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 45.0))
        
        searchsubView.addSubview((searchController?.searchBar)!)
        self.view.addSubview(searchsubView)
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = true
        
        searchController?.dimsBackgroundDuringPresentation = true
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        if current_location != nil {
            location_coordinate = current_location.coordinate
        }
        
        if let location = defaults.value(forKey: "locationAddress") as? String {
            print("location_coordinate:- \(location)")
            searchController?.searchBar.text = location
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        searchController?.view.frame = self.view.bounds
        
    }
    
    
    func setLocation(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 10.0)
        //        let mapview = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.camera = camera
        //        self.mapView = mapview
        mapView.delegate = self
        // Creates a marker in the center of the map.
        //                marker = GMSMarker()
        marker.isDraggable = true
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        //        marker.title = "Sydney"
        //        marker.snippet = "Australia"
        marker.map = self.mapView
    }
    //    override func viewWillDisappear(_ animated: Bool) {
    //        super.viewWillDisappear(true)
    //        if self.isMovingFromParentViewController {
    //            if location_coordinate != nil {
    //                if isFromSetting {
    //                    let vc = self.navigationController?.viewControllers[1] as? SettingsVC
    //
    //                    vc?.location_coordinate = location_coordinate
    //                }else {
    //                    let vc = self.navigationController?.viewControllers[0] as? FindUserVC
    //
    //                    vc?.location_coordinate = location_coordinate
    //
    //                }
    //            }
    //
    //        }
    //    }
    
    
    //    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
    //        let vc = viewController as? SettingsVC
    //        if location_coordinate != nil {
    //            vc?.location_coordinate = location_coordinate
    //
    //        }
    //    }
    //    override func willMove(toParentViewController parent: UIViewController?) {
    //        print("Moved")
    //    }
    
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        //        searchController?.isActive = false
        // Do something with the selected place.
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        if let arr = place.formattedAddress?.components(separatedBy: ","){
            userLocation = arr[0]
            for i in 1..<arr.count {
                
                userLocation = userLocation + ", " + arr[i]
                
            }
            defaults.set(userLocation, forKey: "locationAddress")
        }
        self.searchController?.searchBar.text = place.formattedAddress
        print("Place attributions: \(place.coordinate)")
        //        resultVC?.dismiss(animated: true, completion: nil)
        resultVC?.dismiss(animated: true, completion: nil)
        location_coordinate = place.coordinate
        loadMap(current_location: place.coordinate, viewFrame: self.mapView.frame)
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    func findPlace(placeID: String) {
        let placesClient = GMSPlacesClient()
        placesClient.lookUpPlaceID(placeID, callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            
            guard let place = place else {
                print("No place details for \(placeID)")
                return
            }
            
            print("Place name \(place.name)")
            print("Place address \(place.formattedAddress)")
            print("Place placeID \(place.placeID)")
            print("Place attributions \(place.attributions)")
        })
    }
    func loadMap(current_location: CLLocationCoordinate2D, viewFrame: CGRect) {
        
        
        // Creates a marker in the center of the map.
        //        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: current_location.latitude, longitude: current_location.longitude)
        let camera = GMSCameraPosition.camera(withTarget: current_location, zoom: 10.0)
        //        marker.title = "Sydney"
        //        marker.snippet = "Australia"
        marker.map = self.mapView
        marker.isDraggable = true
        mapView.camera = camera
        
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        print("Co-ordinate:- \(marker.position)")
        let coordinatesArr = ["latitude":marker.position.latitude,"longtitude":marker.position.longitude]
        defaults.set(coordinatesArr, forKey: "locationCoordinatesArr")
        location_coordinate = marker.position
        print("address:- \(marker.classForCoder)")
        // Create Location
        
        let location = CLLocation(latitude: marker.position.latitude, longitude: marker.position.longitude)
        // Create Location
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            print("place:- \(placemarks?.first?.compactAddress)")
            print("place12:- \(placemarks?.first?.addressDictionary)")
            if let address = placemarks?.first?.addressDictionary {
                let formattedAddress = ABCreateStringWithAddressDictionary(address, true)
                let arr = formattedAddress.components(separatedBy: ",")
                if !arr.isEmpty{
                    self.userLocation = arr[0]
                    for i in 1..<arr.count {
                        
                        self.userLocation = self.userLocation + ", " + arr[i]
                        
                    }
                    defaults.set(self.userLocation, forKey: "locationAddress")
                }
                self.searchController?.searchBar.text = "\(formattedAddress)"
            }
        }
        //        searchController?.searchBar.text = mapView.myLocation
    }
    func updateSearchResults(for searchController: UISearchController) {
        print("search:- \(searchController.isActive)")
    }
    //Set NavigationBar
    func configureNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: UIFont(name: "Arial", size: 18)!]
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: redColor)
        let navigationItem = self.navigationItem
        navigationItem.title = "CHANGE LOCATION"
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        //        self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: "EEEEEE")
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let doneButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 44))
        
        if #available(iOS 11.0, *) {
            let widthConstraint = doneButton.widthAnchor.constraint(equalToConstant: 44)
            let heightConstraint = doneButton.heightAnchor.constraint(equalToConstant: 44)
            heightConstraint.isActive = true
            widthConstraint.isActive = true
            doneButton.contentEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 0)
        }else {
            doneButton.contentEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 0)
            
        }
        doneButton.setImage(UIImage(named: "ic_done"), for: .normal)
        
        doneButton.addTarget(self, action: #selector(doneBtnPressed(_:)), for: .touchUpInside)
        let donwBtn = UIBarButtonItem(customView: doneButton)
        navigationItem.rightBarButtonItem = donwBtn
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let data =  defaults.value(forKey: "locationCoordinatesArr") as?  [String:CLLocationDegrees]{
            print("date coordinate:- \(data)")
        }else if current_location == nil {
            current_location = locationManager.location
            location_coordinate = current_location.coordinate
            
            loadMap(current_location: current_location.coordinate, viewFrame: self.mapView.frame)
            
        }
        
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .denied,.notDetermined,.restricted:
            print("No access")
            
        default:
            //            self.navigationController?.navigationBar.isHidden = false
            if let data =  defaults.value(forKey: "trem") as?  [String:CLLocationDegrees]{
                print("date coordinate:- \(data)")
            }else if current_location == nil {
                current_location = locationManager.location
                location_coordinate = current_location.coordinate
                
                loadMap(current_location: current_location.coordinate, viewFrame: self.mapView.frame)
                
            }
            
        }
    }
    
    @objc func doneBtnPressed(_ sender: UIButton) {
        if (Reachability()?.isReachable)! {
            if location_coordinate != nil {
                let arr = ["latitude":location_coordinate.latitude,"longtitude":location_coordinate.longitude]
                defaults.set(arr, forKey: "locationCoordinatesArr")
                updateUserLocation(coordinates: location_coordinate)
            }else if current_location != nil {
                let arr = ["latitude":current_location.coordinate.latitude,"longtitude":current_location.coordinate.longitude]
                defaults.set(arr, forKey: "locationCoordinatesArr")
                updateUserLocation(coordinates: current_location.coordinate)
                
            }  else if let data =  defaults.value(forKey: "locationCoordinatesArr") as?  [String:CLLocationDegrees] {
                let latitude = data["latitude"]!
                let longitude = data["longtitude"]!
                let newLat = Double(latitude)
                let newlongitude = Double(longitude)
                
                let locationData = CLLocationCoordinate2D(latitude: newLat, longitude: newlongitude)
                updateUserLocation(coordinates: locationData)
                
            }
        }else{
            
            noInternetMsgDialog()

        }
    }
    
    func updateUserLocation(coordinates: CLLocationCoordinate2D) {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)

        let user = UpdateLocationRequestModel(latitude: coordinates.latitude, longitude: coordinates.longitude)
        let data_temp = Mapper<UpdateLocationRequestModel>().toJSON(user)
        print("Change Location request:- \(data_temp)")
        
        //Call api
        alamofireManager.request(updateLocationByUser, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        let alertView = SCLAlertView()
                        alertView.showSuccess("Success", subTitle: (response.result.value?.message)!, closeButtonTitle: "Ok",  colorStyle: int_red)
                        if self.isFromSetting {
                            //                            let cnt = self.navigationController?.viewControllers.count
                            //                            if cnt != 0 {
                            //                                let nextVC = self.navigationController?.viewControllers[cnt! - 2]  as! SettingsVC
                            //                                nextVC.location_coordinate = coordinates
                            //                                nextVC.userlocation = self.userLocation
                            //                            }
                            self.navigationController?.popViewController(animated: true)
                            
                        }else {
                            let story = UIStoryboard(name: "Main", bundle: nil)
                            isSettingChanged = true
                            let nextVC = story.instantiateViewController(withIdentifier: "FindUserVC") as! FindUserVC
                            nextVC.location_coordinate = coordinates
                            self.navigationController?.popViewController(animated: true)
                            //                            self.navigationController?.pushViewController(nextVC, animated: true)
                        }
                        break
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.updateUserLocation(coordinates: coordinates)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        
                        
                        break
                    }
                }
                break
            case .failure(let error):
                print("error: \(error.localizedDescription)")
                
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                break
            }
        }
    }
    
}

//extension CLPlacemark {
//
//    var compactAddress: String? {
//        if let name = name {
//            var result = name
//
//            if let street = thoroughfare {
//                result += ", \(street)"
//            }
//
//            if let city = locality {
//                result += ", \(city)"
//            }
//
//            if let country = country {
//                result += ", \(country)"
//            }
//
//            return result
//        }
//
//        return nil
//    }
//
//}

