//
//  PopupDialogVC.swift
//  need_a_reader
//
//  Created by ob_apple_3 on 20/09/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import AlamofireObjectMapper
import ObjectMapper
import ReachabilitySwift
import SDWebImage


protocol setReaderlist {
    func readerList(user_details:User_detail)
}
protocol moveNextview {
    func nextview(user_details:User_detail)
}

class PopupDialogVC: UIViewController {

    @IBOutlet weak var subview: UIView!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var readerlistdelegate:setReaderlist!
    var nextviewdelegate:moveNextview!
    var userDetail:User_detail!
    override func viewDidLoad() {
        super.viewDidLoad()

        viewCorners.addCornerToView(view: subview, value: 10)
        viewCorners.addCornerToView(view: yesBtn, value: 17)
        viewCorners.addCornerToView(view: noBtn, value: 17)
        viewCorners.addCornerToView(view: cancelBtn, value: 17)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func yesBtnClick(_ sender: Any) {
        readerlistdelegate.readerList(user_details: userDetail)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func noBtnClick(_ sender: Any) {
        
        
       nextviewdelegate.nextview(user_details: userDetail)
        self.dismiss(animated: true, completion: nil)
        

    }
    
    @IBAction func cancelBtnClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
