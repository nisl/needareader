//
//  ViewresumeVC.swift
//  need_a_reader
//
//  Created by ob_apple_3 on 18/09/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import AlamofireObjectMapper
import ObjectMapper
import ReachabilitySwift
import SDWebImage
import WebKit
import TOCropViewController

class ViewresumeVC: UIViewController ,WKUIDelegate, UIWebViewDelegate,UIDocumentPickerDelegate,TOCropViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet var webview: UIWebView!
    @IBOutlet var pdfsubview: UIView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var nodataView: UIView!
    var userData : User_detail!
    
    var sendPDFData : Data!
    var documentPicker : UIDocumentPickerViewController!
    var imagePicker : UIImagePickerController!
    
    var userName = ""
    var navColor : UIColor = hexStringToUIColor(hex: lightGrayColor)
    var location_coordinate : CLLocationCoordinate2D!
    var resumeImage : Bool = false
    
    var resumeSelectedImg : UIImage!
    var userlocation : String = ""
    var imagePickerOpen : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavigationBar()
        webview.delegate = self
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        
        self.resumeDisplay()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func resumeDisplay()  {
        if let url = URL(string: "about:blank") {
            let myRequest = NSURLRequest(url: url)//URLRequest(url: url)
            webview.loadRequest(URLRequest(url: url))
        }else {
            print("Error")
        }
        if userData.resume_file != "" {
            self.pdfsubview.isHidden = false
            self.nodataView.isHidden = true
        }else {
            self.pdfsubview.isHidden = true
            self.nodataView.isHidden = false
        }
        if let resumeFile = userData.resume_file  {
            if resumeFile == "" {
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: "No resume found for selected reader.")
            } else {
                pdfsubview.isHidden = false
                activityIndicator.isHidden = false
                activityIndicator.startAnimating()
                openPdf()
                //show(animated: true, displayView: pdfsubview)
            }
        }else {
            let alertview = SCLAlertView()
            alertview.showError("Error", subTitle: "No resume found for selected reader.")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Set NavigationBar
    func configureNavigationBar()
    {
        setNavigaion(vc: self)
        let navigationItem = self.navigationItem
        navigationItem.title = "RESUME"
        
        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        let addBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
       // let backBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        
       // backBtn.addTarget(self, action: #selector(backBtnPressed(_:)), for: .touchUpInside)
        addBtn.setImage(UIImage(named: "ic_resume"), for: .normal)
        addBtn.addTarget(self, action: #selector(addBtnPressed(_:)), for: .touchUpInside)
        
        let addButton = UIBarButtonItem(customView: addBtn)
      //   let backButton = UIBarButtonItem(customView: backBtn)
       // navigationItem.leftBarButtonItem = backButton
        navigationItem.rightBarButtonItems = [addButton]

    }
    
    @objc func addBtnPressed(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Choose Resume From", message: "", preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: 0, y: 64, width: screenWidth, height: 0)
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (success) in
            self.resumeImage = true
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
            self.imagePickerOpen = true
            
            
        }))
        alert.addAction(UIAlertAction(title: "Documents/iCloud", style: .default, handler: { (success) in
            self.documentPicker = UIDocumentPickerViewController(documentTypes: ["public.content"], in: .import)
            self.documentPicker.delegate = self
            self.documentPicker.modalPresentationStyle = .fullScreen
            self.present(self.documentPicker, animated: true, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: { (success) in
            
        }))
        
        present(alert, animated: true, completion: nil)
        
    }
    
    func openPdf() {
        if let resumeFile = userData.resume_file {
            let pdfUrl = URL(string: resumeFile)
            if let url = pdfUrl {
                let myRequest = NSURLRequest(url: url)
                webview.loadRequest(URLRequest(url: url))
            }else {
                print("Error")
            }
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        print("start")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
    //DocumentPicker Delegate
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        print("url:- \(url)")
        let extensionString = (url.absoluteString).suffix(4)
        let theFileName = ((url.absoluteString) as NSString).lastPathComponent
        //        filenameLbl.text = theFileName
        if controller.documentPickerMode == UIDocumentPickerMode.import {
            // This is what it should be
            //                let data = String(contentsOfFile: url.path)
            do {
                let pdfData = try Data(contentsOf: url)
                sendPDFData = pdfData
                if resumeSelectedImg != nil {
                    resumeSelectedImg = nil
                }
                self.setResumeApi()
            } catch {
                print("Unable to load data: \(error)")
            }
        }
        //        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        tabBarController?.tabBar.isHidden = true
        
        print("document picker cancel")
        
    }
    
    //ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //        UIImagePickerControllerEditedImage
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let cropVC = TOCropViewController(croppingStyle: .default, image: pickedImage)
            cropVC.delegate = self
            if !resumeImage {
                cropVC.rotateClockwiseButtonHidden = false
                cropVC.resetAspectRatioEnabled = false
                cropVC.aspectRatioPickerButtonHidden = true
                //            cropVC.aspectRatioPreset = .preset4x3
//                cropVC.customAspectRatio = CGSize(width: profileImg.frame.size.width, height: profileImg.frame.size.height)
//                cropVC.cropView.cropBoxResizeEnabled = false
            }
            
            
            
            //            cropVC.deviceOrientation = "Landscape"
            tabBarController?.tabBar.isHidden = true
            navigationController?.pushViewController(cropVC, animated: true)
            //            if resumeImage {
            //                resumeSelectedImg = pickedImage
            //                if sendPDFData != nil {
            //                    sendPDFData = nil
            //                }
            //                self.setResumeApi()
            //            }else {
            //                self.profileImg.image = pickedImage
            //                profileImg.contentMode = .scaleToFill
            //                setImageApi(profileImage: pickedImage)
            //            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        tabBarController?.tabBar.isHidden = false
        
        dismiss(animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        
        cropViewController.navigationController?.popViewController(animated: true)
        if let imageData = UIImageJPEGRepresentation(image, 1) {
            
            let imageSize: Double = Double(imageData.count)
            //            print("size of original image in MB:\((imageSize/1024)/1024)")
            let size = imageSize/(1024*1024)
            print("size of original image in MB:\(size)")
            if size > 1.5 {
                let compressedImage = imageCompression().compressImage(image: image)
                
                if compressedImage != nil {
                    if self.resumeImage {
                        self.resumeSelectedImg = compressedImage
                        if self.sendPDFData != nil {
                            self.sendPDFData = nil
                        }
                        self.setResumeApi()
                    }
                }
            } else {
                if self.resumeImage {
                    self.resumeSelectedImg = image
                    if self.sendPDFData != nil {
                        self.sendPDFData = nil
                    }
                    self.setResumeApi()
                }
            }
        }
        
    }
    
    
    func setResumeApi() {
        
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        let URL = try! URLRequest(url: setResumeForUser, method: .post, headers: tokenHeader)
        
        alamofireManager.upload(multipartFormData:  { multipartFormData in
            
            if self.sendPDFData != nil {
                multipartFormData.append(self.sendPDFData, withName: "file", fileName: "doc.pdf", mimeType: "application/pdf")
            }else {
                if self.resumeSelectedImg != nil {
                    if let data = UIImageJPEGRepresentation(self.resumeSelectedImg, 1.0) {
                        multipartFormData.append(data, withName: "file",  fileName: "file.jpeg", mimeType: "image/jpeg")
                    }
                }
            }
        }, with: URL) { encodingResult in
            //            , to: setResumeForUser, method: .post, headers: tokenHeader, encodingCompletion: { encodingResult in
            
            switch encodingResult
            {
            case .success(let upload,_,_):
                upload.responseObject { (response: DataResponse<ResponseModel> )in
                    switch response.result {
                    case .success(let value):
                        if let code = value.code {
                            print("Code:- \(code)")
                            switch code {
                            case 200:
                                alert.hideView()
                                if let msg = value.message {
                                    let alertView = SCLAlertView()
                                    alertView.showSuccess("Success", subTitle: msg, closeButtonTitle: "Ok",  colorStyle: int_red)
                                }
                                if let resumeURL = value.headShotResponse?.resume_file {
                                    
                                    self.userData.resume_file = resumeURL
                                    self.resumeDisplay()
                                    defaults.set(resumeURL, forKey: "ResumeURL")
                                    
                                }
                                defaults.set(true, forKey: "resume_setup")
                                
                                //                                self.navigationController?.pushViewController(nextVC, animated: true)
                                break
                            case 400:
                                alert.hideView()
                                
                                clearSession()
                                let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                let nav = redirectToRootViewController(vcName: vc)
                                self.present(nav, animated: true)
                                break
                            case 401:
                                alert.hideView()
                                if let new_token = value.signUpResponse?.new_token {
                                    UserDefaults.standard.set(new_token, forKey: "token")
                                    self.setResumeApi()
                                }else {
                                    if let error = value.message {
                                        let alertview = SCLAlertView()
                                        alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                    }
                                }
                                break
                                
                            default:
                                alert.hideView()
                                
                                if let error = value.message {
                                    let alertview = SCLAlertView()
                                    alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                                }
                                break
                            }
                        }
                        break
                    case .failure(let error):
                        print("error: \(error.localizedDescription)")
                        
                        alert.hideView()
                        
                        let alertview = SCLAlertView()
                        alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                        break
                    }
                    
                }
            case .failure(let encodingError):
                alert.hideView()
                let alertview = SCLAlertView()
                
                alertview.showError("Error", subTitle: encodingError.localizedDescription, closeButtonTitle: "Ok")
                //print("Request failed with error: \(error)")
                break
            }

        }
        
    }
}
