//
//  LoginVC.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/15/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SCLAlertView
import AlamofireObjectMapper
import ReachabilitySwift

class LoginVC: UIViewController {
    
    @IBOutlet var joinNowLbl: UILabel!
    @IBOutlet var emailTxt: UITextField!
    @IBOutlet var passwordTxt: UITextField!
    @IBOutlet var loginBtn: UIButton!
    var isFromSignUp : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.isHidden = false
        configureNavigationBar()
        //        self.view.backgroundColor = hexStringToUIColor(hex: "E4E1DA")
        loginBtn.backgroundColor = UIColor.white
        loginBtn.setTitleColor(hexStringToUIColor(hex: redColor), for: .normal)
        if UIDevice.current.userInterfaceIdiom == .pad {
            loginBtn.frame.size.height = 70
        }
        joinNowLbl.textColor = hexStringToUIColor(hex: redColor)
        viewCorners.addBorderCornerTobutton(view: loginBtn, color: UIColor.white)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    //Set NavigationBar
    func configureNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "OpenSans-Semibold", size: 18.0)]
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        let navigationItem = self.navigationItem
        navigationItem.title = ""
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @IBAction func loginBtnclicked(_ sender: Any) {
        self.view.endEditing(true)
        testValidation()
    }
    
    @IBAction func joinNowBtnClicked(_ sender: Any) {
        self.view.endEditing(true)
        
        if isFromSignUp {
            self.navigationController?.popViewController(animated: true)
        }else {
            let vc = storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
            vc.isFromLogin = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func forgotPasswordBtnClicked(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func testValidation()  {
        let email = emailTxt.text!
        let password = passwordTxt.text!
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        if !checkLength(testStr: email){
            alert.hideView()
            emptyFieldError(str: "email.")
        } else if !checkLength(testStr: password) {
            alert.hideView()
            emptyFieldError(str: "password.")
        }else if isValidEmail(testStr: email) {
            if validatePassword(testStr: password)
            {
                
                if (Reachability()?.isReachable)! {
                    
                    //                        if self.loginBtn.title(for: .normal) == "Save" {
                    //                            updateUserProfile()
                    //                        } else {
                    self.userLogin(email_id: email, password: password)
                    
                    //                        }
                } else {
                    alert.hideView()
                    noInternetMsgDialog()
                }
                
                
            }else
            {
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: passwordlengthMessage, closeButtonTitle: "Ok")
                
            }
        }else {
            alert.hideView()
            let alertview = SCLAlertView()
            alertview.showError("Error", subTitle: invalidEmailMessage, closeButtonTitle: "Ok")
        }
        
    }
    
    func userLogin(email_id: String, password: String) {
        //Object to be serialized to JSON
        let user = LoginRequestModel(email_id: email_id, password: password, device_info: deviceInfo)
        
        
        //convert object to dictionary
        let data_temp = Mapper<LoginRequestModel>().toJSON(user)
        print("data:- \(data_temp)")
        alamofireManager.request(doLoginForUser, method: .post, parameters: data_temp, encoding: JSONEncoding(options: [])).responseObject{ (response: DataResponse<ResponseModel> )in
            //                    let responseClass = response.result.value
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        if let token = value.loginResponse?.token {
                            UserDefaults.standard.set(token, forKey: "token")
                        }
                        
                        if let is_first_time = value.loginResponse?.user_detail?.is_first_time {
                            UserDefaults.standard.set(is_first_time, forKey: "isFirstSwipe")
                        }
                        
                        if let userID = value.loginResponse?.user_detail?.user_id {
                            UserDefaults.standard.set(userID, forKey: "userId")
                            //                                    registerDevice(user_id: userID)
                            //                                    registerDevice()
                            
                        }
                        if let userChatID = value.loginResponse?.user_detail?.chat_id {
                            defaults.set(userChatID, forKey: "userChatId")
                        }
                        if let user_detail = value.loginResponse?.user_detail {
                            defaults.set(user_detail.toJSON(), forKey: "user_detail")
                            QBCore.instance().qb_DEFAULT_PASSOWORD = default_password
                            appDelegate.login()
                            delete24HoursDialogs()
                        }
                        
                        if let headshot_setup = value.loginResponse?.user_detail?.headshot_setup {
                            if headshot_setup {
                                defaults.set(headshot_setup, forKey: "headshot_setup")
                                
                                //                                        if let resume_setup = value.loginResponse?.user_detail?.resume_setup {
                                //                                            if resume_setup {
                                //                                                defaults.set(resume_setup, forKey: "resume_setup")
                                
                                if let skillSetup = value.loginResponse?.user_detail?.skill_setup {
                                    defaults.set(skillSetup, forKey: "skillSetup")
                                    
                                    if !skillSetup {
                                        
                                        let story = UIStoryboard(name: "Main", bundle: nil)
                                        let nextVC = story.instantiateViewController(withIdentifier: "AddSkillsVC") as! AddSkillsVC
//                                        self.navigationController?.pushViewController(nextVC, animated: true)
                                        let nav = redirectToRootViewController(vcName: nextVC)
                                        self.present(nav, animated: true)

                                    }else {
                                        //Home
                                        let story = UIStoryboard(name: "Main", bundle: nil)
                                        let nextVC = story.instantiateViewController(withIdentifier: "MyTabBarViewController")
                                        self.navigationController?.present(nextVC, animated: true) //self.navigationController?.pushViewController(nextVC, animated: true)
                                    }
                                }
                                //                                            }else {
                                //                                                let story = UIStoryboard(name: "Main", bundle: nil)
                                //                                                let nextVC = story.instantiateViewController(withIdentifier: "AddProfileVC") as! AddProfileVC
                                //                                                if let imgStr = value.loginResponse?.user_detail?.headshot_compressed_image {
                                //                                                    nextVC.profileImgString = imgStr
                                //                                                }
                                //                                                if let full_name = value.loginResponse?.user_detail?.user_name {
                                //                                                    nextVC.userName = full_name
                                //                                                    defaults.set(full_name, forKey: "user_name")
                                //                                                }
                                //                                                self.navigationController?.pushViewController(nextVC, animated: true)
                                //                                            }
                                //                                        }
                                
                            }else {
                                let story = UIStoryboard(name: "Main", bundle: nil)
                                let nextVC = story.instantiateViewController(withIdentifier: "AddProfileVC") as! AddProfileVC
                                if let imgStr = value.loginResponse?.user_detail?.headshot_compressed_image {
                                    nextVC.profileImgString = imgStr
                                }
                                if let full_name = value.loginResponse?.user_detail?.user_name {
                                    if let last_name = value.loginResponse?.user_detail?.last_name {
                                        nextVC.userName = full_name + " " + last_name
                                        
                                    } else {
                                        nextVC.userName = full_name
                                        
                                    }
                                    defaults.set(full_name, forKey: "first_name")
                                    defaults.set(nextVC.userName, forKey: "user_name")
                                    
                                }
                                self.navigationController?.pushViewController(nextVC, animated: true)
                            }
                        }
                        
                        break
                        
                    case 400:
                        alert.hideView()
                        
                        clearSession()
                        let story = UIStoryboard(name: "Main", bundle: nil)
                        let vc = story.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                        
                    case 401:
                        alert.hideView()
                        if let new_token = value.loginResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.userLogin(email_id: email_id, password: password)
                        }else {
                            if let error = value.message {
                                let alertview = SCLAlertView()
                                
                                alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                            }
                        }
                        break
                    default:
                        alert.hideView()
                        
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        break
                    }
                }
                break
            case .failure(let error):
                print("error:- \(error.localizedDescription)")
                alert.hideView()
                let alertview = SCLAlertView()
                alertview.showError("Error", subTitle: serverError, closeButtonTitle: "Ok")
                
                break
                
            default:
                alert.hideView()
                break
                
            }
            
        }
        
    }
    
}
