//
//  ReaderprofileVC.swift
//  need_a_reader
//
//  Created by ob_apple_3 on 18/09/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import UIKit
import AlamofireObjectMapper
import Alamofire
import SCLAlertView
import ReachabilitySwift
import ObjectMapper
import SDWebImage
class ReaderprofileVC: UIViewController {
    
    
    @IBOutlet var collectionHeightConstant: NSLayoutConstraint!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var profileImgHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var tapingRateLbl: UILabel!
    @IBOutlet weak var editingRateLbl: UILabel!
    @IBOutlet weak var finalRateLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userPriceLbl: UILabel!
    @IBOutlet var totalMutualFrndLbl: UILabel!
    @IBOutlet var mutualLblHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var finalLbl: UILabel!
    
    var mutualFrndList : [User_detail] = []
    var userDetail : User_detail!
    var mutualFrndResponse : contactResponseModel!
    
    let layout = UICollectionViewFlowLayout()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigationBar()
        
        layout.itemSize = CGSize(width: 100, height: 120)
        layout.scrollDirection = .horizontal
        // Then initialize collectionView
        collectionView.collectionViewLayout = layout
        //        let collectionView = UICollectionView(frame: CGRect(x:20, y: 0, width: self.view.frame.size.width - 40, height: 120), collectionViewLayout: layout)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isPagingEnabled = true
        
         profileImgHeightConstant.constant = self.view.frame.size.height * 0.55
        viewCorners.addCornerToView(view:outerView, value: 10)
        viewCorners.addShadowToView(view: outerView, value: 2.0)
        
        profileImg.sd_setShowActivityIndicatorView(true)
        profileImg.sd_setIndicatorStyle(.gray)

        
        if let img = userDetail.headshot_thumbnail_image {
            profileImg.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
        }
        if let price = userDetail.hourly_rate {
            
            self.userPriceLbl.text = " $\(price)" 
        }
        if let taping = userDetail.taping_rate {
            self.tapingRateLbl.text = "Taping Rate:\(" $" + String(format: "%.2f", Float(taping)!))"
        }
        if let editing = userDetail.editing_rate {
            self.editingRateLbl.text = "Editing Rate:\(" $" + String(format: "%.2f", Float(editing)!))"
        }
     
        
        let finalrate = Float(Float(userDetail.hourly_rate!)+Float(userDetail.taping_rate!)!+Float(userDetail.editing_rate!)!)
            print(finalrate)
        
        let stringValue = "\(" $" + String(format: "%.2f", Float(finalrate)))"
        
        //let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringValue)
        //attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: NSMakeRange(0, 21))
        
       // self.finalRateLbl.text = "Final Reader Rate:- $\(finalrate)"
       // self.finalRateLbl.attributedText = attributedString
        self.finalLbl.text = stringValue
        if let name = userDetail.user_name {
            self.userNameLbl.text = name
        }
        
        //tabBarController?.tabBar.isHidden = true
        
        self.getUserMutualFriends()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Set NavigationBar
//    func configureNavigationBar() {
//
//        setNavigaion(vc: self)
//        let navigationItem = self.navigationItem
//        navigationItem.title = ""
//        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
//        navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
//        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
//
//    }
    @objc func backBtnPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    func configureNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "OpenSans-Semibold", size: 18.0)]
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        navigationController?.navigationBar.tintColor = hexStringToUIColor(hex: redColor)
        let navigationItem = self.navigationItem
        navigationItem.title = "PROFILE"

        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        let backBtn = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))

        if #available(iOS 11.0, *) {
            let chatwidthConstraint = backBtn.widthAnchor.constraint(equalToConstant: 44)
            let chatheightConstraint = backBtn.heightAnchor.constraint(equalToConstant: 44)
            chatheightConstraint.isActive = true
            chatwidthConstraint.isActive = true
            backBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 8, 16)
        }else {
            backBtn.contentEdgeInsets = UIEdgeInsetsMake(8, 0, 6, 12)
        }
        backBtn.setImage(UIImage(named: "ic_back"), for: .normal)
        backBtn.addTarget(self, action: #selector(backBtnPressed(_:)), for: .touchUpInside)
        let backButton = UIBarButtonItem(customView: backBtn)
        navigationItem.leftBarButtonItem = backButton
    }

    func hideMutualFrndView() {
        collectionHeightConstant.constant = 0
        collectionView.isHidden = true
        totalMutualFrndLbl.isHidden = true
        mutualLblHeightConstant.constant = 0
    }
    func getUserMutualFriends() {
        alert.showWait(loadingTitle, subTitle: "", colorStyle: int_red)
        
        let user = NotificationRequestModel(receiver_user_id: userDetail.user_id)
        let data_temp = Mapper<NotificationRequestModel>().toJSON(user)
        
        alamofireManager.request(getMutualFriendList, method: .post, parameters: data_temp, encoding: JSONEncoding(options: []), headers: tokenHeader).responseObject{ (response: DataResponse<ResponseModel> )in
            
            switch response.result
            {
            case .success(let value):
                if let code = value.code {
                    print("Code:- \(code)")
                    switch code {
                    case 200:
                        alert.hideView()
                        if let data = value.contactReponse {
                            self.mutualFrndResponse = data
                            if let arr = data.mutualFrndList {
                                if !arr.isEmpty {
                                    self.mutualFrndList = arr
                                    self.collectionHeightConstant.constant = 120
                                    self.mutualLblHeightConstant.constant = 15
                                    self.collectionView.isHidden = false
                                    self.totalMutualFrndLbl.isHidden = false
                                    self.totalMutualFrndLbl.text = "\(arr.count) MUTUAL FRIENDS"
                                    self.collectionView.reloadData()
                                } else {
                                    self.hideMutualFrndView()
                                }
                            } else {
                                self.hideMutualFrndView()
                            }
                        } else {
                            self.hideMutualFrndView()
                        }
                        print("Success")
                        break
                    case 400:
                        alert.hideView()
                        clearSession()
                        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        let nav = redirectToRootViewController(vcName: vc)
                        self.present(nav, animated: true)
                        break
                    case 401:
                        alert.hideView()
                        if let new_token = value.settingResponse?.new_token {
                            UserDefaults.standard.set(new_token, forKey: "token")
                            self.getUserMutualFriends()
                        }else {
                            clearSession()
                            let vc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                            let nav = redirectToRootViewController(vcName: vc)
                            self.present(nav, animated: true)
                        }
                        break
                    default:
                        alert.hideView()
                        if let error = value.message {
                            let alertview = SCLAlertView()
                            alertview.showError("Error", subTitle: error, closeButtonTitle: "Ok")
                        }
                        self.hideMutualFrndView()
                        
                        break
                    }
                }
                break
            case .failure(let error):
                alert.hideView()
                SCLAlertView().showError(errorTitle, subTitle: serverError)
                //                    self.addErrorView()
                print("Request failed with error: \(error.localizedDescription)")
                self.hideMutualFrndView()
                
                break
            }
            }.responseJSON { response in
                print("response mutual Friend:- \(response.result.value)")
        }
    }
    

}
extension ReaderprofileVC : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mutualFrndList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! FriendCellVC
        cell.profileImgview.sd_setShowActivityIndicatorView(true)
        cell.profileImgview.sd_setIndicatorStyle(.gray)
        //        viewCorners.addCornerToView(view: cell.profileImgview)
        viewCorners.addBorderCornerTobutton(view: cell.profileImgview, color: hexStringToUIColor(hex: purpleColor))
        
        if let img = mutualFrndList[indexPath.row].headshot_compressed_image {
            cell.profileImgview.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "ic_defaultProfile"))
        }

        if let name = mutualFrndList[indexPath.row].user_name {
            cell.userNameLbl.text = name
        } else {
            cell.userNameLbl.text = ""
        }
        
        if let last_name = mutualFrndList[indexPath.row].last_name {
            cell.userNameLbl.text =  cell.userNameLbl.text! + " " + last_name
        } else {
            //cell.userNameLbl.text = ""
        }
        
        return cell
    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    //        return 0
    //    }
}
