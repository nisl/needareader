
//
//  AppDelegate.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/14/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import Alamofire
import Fabric
import Crashlytics
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import SwiftyStoreKit
import ObjectMapper
import SCLAlertView
import Stripe
//import ReachabilitySwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, NotificationServiceDelegate, QBCoreDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var sessionManager = Alamofire.SessionManager.default
    var dataSource: UsersDataSource!
    var session: QBRTCSession!
//    var callUUID: UUID!
//    var backgroundTask: UIBackgroundTaskIdentifier!
//    var opponentIDs: [NSNumber] = []
//    let reachability = Reachability()!


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        //Fabric crashalytics
        Fabric.with([Crashlytics.self])
        
       

        self.window = UIWindow(frame: UIScreen.main.bounds)
        STPPaymentConfiguration.shared().publishableKey = publicKey 
        application.registerForRemoteNotifications()
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                UITabBar.appearance().selectionIndicatorImage = getImageWithColorPosition(color: hexStringToUIColor(hex: redColor), size: CGSize(width:(self.window?.frame.size.width)!/5,height: 80), lineSize: CGSize(width:(self.window?.frame.size.width)!/5, height:2))
                break
            default:
                UITabBar.appearance().selectionIndicatorImage = getImageWithColorPosition(color: hexStringToUIColor(hex: redColor), size: CGSize(width:(self.window?.frame.size.width)!/5,height: 49), lineSize: CGSize(width:(self.window?.frame.size.width)!/5, height:2))
                break
            }
        } else {
            UITabBar.appearance().selectionIndicatorImage = getImageWithColorPosition(color: hexStringToUIColor(hex: redColor), size: CGSize(width:(self.window?.frame.size.width)!/5,height: 49), lineSize: CGSize(width:(self.window?.frame.size.width)!/5, height:2))
        }
        
        
        //Quickblox
        QBSettings.applicationID = qb_app_id
        QBSettings.authKey = qb_auth_key
        QBSettings.authSecret = qb_auth_secret
        QBSettings.accountKey = qb_account_key

        QBSettings.logLevel = .debug
        QBSettings.enableXMPPLogging()
        
        QBRTCConfig.setAnswerTimeInterval(kQBAnswerTimeInterval)
        QBRTCConfig.setDialingTimeInterval(kQBDialingTimeInterval)
        QBRTCConfig.setStatsReportTimeInterval(1.0)
        
        
        QBRTCClient.initializeRTC()
        QBChat.initialize()
        Settings.instance()
        QBCore.instance().add(self)
        self.retrieveAllUsers(fromPage: 1)

//        IQKeyboardManager
        IQKeyboardManager.sharedManager().enable = true

        GMSServices.provideAPIKey(googleApiKey)
        GMSPlacesClient.provideAPIKey(googleApiKey)
        defaults.removeObject(forKey: "locationCoordinatesArr")
        defaults.removeObject(forKey: "locationAddress")
        
        DispatchQueue.main.async {
            if #available(iOS 10.0, *) {
                print("register for UNUser")
                //register APNs for iOS 10 and delegate method becomes userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
                UNUserNotificationCenter.current().delegate = self
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound], completionHandler:
                    {granted,error in
                        //                        application.registerForRemoteNotifications()
                        if granted {
                            application.registerForRemoteNotifications()
                        }else {
                            print("UserNotifications error : \(error?.localizedDescription)")
                        }
                })
            }else {
                print("register for UIUser")
                // register APNs for all OS
                let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
                let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
                application.registerUserNotificationSettings(pushNotificationSettings)
                application.registerForRemoteNotifications()
            }
        }
        
        
        

//        completeAPITransaction()
        //NavigationBar
        UINavigationBar.appearance().barTintColor = hexStringToUIColor(hex: "F5F3F4")
        UINavigationBar.appearance().tintColor = hexStringToUIColor(hex: redColor)
//        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: ReachabilityChangedNotification, object: reachability)
//        do{
//            try reachability.startNotifier()
//        }catch{
//            print("could not start reachability notifier")
//        }
       
        let remoteNotification: NSDictionary! = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary
        if (remoteNotification != nil) {
            
            if let userDetail = remoteNotification["aps"] as? [String : Any]{
                if let userDetailAlert = userDetail["receiver_user_id"]{
                     manageNotificationWhenAppTerminated(userDetail : userDetail)
                } else {
                    ServicesManager.instance().notificationService.pushDialogID = remoteNotification["SA_STR_PUSH_NOTIFICATION_DIALOG_ID".localized] as? String
                    DispatchQueue.main.async {
                        ServicesManager.instance().notificationService.handlePushNotificationWithDelegate(delegate: self)
                    }
                }
            }
            
//                    let nextVC = storyBoard.instantiateViewController(withIdentifier: "MyTabBarViewController") as! MyTabBarViewController
//                    nextVC.selectedIndex = 2
//                    self.window?.rootViewController = nextVC
//                    self.window?.makeKeyAndVisible()
        }
            let headshot_setup = defaults.bool(forKey: "headshot_setup")
            let resume_setup = defaults.bool(forKey: "resume_setup")
            let skillSetup = defaults.bool(forKey: "skillSetup")
            if headshot_setup {
                if skillSetup {
                    let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabBarViewController") as! MyTabBarViewController
                    self.window?.rootViewController = vc
                    self.window?.makeKeyAndVisible()
                   
                } else {
                    let vc = storyBoard.instantiateViewController(withIdentifier: "AddSkillsVC") as! AddSkillsVC
                    let nav = UINavigationController(rootViewController: vc)
                    nav.navigationBar.isTranslucent = false
                    nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                    nav.navigationBar.shadowImage = UIImage()
                    nav.interactivePopGestureRecognizer?.isEnabled = false
                    self.window?.rootViewController = nav
                    self.window?.makeKeyAndVisible()
                    
                }
            }else {
                if let token = defaults.value(forKey: "token") as? String {
                    let vc = storyBoard.instantiateViewController(withIdentifier: "AddProfileVC") as! AddProfileVC
                    let nav = UINavigationController(rootViewController: vc)
                    nav.navigationBar.isTranslucent = false
                    nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                    nav.navigationBar.shadowImage = UIImage()
                    nav.interactivePopGestureRecognizer?.isEnabled = false
                    self.window?.rootViewController = nav
                    self.window?.makeKeyAndVisible()
                }else {
                    let vc = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    let nav = UINavigationController(rootViewController: vc)
                    nav.navigationBar.isTranslucent = false
                    nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                    nav.navigationBar.shadowImage = UIImage()
                    nav.interactivePopGestureRecognizer?.isEnabled = false
                    self.window?.rootViewController = nav
                    self.window?.makeKeyAndVisible()
                }
            }
        
        return true
    }

//    @objc func reachabilityChanged(note: Notification) {
//
//        let reachability = note.object as! Reachability
//
//        if reachability.isReachable {
//            delete24HoursDialogs()
//        }
//    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//        ServicesManager.instance().chatService.disconnect(completionBlock: nil)
        if QBChat.instance.isConnected {
            QBChat.instance.disconnect { (error : Error?) in
                print("disconnect")
                ServicesManager.instance().chatService.disconnect(completionBlock: nil)
            }
        }
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print("forground")
        print(QBChat.instance.isConnected)
        print(QBCore.instance().isAuthorized)
        if !QBChat.instance.isConnected && QBCore.instance().isAuthorized {
            QBCore.instance().loginWithCurrentUser()
        }
        ServicesManager.instance().chatService.connect(completionBlock: nil)
        
        if defaults.value(forKey: "user_detail") != nil {
            print(defaults.value(forKey: "user_detail"))
            if !QBChat.instance.isConnected {
                QBCore.instance().qb_DEFAULT_PASSOWORD = default_password
                QBCore.instance().loginWithCurrentUser()
            }
            appDelegate.dataSource = UsersDataSource(currentUser: QBCore.instance().currentUser)
            CallKitManager.instance.usersDatasource = appDelegate.dataSource
            self.getOpponentsUsers()
            delete24HoursDialogs()
        }
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        if defaults.value(forKey: "user_detail") != nil {
            print(defaults.value(forKey: "user_detail"))
            if !QBChat.instance.isConnected {
                QBCore.instance().qb_DEFAULT_PASSOWORD = default_password
                QBCore.instance().loginWithCurrentUser()
            }
            appDelegate.dataSource = UsersDataSource(currentUser: QBCore.instance().currentUser)
            CallKitManager.instance.usersDatasource = appDelegate.dataSource
            self.getOpponentsUsers()
            delete24HoursDialogs()
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        
        self.saveContext()
    }

    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})


        if let deviceToken = defaults.value(forKey: "devicetoken") {
            UserDefaults.standard.set(deviceTokenString, forKey: "devicetoken")
            if deviceTokenString == deviceToken as! String {
                let serverSync = defaults.value(forKey: "server_sync") as! Bool
                if !serverSync {
                    if (defaults.value(forKey: "token") != nil) {
//                        if let id = defaults.value(forKey: "userId") {
                            registerDevice()

//                        }
                    }
                }
            }else {
                defaults.set(false, forKey: "server_sync")
                if (defaults.value(forKey: "token") != nil) {
//                    if let id = defaults.value(forKey: "userId") {
//                        registerDevice(user_id: id as! Int)
//
//                    }
                     registerDevice()
                }
            }

        }else {
            UserDefaults.standard.set(deviceTokenString, forKey: "devicetoken")

            defaults.set(false, forKey: "server_sync")

            if (defaults.value(forKey: "token") != nil) {
//                if let id = defaults.value(forKey: "userId") {
//                    registerDevice(user_id: id as! Int)
//
//                }
                 registerDevice()
            }
        }
        QBCore.instance().registerForRemoteNotifications(withDeviceToken: deviceToken)
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("Did receive remote notification \(userInfo)")
        print("my push is: %@", userInfo)
     
//        let aps = userInfo["aps"] as! [String: AnyObject]
        if let userDetail = userInfo["aps"] as? [String : Any]{
            if "\(userDetail["messageType"]!)" == "4" {
                print("Time:- \("\(userDetail["notification_time"]!)")!)")
                setPaymentNotification(jobId: Int("\(userDetail["job_id"]!)")!, notificationDate: convertstringToDate(str: "\(userDetail["notification_time"]!)"))
            }
            if "\(userDetail["messageType"]!)" == "5" {
                deleteNotificationByJobId(jobId: Int("\(userDetail["job_id"]!)")!)
            }
            if let userDetailAlert = userDetail["receiver_user_id"]  {
                manageNotificationWhenAppRunning(userDetail: userDetail)
                
            }
            
        }
        guard application.applicationState == UIApplicationState.inactive else {
            return
        }
        
        guard let dialogID = userInfo["SA_STR_PUSH_NOTIFICATION_DIALOG_ID".localized] as? String else {
            return
        }
        
        guard !dialogID.isEmpty else {
            return
        }
        
        
        let dialogWithIDWasEntered: String? = ServicesManager.instance().currentDialogID
        if dialogWithIDWasEntered == dialogID {
            return
        }
        
        ServicesManager.instance().notificationService.pushDialogID = dialogID
        
        // calling dispatch async for push notification handling to have priority in main queue
        DispatchQueue.main.async {
            
            ServicesManager.instance().notificationService.handlePushNotificationWithDelegate(delegate: self)
        }
    }

 
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // Called to let your app know which action was selected by the user for a given notification.
        let userInfo = response.notification.request.content.userInfo
        print("\(userInfo)")
        
//        if "\(userDetail["messageType"]!)" == "4" {
//            print("Time:- \("\(userDetail["notification_time"]!)")!)")
//            setPaymentNotification(jobId: Int("\(userDetail["job_id"]!)")!, notificationDate: convertstringToDate(str: "\(userDetail["notification_time"]!)"))
//        }
//        if "\(userDetail["messageType"]!)" == "5" {
//            deleteNotificationByJobId(jobId: Int("\(userDetail["job_id"]!)")!)
//        }
//        if let userDetailAlert = userDetail["receiver_user_id"]  {
//            manageNotificationWhenAppRunning(userDetail: userDetail)
//
//        }

            if let userDetail = userInfo["aps"] as? [String : Any]{
                
                if let messageType = userDetail["messageType"] {
                    print(messageType)
                    
                    if "\(userDetail["messageType"]!)" == "4" {
                        print("Time:- \("\(userDetail["notification_time"]!)")!)")
                        setPaymentNotification(jobId: Int("\(userDetail["job_id"]!)")!, notificationDate: convertstringToDate(str: "\(userDetail["notification_time"]!)"))
                    }
                    if "\(userDetail["messageType"]!)" == "5" {
                        deleteNotificationByJobId(jobId: Int("\(userDetail["job_id"]!)")!)
                    }
                    if let userDetailAlert = userDetail["receiver_user_id"]  {
                        manageNotificationWhenAppRunning(userDetail: userDetail)
                        
                    }
                }

            }
        
        guard let dialogID = userInfo["SA_STR_PUSH_NOTIFICATION_DIALOG_ID".localized] as? String else {
            return
        }
        
        guard !dialogID.isEmpty else {
            return
        }
        
        
        let dialogWithIDWasEntered: String? = ServicesManager.instance().currentDialogID
        if dialogWithIDWasEntered == dialogID {
            return
        }
        
        ServicesManager.instance().notificationService.pushDialogID = dialogID
        
        // calling dispatch async for push notification handling to have priority in main queue
        DispatchQueue.main.async {
            
            ServicesManager.instance().notificationService.handlePushNotificationWithDelegate(delegate: self)
        }
        
    }
    
    @available(iOS 10.0, *)
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //Called when a notification is delivered to a foreground app.
        
        let userInfo = notification.request.content.userInfo as? NSDictionary
        print("userInfo:- \(userInfo)")
        if let userDetail = userInfo!["aps"] as? [String : Any]{
            if "\(userDetail["messageType"]!)" == "4" {
                print("Time:- \("\(userDetail["notification_time"]!)")!)")
                setPaymentNotification(jobId: Int("\(userDetail["job_id"]!)")!, notificationDate: convertstringToDate(str: "\(userDetail["notification_time"]!)"))
            }
            if "\(userDetail["messageType"]!)" == "5" {
                deleteNotificationByJobId(jobId: Int("\(userDetail["job_id"]!)")!)
            }
        }
//        if let userDetail = userInfo!["aps"] as? [String : Any]{
//            if let userDetailAlert = userDetail["receiver_user_id"]  {
//                let dateValue = defaults.object(forKey: "terminationTime") as? Date
//                let rec_id = "\(userDetail["action_user_id"]!)"
//
//                if "\(userDetail["messageType"]!)" == "2" || "\(userDetail["messageType"]!)" == "3" || "\(userDetail["messageType"]!)" == "4" {
//                    if  let requestedId = defaults.value(forKey: "receiverUserId") as? String {
//                        //                        if  let rec_id = userDetail["action_user_id"] as? String {
//                        if requestedId == rec_id {
//                            defaults.set(true , forKey: "userAcceptedYourRequest")
//                            defaults.set(false, forKey: "isWaiting")
//                        }
//                        //                        }
//                    }
//
//                } else {
//                    defaults.set(true , forKey: "userAcceptedYourRequest")
//                    defaults.set(false, forKey: "isWaiting")
//                }
//            }
//
//        }
        completionHandler([.alert, .sound])
        
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("devicetoken register error : \(error)")
    }
    
    // MARK: NotificationServiceDelegate protocol
    
    func notificationServiceDidStartLoadingDialogFromServer() {
    }
    
    func notificationServiceDidFinishLoadingDialogFromServer() {
    }
    
    func notificationServiceDidSucceedFetchingDialog(chatDialog: QBChatDialog!) {
        let chatController = storyBoard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        chatController.dialog = chatDialog
        let nav = UINavigationController(rootViewController: chatController)
        if self.window?.rootViewController is MyTabBarViewController {
            let tabController = self.window?.rootViewController as! MyTabBarViewController
            tabController.viewControllers![0].present(nav, animated: true, completion: nil)

        }else {
            let navigatonController: UINavigationController! = self.window?.rootViewController as! UINavigationController
            
            
            let dialogWithIDWasEntered = ServicesManager.instance().currentDialogID
            if !dialogWithIDWasEntered.isEmpty {
                // some chat already opened, return to dialogs view controller first
                //            navigatonController.popViewController(animated: false);
            }
            
            //        navigatonController.pushViewController(chatController, animated: true)
            navigatonController.present(nav, animated: true, completion: nil)
        }
    }
    
    func notificationServiceDidFailFetchingDialog() {
        
    }

    func getOpponentsUsers() {
        
        let users = ServicesManager.instance().usersService.usersMemoryStorage.unsortedUsers()

//        if users.count > 0 {
//
//            guard let users = ServicesManager.instance().sortedUsers() else {
//                print("No cached users")
//                return
//            }
//
//            appDelegate.dataSource.setUsers(users)
//        }
//        else {

            //            SVProgressHUD.show(withStatus: "SA_STR_LOADING_USERS".localized)

            // Downloading users from Quickblox.

            ServicesManager.instance().downloadCurrentEnvironmentUsers(successBlock: { (users) -> Void in

                guard let unwrappedUsers = users else {

                    //                    SVProgressHUD.showError(withStatus: "No users downloaded")
                    return
                }

                //                SVProgressHUD.showSuccess(withStatus: "SA_STR_COMPLETED".localized)

                self.dataSource.setUsers(unwrappedUsers)


            }, errorBlock: { (error) -> Void in

                //                SVProgressHUD.showError(withStatus: error.localizedDescription)
            })
//        }
    }

    //Quickblox
    func login() {
        if QBCore.instance().currentUser.fullName != nil {
            QBCore.instance().loginWithCurrentUser()
        }else {
            if let user_detail = current_user_detail {
                QBCore.instance().login(withLogin: user_detail.email_id!)
            }
        }
    }

    //QBCoreDelegate
    func coreDidLogin(_ core: QBCore) {
        self.logInChatWithUser(user: core.currentUser)
    }
    
    func core(_ core: QBCore, error: Error, domain: ErrorDomain) {
        var infoText = error.localizedDescription
//        SCLAlertView().showInfo("Error", subTitle: infoText)
        if error._code == NSURLErrorNotConnectedToInternet {
            infoText = "Please check your Internet connection"
        }else if core.networkStatus() != QBNetworkStatus.notReachable {
            if domain == ErrorDomain.signUp || domain == ErrorDomain.logIn {
                self.login()
            }
        }
    }
    
    func logInChatWithUser(user: QBUUser) {
        ServicesManager.instance().logIn(with: user, completion:{
            [unowned self] (success, errorMessage) -> Void in
            
            guard success else {
                return
            }
            ServicesManager.instance().chatService.connect(completionBlock: nil)
        })
    }
    
    // Fatch all Quickblox users
    var userNumber = Int()
    func retrieveAllUsers(fromPage page: Int)  {
        QBRequest.users(for: QBGeneralResponsePage(currentPage: UInt(page), perPage: 100), successBlock: { response, pageInformation, users in
            
            self.userNumber += users.count
//            print(users)
            AllQuickbloxUser += users
//            print(pageInformation)
//            print(pageInformation.totalEntries)
//            print(self.userNumber)
            if pageInformation.totalEntries > self.userNumber {
                self.retrieveAllUsers(fromPage: Int(pageInformation.currentPage + 1))
                
            }else {
                print("All user : \(self.userNumber)")
              //  print(AllQuickbloxUser)
            }
            
        }, errorBlock: { response in
            // Handle error
            print("Error")
            print(response)
        })
    }

 
    // Drow undeline selected tab
    func getImageWithColorPosition(color: UIColor, size: CGSize, lineSize: CGSize) -> UIImage {
        let rect = CGRect(x:0, y: 0, width: size.width, height: size.height)
        let rectLine = CGRect(x:0, y:0,width: lineSize.width,height: lineSize.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.clear.setFill()
        UIRectFill(rect)
        color.setFill()
        UIRectFill(rectLine)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    // MARK: - Core Data stack
   @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "need_a_reader")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
         if #available(iOS 10.0, *) {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
         }else {
            // Fallback on earlier versions
        }
        
        
    }
    
//    func completeAPITransaction() {
//        SwiftyStoreKit.completeTransactions(atomically: true, completion: { purchases in
//            for itemPurchase in purchases {
//                if itemPurchase.transaction.transactionState == .purchased || itemPurchase.transaction.transactionState == .restored {
//                    if itemPurchase.needsFinishTransaction {
//                        // Deliver content from server, then:
//                        SwiftyStoreKit.finishTransaction(itemPurchase.transaction)
//
//                    }
//                    print("purchased: \(itemPurchase.productId)")
//                }
//            }
//        })
//    }
    
    func manageNotificationWhenAppRunning(userDetail : [String : Any]) {
        let rec_id = "\(userDetail["action_user_id"]!)"
        let messageType = "\(userDetail["messageType"]!)"
        print("rec_id:- \(rec_id)")
        if self.window?.rootViewController is MyTabBarViewController {
            let tabController = self.window?.rootViewController as! MyTabBarViewController
            if UIApplication.shared.applicationState == .inactive {
                tabController.selectedIndex = 2
            } else if UIApplication.shared.applicationState == .active {
//                if  let requestedId = defaults.value(forKey: "receiverUserId") as? String {
//                    if requestedId == rec_id {
//                        if (messageType == "2") || (messageType == "3") {
//                            defaults.set(false, forKey: "isWaiting")
//                            defaults.set(true , forKey: "userAcceptedYourRequest")
//                        }
//                    } else {
//                        if (messageType == "1")   {
//                            defaults.set(false, forKey: "isWaiting")
//                            defaults.set(true , forKey: "userAcceptedYourRequest")
//                        } else {
//                            tabController.selectedIndex = 2
//                        }
//
//                    }
//                }
                tabController.selectedIndex = 2

            }
        }
        
        
    }
    
    func manageNotificationWhenAppTerminated(userDetail : [String : Any]) {
        print("userDetail :- \(userDetail)")
        let rec_id = "\(userDetail["action_user_id"]!)"
        let messageType = "\(userDetail["messageType"]!)"
//        let isProgressTimer = defaults.bool(forKey: "isTimerProgress")
        
        print("rec_id:- \(rec_id)")
        
//        let dateValue = defaults.object(forKey: "terminationTime") as? Date
        if self.window?.rootViewController is MyTabBarViewController {
            let tabController = self.window?.rootViewController as! MyTabBarViewController
            tabController.selectedIndex = 2

        }
//        else {
//            if (messageType == "2") || (messageType == "3") || (messageType == "4") {
//                if let requestedId = defaults.value(forKey: "receiverUserId") as? String {
//                    if requestedId == rec_id {
//                        defaults.set(true , forKey: "userAcceptedYourRequest")
//                        defaults.set(false, forKey: "isWaiting")
//                    }
//                }
//            } else {
//                defaults.set(true , forKey: "userAcceptedYourRequest")
//                defaults.set(false, forKey: "isWaiting")
//            }
//        }
    }

}



