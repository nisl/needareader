//
//  RatingViewCell.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/20/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit
import SwipeCellKit

class RatingViewCell: UITableViewCell {
    @IBOutlet var questionLbl: UILabel!
    @IBOutlet var likeLbl: UILabel!
    @IBOutlet var dislikeLbl: UILabel!
    @IBOutlet var totalVotesLbl: UILabel!
}


class JobViewCell : SwipeTableViewCell {
    @IBOutlet var outerView: UIView!

    @IBOutlet var userNameLbl: UILabel!
    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var jobDescriptionLbl: UILabel!
    @IBOutlet var userProfile: UIImageView!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var countLBL: UILabel!
}
class JobTextdetailCell : UITableViewCell {
    
    @IBOutlet weak var jobDetailTV: UITextView!
}
class JobdetailCell : UITableViewCell {
    
    @IBOutlet var outerView: UIView!
    @IBOutlet var userNameLbl: UILabel!
    @IBOutlet var userProfile: UIImageView!
    @IBOutlet weak var readImageview: UIImageView!
    @IBOutlet weak var chatBtn: UIButton!
}
class RatingCell : UITableViewCell {
    
    @IBOutlet var outerView: UIView!
    @IBOutlet var userNameLbl: UILabel!
    @IBOutlet var jobDescriptionLbl: UILabel!
    @IBOutlet var userProfile: UIImageView!
    @IBOutlet weak var viewBtn: UIButton!
    @IBOutlet weak var cameraImageView: UIImageView!
    
}

class notificationViewCell : SwipeTableViewCell {
    @IBOutlet var outerView: UIView!

    @IBOutlet var btnWidthConstant: NSLayoutConstraint!
    @IBOutlet var userStatus: UILabel!
    @IBOutlet var timeLbl: UILabel!
    @IBOutlet var viewBtn: UIButton!
    @IBOutlet var declineBtn: UIButton!
    @IBOutlet var userProfile: UIImageView!
}

class TappingCell : UITableViewCell {
    
    @IBOutlet var tappingBtn: UIButton!
    @IBOutlet var checkBoxImg: UIImageView!
    @IBOutlet var tappingLblTxt: UILabel!
}

class AddContactTableCellVC: UITableViewCell {
    
    @IBOutlet var contactAddBtn: UIButton!
    @IBOutlet var selectedImgView: UIView!
    @IBOutlet var profileImgview: UIImageView!
    @IBOutlet var userNameLbl: UILabel!
    
    @IBOutlet var friendStatusLbl: UILabel!
    @IBOutlet var addBtnImg: UIImageView!
}

