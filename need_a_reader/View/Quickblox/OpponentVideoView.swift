//
//  OpponentVideoView.swift
//  quickblox_video_call
//
//  Created by ob_apple_1 on 25/12/17.
//  Copyright © 2017 optimumbrew. All rights reserved.
//

import UIKit

class OpponentVideoView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    @IBOutlet weak var view: UIView!
    var videoView: UIView!
    @IBOutlet weak var placeholderImageView: UIImageView!
    var connectionState: QBRTCConnectionState!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var muteButton: UIButton!
    
    var didPressMuteButton: ((_ isMuted: Bool) -> Void)? = nil
    
    
    func unmutedImage() -> UIImage {
        if let image = UIImage(named: "ic-qm-videocall-dynamic-off") {
            return image
        }else {
            return UIImage()
        }
    }
    
    func mutedImage() -> UIImage {
        if let image = UIImage(named: "ic-qm-videocall-dynamic-on") {
            return image
        }else {
            return UIImage()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("OpponentVideoView", owner: self, options: nil)
        self.addSubview(view)
        view.frame = self.bounds
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.black
        self.statusLabel.backgroundColor = UIColor(red: 0.9441, green: 0.9441, blue: 0.9441, alpha: 0.350031672297297)
        self.statusLabel.text = ""
        
        self.muteButton.setImage(unmutedImage(), for: .normal)
        self.muteButton.setImage(mutedImage(), for: .selected)
        self.muteButton.isHidden = true
    }
    
    func setVideoView(_ videoView: UIView) {
        if self.videoView != videoView {
            if self.videoView != nil {
                self.videoView.removeFromSuperview()
            }
            self.videoView = videoView
            self.videoView.frame = self.bounds
            self.containerView.insertSubview(self.videoView, aboveSubview: self.statusLabel)
        }
    }
    
    func setConnectionState(_ connectionState: QBRTCConnectionState) {
        if self.connectionState != connectionState {
            self.connectionState = connectionState
            switch connectionState {
            case .new:
                self.statusLabel.text = "New"
            case .pending:
                self.statusLabel.text = "Pending"
            case .checking:
                self.statusLabel.text = "Checking"
            case .connecting:
                self.statusLabel.text = "Connecting"
            case .connected:
                self.statusLabel.text = "Connected"
            case .closed:
                self.statusLabel.text = "Closed"
            case .hangUp:
                self.statusLabel.text = "Hung Up"
            case .rejected:
                self.statusLabel.text = "Rejected"
            case .noAnswer:
                self.statusLabel.text = "No Answer"
            case .disconnectTimeout:
                self.statusLabel.text = "Time out"
            case .disconnected:
                self.statusLabel.text = "Disconnected"
            default:
                break
            }
            self.muteButton.isHidden = !(connectionState == .connected)
        }
    }
    
    
    @IBAction func PressMuteButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if self.didPressMuteButton != nil {
            self.didPressMuteButton!(sender.isSelected)
        }
    }
}
