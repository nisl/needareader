//
//  ContactCellVC.swift
//  Hmm
//
//  Created by ob_apple_2 on 1/26/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit

class ContactCellVC: UICollectionViewCell {
    
    @IBOutlet var selectedImgView: UIImageView!
    @IBOutlet var profileImgview: UIImageView!
    @IBOutlet var userNameLbl: UILabel!

    @IBOutlet var deleteBtn: UIButton!
    @IBOutlet var deleteImg: UIImageView!
}

class FriendCellVC: UICollectionViewCell {
    
    @IBOutlet var profileImgview: UIImageView!
    @IBOutlet var userNameLbl: UILabel!
    
}
class ReaderCell: UICollectionViewCell {
    
    @IBOutlet var profileImgview: UIImageView!
   
    
}
