//
//  ViewController.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 12/14/17.
//  Copyright © 2017 ob_apple_2. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureNavigationBar()
        isSettingChanged = false
        self.navigationController?.navigationBar.isHidden = false
    }

 
    //Set NavigationBar
    func configureNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: hexStringToUIColor(hex: redColor), NSAttributedStringKey.font: UIFont(name: "OpenSans-Semibold", size: 18.0)]
        //        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        let navigationItem = self.navigationItem
        navigationItem.title = "CHAT ROOM"
        
        self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: lightGrayColor)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
}

