//
//  Extension+needAReader.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 4/1/17.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//



import Foundation
import UIKit
import UserNotifications

extension DateFormatter
{
    func timeSince(from: NSDate, numericDates: Bool = false) -> String
    {
        let calendar = Calendar.current
//        print("Calendar:- \(calendar)")
        let now = NSDate()
//        print("now:- \(now)")
        let earliest = now.earlierDate(from as Date)
//        print("earliest:- \(earliest)")
        let latest = earliest == now as Date ? from : now
//        print("latest:- \(latest)")
        let components = calendar.dateComponents([.year, .weekOfYear, .month, .day, .hour, .minute, .second],from: earliest, to: latest as Date)
//        print("components:- \(components)")
        var result = ""
        if components.year! >= 2 {
            result = "\(components.year!) years ago"
        }
        else if components.year! >= 1
        {
            if numericDates {
                result = "1 year ago"
            }
            else {
                result = "Last year"
            }
        }
        else if components.month! >= 2
        {
            result = "\(components.month!) months ago"
        }
        else if components.month! >= 1 {
            if numericDates {
                result = "1 month ago"
            }
            else {
                result = "Last month"
            }
        }
        else if components.weekOfYear! >= 2
        {
            result = "\(components.weekOfYear!) weeks ago"
        }
        else if components.weekOfYear! >= 1 {
            if numericDates {
                result = "1 week ago"
            }
            else {
                result = "Last week"
            }
        }
        else if components.day! >= 2
        {
            result = "\(components.day!) days ago"
        }
        else if components.day! >= 1 {
            if numericDates {
                result = "1 day ago"
            }
            else {
                result = "Yesterday"
            }
        }
        else if components.hour! >= 2
        {
            result = "\(components.hour!) hours ago"
        }
        else if components.hour! >= 1 {
            if numericDates {
                result = "1 hour ago"
            }
            else {
                result = "An hour ago"
            }
        }
        else if components.minute! >= 2
        {
            result = "\(components.minute!) minute ago"
        }
        else if components.minute! >= 1 {
            if numericDates {
                result = "1 minute ago"
            }
            else {
                result = "A minute ago"
            }
        }
        else if components.second! >= 2
        {
            result = "\(components.second!) second ago"
        }
        else if components.second! >= 1 {
            if numericDates {
                result = "1 second ago"
            }
            else {
                result = "Just now"
            }
        }
        else
        {
            result = "Just now"
        }
        return result
    }
}


extension String {
    
    func localized(lang:String) -> String {
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)       
        
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        
    }
    
}

extension String{
    var encodeEmoji: String? {
        let encodedStr = NSString(cString: self.cString(using: String.Encoding.nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue)
        return encodedStr as String?
    }
    
    var decodeEmoji: String {
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        if data != nil {
            let valueUniCode = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue) as String?
            if valueUniCode != nil {
                return valueUniCode!
            } else {
                return self
            }
        } else {
            return self
        }
    }
    
    
}

//extension CAGradientLayer
extension CAGradientLayer {
    
    convenience init(frame: CGRect, colors: [UIColor]) {
        self.init()
        self.frame = frame
        self.colors = []
        for color in colors {
            self.colors?.append(color.cgColor)
        }
        startPoint = CGPoint(x: 0.0, y: 0.5)
        endPoint = CGPoint(x: 0.8, y: 0.5)
    }
    
    func creatGradientImage() -> UIImage? {
        
        var image: UIImage? = nil
        UIGraphicsBeginImageContext(bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        return image
    }
    
}

//extension UINavigationBar
extension UINavigationBar {
    
    func setGradientBackground(colors: [UIColor]) {
        
        var updatedFrame = bounds
        updatedFrame.size.height += 20
        let gradientLayer = CAGradientLayer(frame: updatedFrame, colors: colors)
        
        setBackgroundImage(gradientLayer.creatGradientImage(), for: UIBarMetrics.default)
    }
}

extension UIViewController : UNUserNotificationCenterDelegate{
    //for displaying notification when app is in foreground
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //If you don't want to show notification when app is open, do something here else and make a return here.
        //Even you you don't implement this delegate method, you will not see the notification on the specified controller. So, you have to implement this delegate and make sure the below line execute. i.e. completionHandler.
        
        completionHandler([.alert, .sound, .badge])
    }
}


