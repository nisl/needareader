//
//  Extension.swift
//  quickblox_video_call
//
//  Created by ob_apple_1 on 22/12/17.
//  Copyright © 2017 optimumbrew. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}

extension Date {
    func daysBetweenDate(toDate: Date) -> Int {
        let components = Calendar.current.dateComponents([.day], from: self, to: toDate)
        return components.day ?? 0
    }
    
    var earlyDate : Date {
        return Calendar.current.date(
            byAdding: .day,
            value: -1,
            to: self)!
    }
}


