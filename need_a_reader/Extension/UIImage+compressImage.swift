//
//  UIImage+compressImage.swift
//  need_a_reader
//
//  Created by ob_apple_2 on 1/19/18.
//  Copyright © 2018 ob_apple_2. All rights reserved.
//

import Foundation
import UIKit

class imageCompression {
    
    func compressImage(image:UIImage) -> UIImage? {
        // Reducing file size to a 10th
        var actualHeight : CGFloat = image.size.height
        var actualWidth : CGFloat = image.size.width
        let maxHeight : CGFloat = 600.0
        let maxWidth : CGFloat = 800.0
        var imgRatio : CGFloat = actualWidth/actualHeight
        let maxRatio : CGFloat = maxWidth/maxHeight
        var compressionQuality : CGFloat = 0.5
        
        if (actualHeight > maxHeight || actualWidth > maxWidth){
            if(imgRatio < maxRatio){
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if(imgRatio > maxRatio){
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else{
                actualHeight = maxHeight
                actualWidth = maxWidth
//                compressionQuality = 1
            }
        } else {
            compressionQuality = 1
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        guard let img = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        
        guard let imageData = UIImageJPEGRepresentation(img, compressionQuality)else{
            return nil
        }
        UIGraphicsEndImageContext()
        return UIImage(data: imageData)
    }
}
